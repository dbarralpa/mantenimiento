package mac.sc.sis.mantenimiento.controllers;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoBean;
import mac.sc.sis.mantenimiento.models.ModAuxiliar;
import mac.sc.sis.mantenimiento.models.ModHorario;
import mac.sc.sis.mantenimiento.models.ModMantenimiento;
import mac.sc.sis.mantenimiento.models.ModMaquina;
import mac.sc.sis.mantenimiento.models.ModPrograma;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Minutes;

public class PrmApp {
    
    public static String FORMATO_DMY = "dd/MM/yyyy";
    public static String FORMATO_HMS = "HH:mm:ss";
    public static String FORMATO_HM = "HH:mm";
    public static String FORMATO_YMD = "yyyy-MM-dd";
    public static String FORMATO_YDM = "yyyy-dd-MM";
    
    public static String verOverTip(String Contenido) {
        return verOverLib("Info. Tip", Contenido, "#FFFFFF", "#000000");
    }
    
    public static String verOverLib(String Contenido) {
        return verOverLib("Mantenimiento", Contenido, "#FFFFCC", "#FF9900");
    }
    
    public static String verOverLib(String Titulo, String Contenido, String fg, String bg) {
        String tmp = "'";
        Contenido = Contenido.replace(tmp.charAt(0), '�');
        Contenido = Contenido.replace('"', '�');
        Contenido = Contenido.replaceAll("�", "<br>");
        Contenido = Contenido.replaceAll("\n", "<br>");
        Contenido = Contenido.replaceAll("\r", "");
        return "onMouseOver=" + '"' + "showOverLib('<center>" + Titulo + "</center>','" + Contenido + "', '" + fg + "', '" + bg + "');" + '"' + " OnMouseOut=" + '"' + "return nd();" + '"';
    }
    
    public static String verHtmlNull(String dato, int largo) {
        String tmp = verHtmlNull(dato);
        if (tmp.length() > largo) {
            tmp = tmp.substring(0, (largo - 3)) + "...";
        }
        return tmp;
    }
    
    public static String verHtmlNull(String dato) {
        if (dato != null) {
            dato = dato.trim();
        } else {
            dato = "";
        }
        return dato;
    }
    
    public static String getFechaActual(String formato) {
        return verFecha(new java.util.Date(), formato);
    }
    
    public static String getFechaActual() {
        return verFecha(new java.util.Date(), FORMATO_DMY);
    }
    
    public static String getHoraActual() {
        return verFecha(new java.util.Date(), FORMATO_HMS);
    }
    
    public static String verFecha(String fecha) {
        Calendar cFecha = Calendar.getInstance();
        cFecha.set(Integer.parseInt(fecha.substring(0, 4)), //a�o
                Integer.parseInt(fecha.substring(5, 7)) - 1, //mes
                Integer.parseInt(fecha.substring(8, 10)));    //dia
        
        return verFecha(cFecha.getTime(), FORMATO_DMY); //FORMATO_YDM
    }
    
    private static String verFecha(Date fecha, String formato) {
        SimpleDateFormat f = new SimpleDateFormat(formato);
        if (fecha != null) {
            return f.format(fecha);
        } else {
            return "";
        }
    }
    
    public static boolean vFormatoHora(String hora, String formato) {
        boolean valido = false;
        SimpleDateFormat f = new SimpleDateFormat(formato);
        try {
            Date d = f.parse(hora);
            valido = true;
        } catch (Exception ex) {
        }
        return valido;
    }
    
    public static String verHora(String hora, String formato) {
        SimpleDateFormat f = new SimpleDateFormat(formato);
        try {
            Date d = f.parse(hora);
            hora = f.format(d);
        } catch (Exception ex) {
        }
        return hora;
    }
    
    public static ModMantenimiento setEstadosListado(ModMantenimiento mantenimiento) {
        mantenimiento.setEDIAG(new ArrayList());
        mantenimiento.setEADQN(new ArrayList());
        mantenimiento.setESERE(new ArrayList());
        mantenimiento.setESERI(new ArrayList());
        mantenimiento.setEVISB(new ArrayList());
        mantenimiento.setENOCO(new ArrayList());
        mantenimiento.setENONE(new ArrayList());
        
        for (int i = 0; i < mantenimiento.getListadoPricipal().size(); i++) {
            Mantenimiento obj = (Mantenimiento) mantenimiento.getListadoPricipal().get(i);
            ModAuxiliar aux = new ModAuxiliar();
            if (obj.getFechaNoConformidad() != null) {
                aux.setId(i);
                mantenimiento.getENOCO().add(aux);
                obj.setEstado("NC");
            } else if (obj.getFechaVistoBueno() != null) {
                aux.setId(i);
                mantenimiento.getEVISB().add(aux);
                obj.setEstado("VB");
            } else if (obj.getFechaMantenimientoCorrectivo() != null) {
                aux.setId(i);
                mantenimiento.getESERI().add(aux);
                obj.setEstado("SI");
            } else if (obj.getFechaSevicioExterno() != null) {
                aux.setId(i);
                mantenimiento.getESERE().add(aux);
                obj.setEstado("SE");
            } else if (obj.getFechaAdquisicion() != null) {
                aux.setId(i);
                mantenimiento.getEADQN().add(aux);
                obj.setEstado("AQ");
            } else if (obj.getFechaDiagnostico() != null) {
                aux.setId(i);
                mantenimiento.getEDIAG().add(aux);
                obj.setEstado("DG");
            } else {
                aux.setId(i);
                mantenimiento.getENONE().add(aux);
                obj.setEstado("NN");
            }
            mantenimiento.getListadoPricipal().set(i, obj);
            obj = null;
            aux = null;
        }
        return mantenimiento;
    }
    
    /** METODO DE QUE RETORNA LA CANTIDAD MINUTOS DETENCION**/
    public static int mDetencion(String Fdesde, String Fhasta, ArrayList horarios) throws Exception{
        int mDetencion = 0;
        Calendar Ctmp   = Calendar.getInstance();
        Ctmp.set(Integer.parseInt(Fdesde.substring(0, 4)),
                Integer.parseInt(Fdesde.substring(5, 7)) - 1,
                Integer.parseInt(Fdesde.substring(8, 10)),
                Integer.parseInt(Fdesde.substring(11, 13)),
                Integer.parseInt(Fdesde.substring(14, 16)),
                Integer.parseInt(Fdesde.substring(17, 19)));
        
        int Xdias = PrmApp.getDiferencia(Fdesde, Fhasta) + 1;
        
        for(int i=0;i<Xdias; i++){
            for(int j=0; j<horarios.size(); j++){
                ModHorario horario = (ModHorario)horarios.get(j);
                if(horario.isEstado() && horario.getDiaHorario().equals(obtenerDiaSemana(Ctmp.get(Calendar.DAY_OF_WEEK)))){
                    if(i==0){
                        if(Xdias==1){
                            mDetencion += mDetencion(Fdesde, Fhasta);
                        }else{
                            mDetencion += mDetencionInicial(Fdesde, horario.getHoraTermino());
                        }                        
                    }else if(i==(Xdias-1)){
                        mDetencion += mDetencionFinal(horario.getHoraInicio(), Fhasta);
                    }else{
                        mDetencion += mDetencionMedio(horario.getHoraInicio(), horario.getHoraTermino());
                    }
                    break;
                }
            }
            Ctmp.add(Calendar.DATE, 1);
        }
        Ctmp = null;
        return mDetencion;
    }
    
    public static int[] calcDetHorasMin(double num){   
        int dato[] = new int[2];
        int minutos = 0;
        int horas = 0;
        if(num > 59){
            String tmp = String.valueOf((double)num / 60);
            tmp = tmp.replace(".", ",");
            horas = Integer.parseInt(tmp.split(",")[0]);    
            int aux = horas * 60;
            minutos = (int)num - aux;
        }else{        
            String vvv = String.valueOf(num).replace('.', ',');
            minutos = Integer.parseInt(vvv.split(",")[0]);
        }   
        dato[0] = horas;
        dato[1] = minutos;        
        return dato;
    }    
    
    public static int mDetencionInicial(String Fdesde, String Hhasta) throws Exception{
        String Fhasta = Fdesde.substring(0, 10) + " " + Hhasta + ":00";
        return mDetencion(Fdesde, Fhasta);
    }
    
    public static int mDetencionMedio(String Hdesde, String Hhasta) throws Exception{
        String Fdesde = "2010-01-01" + " " + Hdesde + ":00";
        String Fhasta = "2010-01-01" + " " + Hhasta + ":00";
        return mDetencion(Fdesde, Fhasta);
    }
    
    public static int mDetencionFinal(String Hdesde, String Fhasta) throws Exception{
        String Fdesde = Fhasta.substring(0, 10) + " " + Hdesde + ":00";
        return mDetencion(Fdesde, Fhasta);
    }
    
    /** METODO DE QUE RETORNA LA CANTIDAD DE MINUTOS DE DETENCION**/
    public static int mDetencion(String Fdesde, String Fhasta) throws Exception{
        int mDetencion = 0;
        DateTime start = new DateTime(Integer.parseInt(Fdesde.substring(0, 4)),
                Integer.parseInt(Fdesde.substring(5, 7)),
                Integer.parseInt(Fdesde.substring(8, 10)),
                Integer.parseInt(Fdesde.substring(11, 13)),
                Integer.parseInt(Fdesde.substring(14, 16)),
                Integer.parseInt(Fdesde.substring(17, 19)), 0);
        
        DateTime end = new DateTime(Integer.parseInt(Fhasta.substring(0, 4)),
                Integer.parseInt(Fhasta.substring(5, 7)),
                Integer.parseInt(Fhasta.substring(8, 10)),
                Integer.parseInt(Fhasta.substring(11, 13)),
                Integer.parseInt(Fhasta.substring(14, 16)),
                Integer.parseInt(Fhasta.substring(17, 19)), 0);
        
        Minutes minutos = Minutes.minutesBetween(start, end);
        mDetencion = minutos.getMinutes();
        
        return mDetencion;
    }
    
    /** METODO QUE RETORNA LA DIFERENCIA DE DIAS DE DOS FECHAS DADAS **/
    public static int getDiferencia(String Fdesde, String Fhasta) {
        DateTime start = new DateTime(Integer.parseInt(Fdesde.substring(0, 4)),
                Integer.parseInt(Fdesde.substring(5, 7)),
                Integer.parseInt(Fdesde.substring(8, 10)), 0, 0, 0,0);
        
        DateTime end = new DateTime(Integer.parseInt(Fhasta.substring(0, 4)),
                Integer.parseInt(Fhasta.substring(5, 7)),
                Integer.parseInt(Fhasta.substring(8, 10)), 0, 0, 0, 0);
        
        Days dias = Days.daysBetween(start, end);
        return dias.getDays();
    }
    
    private static String obtenerDiaSemana(int nDia) {
        String dia = "";
        switch (nDia) {
            case Calendar.SUNDAY:
                dia = "Domingo";
                break;
            case Calendar.MONDAY:
                dia = "Lunes";
                break;
            case Calendar.TUESDAY:
                dia = "Martes";
                break;
            case Calendar.WEDNESDAY:
                dia = "Miercoles";
                break;
            case Calendar.THURSDAY:
                dia = "Jueves";
                break;
            case Calendar.FRIDAY:
                dia = "Viernes";
                break;
            case Calendar.SATURDAY:
                dia = "Sabado";
                break;
            default:
                dia = "Invalido!";
        }
        return dia;
    }
    
    public static void calcularHorasMinutosDetencion(Mantenimiento detalle) throws Exception{
        int minutosDetencion = PrmApp.mDetencion(detalle.getFechaDetencionEquipo(), 
                                                 detalle.getFechaEntregaEquipo(), 
                                                 detalle.getTurnoMaq().getHorarios());                       
        int dato[] = PrmApp.calcDetHorasMin(minutosDetencion);
        detalle.setHorasDetencion(String.valueOf(dato[0]));
        detalle.setMinutosDetencion(String.valueOf(dato[1]));        
    }
//    public static double horasDeMantencion(Timestamp fechaIni, Timestamp fechaFin){
//        double result = 0f;
//        String fini = fechaIni.toString().substring(8,10);
//        String ffin = fechaFin.toString().substring(8,10);
//        double resto = 0d;
//        double resto1 = 0d;
//      try{  
//        String[] dias = {"lunes","martes","mi�rcoles","jueves","viernes","s�bado","domingo"};
//        if(Integer.parseInt(fini) == Integer.parseInt(ffin)){
//            resto = (fechaFin.getTime() - fechaIni.getTime());
//        }else{
//            int day = 0;
//            String fini1 = fechaIni.toString().substring(0,10);
//            String ffin1 = fechaFin.toString().substring(0,10);
//            resto = (java.sql.Date.valueOf(ffin1).getTime() - java.sql.Date.valueOf(fini1).getTime());
//            resto /= 86400000;
//            SimpleDateFormat forma = new SimpleDateFormat("EEEE");
//            for(int i=0;i<dias.length;i++){
//                if(dias[i].equals(forma.format(fechaIni))){
//                    day = i;
//                }
//            }
//            resto1 = (Timestamp.valueOf(fechaIni.toString().substring(0,10) + " 16:45:00").getTime() - fechaIni.getTime());
//            for(int i=0;i<resto;i++){
//                if(!dias[day].equals("domingo") && !dias[day].equals("s�bado")){
//                    resto1 += (fechaFin.getTime() - Timestamp.valueOf(fechaFin.toString().substring(0,10) + " 07:15:00").getTime());
//                }
//                if(day == 6){
//                    day = 0;
//                }else{
//                    day++;
//                }
//            }
//            result = resto1/3600000;
//        }
//      }catch(Exception e){
//       EscribeFichero.addError(e, true); 
//      } 
//        return result;
//    }
    public static double horasDeMantencion(Timestamp fechaIni, Timestamp fechaFin){
        double result = 0f;
        String fini = fechaIni.toString().substring(8,10);
        String ffin = fechaFin.toString().substring(8,10);
        double resto = 0d;
        double resto1 = 0d;
      try{  
        String[] dias = {"lunes","martes","mi�rcoles","jueves","viernes","s�bado","domingo"};
        if(Integer.parseInt(fini) == Integer.parseInt(ffin)){
            resto = (fechaFin.getTime() - fechaIni.getTime());
            result = resto/3600000;
        }else{
            int day = 0;
            String fini1 = fechaIni.toString().substring(0,10);
            String ffin1 = fechaFin.toString().substring(0,10);
            resto = (java.sql.Date.valueOf(ffin1).getTime() - java.sql.Date.valueOf(fini1).getTime());
            resto /= 86400000;
            resto -= 1;
            SimpleDateFormat forma = new SimpleDateFormat("EEEE");
            for(int i=0;i<dias.length;i++){
                if(dias[i].equals(forma.format(fechaIni))){
                    day = i;
                }
            }
            resto1 = (Timestamp.valueOf(fechaIni.toString().substring(0,10) + " 16:45:00").getTime() - fechaIni.getTime());
            for(int i=0;i<resto;i++){
                if(!dias[day].equals("domingo") && !dias[day].equals("s�bado")){
                    resto1 += 32400000;
                }
                if(day == 6){
                    day = 0;
                }else{
                    day++;
                }
            }
            resto1 += (fechaFin.getTime() - Timestamp.valueOf(fechaFin.toString().substring(0,10) + " 07:15:00").getTime());
            result = resto1/3600000;
        }
      }catch(Exception e){
       EscribeFichero.addError(e, true); 
      } 
        return result;
    }
    
    public static double forecast(int id_maquina, byte mes, ArrayList forecast){
        double valor = 0;
        for(int i=0;i<forecast.size();i++){
            ModPrograma mod = (ModPrograma)forecast.get(i);
            if(mod.getMes() <= mes && id_maquina == mod.getIdMaquina()){
                valor = mod.getTiempo();
            }
        }
        return valor;
    }
     public static String devolverObservacion(String observacion){
     String devolver = "";   
        if(observacion == null || observacion.trim().length() == 0){
            observacion = "Sin observaci�n";
        }
     devolver = observacion.replace("\n"," ");
     devolver = devolver.replace("\r"," ");
     devolver = devolver.replace("\t"," ");
     devolver = devolver.replace("/"," ");
     devolver = devolver.replace('"',' ');
     devolver = devolver.replace('�',' ');
     devolver = devolver.replace("'"," ");
     devolver = devolver.replace("#"," ");
     //devolver = devolver.replace("$"," ");
     return devolver;
    }
     public static double horasTotalesCorrectivas(int id_maquina, ArrayList maquinas){
         double val = 0d;
         for(int i=0;i<maquinas.size();i++){
             ModMaquina maquina = (ModMaquina)maquinas.get(i);
             if(maquina.getIdMaquina() == id_maquina){
                 val = maquina.getHorasReales();
             }
         }
         return val;         
     }
}
