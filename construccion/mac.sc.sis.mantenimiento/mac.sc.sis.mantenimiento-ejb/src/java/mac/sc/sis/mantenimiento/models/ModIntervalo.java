/*
 * ModIntervalo.java
 *
 * Created on 20 de febrero de 2012, 14:37
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.sc.sis.mantenimiento.models;

/**
 *
 * @author dbarra
 */
public class ModIntervalo {
    private int id;
    private String descripcion;
    /** Creates a new instance of ModIntervalo */
    public ModIntervalo() {
        setId(0);
        setDescripcion("");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
