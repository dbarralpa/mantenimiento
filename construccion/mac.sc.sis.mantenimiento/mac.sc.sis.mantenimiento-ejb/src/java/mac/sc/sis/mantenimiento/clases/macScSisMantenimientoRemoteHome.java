
package mac.sc.sis.mantenimiento.clases;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for macScSisMantenimiento enterprise bean.
 */
public interface macScSisMantenimientoRemoteHome extends EJBHome {
    
    macScSisMantenimientoRemote create()  throws CreateException, RemoteException;
    
    
}
