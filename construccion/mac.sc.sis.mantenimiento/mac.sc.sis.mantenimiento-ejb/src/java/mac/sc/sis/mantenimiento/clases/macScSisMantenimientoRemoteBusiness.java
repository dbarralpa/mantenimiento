package mac.sc.sis.mantenimiento.clases;

import java.rmi.RemoteException;
import java.util.ArrayList;
import mac.sc.sis.mantenimiento.models.ModEmpExt;
import mac.sc.sis.mantenimiento.models.ModEmplExt;
import mac.sc.sis.mantenimiento.models.ModFalla;
import mac.sc.sis.mantenimiento.models.ModPerMant;
import mac.sc.sis.mantenimiento.models.ModTipoFalla;
import mac.sc.sis.mantenimiento.models.ModTurnoMaq;

/**
 * This is the business interface for macScSisMantenimiento enterprise bean.
 */
public interface macScSisMantenimientoRemoteBusiness {

    ArrayList listarFallas(int maquina) throws RemoteException;
    int obtenerIdUsuario(String nombreUsuario) throws RemoteException;
    String obtenernombreUsuario(int idUsuario) throws RemoteException;
    ArrayList listarPedidosMantenimiento(int idUsuario) throws RemoteException;
    ArrayList listarTodosLosPedidosDeMantenimiento() throws RemoteException;
    ArrayList listarMaquinas(int CodigoTipoMantenimiento) throws RemoteException;
    ArrayList listarTodasLasAreas() throws RemoteException;
    ArrayList solicitudesPorArea(int codigoArea) throws RemoteException;
    ArrayList listarTodosLosUsuarios() throws RemoteException;
    ArrayList listarTodosLosEstados() throws RemoteException;
    boolean insertarNuevoEstado(int idOrden, int idEstado, String fecha, String observacion) throws RemoteException;
    boolean insertarNuevoFallo(ModFalla falla) throws RemoteException;
    boolean insertarMantenimiento(int especialidad, int idUsuario1, String maquina, int falla, String mensaje, String fechaDetencionEquipo, String fechaHoy, String tipoFalla) throws RemoteException;
    boolean actualizarFalla(ModFalla falla) throws RemoteException;
    boolean eliminarFalla(int idFalla) throws RemoteException;
    boolean eliminarMaquina(int idArea, int idMaquina) throws RemoteException;
    boolean insertarNuevoTipoFallo(ModTipoFalla TipoFalla) throws RemoteException;
    ArrayList listarTipoFallas() throws RemoteException;
    boolean actualizarTipoFalla(ModTipoFalla tipoFalla) throws RemoteException;
    boolean tipoDeFallaAsociado(int idTipoFalla) throws RemoteException;
    boolean eliminarTipoFalla(int idTipoFalla) throws RemoteException;
    ArrayList solicitudesPorUsuario(int idUsuario) throws RemoteException;
    ArrayList listarEmpExt() throws RemoteException;
    boolean insertarNuevoEmpExt(ModEmpExt empExt) throws RemoteException;
    boolean actualizarEmpExt(ModEmpExt empExt) throws RemoteException;
    boolean eliminarEmpExt(String vc_rut) throws RemoteException;
    boolean insertarNuevoEmplExt(ModEmplExt empleado) throws RemoteException;
    boolean actualizarEmplExt(ModEmplExt empleado) throws RemoteException;
    boolean eliminarEmplExt(String vc_rut_empl, String vc_rut_empr) throws RemoteException;
    ArrayList listarPerMant() throws RemoteException;
    boolean insertarNuevoPerMant(ModPerMant personal) throws RemoteException;
    boolean actualizarPerMant(ModPerMant personal) throws RemoteException;
    boolean eliminarPerMant(String vc_rut) throws RemoteException;
    boolean updateSolicitud(int CodigoMantenimiento, String nSap, String rut_encargado, String fechaEntrega) throws RemoteException;
    boolean updateSolicitud(int CodigoMantenimiento, String nSap, String rut_encargado) throws RemoteException;
    ArrayList listarEmplExt(String vc_rut_emp) throws RemoteException;
    boolean updateSolicitud(int CodigoMantenimiento, String nSap, String rut_encargado, String fechaEntrega, String rutEmpr, String rutEmpl) throws RemoteException;
    ArrayList listarTurnosMaquinas() throws RemoteException;
    boolean insertarNuevoTurnoMaquina(ModTurnoMaq turnoMaq) throws RemoteException;
    boolean actualizarTurnoMaquina(ModTurnoMaq turnoMaq) throws RemoteException;
    ArrayList listarHorariosXTurno(int idTurnoMaq) throws RemoteException;
    boolean eliminarTurnoMaq(int idTurnoMaq) throws RemoteException;
    boolean insertarNuevaMaquina(int idArea, String Uso, String nomMaquina, int idTurnoMaq) throws RemoteException;
    boolean actualizarMaquina(int idArea, int idMaquina, String usoMaquina, String nombreMaquina, int idTurnoMaq) throws RemoteException;
   // java.util.ArrayList solicitudesPreventivas() throws RemoteException;
    ArrayList listarPedidosMantePreventivos(int idUsuario, boolean estado) throws RemoteException;
    ArrayList listarTareasNoHechas(int id) throws RemoteException;
    boolean insertarTareaHecha(int id_man_preventiva, int id_tarea) throws RemoteException;
    boolean actualizarMantencionPreventiva(int id, String fechaIni, String fechaFin, String ejecutor, String observacion, boolean est) throws RemoteException;
    boolean actualizarPrograma(int id) throws RemoteException;
    ArrayList programaPreventivoHoras() throws RemoteException;
    ArrayList listarMaquinas() throws RemoteException;
    ArrayList listarIntervalos(int id) throws RemoteException;
    ArrayList listareasIntervalos(int id_intervalo, int id_maquina) throws RemoteException;
    int maxTareas() throws RemoteException;
    int idMquinaTarea(int idMaquina, int intervalo) throws RemoteException;
    boolean insertarTarea(int idTarea, String tarea, int tipoTarea, int idMaquinaTarea, int idMaquina) throws RemoteException;
    boolean borrarTarea(int idMaquinaTarea, int idMaquina, int idTarea) throws RemoteException;
    boolean insertarForecast(int idMaquina, double tiempo, double presupuesto, int ano, int mes, boolean est) throws RemoteException;
    boolean countForecast(int idMaquina, int ano, int mes) throws RemoteException;
    ArrayList cartaGantt(int ano, String tipo) throws RemoteException;
    ArrayList listarMaquinasConPrograma(int ano, String tipo) throws RemoteException;
    ArrayList forecast_tiempo(int ano, String tipo) throws RemoteException;
    ArrayList horasDetenidasCorrectivas(int ano, int mes) throws RemoteException;
    ArrayList listarPedidosMantePreventivos(int idUsuario, int valor) throws RemoteException;
    ArrayList listarMaquinasPreventivas() throws RemoteException;
    ArrayList listarPedidosMantePreventivos(int idUsuario, String fecha) throws RemoteException;
}
