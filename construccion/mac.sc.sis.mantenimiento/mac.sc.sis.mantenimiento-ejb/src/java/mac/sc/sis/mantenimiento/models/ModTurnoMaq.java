package mac.sc.sis.mantenimiento.models;

import java.util.ArrayList;

public class ModTurnoMaq implements Cloneable {

    private int id;
    private int idTurnoMaq;
    private String nombreTurnoMaq;
    private String descTurnoMaq;
    private ArrayList horarios;

    public ModTurnoMaq() {
        id = 0;
        idTurnoMaq = 0;
        nombreTurnoMaq = "";
        horarios = new ArrayList();
        descTurnoMaq = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTurnoMaq() {
        return idTurnoMaq;
    }

    public void setIdTurnoMaq(int idTurnoMaq) {
        this.idTurnoMaq = idTurnoMaq;
    }

    public String getNombreTurnoMaq() {
        return nombreTurnoMaq;
    }

    public void setNombreTurnoMaq(String nombreTurnoMaq) {
        this.nombreTurnoMaq = nombreTurnoMaq;
    }

    public ArrayList getHorarios() {
        return horarios;
    }

    public void setHorarios(ArrayList horarios) {
        this.horarios = horarios;
    }

    public String getDescTurnoMaq() {
        return descTurnoMaq;
    }

    public void setDescTurnoMaq(String descTurnoMaq) {
        this.descTurnoMaq = descTurnoMaq;
    }

    public ModTurnoMaq copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
            //
        }
        return (ModTurnoMaq) obj;
    }
}
