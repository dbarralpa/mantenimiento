/*
 * ModPrograma.java
 *
 * Created on 22 de febrero de 2012, 16:31
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.sc.sis.mantenimiento.models;

import java.util.ArrayList;

/**
 *
 * @author dbarra
 */
public class ModPrograma {
    
    private String nombre;
    private double tiempo;
    private byte mes;
    private String fecha;
    private int idMaquina;
    private ArrayList programas;
    public ModPrograma() {
        nombre = "";
        tiempo = 0d;
        mes = 0;
        fecha = "";
        idMaquina = 0;
        programas = new ArrayList();
    }
    
    public byte getMes() {
        return mes;
    }
    
    public void setMes(byte mes) {
        this.mes = mes;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public double getTiempo() {
        return tiempo;
    }
    
    public void setTiempo(double tiempo) {
        this.tiempo = tiempo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdMaquina() {
        return idMaquina;
    }

    public void setIdMaquina(int idMaquina) {
        this.idMaquina = idMaquina;
    }

    public ArrayList getProgramas() {
        return programas;
    }

    public void setProgramas(ArrayList programas) {
        this.programas = programas;
    }
    
}
