package mac.sc.sis.mantenimiento.models;

import java.util.ArrayList;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;

/**
 *
 * @author ngonzalez
 */
public class ModMantenimiento {

    private int etapa;
    private int etapaFalla;
    private int etapaMaquina;
    private int etapaTipoFalla;
    private int etapaEmpExt;
    private int etapaEmplExt;
    private int etapaPerMant;
    private int etapaTMaq;
    private int etapaHorario;
    private int codigoEspecialidad;
    private int codigoFalla;
    private String nombreFalla;
    private String descFalla;
    private String fechaDetencionEquipo;
    private String diaDetencionEquipo;
    private String horaDetencionEquipo;
    private String mensaje;
    private int idUsuarioArea;
    private String nombreUsuario;
    private String tipo;
    private String tipoAnterior;
    private boolean ordenAsc;
    private boolean ordenAscNew;
    private boolean ordenAscPreventivo;
    private String tituloBitacora;
    private String nombreEstado;
    private int idDetalle;
    private int idDetalle1;
    private ArrayList listadoPricipal;
    private ArrayList listadoTMP;
    private ArrayList listadoPedidosMantenimientos;
    private ArrayList listadoPedidosMantenPreventivos;
    private ArrayList listaEstados;
    private ArrayList listadoFallas;
    private ArrayList listadoTipoFallas;
    private ArrayList listadoEmpExt;
    private ArrayList listadoPerMant;
    private ArrayList listadoMaquinas;
    private ArrayList listadoTareas;
    private ArrayList listadoTurnosMaquina;
    private ArrayList eDIAG;
    private ArrayList eADQN;
    private ArrayList eSERE;
    private ArrayList eSERI;
    private ArrayList eVISB;
    private ArrayList eNOCO;
    private ArrayList eNONE;
    private ArrayList intervalos;
    private Mantenimiento detalle;
    private ModTipoFalla tipoFalla;
    private ModFalla falla;
    private ModEmpExt empExt;
    private ModPerMant perMant;
    private ModTurnoMaq turnoMaquina;
    private ModMaquina maquina;

    public ModMantenimiento() {
        etapa = 0;
        etapaFalla = 0;
        etapaMaquina = 0;
        etapaTipoFalla = 0;
        etapaEmpExt = 0;
        etapaEmplExt = 0;
        etapaPerMant = 0;
        etapaTMaq = 0;
        codigoEspecialidad = -1;
        codigoFalla = -1;
        nombreFalla = "";
        descFalla = "";
        mensaje = "";
        idUsuarioArea = -1;
        intervalos = new ArrayList();
        listadoPricipal = new ArrayList();
        listadoTMP = new ArrayList();
        tipo = "";
        tipoAnterior = "";
        detalle = new Mantenimiento();
        tipoFalla = new ModTipoFalla();
        falla = new ModFalla();
        empExt = new ModEmpExt();
        perMant = new ModPerMant();
        turnoMaquina = new ModTurnoMaq();
        maquina = new ModMaquina();
        listadoPedidosMantenimientos = new ArrayList();
        listadoPedidosMantenPreventivos = new ArrayList();
        nombreUsuario = "";
        listaEstados = new ArrayList();
        ordenAsc = false;
        ordenAscPreventivo = false;
        tituloBitacora = "";
        nombreEstado = "";
        idDetalle = 0;
        idDetalle1 = 0;
        ordenAscNew = false;
        listadoFallas = new ArrayList();
        listadoTipoFallas = new ArrayList();
        listadoEmpExt = new ArrayList();
        listadoPerMant = new ArrayList();
        listadoMaquinas = new ArrayList();
        listadoTurnosMaquina = new ArrayList();
        listadoTareas = new ArrayList();
        eDIAG = null;
        eADQN = null;
        eSERE = null;
        eSERI = null;
        eVISB = null;
        eNOCO = null;
        eNONE = null;
    }

    public int getCodigoEspecialidad() {
        return codigoEspecialidad;
    }

    public void setCodigoEspecialidad(int codigoEspecialidad) {
        this.codigoEspecialidad = codigoEspecialidad;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public int getEtapa() {
        return etapa;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    public int getIdUsuarioArea() {
        return idUsuarioArea;
    }

    public void setIdUsuarioArea(int idUsuarioArea) {
        this.idUsuarioArea = idUsuarioArea;
    }

    public ArrayList getListadoPricipal() {
        return listadoPricipal;
    }

    public void setListadoPricipal(ArrayList listadoPricipal) {
        this.listadoPricipal = listadoPricipal;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Mantenimiento getDetalle() {
        return detalle;
    }

    public void setDetalle(Mantenimiento detalle) {
        this.detalle = detalle;
    }

    public ArrayList getListadoPedidosMantenimientos() {
        return listadoPedidosMantenimientos;
    }

    public void setListadoPedidosMantenimientos(ArrayList listadoPedidosMantenimientos) {
        this.listadoPedidosMantenimientos = listadoPedidosMantenimientos;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public ArrayList getListaEstados() {
        return listaEstados;
    }

    public void setListaEstados(ArrayList listaEstados) {
        this.listaEstados = listaEstados;
    }

    public boolean isOrdenAsc() {
        return ordenAsc;
    }

    public void setOrdenAsc(boolean ordenAsc) {
        this.ordenAsc = ordenAsc;
    }

    public String getTituloBitacora() {
        return tituloBitacora;
    }

    public void setTituloBitacora(String tituloBitacora) {
        this.tituloBitacora = tituloBitacora;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public int getIdDetalle() {
        return idDetalle;
    }

    public void setIdDetalle(int idDetalle) {
        this.idDetalle = idDetalle;
    }
    
    public int getIdDetalle1() {
        return idDetalle1;
    }

    public void setIdDetalle1(int idDetalle1) {
        this.idDetalle1 = idDetalle1;
    }

    public boolean isOrdenAscNew() {
        return ordenAscNew;
    }

    public void setOrdenAscNew(boolean ordenAscNew) {
        this.ordenAscNew = ordenAscNew;
    }

    public ArrayList getListadoFallas() {
        return listadoFallas;
    }

    public void setListadoFallas(ArrayList listadoFallas) {
        this.listadoFallas = listadoFallas;
    }

    public int getEtapaFalla() {
        return etapaFalla;
    }

    public void setEtapaFalla(int etapaFalla) {
        this.etapaFalla = etapaFalla;
    }

    public int getEtapaMaquina() {
        return etapaMaquina;
    }

    public void setEtapaMaquina(int etapaMaquina) {
        this.etapaMaquina = etapaMaquina;
    }

    public ArrayList getListadoMaquinas() {
        return listadoMaquinas;
    }

    public void setListadoMaquinas(ArrayList listadoMaquinas) {
        this.listadoMaquinas = listadoMaquinas;
    }

    public ArrayList getEDIAG() {
        return eDIAG;
    }

    public void setEDIAG(ArrayList eDIAG) {
        this.eDIAG = eDIAG;
    }

    public ArrayList getEADQN() {
        return eADQN;
    }

    public void setEADQN(ArrayList eADQN) {
        this.eADQN = eADQN;
    }

    public ArrayList getESERE() {
        return eSERE;
    }

    public void setESERE(ArrayList eSERE) {
        this.eSERE = eSERE;
    }

    public ArrayList getESERI() {
        return eSERI;
    }

    public void setESERI(ArrayList eSERI) {
        this.eSERI = eSERI;
    }

    public ArrayList getEVISB() {
        return eVISB;
    }

    public void setEVISB(ArrayList eVISB) {
        this.eVISB = eVISB;
    }

    public ArrayList getENONE() {
        return eNONE;
    }

    public void setENONE(ArrayList eNONE) {
        this.eNONE = eNONE;
    }

    public ArrayList getListadoTMP() {
        return listadoTMP;
    }

    public void setListadoTMP(ArrayList listadoTMP) {
        this.listadoTMP = listadoTMP;
    }

    public String getTipoAnterior() {
        return tipoAnterior;
    }

    public void setTipoAnterior(String tipoAnterior) {
        this.tipoAnterior = tipoAnterior;
    }

    public ArrayList getENOCO() {
        return eNOCO;
    }

    public void setENOCO(ArrayList eNOCO) {
        this.eNOCO = eNOCO;
    }

    public ArrayList getListadoTipoFallas() {
        return listadoTipoFallas;
    }

    public void setListadoTipoFallas(ArrayList listadoTipoFallas) {
        this.listadoTipoFallas = listadoTipoFallas;
    }

    public ModTipoFalla getTipoFalla() {
        return tipoFalla;
    }

    public void setTipoFalla(ModTipoFalla tipoFalla) {
        this.tipoFalla = tipoFalla;
    }

    public int getEtapaTipoFalla() {
        return etapaTipoFalla;
    }

    public void setEtapaTipoFalla(int etapaTipoFalla) {
        this.etapaTipoFalla = etapaTipoFalla;
    }

    public ModFalla getFalla() {
        return falla;
    }

    public void setFalla(ModFalla falla) {
        this.falla = falla;
    }

    public int getCodigoFalla() {
        return codigoFalla;
    }

    public void setCodigoFalla(int codigoFalla) {
        this.codigoFalla = codigoFalla;
    }

    public String getNombreFalla() {
        return nombreFalla;
    }

    public void setNombreFalla(String nombreFalla) {
        this.nombreFalla = nombreFalla;
    }

    public String getDescFalla() {
        return descFalla;
    }

    public void setDescFalla(String descFalla) {
        this.descFalla = descFalla;
    }

    public String getFechaDetencionEquipo() {
        return fechaDetencionEquipo;
    }

    public void setFechaDetencionEquipo(String fechaDetencionEquipo) {
        this.fechaDetencionEquipo = fechaDetencionEquipo;
    }

    public String getHoraDetencionEquipo() {
        return horaDetencionEquipo;
    }

    public void setHoraDetencionEquipo(String horaDetencionEquipo) {
        this.horaDetencionEquipo = horaDetencionEquipo;
    }

    public String getDiaDetencionEquipo() {
        return diaDetencionEquipo;
    }

    public void setDiaDetencionEquipo(String diaDetencionEquipo) {
        this.diaDetencionEquipo = diaDetencionEquipo;
    }

    public int getEtapaEmpExt() {
        return etapaEmpExt;
    }

    public void setEtapaEmpExt(int etapaEmpExt) {
        this.etapaEmpExt = etapaEmpExt;
    }

    public ArrayList getListadoEmpExt() {
        return listadoEmpExt;
    }

    public void setListadoEmpExt(ArrayList listadoEmpExt) {
        this.listadoEmpExt = listadoEmpExt;
    }

    public ModEmpExt getEmpExt() {
        return empExt;
    }

    public void setEmpExt(ModEmpExt empExt) {
        this.empExt = empExt;
    }

    public int getEtapaEmplExt() {
        return etapaEmplExt;
    }

    public void setEtapaEmplExt(int etapaEmplExt) {
        this.etapaEmplExt = etapaEmplExt;
    }

    public int getEtapaPerMant() {
        return etapaPerMant;
    }

    public void setEtapaPerMant(int etapaPerMant) {
        this.etapaPerMant = etapaPerMant;
    }

    public ArrayList getListadoPerMant() {
        return listadoPerMant;
    }

    public void setListadoPerMant(ArrayList listadoPerMant) {
        this.listadoPerMant = listadoPerMant;
    }

    public ModPerMant getPerMant() {
        return perMant;
    }

    public void setPerMant(ModPerMant perMant) {
        this.perMant = perMant;
    }

    public int getEtapaTMaq() {
        return etapaTMaq;
    }

    public void setEtapaTMaq(int etapaTMaq) {
        this.etapaTMaq = etapaTMaq;
    }

    public int getEtapaHorario() {
        return etapaHorario;
    }

    public void setEtapaHorario(int etapaHorario) {
        this.etapaHorario = etapaHorario;
    }

    public ArrayList getListadoTurnosMaquina() {
        return listadoTurnosMaquina;
    }

    public void setListadoTurnosMaquina(ArrayList listadoTurnosMaquina) {
        this.listadoTurnosMaquina = listadoTurnosMaquina;
    }

    public ModTurnoMaq getTurnoMaquina() {
        return turnoMaquina;
    }

    public void setTurnoMaquina(ModTurnoMaq turnoMaquina) {
        this.turnoMaquina = turnoMaquina;
    }

    public ModMaquina getMaquina() {
        return maquina;
    }

    public void setMaquina(ModMaquina maquina) {
        this.maquina = maquina;
    }

    public ArrayList getListadoPedidosMantenPreventivos() {
        return listadoPedidosMantenPreventivos;
    }

    public void setListadoPedidosMantenPreventivos(ArrayList listadoPedidosMantenPreventivos) {
        this.listadoPedidosMantenPreventivos = listadoPedidosMantenPreventivos;
    }

    public ArrayList getListadoTareas() {
        return listadoTareas;
    }

    public void setListadoTareas(ArrayList listadoTareas) {
        this.listadoTareas = listadoTareas;
    }

    public ArrayList getIntervalos() {
        return intervalos;
    }

    public void setIntervalos(ArrayList intervalos) {
        this.intervalos = intervalos;
    }

    public boolean isOrdenAscPreventivo() {
        return ordenAscPreventivo;
    }

    public void setOrdenAscPreventivo(boolean ordenAscPreventivo) {
        this.ordenAscPreventivo = ordenAscPreventivo;
    }
}
