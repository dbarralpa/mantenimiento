package mac.sc.sis.mantenimiento.models;

import java.util.ArrayList;

/**
 *
 * @author ngonzalez
 */
public class ModEmpExt implements Cloneable {

    private int id;
    private String vc_rut;
    private String vc_nombre;
    private String vc_direccion;
    private ArrayList empleados;
    private ModEmplExt empleado;

    public ModEmpExt() {
        id = 0;
        vc_rut = "";
        vc_nombre = "";
        vc_direccion = "";
        empleados = new ArrayList();
        empleado = new ModEmplExt();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVc_rut() {
        return vc_rut;
    }

    public void setVc_rut(String vc_rut) {
        this.vc_rut = vc_rut;
    }

    public String getVc_nombre() {
        return vc_nombre;
    }

    public void setVc_nombre(String vc_nombre) {
        this.vc_nombre = vc_nombre;
    }

    public String getVc_direccion() {
        return vc_direccion;
    }

    public void setVc_direccion(String vc_direccion) {
        this.vc_direccion = vc_direccion;
    }

    public ArrayList getEmpleados() {
        return empleados;
    }

    public void setEmpleados(ArrayList empleados) {
        this.empleados = empleados;
    }

    public ModEmplExt getEmpleado() {
        return empleado;
    }

    public void setEmpleado(ModEmplExt empleado) {
        this.empleado = empleado;
    }

    public ModEmpExt copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
            //
        }
        return (ModEmpExt) obj;
    }
}
