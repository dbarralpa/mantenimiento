package mac.sc.sis.mantenimiento.models;

public class ModFalla implements Cloneable {

    private int id;
    private int idMaquina;
    private int idFalla;
    private String nombre;
    private String descripcion;
    private ModTipoFalla tipoFalla;

    public ModFalla() {
        id = 0;
        idMaquina = -1;
        idFalla = -1;
        nombre = "";
        descripcion = "";
        tipoFalla = new ModTipoFalla();
        tipoFalla.setIdTipoFalla(-1);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdFalla() {
        return idFalla;
    }

    public void setIdFalla(int idFalla) {
        this.idFalla = idFalla;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdMaquina() {
        return idMaquina;
    }

    public void setIdMaquina(int idMaquina) {
        this.idMaquina = idMaquina;
    }

    public ModTipoFalla getTipoFalla() {
        return tipoFalla;
    }

    public void setTipoFalla(ModTipoFalla tipoFalla) {
        this.tipoFalla = tipoFalla;
    }

    public ModFalla copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
            //
        }
        return (ModFalla) obj;
    }
}
