package mac.sc.sis.mantenimiento.beans;

public class Trabajo {

    /** Creates a new instance of Falla */
    public Trabajo() {
    }
    private long CodigoMantenimiento;
    private long CodigoTrabajo;
    private String FechaTrabajo;
    private int idUsuario;
    private String nombreUsuario;
    private String apellidoUsuario;
    private int idEstado;
    private String nombreEstado;
    private int ordenMantenimiento;
    private String diagnostico;
    private String fechaDiagnostico;
    private String observacionDiagnostico;
    private String adquisicion;
    private String fechaAdquisicion;
    private String observacionAdquisicion;
    private String servicioExterno;
    private String fechaServicioExterno;
    private String observacionServicioExterno;
    private String mantenimientoCorrectivo;
    private String fechaMantenimientoCorrectivo;
    private String observacionMantenimientoCorrectivo;
    private String vistoBueno;
    private String fechaVistoBueno;
    private String observacionVistoBueno;
    private String cerrar;
    private String fechaCerrar;
    private String observacionCerrar;
    private int horasEstimadas;
    private int costo;

    public long getCodigoMantenimiento() {
        return CodigoMantenimiento;
    }

    public void setCodigoMantenimiento(long CodigoMantenimiento) {
        this.CodigoMantenimiento = CodigoMantenimiento;
    }

    public long getCodigoTrabajo() {
        return CodigoTrabajo;
    }

    public void setCodigoTrabajo(long CodigoTrabajo) {
        this.CodigoTrabajo = CodigoTrabajo;
    }

    public String getFechaTrabajo() {
        return FechaTrabajo;
    }

    public void setFechaTrabajo(String FechaTrabajo) {
        this.FechaTrabajo = FechaTrabajo;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getApellidoUsuario() {
        return apellidoUsuario;
    }

    public void setApellidoUsuario(String apellidoUsuario) {
        this.apellidoUsuario = apellidoUsuario;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public int getOrdenMantenimiento() {
        return ordenMantenimiento;
    }

    public void setOrdenMantenimiento(int ordenMantenimiento) {
        this.ordenMantenimiento = ordenMantenimiento;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getFechaDiagnostico() {
        return fechaDiagnostico;
    }

    public void setFechaDiagnostico(String fechaDiagnostico) {
        this.fechaDiagnostico = fechaDiagnostico;
    }

    public String getObservacionDiagnostico() {
        return observacionDiagnostico;
    }

    public void setObservacionDiagnostico(String observacionDiagnostico) {
        this.observacionDiagnostico = observacionDiagnostico;
    }

    public String getAdquisicion() {
        return adquisicion;
    }

    public void setAdquisicion(String adquisicion) {
        this.adquisicion = adquisicion;
    }

    public String getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public String getObservacionAdquisicion() {
        return observacionAdquisicion;
    }

    public void setObservacionAdquisicion(String observacionAdquisicion) {
        this.observacionAdquisicion = observacionAdquisicion;
    }

    public String getServicioExterno() {
        return servicioExterno;
    }

    public void setServicioExterno(String servicioExterno) {
        this.servicioExterno = servicioExterno;
    }

    public String getFechaServicioExterno() {
        return fechaServicioExterno;
    }

    public void setFechaServicioExterno(String fechaServicioExterno) {
        this.fechaServicioExterno = fechaServicioExterno;
    }

    public String getObservacionServicioExterno() {
        return observacionServicioExterno;
    }

    public void setObservacionServicioExterno(String observacionServicioExterno) {
        this.observacionServicioExterno = observacionServicioExterno;
    }

    public String getMantenimientoCorrectivo() {
        return mantenimientoCorrectivo;
    }

    public void setMantenimientoCorrectivo(String mantenimientoCorrectivo) {
        this.mantenimientoCorrectivo = mantenimientoCorrectivo;
    }

    public String getFechaMantenimientoCorrectivo() {
        return fechaMantenimientoCorrectivo;
    }

    public void setFechaMantenimientoCorrectivo(String fechaMantenimientoCorrectivo) {
        this.fechaMantenimientoCorrectivo = fechaMantenimientoCorrectivo;
    }

    public String getObservacionMantenimientoCorrectivo() {
        return observacionMantenimientoCorrectivo;
    }

    public void setObservacionMantenimientoCorrectivo(String observacionMantenimientoCorrectivo) {
        this.observacionMantenimientoCorrectivo = observacionMantenimientoCorrectivo;
    }

    public String getVistoBueno() {
        return vistoBueno;
    }

    public void setVistoBueno(String vistoBueno) {
        this.vistoBueno = vistoBueno;
    }

    public String getFechaVistoBueno() {
        return fechaVistoBueno;
    }

    public void setFechaVistoBueno(String fechaVistoBueno) {
        this.fechaVistoBueno = fechaVistoBueno;
    }

    public String getObservacionVistoBueno() {
        return observacionVistoBueno;
    }

    public void setObservacionVistoBueno(String observacionVistoBueno) {
        this.observacionVistoBueno = observacionVistoBueno;
    }

    public String getCerrar() {
        return cerrar;
    }

    public void setCerrar(String cerrar) {
        this.cerrar = cerrar;
    }

    public String getFechaCerrar() {
        return fechaCerrar;
    }

    public void setFechaCerrar(String fechaCerrar) {
        this.fechaCerrar = fechaCerrar;
    }

    public String getObservacionCerrar() {
        return observacionCerrar;
    }

    public void setObservacionCerrar(String observacionCerrar) {
        this.observacionCerrar = observacionCerrar;
    }

    public int getHorasEstimadas() {
        return horasEstimadas;
    }

    public void setHorasEstimadas(int horasEstimadas) {
        this.horasEstimadas = horasEstimadas;
    }

    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    /*public String getEstado() {
    return estado;
    }

    public void setEstado(String estado) {
    this.estado = estado;
    }*/
}
