package mac.sc.sis.mantenimiento.models;

import mac.sc.sis.mantenimiento.beans.Mantenimiento;

public class ModMaquina implements Cloneable{
    
    /** Creates a new instance of Maquina */
    public ModMaquina() {}
    
    private int area;
    private int idMaquina;
    private String Uso;
    private String NombreCorto;
    private String nombre;
    private String tipo;
    private double horasTrabajo;
    private double horasReales;
    private double presupuesto;
    private ModTurnoMaq turnoMaq;
    private Mantenimiento modMante;
    private ModIntervalo modIntervalo;
    
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public int getArea() {
        return area;
    }
    
    public void setArea(int area) {
        this.area = area;
    }
    
    public int getIdMaquina() {
        return idMaquina;
    }
    
    public void setIdMaquina(int idMaquina) {
        this.idMaquina = idMaquina;
    }
    
    public String getUso() {
        return Uso;
    }
    
    public void setUso(String Uso) {
        this.Uso = Uso;
    }
    
    public String getNombreCorto() {
        return NombreCorto;
    }
    
    public void setNombreCorto(String NombreCorto) {
        this.NombreCorto = NombreCorto;
    }
    
    public ModTurnoMaq getTurnoMaq() {
        return turnoMaq;
    }
    
    public void setTurnoMaq(ModTurnoMaq turnoMaq) {
        this.turnoMaq = turnoMaq;
    }
    
    public ModMaquina copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch(CloneNotSupportedException ex) { }
        return (ModMaquina)obj;
    }

    public double getHorasTrabajo() {
        return horasTrabajo;
    }

    public void setHorasTrabajo(double horasTrabajo) {
        this.horasTrabajo = horasTrabajo;
    }

    public double getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }

    public Mantenimiento getModMante() {
        return modMante;
    }

    public void setModMante(Mantenimiento modMante) {
        this.modMante = modMante;
    }

    public double getHorasReales() {
        return horasReales;
    }

    public void setHorasReales(double horasReales) {
        this.horasReales = horasReales;
    }

    public ModIntervalo getModIntervalo() {
        return modIntervalo;
    }

    public void setModIntervalo(ModIntervalo modIntervalo) {
        this.modIntervalo = modIntervalo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
