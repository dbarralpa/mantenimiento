package mac.sc.sis.mantenimiento.beans;

import java.util.Date;
import mac.sc.sis.mantenimiento.models.ModEmpExt;
import mac.sc.sis.mantenimiento.models.ModEmplExt;
import mac.sc.sis.mantenimiento.models.ModPerMant;
import mac.sc.sis.mantenimiento.models.ModTurnoMaq;

/**
 *
 * @author pgonzalezc
 */
public class Mantenimiento {

    private int CodigoMantenimiento;
    private int CodigoEspecialidad;
    private int CodigoSolicitante;
    private int CodigoMaquina;
    private int CodigoFalla;
    private int ano;
    private int mes;
    private String ObservacionFalla;
    private String FechaDetencionEquipo;
    private String FechaEntregaEquipo;
    private String horasDetencion;
    private double horasDetenidas;
    private String minutosDetencion;
    private String FechaHoy;
    private String Comentario;
    private String Activa;
    private String Recepcionar;
    private String fechaEstado;
    private String fechaMantePreventivo;
    private int idEstado;
    private String nombreEstado;
    private String fechaDiagnostico;
    private String fechaAdquisicion;
    private String fechaSevicioExterno;
    private String fechaMantenimientoCorrectivo;
    private String fechaVistoBueno;
    private String fechaCerrar;
    private String observacionDiagnostico;
    private String observacionAdquisicion;
    private String observacionSevicioExterno;
    private String observacionMantenimientoCorrectivo;
    private String observacionVistoBueno;
    private String observacionCerrar;
    //agregado ultima version/////
    private String nombreUsuarioMantenimiento;
    private String nombreMaquina;
    private String nombreFalla;
    private String nombreTipoFalla;
    private String descFalla;
    private String descTipoFalla;
    private String numeroSap;
    private String estado;
    private String noConformidad;
    private String fechaNoConformidad;
    private String observacionNoConformidad;
    private String nombreArea;
    private ModPerMant personalMant;
    private ModEmpExt empExt;
    private ModEmplExt emplEmt;
    private ModTurnoMaq turnoMaq;
    private String tipo;
    private float costo;
    private String ejecutor1;
    private String ejecutor2;
    private int id_tareas;
    private String uso;
    private String fechaPrograma;
    private double presupuesto;
    private String codProducto;
    private String tipoFalla;

    public Mantenimiento() {
        empExt = new ModEmpExt();
        emplEmt = new ModEmplExt();
        turnoMaq = new ModTurnoMaq();
        horasDetencion = null;
        ano = 0;
        mes = 0;
    }

    public int getCodigoMantenimiento() {
        return CodigoMantenimiento;
    }

    public void setCodigoMantenimiento(int CodigoMantenimiento) {
        this.CodigoMantenimiento = CodigoMantenimiento;
    }

    public int getCodigoEspecialidad() {
        return CodigoEspecialidad;
    }

    public void setCodigoEspecialidad(int CodigoEspecialidad) {
        this.CodigoEspecialidad = CodigoEspecialidad;
    }

    public int getCodigoSolicitante() {
        return CodigoSolicitante;
    }

    public void setCodigoSolicitante(int CodigoSolicitante) {
        this.CodigoSolicitante = CodigoSolicitante;
    }

    public int getCodigoMaquina() {
        return CodigoMaquina;
    }

    public void setCodigoMaquina(int CodigoMaquina) {
        this.CodigoMaquina = CodigoMaquina;
    }

    public int getCodigoFalla() {
        return CodigoFalla;
    }

    public void setCodigoFalla(int CodigoFalla) {
        this.CodigoFalla = CodigoFalla;
    }

    public String getObservacionFalla() {
        return ObservacionFalla;
    }

    public void setObservacionFalla(String ObservacionFalla) {
        this.ObservacionFalla = ObservacionFalla;
    }

    public String getFechaDetencionEquipo() {
        return FechaDetencionEquipo;
    }

    public void setFechaDetencionEquipo(String FechaDetencionEquipo) {
        this.FechaDetencionEquipo = FechaDetencionEquipo;
    }

    public String getFechaEntregaEquipo() {
        return FechaEntregaEquipo;
    }

    public void setFechaEntregaEquipo(String FechaEntregaEquipo) {
        this.FechaEntregaEquipo = FechaEntregaEquipo;
    }

    public String getComentario() {
        return Comentario;
    }

    public void setComentario(String Comentario) {
        this.Comentario = Comentario;
    }

    public String getActiva() {
        return Activa;
    }

    public void setActiva(String Activa) {
        this.Activa = Activa;
    }

    public String getRecepcionar() {
        return Recepcionar;
    }

    public void setRecepcionar(String Recepcionar) {
        this.Recepcionar = Recepcionar;
    }

    public String getFechaEstado() {
        return fechaEstado;
    }

    public void setFechaEstado(String fechaEstado) {
        this.fechaEstado = fechaEstado;
    }

    public int getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(int idEstado) {
        this.idEstado = idEstado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public String getFechaDiagnostico() {
        return fechaDiagnostico;
    }

    public void setFechaDiagnostico(String fechaDiagnostico) {
        this.fechaDiagnostico = fechaDiagnostico;
    }

    public String getFechaAdquisicion() {
        return fechaAdquisicion;
    }

    public void setFechaAdquisicion(String fechaAdquisicion) {
        this.fechaAdquisicion = fechaAdquisicion;
    }

    public String getFechaSevicioExterno() {
        return fechaSevicioExterno;
    }

    public void setFechaSevicioExterno(String fechaSevicioExterno) {
        this.fechaSevicioExterno = fechaSevicioExterno;
    }

    public String getFechaMantenimientoCorrectivo() {
        return fechaMantenimientoCorrectivo;
    }

    public void setFechaMantenimientoCorrectivo(String fechaMantenimientoCorrectivo) {
        this.fechaMantenimientoCorrectivo = fechaMantenimientoCorrectivo;
    }

    public String getFechaVistoBueno() {
        return fechaVistoBueno;
    }

    public void setFechaVistoBueno(String fechaVistoBueno) {
        this.fechaVistoBueno = fechaVistoBueno;
    }

    public String getFechaCerrar() {
        return fechaCerrar;
    }

    public void setFechaCerrar(String fechaCerrar) {
        this.fechaCerrar = fechaCerrar;
    }

    public String getFechaHoy() {
        return FechaHoy;
    }

    public void setFechaHoy(String FechaHoy) {
        this.FechaHoy = FechaHoy;
    }

    public String getObservacionDiagnostico() {
        return observacionDiagnostico;
    }

    public void setObservacionDiagnostico(String observacionDiagnostico) {
        this.observacionDiagnostico = observacionDiagnostico;
    }

    public String getObservacionAdquisicion() {
        return observacionAdquisicion;
    }

    public void setObservacionAdquisicion(String observacionAdquisicion) {
        this.observacionAdquisicion = observacionAdquisicion;
    }

    public String getObservacionSevicioExterno() {
        return observacionSevicioExterno;
    }

    public void setObservacionSevicioExterno(String observacionSevicioExterno) {
        this.observacionSevicioExterno = observacionSevicioExterno;
    }

    public String getObservacionMantenimientoCorrectivo() {
        return observacionMantenimientoCorrectivo;
    }

    public void setObservacionMantenimientoCorrectivo(String observacionMantenimientoCorrectivo) {
        this.observacionMantenimientoCorrectivo = observacionMantenimientoCorrectivo;
    }

    public String getObservacionVistoBueno() {
        return observacionVistoBueno;
    }

    public void setObservacionVistoBueno(String observacionVistoBueno) {
        this.observacionVistoBueno = observacionVistoBueno;
    }

    public String getObservacionCerrar() {
        return observacionCerrar;
    }

    public void setObservacionCerrar(String observacionCerrar) {
        this.observacionCerrar = observacionCerrar;
    }

    public void setNombreUsuarioMantenimiento(String nUs) {
        nombreUsuarioMantenimiento = nUs;
    }

    public String getNombreUsuarioMantenimiento() {
        return nombreUsuarioMantenimiento;
    }

    public void setNombreMaquina(String nMa) {
        nombreMaquina = nMa;
    }

    public String getNombreMaquina() {
        return nombreMaquina;
    }

    public void setNombreFalla(String f) {
        nombreFalla = f;
    }

    public String getNombreFalla() {
        return nombreFalla;
    }

    public String getDescFalla() {
        return descFalla;
    }

    public void setDescFalla(String descFalla) {
        this.descFalla = descFalla;
    }

    public String getNumeroSap() {
        return numeroSap;
    }

    public void setNumeroSap(String numeroSap) {
        this.numeroSap = numeroSap;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNoConformidad() {
        return noConformidad;
    }

    public void setNoConformidad(String noConformidad) {
        this.noConformidad = noConformidad;
    }

    public String getFechaNoConformidad() {
        return fechaNoConformidad;
    }

    public void setFechaNoConformidad(String fechaNoConformidad) {
        this.fechaNoConformidad = fechaNoConformidad;
    }

    public String getObservacionNoConformidad() {
        return observacionNoConformidad;
    }

    public void setObservacionNoConformidad(String observacionNoConformidad) {
        this.observacionNoConformidad = observacionNoConformidad;
    }

    public String getNombreArea() {
        return nombreArea;
    }

    public void setNombreArea(String nombreArea) {
        this.nombreArea = nombreArea;
    }

    public String getNombreTipoFalla() {
        return nombreTipoFalla;
    }

    public void setNombreTipoFalla(String nombreTipoFalla) {
        this.nombreTipoFalla = nombreTipoFalla;
    }

    public String getDescTipoFalla() {
        return descTipoFalla;
    }

    public void setDescTipoFalla(String descTipoFalla) {
        this.descTipoFalla = descTipoFalla;
    }

    public ModPerMant getPersonalMant() {
        return personalMant;
    }

    public void setPersonalMant(ModPerMant personalMant) {
        this.personalMant = personalMant;
    }

    public ModEmpExt getEmpExt() {
        return empExt;
    }

    public void setEmpExt(ModEmpExt empExt) {
        this.empExt = empExt;
    }

    public ModEmplExt getEmplEmt() {
        return emplEmt;
    }

    public void setEmplEmt(ModEmplExt emplEmt) {
        this.emplEmt = emplEmt;
    }

    public ModTurnoMaq getTurnoMaq() {
        return turnoMaq;
    }

    public void setTurnoMaq(ModTurnoMaq turnoMaq) {
        this.turnoMaq = turnoMaq;
    }

    public String getHorasDetencion() {
        return horasDetencion;
    }

    public void setHorasDetencion(String horasDetencion) {
        this.horasDetencion = horasDetencion;
    }

    public String getMinutosDetencion() {
        return minutosDetencion;
    }

    public void setMinutosDetencion(String minutosDetencion) {
        this.minutosDetencion = minutosDetencion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public String getFechaMantePreventivo() {
        return fechaMantePreventivo;
    }

    public void setFechaMantePreventivo(String fechaMantePreventivo) {
        this.fechaMantePreventivo = fechaMantePreventivo;
    }

    public String getEjecutor1() {
        return ejecutor1;
    }

    public void setEjecutor1(String ejecutor1) {
        this.ejecutor1 = ejecutor1;
    }

    public String getEjecutor2() {
        return ejecutor2;
    }

    public void setEjecutor2(String ejecutor2) {
        this.ejecutor2 = ejecutor2;
    }

    public int getId_tareas() {
        return id_tareas;
    }

    public void setId_tareas(int id_tareas) {
        this.id_tareas = id_tareas;
    }

    public String getUso() {
        return uso;
    }

    public void setUso(String uso) {
        this.uso = uso;
    }

    public String getFechaPrograma() {
        return fechaPrograma;
    }

    public void setFechaPrograma(String fechaPrograma) {
        this.fechaPrograma = fechaPrograma;
    }

    public double getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(double presupuesto) {
        this.presupuesto = presupuesto;
    }

    public double getHorasDetenidas() {
        return horasDetenidas;
    }

    public void setHorasDetenidas(double horasDetenidas) {
        this.horasDetenidas = horasDetenidas;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public String getTipoFalla() {
        return tipoFalla;
    }

    public void setTipoFalla(String tipoFalla) {
        this.tipoFalla = tipoFalla;
    }
}
