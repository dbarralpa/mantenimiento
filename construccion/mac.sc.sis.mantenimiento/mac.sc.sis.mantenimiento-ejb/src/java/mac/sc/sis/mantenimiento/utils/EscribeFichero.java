package mac.sc.sis.mantenimiento.utils;

import java.io.*;
import mac.sc.sis.mantenimiento.controllers.PrmApp;

public class EscribeFichero{

    private static FileWriter   fichero     = null;
    private static PrintWriter  pw          = null;
    private static String       urlLog      = "C:/logError.txt";    
    private static String       txtErrorEnc;
    private static String       txtErrorDet;
    

    private static void addLOG(){
        try
        {        
            fichero = new FileWriter(urlLog, true);
            pw = new PrintWriter(fichero);
            pw.println(PrmApp.getFechaActual() + " " + PrmApp.getHoraActual());
            pw.println(getTxtErrorEnc());
            pw.println(getTxtErrorDet());    
            pw.println("");                
        } catch (Exception e) {
        } finally {
           if (null != fichero)
              try {
                fichero.close();
            } catch (Exception e1) {
            }
           try {
           } catch (Exception e2) {
           }
        }
    }

    public static String getLogHTML(){
        String glosa  = null;
        glosa  = "<b>" + EscribeFichero.getTxtErrorEnc() + "</b></p>";
        glosa += EscribeFichero.getTxtErrorDet();
        return glosa;
    }

    public static void addError(Exception ex){
        addError(ex, false);        
    }
    
    public static void addError(Exception ex, boolean conTraza){
        setTxtErrorEnc(ex.toString());
        if(conTraza){            
            StackTraceElement[] nn = ex.getStackTrace();
            String Err = "";
            for (int i = 0; i < nn.length; i++) {
               Err += nn[i].toString() + " ";
            }
            setTxtErrorDet(Err);
        }else{
            setTxtErrorDet("");
        }
        addLOG();
    }

    private static String getTxtErrorEnc() {
        return txtErrorEnc;
    }

    private static void setTxtErrorEnc(String aTxtErrorEnc) {
        txtErrorEnc = aTxtErrorEnc;
    }

    private static String getTxtErrorDet() {
        return txtErrorDet;
    }

    private static void setTxtErrorDet(String aTxtErrorDet) {
        txtErrorDet = aTxtErrorDet;
    }
}
