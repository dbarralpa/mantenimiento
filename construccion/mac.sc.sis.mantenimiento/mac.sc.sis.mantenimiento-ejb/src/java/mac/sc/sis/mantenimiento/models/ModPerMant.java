package mac.sc.sis.mantenimiento.models;

/**
 *
 * @author ngonzalez
 */
public class ModPerMant implements Cloneable {

    private int id;
    private String vc_rut;
    private String vc_nombre;
    private String vc_apellido;

    public ModPerMant() {
        id = 0;
        vc_rut = "";
        vc_nombre = "";
        vc_apellido = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVc_rut() {
        return vc_rut;
    }

    public void setVc_rut(String vc_rut) {
        this.vc_rut = vc_rut;
    }

    public String getVc_nombre() {
        return vc_nombre;
    }

    public void setVc_nombre(String vc_nombre) {
        this.vc_nombre = vc_nombre;
    }

    public String getVc_apellido() {
        return vc_apellido;
    }

    public void setVc_apellido(String vc_apellido) {
        this.vc_apellido = vc_apellido;
    }

    public ModPerMant copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
            //
        }
        return (ModPerMant) obj;
    }
}
