package mac.sc.sis.mantenimiento.clases;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModIntervalo;
import mac.sc.sis.mantenimiento.models.ModMantenimiento;


import mac.sc.sis.mantenimiento.models.ModMaquina;
import mac.sc.sis.mantenimiento.models.ModEmpExt;
import mac.sc.sis.mantenimiento.models.ModEmplExt;
import mac.sc.sis.mantenimiento.models.ModFalla;
import mac.sc.sis.mantenimiento.beans.Trabajo;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.models.ModArea;
import mac.sc.sis.mantenimiento.models.ModHorario;
import mac.sc.sis.mantenimiento.models.ModPerMant;
import mac.sc.sis.mantenimiento.models.ModPrograma;
import mac.sc.sis.mantenimiento.models.ModTipoFalla;
import mac.sc.sis.mantenimiento.models.ModTurnoMaq;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 * This is the bean class for the macScSisMantenimientoBean enterprise bean.
 * Created 20-02-2008 03:06:41 PM
 * @author pgonzalezc
 */
public class macScSisMantenimientoBean implements SessionBean, macScSisMantenimientoRemoteBusiness {
    
    private SessionContext context;
    private Conexion mConexion = new Conexion();
    private Conexion mConexionAux = new Conexion();
    private Statement st = null;
    private Statement stAux = null;
    private ResultSet rs = null;
    private ResultSet rsAux = null;
    private Connection conMac = null;
    private Connection conMacAux = null;
    private Connection conENS = null;
    private Loger loger = new Loger();
    private SimpleDateFormat forma = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat formate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
    
    // Add business logic below. (Right-click in editor and choose
    // "EJB Methods > Add Business Method" or "Web Service > Add Operation")
    public ArrayList listarMaquinas(int codigoEspecialidad) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT Uso, NombreCorto, IdMaquina, ";
            sql += "t.idTurnoMaq AS idTurno, t.nombreTurnoMaq AS nomTurno, t.descTurnoMaq AS descTurno ";
            sql += "FROM dbo.TManMaquina m ";
            sql += "LEFT JOIN dbo.TMantTurnoMaq t ON m.idTurnoMaq = t.idTurnoMaq ";
            sql += "WHERE area = " + codigoEspecialidad + " order by Uso ";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModMaquina maq = new ModMaquina();
                maq.setUso(rs.getString("Uso"));
                maq.setNombreCorto(rs.getString("NombreCorto"));
                maq.setIdMaquina(rs.getInt("IdMaquina"));
                maq.setTurnoMaq(new ModTurnoMaq());
                maq.getTurnoMaq().setIdTurnoMaq(rs.getInt("idTurno"));
                maq.getTurnoMaq().setNombreTurnoMaq(rs.getString("nomTurno"));
                maq.getTurnoMaq().setDescTurnoMaq(rs.getString("descTurno"));
                vec.add(maq);
            }
            conMac.close();
            st.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            loger.logger("listarMaquinas :", e.getMessage(), 0);
        }
        return vec;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarFallas(int maquina) {
        ArrayList listadoFallas = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT a.CodigoFalla AS idFalla, a.NombreFalla AS nombreFalla, a.IdentificadorFalla AS idMaquina, ";
            sql += "a.DescFalla AS descFalla, b.idTipoFalla AS idTipoFalla, b.nombre AS nombreTipoFalla, descripcion AS descTipoFalla ";
            sql += "FROM dbo.TManFalla a ";
            sql += "LEFT JOIN dbo.TManTFalla b ON a.idTipoFalla = b.idTipoFalla ";
            sql += "WHERE IdentificadorFalla = " + maquina + " ";
            sql += "ORDER BY NombreFalla";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModFalla falla = new ModFalla();
                falla.setId(listadoFallas.size());
                falla.setIdFalla(rs.getInt("idFalla"));
                falla.setNombre(rs.getString("nombreFalla"));
                falla.setIdMaquina(rs.getInt("idMaquina"));
                falla.setDescripcion(rs.getString("descFalla"));
                falla.getTipoFalla().setIdTipoFalla(rs.getInt("idTipoFalla"));
                falla.getTipoFalla().setNombre(rs.getString("nombreTipoFalla"));
                falla.getTipoFalla().setDescripcion(rs.getString("descTipoFalla"));
                listadoFallas.add(falla);
                falla = null;
            }
            conMac.close();
            st.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            loger.logger("listarFallas :", e.getMessage(), 0);
        }
        return listadoFallas;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarTipoFallas() {
        ArrayList listadoTipoFallas = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT idTipoFalla, nombre, descripcion, inicial FROM dbo.TManTFalla ORDER BY nombre";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModTipoFalla tipoFalla = new ModTipoFalla();
                tipoFalla.setId(listadoTipoFallas.size());
                tipoFalla.setIdTipoFalla(rs.getInt("idTipoFalla"));
                tipoFalla.setNombre(rs.getString("nombre"));
                tipoFalla.setDescripcion(rs.getString("descripcion"));
                tipoFalla.setInicial(rs.getString("inicial"));
                listadoTipoFallas.add(tipoFalla);
                tipoFalla = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("listarTipoFallas :", e.getMessage(), 0);
        }
        return listadoTipoFallas;
    }
    
    //-----Listado personal de mantenimiento----------------------------------------------------------------------------------------------
    public ArrayList listarPerMant() {
        ArrayList listadoPerMant = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT vc_rut, vc_nombre, vc_apellido ";
            sql += "FROM dbo.TMantPerMant ";
            sql += "ORDER BY vc_nombre";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModPerMant personal = new ModPerMant();
                personal.setId(listadoPerMant.size());
                personal.setVc_rut(rs.getString("vc_rut"));
                personal.setVc_nombre(rs.getString("vc_nombre"));
                personal.setVc_apellido(rs.getString("vc_apellido"));
                listadoPerMant.add(personal);
                personal = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("listarPerMant :", e.getMessage(), 0);
        }
        return listadoPerMant;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarEmpExt() {
        ArrayList listadoEmpExt = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT vc_rut, vc_nombre, vc_direccion  FROM dbo.TManEmpExt ORDER BY vc_nombre";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModEmpExt empExt = new ModEmpExt();
                empExt.setId(listadoEmpExt.size());
                empExt.setVc_rut(rs.getString("vc_rut"));
                empExt.setVc_nombre(rs.getString("vc_nombre"));
                empExt.setVc_direccion(rs.getString("vc_direccion"));
                empExt.setEmpleados(listarEmplExt(empExt.getVc_rut()));
                listadoEmpExt.add(empExt);
                empExt = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("listarEmpExt :", e.getMessage(), 0);
        }
        return listadoEmpExt;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarTurnosMaquinas() {
        ArrayList listadoTurnosMaq = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT idTurnoMaq, nombreTurnoMaq, descTurnoMaq FROM dbo.TMantTurnoMaq ORDER BY nombreTurnoMaq";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModTurnoMaq turnoMaq = new ModTurnoMaq();
                turnoMaq.setId(listadoTurnosMaq.size());
                turnoMaq.setIdTurnoMaq(rs.getInt("idTurnoMaq"));
                turnoMaq.setNombreTurnoMaq(rs.getString("nombreTurnoMaq"));
                turnoMaq.setDescTurnoMaq(rs.getString("descTurnoMaq"));
                turnoMaq.setHorarios(listarHorariosXTurno(turnoMaq.getIdTurnoMaq()));
                listadoTurnosMaq.add(turnoMaq);
                turnoMaq = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("listarTurnosMaquinas :", e.getMessage(), 0);
        }
        return listadoTurnosMaq;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarHorariosXTurno(int idTurnoMaq) {
        ArrayList horarios = new ArrayList();
        try {
            conMacAux = mConexionAux.getMACConnector();
            stAux = conMacAux.createStatement();
            String sql = "SELECT idHorario, horaInicio, horaTermino, diaHorario, estado FROM dbo.TMantHorario WHERE idTurnoMaq = " + idTurnoMaq + " ORDER BY idHorario";
            rsAux = stAux.executeQuery(sql);
            while (rsAux.next()) {
                ModHorario horario = new ModHorario();
                horario.setId(horarios.size());
                horario.setIdHorario(rsAux.getInt("idHorario"));
                horario.setHoraInicio(rsAux.getString("horaInicio"));
                horario.setHoraTermino(rsAux.getString("horaTermino"));
                horario.setDiaHorario(rsAux.getString("diaHorario"));
                if (rsAux.getInt("estado") == 1) {
                    horario.setEstado(true);
                }
                horarios.add(horario);
                horario = null;
            }
            conMacAux.close();
            stAux.close();
            rsAux.close();
        } catch (Exception e) {
            loger.logger("listarHorariosXTurno :", e.getMessage(), 0);
        }
        return horarios;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarEmplExt(String vc_rut_emp) {
        ArrayList empleados = new ArrayList();
        try {
            conMacAux = mConexionAux.getMACConnector();
            stAux = conMacAux.createStatement();
            String sql = "SELECT vc_rut, vc_rut_emp, vc_nombre, vc_apellido FROM dbo.TManEmplExt ";
            sql += "WHERE vc_rut_emp = '" + vc_rut_emp + "' ";
            sql += "ORDER BY vc_nombre";
            rsAux = stAux.executeQuery(sql);
            while (rsAux.next()) {
                ModEmplExt empleado = new ModEmplExt();
                empleado.setId(empleados.size());
                empleado.setVc_rut(rsAux.getString("vc_rut"));
                empleado.setVc_rut_emp(rsAux.getString("vc_rut_emp"));
                empleado.setVc_nombre(rsAux.getString("vc_nombre"));
                empleado.setVc_apellido(rsAux.getString("vc_apellido"));
                empleados.add(empleado);
                empleado = null;
            }
            conMacAux.close();
            stAux.close();
            rsAux.close();
        } catch (Exception e) {
            loger.logger("listarEmplExt :", e.getMessage(), 0);
        }
        return empleados;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarPedidosMantenimiento(int idUsuario) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT a.CodigoMantenimiento, a.CodigoEspecialidad, a.CodigoSolicitante, a.CodigoMaquina, a.CodigoFalla, ";
            sql += "a.ObservacionFalla, a.FechaDetencionEquipo, a.FechaEntregaEquipo, ";
            sql += "a.Comentario, a.Activa, a.Recepcionar, a.FechaHoy, b.Nombre, c.Uso, c.NombreCorto, d.NombreFalla, d.DescFalla, ";
            sql += "e.nombre AS nombreTipoFalla, a.TipoFalla AS descTipoFalla, ";
            sql += "empr.vc_rut AS rutEmpr, empr.vc_nombre AS nomEmpr, empr.vc_direccion, ";
            sql += "empl.vc_rut AS rutEmpl, empl.vc_rut_emp AS rutEmplEmp, empl.vc_nombre AS nomEmpl, empl.vc_apellido ";
            sql += "FROM dbo.TMantencion a LEFT JOIN dbo.TManTipoMantenimiento b ON a.CodigoEspecialidad = b.Codigo ";
            sql += "LEFT JOIN dbo.TManMaquina c ON a.CodigoMaquina = c.IdMaquina ";
            sql += "LEFT JOIN dbo.TManFalla d ON a.CodigoFalla = d.CodigoFalla ";
            sql += "LEFT JOIN dbo.TManTFalla e ON d.idTipoFalla = e.idTipoFalla ";
            sql += "LEFT JOIN dbo.TManEmpExt empr ON a.RutEmprExt = empr.vc_rut ";
            sql += "LEFT JOIN dbo.TManEmplExt empl ON a.RutEmplExt = empl.vc_rut ";
            sql += "WHERE a.CodigoSolicitante = " + idUsuario + " ";
            sql += "ORDER BY a.CodigoMantenimiento";
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                String uso = null;
                String nombre = null;
                Mantenimiento mantenimiento = new Mantenimiento();
                mantenimiento.setCodigoMantenimiento(rs.getInt("CodigoMantenimiento"));
                mantenimiento.setCodigoEspecialidad(rs.getInt("CodigoEspecialidad"));
                mantenimiento.setCodigoSolicitante(rs.getInt("CodigoSolicitante"));
                mantenimiento.setCodigoMaquina(rs.getInt("CodigoMaquina"));
                mantenimiento.setCodigoFalla(rs.getInt("CodigoFalla"));
                mantenimiento.setObservacionFalla(rs.getString("ObservacionFalla"));
                mantenimiento.setFechaDetencionEquipo(rs.getString("FechaDetencionEquipo"));
                mantenimiento.setFechaEntregaEquipo(rs.getString("FechaEntregaEquipo"));
                mantenimiento.setComentario(rs.getString("Comentario"));
                mantenimiento.setActiva(rs.getString("Activa"));
                mantenimiento.setRecepcionar(rs.getString("Recepcionar"));
                mantenimiento.setFechaHoy(rs.getString("FechaHoy"));
                mantenimiento.setNombreArea(rs.getString("Nombre"));
                uso = rs.getString("Uso");
                nombre = rs.getString("NombreCorto");
                if (uso == null) {
                    uso = " ";
                }
                if (nombre == null || nombre.equalsIgnoreCase("<NULL>")) {
                    nombre = "";
                }
                String nP = uso + " " + nombre;
                mantenimiento.setNombreMaquina(nP);
                mantenimiento.setNombreFalla(rs.getString("NombreFalla"));
                mantenimiento.setDescFalla(rs.getString("DescFalla"));
                mantenimiento.setNombreTipoFalla(rs.getString("nombreTipoFalla"));
                mantenimiento.setDescTipoFalla(rs.getString("descTipoFalla"));
                mantenimiento.getEmpExt().setVc_rut(rs.getString("rutEmpr"));
                mantenimiento.getEmpExt().setVc_nombre(rs.getString("nomEmpr"));
                mantenimiento.getEmpExt().setVc_direccion(rs.getString("vc_direccion"));
                mantenimiento.getEmplEmt().setVc_rut(rs.getString("rutEmpl"));
                mantenimiento.getEmplEmt().setVc_rut_emp(rs.getString("rutEmplEmp"));
                mantenimiento.getEmplEmt().setVc_nombre(rs.getString("nomEmpl"));
                mantenimiento.getEmplEmt().setVc_apellido(rs.getString("vc_apellido"));
                vec.add(mantenimiento);
                mantenimiento = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("listarPedidosMantenimiento :", e.getMessage(), 0);
        }
        return vec;
    }
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarPedidosMantePreventivos(int idUsuario, boolean estado) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " SELECT distinct(numero) as numero,tm.Nombre as area,tmm.Nombre as maquina,fecha,fechaInicio,fechaFin,ejecutor1,observacion,pr.id as id_tareas,tmm.Uso as uso,pr.fecha as fechaPrograma";
            sql += " FROM MAC.dbo.PRO_mantencion_preventiva mp";
            sql += " inner join MAC.dbo.PRO_programa_man_pre  pr";
            sql += " on(pr.id = mp.id_programa)";
            sql += " inner join MAC.dbo.PRO_maquina_tarea  mt";
            sql += " on(mt.id =  id_maquina_tarea)";
            sql += " inner join MAC.dbo.TManMaquina tmm";
            sql += " on(tmm.IdMaquina = mt.id_maquina)";
            sql += " inner join MAC.dbo.TManTipoMantenimiento tm";
            sql += " on(tm.Codigo = tmm.area)";
            if(estado){
                sql += " where pr.ejecucion = 'N' ";
            }
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento mante = new Mantenimiento();
                mante.setCodigoMantenimiento(rs.getInt("numero"));
                mante.setNombreArea(rs.getString("area"));
                mante.setNombreMaquina(rs.getString("maquina"));
                if(rs.getString("fecha") != null){
                    mante.setFechaMantePreventivo(forma.format(rs.getTimestamp("fecha")));
                }else{
                    mante.setFechaMantePreventivo(rs.getString("fecha"));
                }
                if(rs.getString("fechaInicio") != null){
                    mante.setFechaDetencionEquipo(forma.format(rs.getTimestamp("fechaInicio")));
                }else{
                    mante.setFechaDetencionEquipo(rs.getString("fechaInicio"));
                }
                if(rs.getString("fechaFin") != null){
                    mante.setFechaEntregaEquipo(forma.format(rs.getTimestamp("fechaFin")));
                }else{
                    mante.setFechaEntregaEquipo(rs.getString("fechaFin"));
                }
                mante.setEjecutor1(rs.getString("ejecutor1"));
                mante.setObservacionCerrar(rs.getString("observacion"));
                mante.setId_tareas(rs.getInt("id_tareas"));
                mante.setUso(rs.getString("uso"));
                mante.setFechaPrograma(forma.format(rs.getDate("fechaPrograma")));
                mante.setCodigoSolicitante(idUsuario);
                vec.add(mante);
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarPedidosMantePreventivos() ", e.getMessage(), 0);
        }
        return vec;
    }
  //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarPedidosMantePreventivos(int idUsuario, int valor) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " SELECT distinct(numero) as numero,tm.Nombre as area,tmm.Nombre as maquina,fecha,fechaInicio,fechaFin,ejecutor1,observacion,pr.id as id_tareas,tmm.Uso as uso,pr.fecha as fechaPrograma";
            sql += " FROM MAC.dbo.PRO_mantencion_preventiva mp";
            sql += " inner join MAC.dbo.PRO_programa_man_pre  pr";
            sql += " on(pr.id = mp.id_programa)";
            sql += " inner join MAC.dbo.PRO_maquina_tarea  mt";
            sql += " on(mt.id =  id_maquina_tarea)";
            sql += " inner join MAC.dbo.TManMaquina tmm";
            sql += " on(tmm.IdMaquina = mt.id_maquina)";
            sql += " inner join MAC.dbo.TManTipoMantenimiento tm";
            sql += " on(tm.Codigo = tmm.area)";
            sql += " where pr.ejecucion = 'N' and tmm.IdMaquina = "+valor;
            sql += " order by fecha";
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento mante = new Mantenimiento();
                mante.setCodigoMantenimiento(rs.getInt("numero"));
                mante.setNombreArea(rs.getString("area"));
                mante.setNombreMaquina(rs.getString("maquina"));
                if(rs.getString("fecha") != null){
                    mante.setFechaMantePreventivo(forma.format(rs.getTimestamp("fecha")));
                }else{
                    mante.setFechaMantePreventivo(rs.getString("fecha"));
                }
                if(rs.getString("fechaInicio") != null){
                    mante.setFechaDetencionEquipo(forma.format(rs.getTimestamp("fechaInicio")));
                }else{
                    mante.setFechaDetencionEquipo(rs.getString("fechaInicio"));
                }
                if(rs.getString("fechaFin") != null){
                    mante.setFechaEntregaEquipo(forma.format(rs.getTimestamp("fechaFin")));
                }else{
                    mante.setFechaEntregaEquipo(rs.getString("fechaFin"));
                }
                mante.setEjecutor1(rs.getString("ejecutor1"));
                mante.setObservacionCerrar(rs.getString("observacion"));
                mante.setId_tareas(rs.getInt("id_tareas"));
                mante.setUso(rs.getString("uso"));
                mante.setFechaPrograma(forma.format(rs.getDate("fechaPrograma")));
                mante.setCodigoSolicitante(idUsuario);
                vec.add(mante);
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarPedidosMantePreventivos() ", e.getMessage(), 0);
        }
        return vec;
    }
 //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarPedidosMantePreventivos(int idUsuario, String fecha) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " SELECT distinct(numero) as numero,tm.Nombre as area,tmm.Nombre as maquina,fecha,fechaInicio,fechaFin,ejecutor1,observacion,pr.id as id_tareas,tmm.Uso as uso,pr.fecha as fechaPrograma";
            sql += " FROM MAC.dbo.PRO_mantencion_preventiva mp";
            sql += " inner join MAC.dbo.PRO_programa_man_pre  pr";
            sql += " on(pr.id = mp.id_programa)";
            sql += " inner join MAC.dbo.PRO_maquina_tarea  mt";
            sql += " on(mt.id =  id_maquina_tarea)";
            sql += " inner join MAC.dbo.TManMaquina tmm";
            sql += " on(tmm.IdMaquina = mt.id_maquina)";
            sql += " inner join MAC.dbo.TManTipoMantenimiento tm";
            sql += " on(tm.Codigo = tmm.area)";
            sql += " where pr.fecha  = '" + fecha + "' ";
            sql += " order by fecha";
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento mante = new Mantenimiento();
                mante.setCodigoMantenimiento(rs.getInt("numero"));
                mante.setNombreArea(rs.getString("area"));
                mante.setNombreMaquina(rs.getString("maquina"));
                if(rs.getString("fecha") != null){
                    mante.setFechaMantePreventivo(forma.format(rs.getTimestamp("fecha")));
                }else{
                    mante.setFechaMantePreventivo(rs.getString("fecha"));
                }
                if(rs.getString("fechaInicio") != null){
                    mante.setFechaDetencionEquipo(forma.format(rs.getTimestamp("fechaInicio")));
                }else{
                    mante.setFechaDetencionEquipo(rs.getString("fechaInicio"));
                }
                if(rs.getString("fechaFin") != null){
                    mante.setFechaEntregaEquipo(forma.format(rs.getTimestamp("fechaFin")));
                }else{
                    mante.setFechaEntregaEquipo(rs.getString("fechaFin"));
                }
                mante.setEjecutor1(rs.getString("ejecutor1"));
                mante.setObservacionCerrar(rs.getString("observacion"));
                mante.setId_tareas(rs.getInt("id_tareas"));
                mante.setUso(rs.getString("uso"));
                mante.setFechaPrograma(forma.format(rs.getDate("fechaPrograma")));
                mante.setCodigoSolicitante(idUsuario);
                vec.add(mante);
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarPedidosMantePreventivos() ", e.getMessage(), 0);
        }
        return vec;
    }   
 //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarTareasPreventivos(int id) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select pt.tareas,pt.id_tipo_tarea from MAC.dbo.PRO_programa_man_pre pm";
            sql += " inner join MAC.dbo.PRO_maquina_tarea mt";
            sql += " on(pm.id_maquina_tarea = mt.id)";
            sql += " inner join MAC.dbo.PRO_tareas pt";
            sql += " on(mt.id_tarea = pt.id)";
            sql += " where pm.id = "+id;
            sql += " order by pm.id_maquina_tarea";
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento mante = new Mantenimiento();
                mante.setNombreArea(rs.getString("tareas"));
                mante.setId_tareas(rs.getInt("id_tipo_tarea"));
                vec.add(mante);
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarTareasPreventivos() ", e.getMessage(), 0);
        }
        return vec;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarTodosLosPedidosDeMantenimiento() {
        ArrayList solicitudes = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT a.CodigoMantenimiento,a.CodigoEspecialidad,a.CodigoSolicitante,a.CodigoMaquina, ";
            sql += "a.CodigoFalla,a.ObservacionFalla,a.FechaDetencionEquipo, ";
            sql += "a.FechaEntregaEquipo,Comentario,Activa,Recepcionar, ";
            sql += "b.fechaDiagnostico, b.fechaAdquisicion, b.fechaSevicioExterno, b.fechaMantenimientoCorrectivo, ";
            sql += "b.fechaVistoBueno, b.fechaCerrar,a.FechaDetencionEquipo,a.FechaHoy, ";
            sql += "b.observacionDiagnostico,b.observacionAdquisicion, b.observacionServicioExterno, ";
            sql += "b.observacionMantenimientoCorrectivo,b.observacionVistoBueno,b.observacionCerrar, ";
            sql += "a.ObservacionFalla,(u.nombre+ ' ' +u.apellido)as NombreUsuario,m.Uso,m.NombreCorto,f.NombreFalla, a.NSap, ";
            sql += "b.noConformidad, b.fechaNoConformidad, b.observacionNoConformidad, tm.Nombre AS nombreArea, ";
            sql += "e.nombre AS nombreTFalla, e.descripcion AS descTFalla, pm.vc_rut AS rutEncargado, pm.vc_nombre AS nombreEncargado, pm.vc_apellido AS apelliEncargado, ";
            sql += "empr.vc_rut AS rutEmpr, empr.vc_nombre AS nomEmpr, empr.vc_direccion, ";
            sql += "empl.vc_rut AS rutEmpl, empl.vc_rut_emp AS rutEmplEmp, empl.vc_nombre AS nomEmpl, empl.vc_apellido, ";
            sql += "turmaq.idTurnoMaq, turmaq.nombreTurnoMaq, turmaq.descTurnoMaq ";
            sql += "FROM dbo.TMantencion a ";
            sql += "LEFT JOIN dbo.TInformacionEstado b ON a.CodigoMantenimiento = b.idOM ";
            sql += "LEFT JOIN TManTipoMantenimiento tm ON a.CodigoEspecialidad = tm.Codigo ";
            sql += "LEFT JOIN dbo.TUsuario u ON u.idUsuario = a.CodigoSolicitante ";
            sql += "LEFT JOIN dbo.TManMaquina m ON a.CodigoMaquina = m.IdMaquina ";
            sql += "LEFT JOIN dbo.TMantTurnoMaq turmaq ON m.idTurnoMaq = turmaq.idTurnoMaq ";
            sql += "LEFT JOIN dbo.TManFalla f ON f.CodigoFalla = a.CodigoFalla ";
            sql += "LEFT JOIN dbo.TManTFalla e ON f.idTipoFalla = e.idTipoFalla ";
            sql += "LEFT JOIN dbo.TMantPerMant pm ON pm.vc_rut = a.RutPerEncargado ";
            sql += "LEFT JOIN dbo.TManEmpExt empr ON a.RutEmprExt = empr.vc_rut ";
            sql += "LEFT JOIN dbo.TManEmplExt empl ON a.RutEmplExt = empl.vc_rut ";
            sql += "ORDER BY a.CodigoMantenimiento";
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                String uso = null;
                String nombre = null;
                Mantenimiento mantenimiento = new Mantenimiento();
                mantenimiento.setCodigoMantenimiento(rs.getInt("CodigoMantenimiento"));
                mantenimiento.setCodigoEspecialidad(rs.getInt("CodigoEspecialidad"));
                mantenimiento.setCodigoSolicitante(rs.getInt("CodigoSolicitante"));
                mantenimiento.setCodigoMaquina(rs.getInt("CodigoMaquina"));
                mantenimiento.setCodigoFalla(rs.getInt("CodigoFalla"));
                mantenimiento.setObservacionFalla(rs.getString("ObservacionFalla"));
                mantenimiento.setFechaDetencionEquipo(rs.getString("FechaDetencionEquipo"));
                mantenimiento.setFechaEntregaEquipo(rs.getString("FechaEntregaEquipo"));
                mantenimiento.setComentario(rs.getString("Comentario"));
                mantenimiento.setActiva(rs.getString("Activa"));
                mantenimiento.setRecepcionar(rs.getString("Recepcionar"));
                mantenimiento.setFechaDiagnostico(rs.getString("fechaDiagnostico"));
                mantenimiento.setFechaAdquisicion(rs.getString("fechaAdquisicion"));
                mantenimiento.setFechaSevicioExterno(rs.getString("fechaSevicioExterno"));
                mantenimiento.setFechaMantenimientoCorrectivo(rs.getString("fechaMantenimientoCorrectivo"));
                mantenimiento.setFechaVistoBueno(rs.getString("fechaVistoBueno"));
                mantenimiento.setFechaCerrar(rs.getString("fechaCerrar"));
                mantenimiento.setFechaDetencionEquipo(rs.getString("FechaDetencionEquipo"));
                mantenimiento.setFechaHoy(rs.getString("FechaHoy"));
                mantenimiento.setObservacionDiagnostico(rs.getString("observacionDiagnostico"));
                mantenimiento.setObservacionAdquisicion(rs.getString("observacionAdquisicion"));
                mantenimiento.setObservacionSevicioExterno(rs.getString("observacionServicioExterno"));
                mantenimiento.setObservacionMantenimientoCorrectivo(rs.getString("observacionMantenimientoCorrectivo"));
                mantenimiento.setObservacionVistoBueno(rs.getString("observacionVistoBueno"));
                mantenimiento.setObservacionCerrar(rs.getString("observacionCerrar"));
                mantenimiento.setObservacionFalla(rs.getString("ObservacionFalla"));
                mantenimiento.setNombreUsuarioMantenimiento(rs.getString("NombreUsuario"));
                uso = rs.getString("Uso");
                nombre = rs.getString("NombreCorto");
                if (uso == null) {
                    uso = " ";
                }
                if (nombre == null || nombre.equalsIgnoreCase("<NULL>")) {
                    nombre = "";
                }
                String nP = uso + " " + nombre;
                mantenimiento.setNombreMaquina(nP);
                mantenimiento.setNombreFalla(rs.getString("NombreFalla"));
                mantenimiento.setNumeroSap(rs.getString("NSap"));
                mantenimiento.setNoConformidad(rs.getString("noConformidad"));
                mantenimiento.setFechaNoConformidad(rs.getString("fechaNoConformidad"));
                mantenimiento.setObservacionNoConformidad(rs.getString("observacionNoConformidad"));
                mantenimiento.setNombreArea(rs.getString("nombreArea"));
                mantenimiento.setNombreTipoFalla(rs.getString("nombreTFalla"));
                mantenimiento.setDescTipoFalla(rs.getString("descTFalla"));
                mantenimiento.setPersonalMant(new ModPerMant());
                mantenimiento.getPersonalMant().setVc_rut(rs.getString("rutEncargado"));
                mantenimiento.getPersonalMant().setVc_nombre(rs.getString("nombreEncargado"));
                mantenimiento.getPersonalMant().setVc_apellido(rs.getString("apelliEncargado"));
                mantenimiento.getEmpExt().setVc_rut(rs.getString("rutEmpr"));
                mantenimiento.getEmpExt().setVc_nombre(rs.getString("nomEmpr"));
                mantenimiento.getEmpExt().setVc_direccion(rs.getString("vc_direccion"));
                mantenimiento.getEmplEmt().setVc_rut(rs.getString("rutEmpl"));
                mantenimiento.getEmplEmt().setVc_rut_emp(rs.getString("rutEmplEmp"));
                mantenimiento.getEmplEmt().setVc_nombre(rs.getString("nomEmpl"));
                mantenimiento.getEmplEmt().setVc_apellido(rs.getString("vc_apellido"));
                mantenimiento.getTurnoMaq().setIdTurnoMaq(rs.getInt("idTurnoMaq"));
                mantenimiento.getTurnoMaq().setNombreTurnoMaq(rs.getString("nombreTurnoMaq"));
                mantenimiento.getTurnoMaq().setDescTurnoMaq(rs.getString("descTurnoMaq"));
                solicitudes.add(mantenimiento);
                mantenimiento = null;
            }
            conMac.close();
            st.close();
            rs.close();
            rs = null;
            
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarTodosLosPedidosDeMantenimiento()", e.getMessage(), 0);
        }
        return solicitudes;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public int obtenerIdUsuario(String nombreUsuario) {
        int validador = 0;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = " select idUsuario"
                    + " from dbo.TUsuario"
                    + " where login in('" + nombreUsuario + "')";
            rs = st.executeQuery(query);
            if (rs.next()) {
                validador = rs.getInt("idUsuario");
            }
            st.close();
            rs.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("obtenerIdUsuario :", e.getMessage(), 0);
        }
        return validador;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public String obtenernombreUsuario(int idUsuario) {
        String validador = null;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = " select nombre + ' ' + apellido AS nombreCompleto"
                    + " from dbo.TUsuario"
                    + " where idUsuario=" + idUsuario + "";
            rs = st.executeQuery(query);
            if (rs.next()) {
                validador = rs.getString("nombreCompleto");
            }
            st.close();
            rs.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("obtenernombreUsuario :", e.getMessage(), 0);
        }
        return validador;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarTodasLasAreas() {
        ArrayList listadoAreas = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT Codigo, Nombre FROM dbo.TManTipoMantenimiento ORDER BY Nombre";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModArea area = new ModArea();
                area.setId(listadoAreas.size());
                area.setIdArea(rs.getInt("Codigo"));
                area.setNombre(rs.getString("Nombre"));
                listadoAreas.add(area);
                area = null;
            }
            st.close();
            conMac.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            loger.logger("listarTodasLasAreas :", e.getMessage(), 0);
        }
        return listadoAreas;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------//
    public ArrayList solicitudesPorArea(int codigoArea) {
        ArrayList solicitudes = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT a.CodigoEspecialidad, a.CodigoSolicitante, a.CodigoMaquina, a.CodigoFalla, ";
            sql += "a.CodigoMantenimiento, b.fechaDiagnostico, b.fechaAdquisicion, b.fechaSevicioExterno, b.fechaMantenimientoCorrectivo, ";
            sql += "b.fechaVistoBueno, b.fechaCerrar, a.FechaDetencionEquipo, a.FechaEntregaEquipo, a.FechaHoy, b.observacionDiagnostico, b.observacionAdquisicion, ";
            sql += "b.observacionServicioExterno, b.observacionMantenimientoCorrectivo, b.observacionVistoBueno, b.observacionCerrar, ";
            sql += "a.ObservacionFalla,(u.nombre+ ' ' +u.apellido)as NombreUsuario, m.Uso,m.NombreCorto,f.NombreFalla, a.NSap, ";
            sql += "b.noConformidad, b.fechaNoConformidad, b.observacionNoConformidad, tm.Nombre AS NombreArea, ";
            sql += "e.nombre AS nombreTFalla, e.descripcion AS descTFalla, pm.vc_rut AS rutEncargado, pm.vc_nombre AS nombreEncargado, pm.vc_apellido AS apelliEncargado, ";
            sql += "empr.vc_rut AS rutEmpr, empr.vc_nombre AS nomEmpr, empr.vc_direccion, ";
            sql += "empl.vc_rut AS rutEmpl, empl.vc_rut_emp AS rutEmplEmp, empl.vc_nombre AS nomEmpl, empl.vc_apellido, ";
            sql += "turmaq.idTurnoMaq, turmaq.nombreTurnoMaq, turmaq.descTurnoMaq ";
            sql += "FROM dbo.TMantencion a ";
            sql += "LEFT JOIN dbo.TInformacionEstado b ON b.idOM = a.CodigoMantenimiento ";
            sql += "LEFT JOIN TManTipoMantenimiento tm ON a.CodigoEspecialidad = tm.Codigo ";
            sql += "LEFT JOIN dbo.TUsuario u ON u.idUsuario = a.CodigoSolicitante ";
            sql += "LEFT JOIN dbo.TManMaquina m ON m.IdMaquina = a.CodigoMaquina ";
            sql += "LEFT JOIN dbo.TMantTurnoMaq turmaq ON m.idTurnoMaq = turmaq.idTurnoMaq ";
            sql += "LEFT JOIN dbo.TManFalla f ON f.CodigoFalla = a.CodigoFalla ";
            sql += "LEFT JOIN dbo.TManTFalla e ON f.idTipoFalla = e.idTipoFalla ";
            sql += "LEFT JOIN dbo.TMantPerMant pm ON pm.vc_rut = a.RutPerEncargado ";
            sql += "LEFT JOIN dbo.TManEmpExt empr ON a.RutEmprExt = empr.vc_rut ";
            sql += "LEFT JOIN dbo.TManEmplExt empl ON a.RutEmplExt = empl.vc_rut ";
            sql += "WHERE a.CodigoEspecialidad = " + codigoArea + " ";
            sql += "ORDER BY a.CodigoMantenimiento";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                String uso = null;
                String nombre = null;
                Mantenimiento mantenimiento = new Mantenimiento();
                mantenimiento.setCodigoEspecialidad(rs.getInt("CodigoEspecialidad"));
                mantenimiento.setCodigoSolicitante(rs.getInt("CodigoSolicitante"));
                mantenimiento.setCodigoMaquina(rs.getInt("CodigoMaquina"));
                mantenimiento.setCodigoFalla(rs.getInt("CodigoFalla"));
                mantenimiento.setCodigoMantenimiento(rs.getInt("CodigoMantenimiento"));
                mantenimiento.setFechaDiagnostico(rs.getString("fechaDiagnostico"));
                mantenimiento.setFechaAdquisicion(rs.getString("fechaAdquisicion"));
                mantenimiento.setFechaSevicioExterno(rs.getString("fechaSevicioExterno"));
                mantenimiento.setFechaMantenimientoCorrectivo(rs.getString("fechaMantenimientoCorrectivo"));
                mantenimiento.setFechaVistoBueno(rs.getString("fechaVistoBueno"));
                mantenimiento.setFechaCerrar(rs.getString("fechaCerrar"));
                mantenimiento.setFechaDetencionEquipo(rs.getString("FechaDetencionEquipo"));
                mantenimiento.setFechaEntregaEquipo(rs.getString("FechaEntregaEquipo"));
                mantenimiento.setFechaHoy(rs.getString("FechaHoy"));
                mantenimiento.setObservacionDiagnostico(rs.getString("observacionDiagnostico"));
                mantenimiento.setObservacionAdquisicion(rs.getString("observacionAdquisicion"));
                mantenimiento.setObservacionSevicioExterno(rs.getString("observacionServicioExterno"));
                mantenimiento.setObservacionMantenimientoCorrectivo(rs.getString("observacionMantenimientoCorrectivo"));
                mantenimiento.setObservacionVistoBueno(rs.getString("observacionVistoBueno"));
                mantenimiento.setObservacionCerrar(rs.getString("observacionCerrar"));
                mantenimiento.setObservacionFalla(rs.getString("ObservacionFalla"));
                mantenimiento.setNombreUsuarioMantenimiento(rs.getString("NombreUsuario"));
                uso = rs.getString("Uso");
                nombre = rs.getString("NombreCorto");
                if (uso == null) {
                    uso = " ";
                }
                if (nombre == null || nombre.equalsIgnoreCase("<NULL>")) {
                    nombre = " ";
                }
                String nP = uso + " " + nombre;
                mantenimiento.setNombreMaquina(nP);
                mantenimiento.setNombreFalla(rs.getString("NombreFalla"));
                mantenimiento.setNumeroSap(rs.getString("NSap"));
                mantenimiento.setNoConformidad(rs.getString("noConformidad"));
                mantenimiento.setFechaNoConformidad(rs.getString("fechaNoConformidad"));
                mantenimiento.setObservacionNoConformidad(rs.getString("observacionNoConformidad"));
                mantenimiento.setNombreArea(rs.getString("NombreArea"));
                mantenimiento.setNombreTipoFalla(rs.getString("nombreTFalla"));
                mantenimiento.setDescTipoFalla(rs.getString("descTFalla"));
                mantenimiento.setPersonalMant(new ModPerMant());
                mantenimiento.getPersonalMant().setVc_rut(rs.getString("rutEncargado"));
                mantenimiento.getPersonalMant().setVc_nombre(rs.getString("nombreEncargado"));
                mantenimiento.getPersonalMant().setVc_apellido(rs.getString("apelliEncargado"));
                mantenimiento.getEmpExt().setVc_rut(rs.getString("rutEmpr"));
                mantenimiento.getEmpExt().setVc_nombre(rs.getString("nomEmpr"));
                mantenimiento.getEmpExt().setVc_direccion(rs.getString("vc_direccion"));
                mantenimiento.getEmplEmt().setVc_rut(rs.getString("rutEmpl"));
                mantenimiento.getEmplEmt().setVc_rut_emp(rs.getString("rutEmplEmp"));
                mantenimiento.getEmplEmt().setVc_nombre(rs.getString("nomEmpl"));
                mantenimiento.getEmplEmt().setVc_apellido(rs.getString("vc_apellido"));
                mantenimiento.getTurnoMaq().setIdTurnoMaq(rs.getInt("idTurnoMaq"));
                mantenimiento.getTurnoMaq().setNombreTurnoMaq(rs.getString("nombreTurnoMaq"));
                mantenimiento.getTurnoMaq().setDescTurnoMaq(rs.getString("descTurnoMaq"));
                solicitudes.add(mantenimiento);
                mantenimiento = null;
            }
            conMac.close();
            st.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            loger.logger("solicitudesPorArea :", e.getMessage(), 0);
        }
        return solicitudes;
    }
    
    //---------------------------------------------------------------------------------------------------------------------------//
    public ArrayList listarTodosLosUsuarios() {
        ArrayList listadoUsuarios = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT DISTINCT idUsuario, nombre, apellido ";
            sql += "FROM TMantencion INNER JOIN TUsuario ON idUsuario = CodigoSolicitante ORDER BY apellido, nombre";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Trabajo trabajo = new Trabajo();
                trabajo.setIdUsuario(rs.getInt("idUsuario"));
                trabajo.setNombreUsuario(rs.getString("nombre"));
                trabajo.setApellidoUsuario(rs.getString("apellido"));
                listadoUsuarios.add(trabajo);
                trabajo = null;
            }
            conMac.close();
            st.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            loger.logger("listarTodosLosUsuarios :", e.getMessage(), 0);
        }
        return listadoUsuarios;
    }
    
    //------------------------------------------------------------------------------------------------------------------------//
    public ArrayList solicitudesPorUsuario(int idUsuario) {
        ArrayList solicitudes = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT a.CodigoEspecialidad, a.CodigoSolicitante, a.CodigoMaquina, a.CodigoFalla, ";
            sql += "a.CodigoMantenimiento, b.fechaDiagnostico, b.fechaAdquisicion, b.fechaSevicioExterno, b.fechaMantenimientoCorrectivo, ";
            sql += "b.fechaVistoBueno, b.fechaCerrar, a.FechaDetencionEquipo, a.FechaEntregaEquipo, a.FechaHoy, b.observacionDiagnostico, ";
            sql += "b.observacionAdquisicion, b.observacionServicioExterno, b.observacionMantenimientoCorrectivo, b.observacionVistoBueno, ";
            sql += "b.observacionCerrar, a.ObservacionFalla, (u.nombre+ ' ' +u.apellido) as NombreUsuario, m.Uso, m.NombreCorto, f.NombreFalla, a.NSap, ";
            sql += "b.noConformidad, b.fechaNoConformidad, b.observacionNoConformidad, tm.Nombre AS nombreArea, ";
            sql += "e.nombre AS nombreTFalla, e.descripcion AS descTFalla, pm.vc_rut AS rutEncargado, pm.vc_nombre AS nombreEncargado, pm.vc_apellido AS apelliEncargado, ";
            sql += "empr.vc_rut AS rutEmpr, empr.vc_nombre AS nomEmpr, empr.vc_direccion, ";
            sql += "empl.vc_rut AS rutEmpl, empl.vc_rut_emp AS rutEmplEmp, empl.vc_nombre AS nomEmpl, empl.vc_apellido, ";
            sql += "turmaq.idTurnoMaq, turmaq.nombreTurnoMaq, turmaq.descTurnoMaq ";
            sql += "FROM dbo.TMantencion a ";
            sql += "LEFT JOIN  TInformacionEstado b ON b.idOM = a.CodigoMantenimiento ";
            sql += "LEFT JOIN TManTipoMantenimiento tm ON a.CodigoEspecialidad = tm.Codigo ";
            sql += "LEFT JOIN dbo.TUsuario u ON u.idUsuario = a.CodigoSolicitante ";
            sql += "LEFT JOIN dbo.TManMaquina m ON m.IdMaquina = a.CodigoMaquina ";
            sql += "LEFT JOIN dbo.TMantTurnoMaq turmaq ON m.idTurnoMaq = turmaq.idTurnoMaq ";
            sql += "LEFT JOIN dbo.TManFalla f ON f.CodigoFalla = a.CodigoFalla ";
            sql += "LEFT JOIN dbo.TManTFalla e ON f.idTipoFalla = e.idTipoFalla ";
            sql += "LEFT JOIN dbo.TMantPerMant pm ON pm.vc_rut = a.RutPerEncargado ";
            sql += "LEFT JOIN dbo.TManEmpExt empr ON a.RutEmprExt = empr.vc_rut ";
            sql += "LEFT JOIN dbo.TManEmplExt empl ON a.RutEmplExt = empl.vc_rut ";
            sql += "WHERE a.CodigoSolicitante = " + idUsuario + " ";
            sql += "ORDER BY a.CodigoMantenimiento";
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                String uso = null;
                String nombre = null;
                Mantenimiento mantenimiento = new Mantenimiento();
                mantenimiento.setCodigoEspecialidad(rs.getInt("CodigoEspecialidad"));
                mantenimiento.setCodigoSolicitante(rs.getInt("CodigoSolicitante"));
                mantenimiento.setCodigoMaquina(rs.getInt("CodigoMaquina"));
                mantenimiento.setCodigoFalla(rs.getInt("CodigoFalla"));
                mantenimiento.setCodigoMantenimiento(rs.getInt("CodigoMantenimiento"));
                mantenimiento.setFechaDiagnostico(rs.getString("fechaDiagnostico"));
                mantenimiento.setFechaAdquisicion(rs.getString("fechaAdquisicion"));
                mantenimiento.setFechaSevicioExterno(rs.getString("fechaSevicioExterno"));
                mantenimiento.setFechaMantenimientoCorrectivo(rs.getString("fechaMantenimientoCorrectivo"));
                mantenimiento.setFechaVistoBueno(rs.getString("fechaVistoBueno"));
                mantenimiento.setFechaCerrar(rs.getString("fechaCerrar"));
                mantenimiento.setFechaDetencionEquipo(rs.getString("FechaDetencionEquipo"));
                mantenimiento.setFechaEntregaEquipo(rs.getString("FechaEntregaEquipo"));
                mantenimiento.setFechaHoy(rs.getString("FechaHoy"));
                mantenimiento.setObservacionDiagnostico(rs.getString("observacionDiagnostico"));
                mantenimiento.setObservacionAdquisicion(rs.getString("observacionAdquisicion"));
                mantenimiento.setObservacionSevicioExterno(rs.getString("observacionServicioExterno"));
                mantenimiento.setObservacionMantenimientoCorrectivo(rs.getString("observacionMantenimientoCorrectivo"));
                mantenimiento.setObservacionVistoBueno(rs.getString("observacionVistoBueno"));
                mantenimiento.setObservacionCerrar(rs.getString("observacionCerrar"));
                mantenimiento.setObservacionFalla(rs.getString("ObservacionFalla"));
                mantenimiento.setNombreUsuarioMantenimiento(rs.getString("NombreUsuario"));
                uso = rs.getString("Uso");
                nombre = rs.getString("NombreCorto");
                if (uso == null) {
                    uso = " ";
                }
                if (nombre == null || nombre.equalsIgnoreCase("<NULL>")) {
                    nombre = " ";
                }
                String nP = uso + " " + nombre;
                mantenimiento.setNombreMaquina(nP);
                mantenimiento.setNombreFalla(rs.getString("NombreFalla"));
                mantenimiento.setNumeroSap(rs.getString("NSap"));
                mantenimiento.setNoConformidad(rs.getString("noConformidad"));
                mantenimiento.setFechaNoConformidad(rs.getString("fechaNoConformidad"));
                mantenimiento.setObservacionNoConformidad(rs.getString("observacionNoConformidad"));
                mantenimiento.setNombreArea(rs.getString("nombreArea"));
                mantenimiento.setNombreTipoFalla(rs.getString("nombreTFalla"));
                mantenimiento.setDescTipoFalla(rs.getString("descTFalla"));
                mantenimiento.setPersonalMant(new ModPerMant());
                mantenimiento.getPersonalMant().setVc_rut(rs.getString("rutEncargado"));
                mantenimiento.getPersonalMant().setVc_nombre(rs.getString("nombreEncargado"));
                mantenimiento.getPersonalMant().setVc_apellido(rs.getString("apelliEncargado"));
                mantenimiento.getEmpExt().setVc_rut(rs.getString("rutEmpr"));
                mantenimiento.getEmpExt().setVc_nombre(rs.getString("nomEmpr"));
                mantenimiento.getEmpExt().setVc_direccion(rs.getString("vc_direccion"));
                mantenimiento.getEmplEmt().setVc_rut(rs.getString("rutEmpl"));
                mantenimiento.getEmplEmt().setVc_rut_emp(rs.getString("rutEmplEmp"));
                mantenimiento.getEmplEmt().setVc_nombre(rs.getString("nomEmpl"));
                mantenimiento.getEmplEmt().setVc_apellido(rs.getString("vc_apellido"));
                mantenimiento.getTurnoMaq().setIdTurnoMaq(rs.getInt("idTurnoMaq"));
                mantenimiento.getTurnoMaq().setNombreTurnoMaq(rs.getString("nombreTurnoMaq"));
                mantenimiento.getTurnoMaq().setDescTurnoMaq(rs.getString("descTurnoMaq"));
                solicitudes.add(mantenimiento);
                mantenimiento = null;
            }
            st.close();
            rs.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" solicitudesPorUsuario()", e.getMessage(), 0);
        }
        return solicitudes;
    }
  /* //------------------------------------------------------------------------------------------------------------------------//
    public ArrayList solicitudesPreventivas() {
        ArrayList solicitudes = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT a.CodigoEspecialidad, a.CodigoSolicitante, a.CodigoMaquina, a.CodigoFalla, ";
            sql += "a.CodigoMantenimiento, b.fechaDiagnostico, b.fechaAdquisicion, b.fechaSevicioExterno, b.fechaMantenimientoCorrectivo, ";
            sql += "b.fechaVistoBueno, b.fechaCerrar, a.FechaDetencionEquipo, a.FechaEntregaEquipo, a.FechaHoy, b.observacionDiagnostico, ";
            sql += "b.observacionAdquisicion, b.observacionServicioExterno, b.observacionMantenimientoCorrectivo, b.observacionVistoBueno, ";
            sql += "b.observacionCerrar, a.ObservacionFalla, (u.nombre+ ' ' +u.apellido) as NombreUsuario, m.Uso, m.NombreCorto, f.NombreFalla, a.NSap, ";
            sql += "b.noConformidad, b.fechaNoConformidad, b.observacionNoConformidad, tm.Nombre AS nombreArea, ";
            sql += "e.nombre AS nombreTFalla, e.descripcion AS descTFalla, pm.vc_rut AS rutEncargado, pm.vc_nombre AS nombreEncargado, pm.vc_apellido AS apelliEncargado, ";
            sql += "empr.vc_rut AS rutEmpr, empr.vc_nombre AS nomEmpr, empr.vc_direccion, ";
            sql += "empl.vc_rut AS rutEmpl, empl.vc_rut_emp AS rutEmplEmp, empl.vc_nombre AS nomEmpl, empl.vc_apellido, ";
            sql += "turmaq.idTurnoMaq, turmaq.nombreTurnoMaq, turmaq.descTurnoMaq,a.Tipo as tipo, ";
            sql += "( ";
            sql += "select sum(isnull(MVD_SubTotal,0)) ";
            sql += "from SiaSchaffner.dbo.STO_MOVDET ";
            sql += "where MVD_CodSistema=7 and ";
            sql += "MVD_CodClase =1 AND ";
            sql += "MVD_TipoDoc = 1 and ";
            sql += "MVD_NumeroDocOrigen in(select numSap from dbo.TordenMantencion_sap where ordenMantencion = a.CodigoMantenimiento) ";
            sql += ") as costo ";
            sql += "FROM dbo.TMantencion a ";
            sql += "LEFT JOIN  TInformacionEstado b ON b.idOM = a.CodigoMantenimiento ";
            sql += "LEFT JOIN TManTipoMantenimiento tm ON a.CodigoEspecialidad = tm.Codigo ";
            sql += "LEFT JOIN dbo.TUsuario u ON u.idUsuario = a.CodigoSolicitante ";
            sql += "LEFT JOIN dbo.TManMaquina m ON m.IdMaquina = a.CodigoMaquina ";
            sql += "LEFT JOIN dbo.TMantTurnoMaq turmaq ON m.idTurnoMaq = turmaq.idTurnoMaq ";
            sql += "LEFT JOIN dbo.TManFalla f ON f.CodigoFalla = a.CodigoFalla ";
            sql += "LEFT JOIN dbo.TManTFalla e ON f.idTipoFalla = e.idTipoFalla ";
            sql += "LEFT JOIN dbo.TMantPerMant pm ON pm.vc_rut = a.RutPerEncargado ";
            sql += "LEFT JOIN dbo.TManEmpExt empr ON a.RutEmprExt = empr.vc_rut ";
            sql += "LEFT JOIN dbo.TManEmplExt empl ON a.RutEmplExt = empl.vc_rut ";
            sql += "WHERE a.Tipo = '" + tipo + "' ";
            sql += "ORDER BY a.CodigoMantenimiento";
   
            rs = st.executeQuery(sql);
            while (rs.next()) {
                String uso = null;
                String nombre = null;
                Mantenimiento mantenimiento = new Mantenimiento();
                mantenimiento.setCodigoEspecialidad(rs.getInt("CodigoEspecialidad"));
                mantenimiento.setCodigoSolicitante(rs.getInt("CodigoSolicitante"));
                mantenimiento.setCodigoMaquina(rs.getInt("CodigoMaquina"));
                mantenimiento.setCodigoFalla(rs.getInt("CodigoFalla"));
                mantenimiento.setCodigoMantenimiento(rs.getInt("CodigoMantenimiento"));
                mantenimiento.setFechaDiagnostico(rs.getString("fechaDiagnostico"));
                mantenimiento.setFechaAdquisicion(rs.getString("fechaAdquisicion"));
                mantenimiento.setFechaSevicioExterno(rs.getString("fechaSevicioExterno"));
                mantenimiento.setFechaMantenimientoCorrectivo(rs.getString("fechaMantenimientoCorrectivo"));
                mantenimiento.setFechaVistoBueno(rs.getString("fechaVistoBueno"));
                mantenimiento.setFechaCerrar(rs.getString("fechaCerrar"));
                mantenimiento.setFechaDetencionEquipo(rs.getString("FechaDetencionEquipo"));
                mantenimiento.setFechaEntregaEquipo(rs.getString("FechaEntregaEquipo"));
                mantenimiento.setFechaHoy(rs.getString("FechaHoy"));
                mantenimiento.setObservacionDiagnostico(rs.getString("observacionDiagnostico"));
                mantenimiento.setObservacionAdquisicion(rs.getString("observacionAdquisicion"));
                mantenimiento.setObservacionSevicioExterno(rs.getString("observacionServicioExterno"));
                mantenimiento.setObservacionMantenimientoCorrectivo(rs.getString("observacionMantenimientoCorrectivo"));
                mantenimiento.setObservacionVistoBueno(rs.getString("observacionVistoBueno"));
                mantenimiento.setObservacionCerrar(rs.getString("observacionCerrar"));
                mantenimiento.setObservacionFalla(rs.getString("ObservacionFalla"));
                mantenimiento.setNombreUsuarioMantenimiento(rs.getString("NombreUsuario"));
                mantenimiento.setTipo(rs.getString("tipo"));
                mantenimiento.setCosto(rs.getFloat("costo"));
                uso = rs.getString("Uso");
                nombre = rs.getString("NombreCorto");
                if (uso == null) {
                    uso = " ";
                }
                if (nombre == null || nombre.equalsIgnoreCase("<NULL>")) {
                    nombre = " ";
                }
                String nP = uso + " " + nombre;
                mantenimiento.setNombreMaquina(nP);
                mantenimiento.setNombreFalla(rs.getString("NombreFalla"));
                mantenimiento.setNumeroSap(rs.getString("NSap"));
                mantenimiento.setNoConformidad(rs.getString("noConformidad"));
                mantenimiento.setFechaNoConformidad(rs.getString("fechaNoConformidad"));
                mantenimiento.setObservacionNoConformidad(rs.getString("observacionNoConformidad"));
                mantenimiento.setNombreArea(rs.getString("nombreArea"));
                mantenimiento.setNombreTipoFalla(rs.getString("nombreTFalla"));
                mantenimiento.setDescTipoFalla(rs.getString("descTFalla"));
                mantenimiento.setPersonalMant(new ModPerMant());
                mantenimiento.getPersonalMant().setVc_rut(rs.getString("rutEncargado"));
                mantenimiento.getPersonalMant().setVc_nombre(rs.getString("nombreEncargado"));
                mantenimiento.getPersonalMant().setVc_apellido(rs.getString("apelliEncargado"));
                mantenimiento.getEmpExt().setVc_rut(rs.getString("rutEmpr"));
                mantenimiento.getEmpExt().setVc_nombre(rs.getString("nomEmpr"));
                mantenimiento.getEmpExt().setVc_direccion(rs.getString("vc_direccion"));
                mantenimiento.getEmplEmt().setVc_rut(rs.getString("rutEmpl"));
                mantenimiento.getEmplEmt().setVc_rut_emp(rs.getString("rutEmplEmp"));
                mantenimiento.getEmplEmt().setVc_nombre(rs.getString("nomEmpl"));
                mantenimiento.getEmplEmt().setVc_apellido(rs.getString("vc_apellido"));
                mantenimiento.getTurnoMaq().setIdTurnoMaq(rs.getInt("idTurnoMaq"));
                mantenimiento.getTurnoMaq().setNombreTurnoMaq(rs.getString("nombreTurnoMaq"));
                mantenimiento.getTurnoMaq().setDescTurnoMaq(rs.getString("descTurnoMaq"));
                solicitudes.add(mantenimiento);
                mantenimiento = null;
            }
            st.close();
            rs.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("solicitudesPorUsuario :", e.getMessage(), 0);
        }
        return solicitudes;
    }  */
    
    //----------------------------------------------------------------------------------------------------------------------------//
    public ArrayList listarTodosLosEstados() {
        ArrayList listarEstado = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = " SELECT idEstado, estado FROM TEstado";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Trabajo listarPorEstado = new Trabajo();
                listarPorEstado.setIdEstado(rs.getInt("idEstado"));
                listarPorEstado.setNombreEstado(rs.getString("estado"));
                listarEstado.add(listarPorEstado);
            }
            st.close();
            rs.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("listarTodosLosEstados :", e.getMessage(), 0);
        }
        return listarEstado;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------//
    public boolean insertarNuevoEstado(int idOrden, int idEstado, String fecha, String observacion) {
        boolean estado = false;
        String sql = null;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            
            String fechaEstado;
            String observacionEstado;
            String nombreEstado;
            
            if (idEstado == 1) {
                nombreEstado = "diagnostico";
                fechaEstado = "fechaDiagnostico";
                observacionEstado = "observacionDiagnostico";
            } else if (idEstado == 2) {
                nombreEstado = "adquisicion";
                fechaEstado = "fechaAdquisicion";
                observacionEstado = "observacionAdquisicion";
            } else if (idEstado == 3) {
                nombreEstado = "servicioExterno";
                fechaEstado = "fechaSevicioExterno";
                observacionEstado = "observacionServicioExterno";
            } else if (idEstado == 4) {
                nombreEstado = "mantenimientoCorrectivo";
                fechaEstado = "fechaMantenimientoCorrectivo";
                observacionEstado = "observacionMantenimientoCorrectivo";
            } else if (idEstado == 5) {
                nombreEstado = "vistoBueno";
                fechaEstado = "fechaVistoBueno";
                observacionEstado = "observacionVistoBueno";
            } else if (idEstado == 6) {
                nombreEstado = "noConformidad";
                fechaEstado = "fechaNoConformidad";
                observacionEstado = "observacionNoConformidad";
            } else {
                nombreEstado = "";
                fechaEstado = "";
                observacionEstado = "";
            }
            
            sql = "SELECT idOM FROM TInformacionEstado WHERE idOM = " + idOrden;
            rs = st.executeQuery(sql);
            
            if (!rs.next()) {
                sql = "INSERT INTO dbo.TInformacionEstado (idOM, " + nombreEstado + ", " + fechaEstado + ", " + observacionEstado + ") "
                        + " VALUES (" + idOrden + ", '" + nombreEstado + "', '" + fecha + "', '" + observacion + "')";
                if (st.executeUpdate(sql) > 0) {
                    estado = true;
                }
            } else {
                sql = " UPDATE TInformacionEstado SET " + nombreEstado + " = '" + nombreEstado + "' , " + fechaEstado + " = '" + fecha + "', " + observacionEstado + " = '" + observacion + "'"
                        + " WHERE idOM = " + idOrden + "";
                if (st.executeUpdate(sql) > 0) {
                    estado = true;
                }
            }
            
            st.close();
            conMac.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
                rs.close();
                rs = null;
            } catch (SQLException ex) {
                loger.logger("insertarNuevoEstado :", ex.getMessage(), 0);
            }
            loger.logger("insertarNuevoEstado :", e.getMessage(), 0);
        }
        return estado;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------//
    public boolean updateSolicitud(int CodigoMantenimiento, String nSap, String rut_encargado) {
        return updateSolicitud(CodigoMantenimiento, nSap, rut_encargado, null);
    }
    
    public boolean updateSolicitud(int CodigoMantenimiento, String nSap, String rut_encargado, String fechaEntrega) {
        return updateSolicitud(CodigoMantenimiento, nSap, rut_encargado, fechaEntrega, null, null);
    }
    
    public boolean updateSolicitud(int CodigoMantenimiento, String nSap, String rut_encargado, String fechaEntrega, String rutEmpr, String rutEmpl) {
        boolean estado = false;
        String sql = null;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            
            sql = " UPDATE TMantencion SET NSap = '" + nSap + "', RutPerEncargado = '" + rut_encargado + "' ";
            if (fechaEntrega != null) {
                sql += ", FechaEntregaEquipo = '" + fechaEntrega + "' ";
            }
            if (rutEmpr != null) {
                sql += ", RutEmprExt = '" + rutEmpr + "' ";
            }
            if (rutEmpl != null) {
                sql += ", RutEmplExt = '" + rutEmpl + "' ";
            }
            sql += " WHERE CodigoMantenimiento = " + CodigoMantenimiento;
            if (st.executeUpdate(sql) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("updateSolicitud :", ex.getMessage(), 0);
            }
            loger.logger("updateSolicitud :", e.getMessage(), 0);
            
        }
        return estado;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------//
    public boolean insertarNuevoFallo(ModFalla falla) {
        boolean estado = false;
        falla.setIdFalla(obtenerUltimoCodigoFalla() + 1);
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "INSERT INTO dbo.TManFalla(CodigoFalla, NombreFalla, IdentificadorFalla, DescFalla, idTipoFalla) ";
            query += "VALUES (" + falla.getIdFalla() + ", '" + falla.getNombre() + "', '" + falla.getIdMaquina() + "', '" + falla.getDescripcion() + "', " + falla.getTipoFalla().getIdTipoFalla() + ") ";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("insertarNuevoFallo :", ex.getMessage(), 0);
            }
            loger.logger("insertarNuevoFallo :", e.getMessage(), 0);
        }
        return estado;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------//
    public boolean insertarNuevoTipoFallo(ModTipoFalla tipoFalla) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "INSERT INTO dbo.TManTFalla(nombre, descripcion) ";
            query += "VALUES ('" + tipoFalla.getNombre() + "', '" + tipoFalla.getDescripcion() + "') ";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("insertarNuevoTipoFallo :", ex.getMessage(), 0);
            }
            loger.logger("insertarNuevoTipoFallo :", e.getMessage(), 0);
        }
        return estado;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------//
    public boolean insertarNuevoEmpExt(ModEmpExt empExt) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "INSERT INTO dbo.TManEmpExt(vc_rut, vc_nombre, vc_direccion) ";
            query += "VALUES ('" + empExt.getVc_rut() + "', '" + empExt.getVc_nombre() + "', '" + empExt.getVc_direccion() + "') ";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("insertarNuevoEmpExt :", ex.getMessage(), 0);
            }
            loger.logger("insertarNuevoEmpExt :", e.getMessage(), 0);
        }
        return estado;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------//
    public boolean insertarNuevoTurnoMaquina(ModTurnoMaq turnoMaq) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "INSERT INTO dbo.TMantTurnoMaq(nombreTurnoMaq, descTurnoMaq) ";
            query += "VALUES ('" + turnoMaq.getNombreTurnoMaq() + "', '" + turnoMaq.getDescTurnoMaq() + "') ";
            
            if (st.executeUpdate(query) > 0) {
                estado = insertarHorarios(turnoMaq);
            }
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("insertarNuevoTurnoMaquina :", ex.getMessage(), 0);
            }
            loger.logger("insertarNuevoTurnoMaquina :", e.getMessage(), 0);
        }
        return estado;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------//
    private boolean insertarHorarios(ModTurnoMaq turnoMaq) {
        return insertarHorarios(turnoMaq, true);
    }
    
    private boolean insertarHorarios(ModTurnoMaq turnoMaq, boolean nuevo) {
        boolean estado = false;
        try {
            int idTurnoMaq = 0;
            if (nuevo) {
                idTurnoMaq = obtenerUltCodTurnoMaquina();
            } else {
                idTurnoMaq = turnoMaq.getIdTurnoMaq();
            }
            conMacAux = mConexionAux.getMACConnector();
            stAux = conMacAux.createStatement();
            String query = "";
            for (int i = 0; i < turnoMaq.getHorarios().size(); i++) {
                ModHorario horario = (ModHorario) turnoMaq.getHorarios().get(i);
                query = "INSERT INTO dbo.TMantHorario(idTurnoMaq, horaInicio, horaTermino, diaHorario, estado) ";
                query += "VALUES (" + idTurnoMaq + ", '" + horario.getHoraInicio() + "', '";
                if (horario.isEstado()) {
                    query += horario.getHoraTermino() + "', '" + horario.getDiaHorario() + "', 1) ";
                } else {
                    query += horario.getHoraTermino() + "', '" + horario.getDiaHorario() + "', 0) ";
                }
                if (stAux.executeUpdate(query) > 0) {
                    estado = true;
                } else {
                    estado = false;
                    break;
                }
            }
            stAux.close();
            conMacAux.close();
        } catch (SQLException e) {
            estado = false;
            try {
                stAux.close();
                conMacAux.close();
            } catch (SQLException ex) {
                loger.logger("insertarHorarios :", ex.getMessage(), 0);
            }
            loger.logger("insertarHorarios :", e.getMessage(), 0);
        }
        return estado;
    }
    
    private int obtenerUltCodTurnoMaquina() {
        int codigo = 0;
        try {
            conMacAux = mConexionAux.getMACConnector();
            stAux = conMacAux.createStatement();
            String sql = " SELECT MAX(idTurnoMaq) FROM dbo.TMantTurnoMaq";
            rsAux = stAux.executeQuery(sql);
            if (rsAux.next()) {
                codigo = rsAux.getInt(1);
            }
            stAux.close();
            conMacAux.close();
            rsAux.close();
            rsAux = null;
        } catch (Exception e) {
            try {
                stAux.close();
                conMacAux.close();
                rsAux.close();
                rsAux = null;
            } catch (SQLException ex) {
                loger.logger("obtenerUltCodTurnoMaquina :", ex.getMessage(), 0);
            }
            loger.logger("obtenerUltCodTurnoMaquina :", e.getMessage(), 0);
        }
        return codigo;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------//
    public boolean insertarNuevoPerMant(ModPerMant personal) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "INSERT INTO dbo.TMantPerMant(vc_rut, vc_nombre, vc_apellido) ";
            query += "VALUES ('" + personal.getVc_rut() + "', '" + personal.getVc_nombre() + "', '" + personal.getVc_apellido() + "') ";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("insertarNuevoPerMant :", ex.getMessage(), 0);
            }
            loger.logger("insertarNuevoPerMant :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean insertarNuevoEmplExt(ModEmplExt empleado) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "INSERT INTO dbo.TManEmplExt(vc_rut, vc_rut_emp, vc_nombre, vc_apellido) ";
            query += "VALUES ('" + empleado.getVc_rut() + "', '" + empleado.getVc_rut_emp() + "', '" + empleado.getVc_nombre() + "', '" + empleado.getVc_apellido() + "') ";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("insertarNuevoEmplExt :", ex.getMessage(), 0);
            }
            loger.logger("insertarNuevoEmplExt :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean tipoDeFallaAsociado(int idTipoFalla) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT * FROM dbo.TManFalla WHERE idTipoFalla = " + idTipoFalla;
            rs = st.executeQuery(sql);
            if (rs.next()) {
                estado = true;
            }
            st.close();
            conMac.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            try {
                st.close();
                conMac.close();
                rs.close();
                rs = null;
            } catch (SQLException ex) {
                loger.logger("tipoDeFallaAsociado :", ex.getMessage(), 0);
            }
            loger.logger("tipoDeFallaAsociado :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean insertarNuevaMaquina(int idArea, String Uso, String nomMaquina, int idTurnoMaq) {
        boolean estado = false;
        int codigoMaquina = obtenerUltimoCodigoMaquina() + 1;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "INSERT INTO dbo.TManMaquina (area, IdMaquina, Uso, NombreCorto, idTurnoMaq) ";
            query += "VALUES(" + idArea + ", " + codigoMaquina + ", '" + Uso + "', '" + nomMaquina + "', " + idTurnoMaq + ") ";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("insertarNuevaMaquina :", ex.getMessage(), 0);
            }
            loger.logger("insertarNuevaMaquina :", e.getMessage(), 0);
        }
        return estado;
    }
    
    private int obtenerUltimoCodigoFalla() {
        int codigo = 0;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = " select max(CodigoFalla)";
            sql += " from dbo.TManFalla";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                codigo = rs.getInt(1);
            }
            
            st.close();
            conMac.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            try {
                st.close();
                conMac.close();
                rs.close();
                rs = null;
            } catch (SQLException ex) {
                loger.logger("obtenerUltimoCodigoFalla :", ex.getMessage(), 0);
            }
            loger.logger("obtenerUltimoCodigoFalla :", e.getMessage(), 0);
        }
        return codigo;
    }
    
    private int obtenerUltimoCodigoMaquina() {
        int codigo = 0;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql = "SELECT MAX(IdMaquina) ";
            sql += "FROM dbo.TManMaquina";
            rs = st.executeQuery(sql);
            if (rs.next()) {
                codigo = rs.getInt(1);
            }
            
            st.close();
            conMac.close();
            rs.close();
            rs = null;
        } catch (Exception e) {
            try {
                st.close();
                conMac.close();
                rs.close();
                rs = null;
            } catch (SQLException ex) {
                loger.logger("obtenerUltimoCodigoMaquina :", ex.getMessage(), 0);
            }
            loger.logger("obtenerUltimoCodigoMaquina :", e.getMessage(), 0);
        }
        return codigo;
    }
    
    public boolean actualizarFalla(ModFalla falla) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "UPDATE dbo.TManFalla SET NombreFalla = '" + falla.getNombre() + "', ";
            query += "DescFalla = '" + falla.getDescripcion() + "', ";
            query += "idTipoFalla = " + falla.getTipoFalla().getIdTipoFalla() + " ";
            query += "WHERE CodigoFalla = " + falla.getIdFalla();
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("actualizarFalla :", ex.getMessage(), 0);
            }
            loger.logger("actualizarFalla :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean actualizarTipoFalla(ModTipoFalla tipoFalla) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "UPDATE dbo.TManTFalla SET nombre = '" + tipoFalla.getNombre() + "' , ";
            query += "descripcion = '" + tipoFalla.getDescripcion() + "' WHERE idTipoFalla = " + tipoFalla.getIdTipoFalla();
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("actualizarTipoFalla :", ex.getMessage(), 0);
            }
            loger.logger("actualizarTipoFalla :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean actualizarEmpExt(ModEmpExt empExt) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "UPDATE dbo.TManEmpExt SET vc_nombre = '" + empExt.getVc_nombre() + "' , ";
            query += "vc_direccion = '" + empExt.getVc_direccion() + "' WHERE vc_rut = '" + empExt.getVc_rut() + "'";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("actualizarEmpExt :", ex.getMessage(), 0);
            }
            loger.logger("actualizarEmpExt :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean actualizarPerMant(ModPerMant personal) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "UPDATE dbo.TMantPerMant SET vc_nombre = '" + personal.getVc_nombre() + "' , ";
            query += "vc_apellido = '" + personal.getVc_apellido() + "' WHERE vc_rut = '" + personal.getVc_rut() + "'";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("actualizarPerMant :", ex.getMessage(), 0);
            }
            loger.logger("actualizarPerMant :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean actualizarEmplExt(ModEmplExt empleado) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "UPDATE dbo.TManEmplExt SET vc_nombre = '" + empleado.getVc_nombre() + "' , vc_apellido = '" + empleado.getVc_apellido() + "' ";
            query += "WHERE vc_rut = '" + empleado.getVc_rut() + "' AND vc_rut_emp = '" + empleado.getVc_rut_emp() + "'";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("actualizarEmplExt :", ex.getMessage(), 0);
            }
            loger.logger("actualizarEmplExt :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean actualizarMaquina(int idArea, int idMaquina, String usoMaquina, String nombreMaquina, int idTurnoMaq) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "UPDATE dbo.TManMaquina SET Uso = '" + usoMaquina + "', NombreCorto = '" + nombreMaquina + "', idTurnoMaq = " + idTurnoMaq + " ";
            query += "WHERE area = " + idArea + " AND IdMaquina = " + idMaquina;
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("actualizarMaquina :", ex.getMessage(), 0);
            }
            loger.logger("actualizarMaquina :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean actualizarTurnoMaquina(ModTurnoMaq turnoMaq) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "UPDATE dbo.TMantTurnoMaq SET nombreTurnoMaq = '" + turnoMaq.getNombreTurnoMaq() + "' , ";
            query += "descTurnoMaq = '" + turnoMaq.getDescTurnoMaq() + "' WHERE idTurnoMaq = '" + turnoMaq.getIdTurnoMaq() + "'";
            
            if (st.executeUpdate(query) > 0) {
                borrarHorariosTurno(turnoMaq.getIdTurnoMaq());
                estado = insertarHorarios(turnoMaq, false);
            }
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("actualizarTurnoMaquina :", ex.getMessage(), 0);
            }
            loger.logger("actualizarTurnoMaquina :", e.getMessage(), 0);
        }
        return estado;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------//
    private boolean borrarHorariosTurno(int idTurnoMaq) {
        boolean estado = false;
        try {
            conMacAux = mConexionAux.getMACConnector();
            stAux = conMacAux.createStatement();
            String query = "";
            query = "DELETE FROM dbo.TMantHorario WHERE idTurnoMaq = " + idTurnoMaq;
            if (stAux.executeUpdate(query) > 0) {
                estado = true;
            }else{
                estado = true;
            }
            stAux.close();
            conMacAux.close();
        } catch (Exception e) {
            estado = false;
            try {
                stAux.close();
                conMacAux.close();
            } catch (SQLException ex) {
                loger.logger("borrarHorariosTurno :", ex.getMessage(), 0);
            }
            loger.logger("borrarHorariosTurno :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean eliminarFalla(int idFalla) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "DELETE FROM dbo.TManFalla WHERE CodigoFalla = " + idFalla;
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("eliminarFalla :", ex.getMessage(), 0);
            }
            loger.logger("eliminarFalla :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean eliminarTipoFalla(int idTipoFalla) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "DELETE FROM dbo.TManTFalla WHERE idTipoFalla = " + idTipoFalla;
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("eliminarTipoFalla :", ex.getMessage(), 0);
            }
            loger.logger("eliminarTipoFalla :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean eliminarEmpExt(String vc_rut) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "DELETE FROM dbo.TManEmpExt WHERE vc_rut = '" + vc_rut + "'";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("eliminarEmpExt :", ex.getMessage(), 0);
            }
            loger.logger("eliminarEmpExt :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean eliminarTurnoMaq(int idTurnoMaq) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "DELETE FROM dbo.TMantTurnoMaq WHERE idTurnoMaq = " + idTurnoMaq;
            
            if (borrarHorariosTurno(idTurnoMaq) && st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("eliminarTurnoMaq :", ex.getMessage(), 0);
            }
            loger.logger("eliminarTurnoMaq :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean eliminarPerMant(String vc_rut) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "DELETE FROM dbo.TMantPerMant WHERE vc_rut = '" + vc_rut + "'";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("eliminarPerMant :", ex.getMessage(), 0);
            }
            loger.logger("eliminarPerMant :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean eliminarEmplExt(String vc_rut_empl, String vc_rut_empr) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "DELETE FROM dbo.TManEmplExt WHERE vc_rut = '" + vc_rut_empl + "' AND vc_rut_emp = '" + vc_rut_empr + "'";
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("eliminarEmplExt :", ex.getMessage(), 0);
            }
            loger.logger("eliminarEmplExt :", e.getMessage(), 0);
        }
        return estado;
    }
    
    public boolean eliminarMaquina(int idArea, int idMaquina) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "DELETE FROM MAC.dbo.TManMaquina WHERE area = " + idArea + " AND IdMaquina = " + idMaquina;
            
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("eliminarMaquina :", ex.getMessage(), 0);
            }
            loger.logger("eliminarMaquina :", e.getMessage(), 0);
        }
        return estado;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public boolean insertarMantenimiento(int especialidad, int idUsuario, String idMaquina, int idFalla, String observacion,
            String fechaDetencionEquipo, String fechaHoy, String tipoFalla) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "INSERT INTO dbo.TMantencion (CodigoEspecialidad, CodigoSolicitante, CodigoMaquina, CodigoFalla, ObservacionFalla, ";
            query += "FechaDetencionEquipo, Activa, Recepcionar, FechaHoy, TipoFalla) VALUES (" + especialidad + ", " + idUsuario + ", '" + idMaquina + "',";
            query += idFalla + ", '" + observacion + "', '" + fechaDetencionEquipo + "',1 , 0, '" + fechaHoy + "', '" + tipoFalla + "')";
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            
            st.close();
            conMac.close();
        } catch (Exception e) {
            estado = false;
            try {
                st.close();
                conMac.close();
            } catch (SQLException ex) {
                loger.logger("insertarMantenimiento :", ex.getMessage(), 0);
            }
            loger.logger("insertarMantenimiento :", e.getMessage(), 0);
        }
        return estado;
    }
    public boolean validarUsuarioALink(String usuario,String nombre){
        boolean valUsuario = false;
        try{
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            
            String query = "  select Nombre from dbo.LG_permiso_a_link where Usuario = '" + usuario.trim() + "'";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                if(rs.getString(1).equals(nombre)){
                    valUsuario = true;
                    break;
                }
            }
            
            st.close();
            conMac.close();
        }catch(Exception e){
            loger.logger("validarUsuarioALink :", e.getMessage(), 0);
        }
        return valUsuario;
    }
    //-------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarTareasNoHechas(int id) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select distinct(pt.tareas) as tareas ,tt.descripcion as descripcion,pt.id as id";
            sql += " from MAC.dbo.PRO_programa_man_pre pm";
            sql += " inner join MAC.dbo.PRO_maquina_tarea mt";
            sql += " on(pm.id_maquina_tarea = mt.id)";
            sql += " inner join MAC.dbo.PRO_tareas pt";
            sql += " on(mt.id_tarea = pt.id)";
            sql += " left outer join MAC.dbo.PRO_programa_man_pre_eje ppe";
            sql += " on(pm.id = ppe.id_man_preventiva and mt.id_tarea = ppe.id_tarea)";
            sql += " inner join MAC.dbo.PRO_tipo_tarea tt";
            sql += " on(pt.id_tipo_tarea = tt.id)";
            sql += " where pm.id = "+id+" and ppe.id_tarea is null";
            
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento mante = new Mantenimiento();
                mante.setId_tareas(rs.getInt("id"));
                mante.setNombreArea(rs.getString("tareas"));
                mante.setComentario(rs.getString("descripcion"));
                vec.add(mante);
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarTareasNoHechas() ", e.getMessage(), 0);
        }
        return vec;
    }
    //--------------------------------------------------------------------------------------------------------------------------//
    public boolean insertarTareaHecha(int id_man_preventiva,int id_tarea) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "insert MAC.dbo.PRO_programa_man_pre_eje (id_man_preventiva,id_tarea) values ("+id_man_preventiva+","+id_tarea+")";
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            st.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" insertarTareaHecha() ", e.getMessage(), 0);
        }
        return estado;
    }
    
    //--------------------------------------------------------------------------------------------------------------------------//
    public boolean actualizarMantencionPreventiva(int id, String fechaIni, String fechaFin, String ejecutor, String observacion, boolean est) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "";
            if(est){
                query = "update MAC.dbo.PRO_mantencion_preventiva set fechaInicio = '"+fechaIni+"' ,fechaFin = '"+fechaFin+"' ,ejecutor1 = '"+ejecutor+"' ,observacion = '"+observacion+"' where numero ="+id;
            }else{
                query = "update MAC.dbo.PRO_mantencion_preventiva set fechaInicio = '"+fechaIni+"' ,ejecutor1 = '"+ejecutor+"' ,observacion = '"+observacion+"' where numero ="+id;
            }
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            st.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" actualizarMantencionPreventiva() ", e.getMessage(), 0);
        }
        return estado;
    }
    //--------------------------------------------------------------------------------------------------------------------------//
    public boolean actualizarPrograma(int id) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String query = "update MAC.dbo.PRO_programa_man_pre set ejecucion = 'S' where id ="+id;
            System.out.println(query);
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            st.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" actualizarPrograma() ", e.getMessage(), 0);
        }
        return estado;
    }
//---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList programaPreventivoHoras() {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            //String sql  = " select IdMaquina,NombreCorto,Uso,sum(hora) as horas,sum(presupuesto) as presupuesto,fecha,mp.fechaInicio,mp.fechaFin";
            String sql  = " select IdMaquina,sum(hora) as horas,sum(presupuesto) as presupuesto,fecha,mp.fechaInicio,mp.fechaFin";
            sql += " FROM MAC.dbo.TManMaquina";
            sql += " inner join MAC.dbo.VW_maquina_tarea mt";
            sql += " on(IdMaquina = id_maquina)";
            sql += " inner join MAC.dbo.PRO_programa_man_pre pp";
            sql += " on(pp.id_maquina_tarea = mt.id)";
            sql += " inner join MAC.dbo.PRO_maq_tarea_rel tr";
            sql += " on(tr.id_maquina_tarea = pp.id_maquina_tarea)";
            sql += " inner join MAC.dbo.PRO_mantencion_preventiva mp";
            sql += " on(mp.id_programa = pp.id)";
            //sql += " where IdMaquina = 92";
            sql += " group by IdMaquina,fecha,mp.fechaInicio,mp.fechaFin";
            sql += " order by IdMaquina";
            
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModMaquina maqui = new ModMaquina();
                maqui.setIdMaquina(rs.getInt("IdMaquina"));
                maqui.setHorasTrabajo(rs.getDouble("horas"));
                maqui.setPresupuesto(rs.getDouble("presupuesto"));
                maqui.setModMante(new Mantenimiento());
                maqui.getModMante().setFechaPrograma(forma.format(rs.getDate("fecha")));
                maqui.getModMante().setFechaDetencionEquipo(formate.format(rs.getTimestamp("fechaInicio")));
                maqui.getModMante().setFechaEntregaEquipo(formate.format(rs.getTimestamp("fechaFin")));
                vec.add(maqui);
                maqui = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" programaPreventivoHoras() ", e.getMessage(), 0);
        }
        return vec;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarMaquinas() {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select IdMaquina,Uso,NombreCorto,Tipo FROM MAC.dbo.TManMaquina order by IdMaquina ";
            
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModMaquina maqui = new ModMaquina();
                maqui.setIdMaquina(rs.getInt("IdMaquina"));
                maqui.setUso(rs.getString("Uso"));
                maqui.setNombreCorto(rs.getString("NombreCorto"));
                maqui.setTipo(rs.getString("tipo"));
                vec.add(maqui);
                maqui = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarMaquinas() ", e.getMessage(), 0);
        }
        return vec;
    }
     //---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarMaquinasPreventivas() {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select IdMaquina,Nombre FROM MAC.dbo.TManMaquina where Nombre is not null order by IdMaquina ";
            
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModMaquina maqui = new ModMaquina();
                maqui.setIdMaquina(rs.getInt("IdMaquina"));
                maqui.setNombre(rs.getString("Nombre"));
                vec.add(maqui);
                maqui = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarMaquinasPreventivas() ", e.getMessage(), 0);
        }
        return vec;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarMaquinasConPrograma(int ano, String tipo) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select distinct(id_maquina) as IdMaquina,Nombre";
            sql += " from MAC.dbo.VW_maquina_tarea  tarea";
            sql += " inner join MAC.dbo.PRO_programa_man_pre pre";
            sql += " on(pre.id_maquina_tarea = tarea.id)";
            sql += " inner join MAC.dbo.TManMaquina";
            sql += " on(IdMaquina = id_maquina)";
            sql += " where year(pre.fecha) = "+ano + " and Tipo = '"+tipo+"'";
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModMaquina maqui = new ModMaquina();
                maqui.setIdMaquina(rs.getInt("IdMaquina"));
                maqui.setNombreCorto(rs.getString("Nombre"));
                vec.add(maqui);
                maqui = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarMaquinasConPrograma() ", e.getMessage(), 0);
        }
        return vec;
    }
//---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarIntervalos(int id) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            //String sql  = "select id,Descripcion from MAC.dbo.PRO_intervalo ";
            String sql  = " select distinct(i.id) as id, i.Descripcion as Descripcion"; 
                   sql += " from MAC.dbo.PRO_maquina_tarea mt ";
                   sql += " inner join MAC.dbo.PRO_maq_tarea_rel tr ";
                   sql += " on(mt.id = tr.id_maquina_tarea) ";
                   sql += " inner join MAC.dbo.PRO_intervalo i ";
                   sql += " on(i.id = tr.id_intervalo) ";
                   sql += " where id_maquina = "+id;
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModIntervalo inter = new ModIntervalo();
                inter.setId(rs.getInt("id"));
                inter.setDescripcion(rs.getString("Descripcion"));
                vec.add(inter);
                inter = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarIntervalos() ", e.getMessage(), 0);
        }
        return vec;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listareasIntervalos(int id_intervalo, int id_maquina) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select ta.id,ta.tareas,rel.hora,tt.descripcion,rel.presupuesto";
            sql += " from MAC.dbo.PRO_maquina_tarea mt";
            sql += " inner join MAC.dbo.PRO_maq_tarea_rel rel";
            sql += " on(rel.id_maquina_tarea =mt.id )";
            sql += " inner join MAC.dbo.PRO_tareas ta";
            sql += " on(ta.id = id_tarea)";
            sql += " inner join MAC.dbo.PRO_tipo_tarea tt";
            sql += " on(ta.id_tipo_tarea = tt.id)";
            sql += " where id_intervalo = "+id_intervalo+" and id_maquina = "+id_maquina;
            sql += " order by tt.descripcion";
            
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento mante = new Mantenimiento();
                mante.setId_tareas(rs.getInt("id"));
                mante.setNombreArea(rs.getString("tareas"));
                mante.setHorasDetencion(rs.getString("hora"));
                mante.setComentario(rs.getString("descripcion"));
                mante.setPresupuesto(rs.getDouble("presupuesto"));
                vec.add(mante);
                mante = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listareasIntervalos() ", e.getMessage(), 0);
        }
        return vec;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------
    public int maxTareas() {
        int resul = 0;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = "select max(id)+1 as resul from MAC.dbo.PRO_tareas";
            
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                resul = rs.getInt("resul");
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" maxTareas() ", e.getMessage(), 0);
        }
        return resul;
    }
//---------------------------------------------------------------------------------------------------------------------------------------
    public int idMquinaTarea(int idMaquina, int intervalo) {
        int resul = 0;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select mt.id as id  from MAC.dbo.PRO_maq_tarea_rel mtr";
            sql += " inner join MAC.dbo.VW_maquina_tarea mt";
            sql += " on(mt.id = mtr.id_maquina_tarea)";
            sql += " where mt.id_maquina = "+idMaquina+" and mtr.id_intervalo = "+intervalo;
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                resul = rs.getInt("id");
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" idMquinaTarea() ", e.getMessage(), 0);
        }
        return resul;
    }
//--------------------------------------------------------------------------------------------------------------------------//
    public boolean insertarTarea(int idTarea, String tarea, int tipoTarea, int idMaquinaTarea, int idMaquina) {
        boolean estado = false;
        String query = "";
        String query1 = "";
        try {
            conMac = mConexion.getMACConnector();
            conMac.setAutoCommit(false);
            st = conMac.createStatement();
            query = "INSERT INTO MAC.dbo.PRO_tareas(id, tareas, id_tipo_tarea) VALUES (" + idTarea + ", '" + tarea + "', " + tipoTarea + ") ";
            if (st.executeUpdate(query) > 0) {
                query1 = "INSERT INTO MAC.dbo.PRO_maquina_tarea(id, id_maquina, id_tarea) VALUES (" + idMaquinaTarea + ", " + idMaquina + ", " + idTarea + ") ";
            }
            if (st.executeUpdate(query1) > 0) {
                conMac.commit();
                estado = true;
            }
            conMac.setAutoCommit(true);
            st.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" insertarTarea()", e.getMessage(), 0);
        }
        return estado;
    }
//--------------------------------------------------------------------------------------------------------------------------//
    public boolean borrarTarea(int idMaquinaTarea, int idMaquina, int idTarea) {
        boolean estado = false;
        try {
            conMac = mConexion.getMACConnector();
            conMac.setAutoCommit(false);
            st = conMac.createStatement();
            String query = "";
            String query1 = "";
            query = "DELETE FROM MAC.dbo.PRO_maquina_tarea WHERE id = " + idMaquinaTarea + " and id_maquina = "+idMaquina+ "and id_tarea = "+idTarea;
            if (st.executeUpdate(query) > 0) {
                query1 = "DELETE FROM MAC.dbo.PRO_tareas WHERE id = " + idTarea;
                if (st.executeUpdate(query1) > 0) {
                    conMac.commit();
                    conMac.setAutoCommit(true);
                    estado = true;
                }
            }
            st.close();
            conMac.close();
        } catch (Exception e) {
            
            loger.logger("macScSisMantenimientoBean.java "+" borrarTarea() ", e.getMessage(), 0);
        }
        return estado;
    }
//--------------------------------------------------------------------------------------------------------------------------
    public boolean insertarForecast(int idMaquina, double tiempo, double presupuesto, int ano, int mes, boolean est) {
        boolean estado = false;
        String query = "";
        String query1 = "";
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            if(!est){
                query = "INSERT INTO MAC.dbo.PRO_forecast(id_maquina, tiempo, presupuesto, ano, mes) VALUES (" + idMaquina + ", " + tiempo + ", " + presupuesto + ", " + ano + ", " + mes + ") ";
            }else{
                query = "UPDATE MAC.dbo.PRO_forecast SET tiempo = "+tiempo+" , presupuesto ="+presupuesto+" where id_maquina = "+idMaquina+" and ano = "+ano+" and mes = "+mes;
            }
            if (st.executeUpdate(query) > 0) {
                estado = true;
            }
            st.close();
            conMac.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" insertarForecast()", e.getMessage(), 0);
        }
        return estado;
    }
//---------------------------------------------------------------------------------------------------------------------------------------
    public boolean countForecast(int idMaquina, int ano, int mes) {
        boolean resul = false;
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select count(*) from MAC.dbo.PRO_forecast where id_maquina = "+idMaquina+" and ano = "+ano+" and mes = "+mes;
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                if(rs.getInt(1) > 0){
                    resul = true;
                }
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" countForecast() ", e.getMessage(), 0);
        }
        return resul;
    }
//---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList cartaGantt(int ano, String tipo) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            /*String sql  = " select id_intervalo,hora,pmp.fecha,month(pmp.fecha) as mes,mt.id_maquina";
            sql += " from MAC.dbo.VW_maquina_tarea mt";
            sql += " inner join MAC.dbo.PRO_programa_man_pre pmp";
            sql += " on(mt.id = pmp.id_maquina_tarea)";
            sql += " inner join MAC.dbo.PRO_maq_tarea_rel tr";
            sql += " on(tr.id_maquina_tarea = mt.id)";
            sql += " where year(pmp.fecha) = "+ano;
            sql += " order by fecha,id_intervalo,mt.id_maquina";*/
            
            String sql  = " select id_intervalo,hora,pmp.fecha,month(pmp.fecha) as mes,mt.id_maquina";
                   sql += " from MAC.dbo.VW_maquina_tarea mt";
                   sql += " inner join MAC.dbo.PRO_programa_man_pre pmp";
                   sql += " on(mt.id = pmp.id_maquina_tarea)";
                   sql += " inner join MAC.dbo.PRO_maq_tarea_rel tr";
                   sql += " on(tr.id_maquina_tarea = mt.id)";
                   sql += " inner join MAC.dbo.TManMaquina";
                   sql += " on(IdMaquina = mt.id_maquina)";
                   sql += " where year(pmp.fecha) = "+ano+" and Tipo = '"+tipo+"'";
                   sql += " order by fecha,id_intervalo,mt.id_maquina";
            
            
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModPrograma prog = new ModPrograma();
                prog.setNombre(rs.getString("id_intervalo"));
                prog.setTiempo(rs.getDouble("hora"));
                prog.setFecha(format.format(rs.getDate("fecha")));
                prog.setMes(rs.getByte("mes"));
                prog.setIdMaquina(rs.getInt("id_maquina"));
                vec.add(prog);
                prog = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" cartaGantt() ", e.getMessage(), 0);
        }
        return vec;
    }
    
//---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList forecast_tiempo(int ano, String tipo) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select tiempo,mes,id_maquina"; 
                   sql += " from MAC.dbo.PRO_forecast ";
                   sql += " inner join MAC.dbo.TManMaquina";
                   sql += " on(IdMaquina = id_maquina) ";
                   sql += " where ano = "+ano+" and Tipo = '"+tipo+"' ";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModPrograma prog = new ModPrograma();
                prog.setTiempo(rs.getDouble("tiempo"));
                prog.setMes(rs.getByte("mes"));
                prog.setIdMaquina(rs.getInt("id_maquina"));
                vec.add(prog);
                prog = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" forecast_tiempo() ", e.getMessage(), 0);
        }
        return vec;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList horasDetenidasCorrectivas(int ano, int mes) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
           /* String sql  = " SELECT a.CodigoMaquina,m.NombreCorto,";
            sql += " a.ObservacionFalla,a.FechaDetencionEquipo, ";
            sql += " a.FechaEntregaEquipo,turmaq.idTurnoMaq,a.TipoFalla";
            sql += " FROM MAC.dbo.TMantencion a ";
            sql += " LEFT JOIN MAC.dbo.TManMaquina m ON a.CodigoMaquina = m.IdMaquina ";
            sql += " LEFT JOIN MAC.dbo.TMantTurnoMaq turmaq ON m.idTurnoMaq = turmaq.idTurnoMaq ";
            sql += " where a.FechaDetencionEquipo is not null and a.FechaEntregaEquipo is not null";
            sql += " and year(a.FechaDetencionEquipo) = "+ano+" and month(a.FechaDetencionEquipo) = "+mes+" and a.TipoFalla is not null";
            sql += " ORDER BY a.CodigoMantenimiento ";*/
            
            String sql  = " SELECT a.CodigoMaquina,m.Nombre,";
                   sql += " ie.observacionDiagnostico,a.FechaDetencionEquipo, ";
                   sql += " a.FechaEntregaEquipo,turmaq.idTurnoMaq,a.TipoFalla";
                   sql += " FROM MAC.dbo.TMantencion a ";
                   sql += " LEFT JOIN MAC.dbo.TManMaquina m ON a.CodigoMaquina = m.IdMaquina ";
                   sql += " LEFT JOIN MAC.dbo.TMantTurnoMaq turmaq ON m.idTurnoMaq = turmaq.idTurnoMaq ";
                   sql += " LEFT JOIN MAC.dbo.TInformacionEstado ie on ie.idOM =  a.CodigoMantenimiento";
                   sql += " where a.FechaDetencionEquipo is not null and a.FechaEntregaEquipo is not null";
                   sql += " and year(a.FechaDetencionEquipo) = "+ano+" and month(a.FechaDetencionEquipo) = "+mes+" and a.TipoFalla is not null";
                   sql += " ORDER BY a.CodigoMantenimiento";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento man = new Mantenimiento();
                man.setCodigoMaquina(rs.getInt("CodigoMaquina"));
                man.setNombreMaquina(rs.getString("Nombre"));
                man.setObservacionFalla(PrmApp.devolverObservacion(rs.getString("observacionDiagnostico")));
                man.setFechaDetencionEquipo(rs.getString("FechaDetencionEquipo"));
                man.setFechaEntregaEquipo(rs.getString("FechaEntregaEquipo"));
                man.setTurnoMaq(new ModTurnoMaq());
                man.getTurnoMaq().setIdTurnoMaq(rs.getInt("idTurnoMaq"));
                man.setTipo(rs.getString("TipoFalla"));
                vec.add(man);
                man = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" horasDetenidasCorrectivas() ", e.getMessage(), 0);
        }
        return vec;
    }
//---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList horasPresupuestadasPreventivas(int ano, int mes) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select mt.id_maquina,tm.Nombre,sum(hora) as horas,mp.fechaInicio,mp.fechaFin";
            sql += " from MAC.dbo.VW_maquina_tarea mt";
            sql += " inner join MAC.dbo.PRO_maq_tarea_rel tr";
            sql += " on(mt.id = tr.id_maquina_tarea)";
            sql += " inner join MAC.dbo.PRO_programa_man_pre pp";
            sql += " on(tr.id_maquina_tarea = pp.id_maquina_tarea)";
            sql += " inner join MAC.dbo.TManMaquina tm";
            sql += " on(mt.id_maquina = tm.IdMaquina)";
            sql += " left outer join MAC.dbo.PRO_mantencion_preventiva mp";
            sql += " on(pp.id = mp.id_programa)";
            sql += " where year(pp.fecha) = "+ano+" and month(pp.fecha) = "+mes+" and tm.Tipo = 'CR'";
            sql += " group by mt.id_maquina,tm.Nombre,mp.fechaInicio,mp.fechaFin";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento man = new Mantenimiento();
                man.setCodigoMaquina(rs.getInt("id_maquina"));
                man.setNombreMaquina(rs.getString("Nombre"));
                man.setPresupuesto(rs.getDouble("horas"));
                if(rs.getTimestamp("fechaInicio") != null && rs.getTimestamp("fechaFin") != null){
                    man.setHorasDetenidas(PrmApp.horasDeMantencion(rs.getTimestamp("fechaInicio"),rs.getTimestamp("fechaFin")));
                }else{
                    man.setHorasDetenidas(0d);
                }
                vec.add(man);
                man = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" horasPresupuestadasPreventivas() ", e.getMessage(), 0);
        }
        return vec;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarMaquinasCriticas() {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            String sql  = " select IdMaquina,Uso,Nombre,Tipo FROM MAC.dbo.TManMaquina where Tipo = 'CR' order by IdMaquina ";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                ModMaquina maqui = new ModMaquina();
                maqui.setIdMaquina(rs.getInt("IdMaquina"));
                maqui.setUso(rs.getString("Uso"));
                maqui.setNombreCorto(rs.getString("Nombre"));
                vec.add(maqui);
                maqui = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarMaquinasCriticas() ", e.getMessage(), 0);
        }
        return vec;
    }
//---------------------------------------------------------------------------------------------------------------------------------------
    public ArrayList listarGastosMantencion(int ano, int mes) {
        ArrayList vec = new ArrayList();
        try {
            conMac = mConexion.getMACConnector();
            st = conMac.createStatement();
            /*String sql  = " select oc.MVD_CodProd,sum(oc.MVD_CantAsignada),sum(oc.MVD_PrecioAjustado),NombreCorto";
            sql += "     from MAC.dbo.PRO_mantencion_preventiva pre";
            sql += "     inner join MAC.dbo.PRO_gastos_mantencion";
            sql += "     on(numero = ordenMantencion)";
            sql += "     inner join MAC.dbo.VW_programa_man_pre pp ";
            sql += "     on(pre.id_programa = pp.id) ";
            sql += "     inner join SiaSchaffner.dbo.VW_sap sap";
            sql += "    on(numPedido = sap.MVE_NumeroDoc) ";
            sql += "     inner join SiaSchaffner.dbo.VW_ordenes_compra oc";
            sql += "     on(sap.MVE_NumeroDoc = oc.MVD_NumeroDocOrigen and sap.MVD_Linea = oc.MVD_LineaOrigen)";
            sql += "     inner join MAC.dbo.VW_maquina_programa mp";
            sql += "     on(mp.id = pre.id_programa)";
            sql += "     inner join MAC.dbo.TManMaquina ";
            sql += "     on(mp.id_maquina = IdMaquina)";
            sql += "     where year(fecha) = "+ano+" and month(fecha) = "+mes+" and tipoMantencion = 'P' and pedido = 'S' and oc.MVD_CantAsignada > 0";
            sql += "     group by oc.MVD_CodProd,NombreCorto";
            sql += "     union all";
            sql += "     select oc.MVD_CodProd,sum(oc.MVD_CantAsignada),sum(oc.MVD_PrecioAjustado),NombreCorto";
            sql += "     from MAC.dbo.TMantencion";
            sql += "     inner join MAC.dbo.PRO_gastos_mantencion";
            sql += "     on(CodigoMantenimiento = ordenMantencion)";
            sql += "     inner join SiaSchaffner.dbo.VW_sap sap";
            sql += "     on(numPedido = sap.MVE_FolioFisico) ";
            sql += "     inner join SiaSchaffner.dbo.VW_ordenes_compra oc";
            sql += "     on(sap.MVE_NumeroDoc = oc.MVD_NumeroDocOrigen and sap.MVD_Linea = oc.MVD_LineaOrigen)";
            sql += "     inner join MAC.dbo.TManMaquina ";
            sql += "     on(CodigoMaquina = IdMaquina)";
            sql += "     where year(FechaDetencionEquipo) = "+ano+" and month(FechaDetencionEquipo) = "+mes+" and tipoMantencion = 'C' and pedido = 'S' and TipoFalla is not null and oc.MVD_CantAsignada > 0";
            sql += "     group by oc.MVD_CodProd,NombreCorto";
            
            sql += "     union all";
            sql += "     select oc.MVD_CodProd,sum(oc.MVD_CantAsignada),sum(oc.MVD_PrecioAjustado),NombreCorto";
            sql += "     from MAC.dbo.PRO_mantencion_preventiva pre";
            sql += "     inner join MAC.dbo.PRO_gastos_mantencion";
            sql += "     on(numero = ordenMantencion)";
            sql += "     inner join MAC.dbo.VW_programa_man_pre pp ";
            sql += "     on(pre.id_programa = pp.id) ";
            sql += "     inner join SiaSchaffner.dbo.vw_pedidoDeMateriales pm";
            sql += "     on(numPedido = pm.MVE_FolioFisico)";
            sql += "     inner join SiaSchaffner.dbo.vw_valeDeConsumo vc";
            sql += "     on(pm.MVD_NumeroDoc = vc.MVD_NumeroDocOrigen and pm.MVD_Linea = vc.MVD_LineaOrigen) ";
            sql += "     inner join SiaSchaffner.dbo.VW_ordenes_compra oc";
            sql += "     on(oc.MVD_CodProd = vc.MVD_CodProd and oc.MVD_NumeroDoc = ";
            sql += "     (SELECT max(ocf.MVD_NumeroDoc)";
            sql += "     FROM SiaSchaffner.dbo.VW_ordenes_compra ocf";
            sql += "     WHERE ocf.MVD_CodProd = oc.MVD_CodProd))";
            sql += "     inner join MAC.dbo.VW_maquina_programa mp";
            sql += "     on(mp.id = pre.id_programa)";
            sql += "     inner join MAC.dbo.TManMaquina ";
            sql += "     on(mp.id_maquina = IdMaquina)";
            sql += "     where year(fecha) = "+ano+" and month(fecha) = "+mes+" and tipoMantencion = 'P' and pedido = 'P' and oc.MVD_CantAsignada > 0";
            sql += "     group by oc.MVD_CodProd,NombreCorto";
            sql += "     union all";
            sql += "     select oc.MVD_CodProd,sum(oc.MVD_CantAsignada),sum(oc.MVD_PrecioAjustado),NombreCorto";
            sql += "     from MAC.dbo.TMantencion";
            sql += "     inner join MAC.dbo.PRO_gastos_mantencion";
            sql += "     on(CodigoMantenimiento = ordenMantencion)";
            sql += "     inner join SiaSchaffner.dbo.vw_pedidoDeMateriales pm";
            sql += "     on(numPedido = pm.MVE_FolioFisico)";
            sql += "     inner join SiaSchaffner.dbo.vw_valeDeConsumo vc";
            sql += "     on(pm.MVD_NumeroDoc = vc.MVD_NumeroDocOrigen and pm.MVD_Linea = vc.MVD_LineaOrigen) ";
            sql += "     inner join SiaSchaffner.dbo.VW_ordenes_compra oc";
            sql += "     on(oc.MVD_CodProd = vc.MVD_CodProd and oc.MVD_NumeroDoc = ";
            sql += "     (SELECT max(ocf.MVD_NumeroDoc)";
            sql += "     FROM SiaSchaffner.dbo.VW_ordenes_compra ocf";
            sql += "     WHERE ocf.MVD_CodProd = oc.MVD_CodProd))";
            sql += "     inner join MAC.dbo.TManMaquina";
            sql += "     on(CodigoMaquina = IdMaquina)";
            sql += "     where year(FechaDetencionEquipo) = "+ano+" and month(FechaDetencionEquipo) = "+mes+" and tipoMantencion = 'C' and pedido = 'P' and TipoFalla is not null and oc.MVD_CantAsignada > 0";
            sql += "     group by oc.MVD_CodProd,NombreCorto";
            sql += "     order by NombreCorto ";*/
            
            String sql  = " SELECT a.CodigoMaquina,m.NombreCorto,ie.observacionServicioExterno, ie.observacionMantenimientoCorrectivo";
                   sql += " FROM MAC.dbo.TMantencion a ";
                   sql += " LEFT JOIN MAC.dbo.TManMaquina m ON a.CodigoMaquina = m.IdMaquina ";
                   sql += " LEFT JOIN MAC.dbo.TInformacionEstado ie on ie.idOM =  a.CodigoMantenimiento";
                   sql += " where a.FechaDetencionEquipo is not null and a.FechaEntregaEquipo is not null";
                   sql += " and year(a.FechaDetencionEquipo) = "+ano+" and month(a.FechaDetencionEquipo) = "+mes+" and a.TipoFalla is not null";
                   sql += " ORDER BY a.CodigoMantenimiento";
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Mantenimiento mante = new Mantenimiento();
                /*mante.setCodProducto(rs.getString(1));
                mante.setPresupuesto(rs.getDouble(2));
                mante.setCosto(rs.getFloat(3));
                mante.setNombreMaquina(rs.getString(4));*/
                mante.setNombreMaquina(rs.getString(2));
                if(rs.getString(3) == null){
                mante.setObservacionMantenimientoCorrectivo(rs.getString(4));    
                }else{
                mante.setObservacionMantenimientoCorrectivo(rs.getString(3));        
                }
                vec.add(mante);
                mante = null;
            }
            conMac.close();
            st.close();
            rs.close();
        } catch (Exception e) {
            loger.logger("macScSisMantenimientoBean.java "+" listarGastosMantencion() ", e.getMessage(), 0);
        }
        return vec;
    }
}//fin clase bean

