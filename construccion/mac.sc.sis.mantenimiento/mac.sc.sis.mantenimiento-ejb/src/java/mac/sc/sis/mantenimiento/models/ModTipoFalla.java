package mac.sc.sis.mantenimiento.models;

/**
 *
 * @author ngonzalez
 */
public class ModTipoFalla implements Cloneable {

    private int id;
    private int idTipoFalla;
    private String nombre;
    private String descripcion;
    private String inicial;

    public ModTipoFalla() {
        id = -1;
        idTipoFalla = 0;
        nombre = "";
        descripcion = "";
        inicial = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdTipoFalla() {
        return idTipoFalla;
    }

    public void setIdTipoFalla(int idTipoFalla) {
        this.idTipoFalla = idTipoFalla;
    }

    public ModTipoFalla copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
            //
        }
        return (ModTipoFalla) obj;
    }

    public String getInicial() {
        return inicial;
    }

    public void setInicial(String inicial) {
        this.inicial = inicial;
    }
}
