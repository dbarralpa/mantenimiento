package mac.sc.sis.mantenimiento.models;

import java.util.ArrayList;
import java.util.Date;

public class ModAuxiliar implements Cloneable {

    private String codigo;
    private int id;
    private int numero;
    private String nombre;
    private String comentario;
    private String auxiliar;
    private String base;
    private int estado;
    private ArrayList detalles;
    private Date fecha;
    private int valor;

    public ModAuxiliar() {
        codigo = "";
        id = 0;
        numero = 0;
        nombre = "";
        comentario = "";
        auxiliar = "";
        base = "";
        estado = 0;
        detalles = new ArrayList();
        fecha = null;
        valor = 0;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public ArrayList getDetalles() {
        return detalles;
    }

    public void setDetalles(ArrayList detalles) {
        this.detalles = detalles;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public ModAuxiliar copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModAuxiliar) obj;
    }
}
