package mac.sc.sis.mantenimiento.models;

public class ModArea implements Cloneable{    
    
    private int id;
    private int idArea;
    private String nombre;
    
    public ModArea() {
        id = 0;
        idArea = 0;
        nombre = "";        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdArea() {
        return idArea;
    }

    public void setIdArea(int idArea) {
        this.idArea = idArea;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public ModArea copia(){
        Object obj=null;
        try{
            obj=super.clone();
        }catch(CloneNotSupportedException ex){
            //
        }
        return (ModArea)obj;
    }
    
}
