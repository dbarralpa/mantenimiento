package mac.sc.sis.mantenimiento.utils;

import java.io.FileInputStream;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.util.CellRangeAddress;

public class AsExcel {

    private HSSFWorkbook workbook;
    private HSSFSheet sheet_estilos;
    private FileInputStream inputfile;    
    private float alturaCell;
    private float alturaCell2;

    public AsExcel() {
        workbook = new HSSFWorkbook();
        sheet_estilos = null;
        inputfile = null;        
        alturaCell = 0;
        alturaCell2 = 0;
    }

    public void crearExcel(String nomArchivo) {
        try {
            inputfile = new FileInputStream(nomArchivo);
            workbook = new HSSFWorkbook(inputfile);
            sheet_estilos = workbook.getSheetAt(workbook.getSheetIndex("ESTILO"));
            alturaCell = Float.parseFloat(sheet_estilos.getRow(0).getCell((short) 1).getRichStringCellValue().getString());
            alturaCell2 = Float.parseFloat(sheet_estilos.getRow(39).getCell((short) 1).getRichStringCellValue().getString());
        } catch (Exception ex) {
            EscribeFichero.addError(ex, true);
        }
    }

    public void borrarHoja() {
        workbook.removeSheetAt(workbook.getSheetIndex("ESTILO"));
    }

    public void cerrarPlantilla() {
        try {
            workbook = null;
            inputfile.close();
        } catch (Exception ex) {
            EscribeFichero.addError(ex, true);
        }
    }

    private HSSFCellStyle getStylePlantilla(String estilo) {
        HSSFCellStyle cellStyle = null;
        if (estilo.equals("CAB_TAB")) {
            cellStyle = sheet_estilos.getRow(1).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("CAB_STAB")) {
            cellStyle = sheet_estilos.getRow(2).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TTEXTO")) {
            cellStyle = sheet_estilos.getRow(3).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TNUMERO")) {
            cellStyle = sheet_estilos.getRow(4).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TFECHA")) {
            cellStyle = sheet_estilos.getRow(5).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_THORA")) {
            cellStyle = sheet_estilos.getRow(6).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TGENERAL")) {
            cellStyle = sheet_estilos.getRow(7).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TESTADO")) {
            cellStyle = sheet_estilos.getRow(8).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TFACTOR")) {
            cellStyle = sheet_estilos.getRow(9).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TMONEDA")) {
            cellStyle = sheet_estilos.getRow(10).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TPORCENTAJE")) {
            cellStyle = sheet_estilos.getRow(11).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_TCABEZERA")) {
            cellStyle = sheet_estilos.getRow(12).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STTEXTO")) {
            cellStyle = sheet_estilos.getRow(13).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STNUMERO")) {
            cellStyle = sheet_estilos.getRow(14).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STFECHA")) {
            cellStyle = sheet_estilos.getRow(15).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STHORA")) {
            cellStyle = sheet_estilos.getRow(16).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STNUMERICO")) {
            cellStyle = sheet_estilos.getRow(17).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STESTADO")) {
            cellStyle = sheet_estilos.getRow(18).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STFACTOR")) {
            cellStyle = sheet_estilos.getRow(19).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("DET_STMONEDA")) {
            cellStyle = sheet_estilos.getRow(20).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TTEXTO")) {
            cellStyle = sheet_estilos.getRow(21).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TNUMERO")) {
            cellStyle = sheet_estilos.getRow(22).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TFECHA")) {
            cellStyle = sheet_estilos.getRow(23).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_THORA")) {
            cellStyle = sheet_estilos.getRow(24).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TNUMERICO")) {
            cellStyle = sheet_estilos.getRow(25).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TESTADO")) {
            cellStyle = sheet_estilos.getRow(26).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TFACTOR")) {
            cellStyle = sheet_estilos.getRow(27).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_TMONEDA")) {
            cellStyle = sheet_estilos.getRow(28).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STTEXTO")) {
            cellStyle = sheet_estilos.getRow(29).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STNUMERO")) {
            cellStyle = sheet_estilos.getRow(30).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STFECHA")) {
            cellStyle = sheet_estilos.getRow(31).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STHORA")) {
            cellStyle = sheet_estilos.getRow(32).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STNUMERICO")) {
            cellStyle = sheet_estilos.getRow(33).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STESTADO")) {
            cellStyle = sheet_estilos.getRow(34).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STFACTOR")) {
            cellStyle = sheet_estilos.getRow(35).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STMONEDA")) {
            cellStyle = sheet_estilos.getRow(36).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("PIE_STPORCENTAJE")) {
            cellStyle = sheet_estilos.getRow(37).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else if (estilo.equals("CELL_NO_EXIST")) {
            cellStyle = sheet_estilos.getRow(38).getCell((short) 1).getCellStyle();
            return cellStyle;
        } else {
            cellStyle = sheet_estilos.getRow(38).getCell((short) 1).getCellStyle();
            return cellStyle;
        }
    }

    public void setValor(int hoja, int fila, int celda, String valorStr, String estilo, boolean est) {
        setValor(hoja, fila, celda, 0, valorStr, estilo, false, est);
    }

    public void setValor(int hoja, int fila, int celda, double valorNum, String estilo, boolean est) {
        setValor(hoja, fila, celda, valorNum, "", estilo, true, est);
    }
    public void setValor(int hoja, int fila, int fromCelda, int toCelda, String valorStr, String estilo, int fila1, boolean est) {
        setValor1(hoja, fila, fromCelda, toCelda, 0, valorStr, estilo, false, fila1, est);
    }
    public void setValor(int hoja, int fila, int fromCelda, int toCelda, double valorStr, String estilo, int fila1, boolean est) {
        setValor1(hoja, fila, fromCelda, toCelda, valorStr, "", estilo, true, fila1, est);
    }

    private void setValor(int hoja, int fila, int celda, double valorNum, String valorStr, String estilo, boolean isNum, boolean est) {
        HSSFCell cell = crearCelda(hoja, fila, celda, estilo, est);

        try {
            if (cell != null) {
                if (isNum) {
                    cell.setCellValue(valorNum);
                } else {
                    cell.setCellValue(new HSSFRichTextString(eliminarCaracteresEspeciales(valorStr)));
                }
            }
        } catch (Exception ex) {
            EscribeFichero.addError(ex, true);
        }
    }
 private void setValor1(int hoja, int fila, int fromCelda, int toCelda, double valorNum, String valorStr, String estilo, boolean isNum,int fila1, boolean est) {
        HSSFCell cell = crearCelda1(hoja, fila, fromCelda, toCelda, estilo, fila1, est);
        try {
            if (cell != null) {
                if (isNum) {
                    cell.setCellValue(valorNum);
                } else {
                    cell.setCellValue(new HSSFRichTextString(valorStr));
                }
            }
        } catch (Exception ex) {
            EscribeFichero.addError(ex);
        }
    }
    private String eliminarCaracteresEspeciales(String valorStr) {
        if (valorStr != null) {
            valorStr = valorStr.replace('\r', ' ');
            valorStr = valorStr.replace('\n', ' ');
            valorStr = valorStr.replace('\t', ' ');
        } else {
            valorStr = "";
        }
        return valorStr;
    }

    private HSSFCell crearCelda(int hoja, int fila, int celda, String estilo, boolean est) {
        HSSFRow row = null;
        try {            
            if (workbook.getSheetAt(hoja).getRow(fila) != null) {
                row = workbook.getSheetAt(hoja).getRow(fila);
            } else {
                row = workbook.getSheetAt(hoja).createRow(fila);
            }
            if(est){
            row.setHeightInPoints(alturaCell);    
            }else{
            row.setHeightInPoints(alturaCell2);    
            }
            row.createCell(celda);
            row.getCell(celda).setCellStyle(getStylePlantilla(estilo));            
        } catch (Exception ex) {
            EscribeFichero.addError(ex);
        }
        return workbook.getSheetAt(hoja).getRow(fila).getCell(celda);
    }
    private HSSFCell crearCelda1(int hoja, int fila, int fromCelda, int toCelda, String estilo, int fila1, boolean est) {
        HSSFCell cell = null;
        try {
            HSSFRow row = null;
            if (workbook.getSheetAt(hoja).getRow(fila) != null) {
                row = workbook.getSheetAt(hoja).getRow(fila);
            }else{
                row = workbook.getSheetAt(hoja).createRow(fila);
            }
            if(est){
            row.setHeightInPoints(alturaCell);    
            }else{
            row.setHeightInPoints(alturaCell2);    
            }
            
            for (int i = fromCelda; i <= toCelda; i++) {
                row.createCell(i);
                row.getCell(i).setCellStyle(getStylePlantilla(estilo));
            }
            crearRegion(hoja, fila, fromCelda, toCelda, fila1);
            cell = workbook.getSheetAt(hoja).getRow(fila).getCell(fromCelda);

        } catch (Exception ex) {
            EscribeFichero.addError(ex);
        }
        return cell;
    }
     private void crearRegion(int hoja, int fila, int fromCelda, int toCelda, int fila1) {
        workbook.getSheetAt(hoja).addMergedRegion(new CellRangeAddress(fila, fila1, fromCelda, toCelda));
    }

    public HSSFWorkbook getWorkBook() {
        return workbook;
    }
    
    
 /*   HSSFWorkbook wb = new HSSFWorkbook();

      HSSFPatriarch patriarch = sheet.createDrawingPatriarch();

      HSSFClientAnchor anchor;

      anchor = new HSSFClientAnchor(500,100,600,200,(short)0,0,(short)1,0);

      anchor.setAnchorType( 2 );

      HSSFPicture picture =  patriarch.createPicture(anchor, loadPicture(files, wb ));
     
      //Metodo que permite insertar imagenes a el excel
      private static int loadPicture( File path, HSSFWorkbook wb ) throws IOException
       {
       int pictureIndex;
       FileInputStream fis = null;
       ByteArrayOutputStream bos = null;
       try
     {
        // read in the image file
         fis = new FileInputStream(path);
         bos = new ByteArrayOutputStream( );

        int c;

        // copy the image bytes into the ByteArrayOutputStream

        while ( (c = fis.read()) != -1)

            bos.write( c );

    

       // add the image bytes to the workbook

         pictureIndex = wb.addPicture(bos.toByteArray(), HSSFWorkbook.PICTURE_TYPE_PNG );



     }

     finally

    {

         if (fis != null)

            fis.close();

        if (bos != null)

            bos.close();

    }

    return pictureIndex;

    }*/
}
