package mac.sc.sis.mantenimiento.controllers;

import java.util.ArrayList;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.models.ModAuxiliar;
import mac.sc.sis.mantenimiento.models.ModPaginacion;

public class PrmPaginacion {

    public int RPP = 25;
    public int TOPE_SET = 15;
    public ModPaginacion mMant = new ModPaginacion();
    public ModPaginacion mMant1 = new ModPaginacion();
    public ModPaginacion mSoli = new ModPaginacion();

    public void indiceMant(ArrayList aux) throws Exception {
        Mantenimiento mant = null;
        ModAuxiliar tmp = null;
        mMant = new ModPaginacion();
        for (int i = 0; i < aux.size(); i++) {
            mant = (Mantenimiento) aux.get(i);
            tmp = new ModAuxiliar();
            tmp.setCodigo(String.valueOf(mant.getCodigoMantenimiento()));
            tmp.setNombre(mant.getNombreMaquina());
            tmp.setAuxiliar("Fecha: " + "<br><i>" + mant.getFechaHoy().substring(0, 10) + "</i><br>");
            tmp.setNumero(i);
            mMant.idx.add(tmp);
            tmp = null;
            mant = null;
        }
    }
    public void indiceMant1(ArrayList aux) throws Exception {
        Mantenimiento mant = null;
        ModAuxiliar tmp = null;
        mMant1 = new ModPaginacion();
        for (int i = 0; i < aux.size(); i++) {
            mant = (Mantenimiento) aux.get(i);
            tmp = new ModAuxiliar();
            tmp.setCodigo(String.valueOf(mant.getCodigoMantenimiento()));
            tmp.setNombre(mant.getNombreMaquina());
            tmp.setAuxiliar("Fecha: " + "<br><i>" + mant.getFechaMantePreventivo().substring(0, 10) + "</i><br>");
            tmp.setNumero(i);
            mMant1.idx.add(tmp);
            tmp = null;
            mant = null;
        }
    }

    public void indiceSoli(ArrayList aux) throws Exception {
        Mantenimiento mant = null;
        ModAuxiliar tmp = null;
        mSoli = new ModPaginacion();
        for (int i = 0; i < aux.size(); i++) {
            mant = (Mantenimiento) aux.get(i);
            tmp = new ModAuxiliar();
            tmp.setCodigo(String.valueOf(mant.getCodigoMantenimiento()));
            tmp.setNombre(mant.getNombreMaquina());
            tmp.setAuxiliar("Fecha: " + "<br><i>" + mant.getFechaHoy().substring(0, 10) + "</i><br>");
            tmp.setNumero(i);
            mSoli.idx.add(tmp);
            tmp = null;
            mant = null;
        }
    }

    public ModPaginacion getDetalle(String id) throws Exception {
        if (id.equals("MANT")) {
            return mMant;
        } else if (id.equals("SOLI")) {
            return mSoli;
        } else if (id.equals("MANT1")) {
            return mMant1;
        } else {
            return new ModPaginacion();
        }
    }

    public void setDetalle(String id) throws Exception {
        if (id.equals("MANT")) {
            mMant = new ModPaginacion();
        } else if (id.equals("MANT1")) {
            mMant1 = new ModPaginacion();
        } else if (id.equals("SOLI")) {
            mSoli = new ModPaginacion();
        }
    }

    public String verPaginador(String id, boolean conResumen, boolean ordenAsc) throws Exception {
        String html = "";
        String htm2 = "";
        String tip = "";
        ModPaginacion idx = null;
        ModPaginacion tmp_idx = null;
        int i = 0;
        int tpag = 0;
        int pi = 0;
        int pt = 0;
        int ri = 0;
        int rt = 0;

        tmp_idx = getDetalle(id);

        if (!ordenAsc) {
            idx = new ModPaginacion();
            for (int j = 0; j < tmp_idx.getTotal(); j++) {
                idx.idx.add(tmp_idx.getDetalle(tmp_idx.getTotal() - (j + 1)));
            }
            idx.pag = tmp_idx.pag;

        } else {
            idx = tmp_idx;
        }

        if (idx.getTotal() > 0) {
            tpag = (idx.getTotal() + (RPP - 1)) / RPP;
            if (tpag > TOPE_SET) {
                int y1 = (idx.pag / TOPE_SET);
                y1 = y1 * TOPE_SET;
                if (idx.pag == y1) {
                    pi = idx.pag - (TOPE_SET - 1);
                } else {
                    pi = y1 + 1;
                }

                pt = pi + (TOPE_SET - 1);
                if (pt > tpag) {
                    pt = tpag;
                }
            } else {
                pi = 1;
                pt = tpag;
            }
            ri = (RPP * (idx.pag - 1)) + 1;
            rt = (RPP * idx.pag);
            if (rt > idx.getTotal()) {
                rt = idx.getTotal();
            }

            html = "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
            html = html + "<tr class='Paginador'>\n";
            html = html + "<td width=\"10\">" + "[" + ri + "/" + rt + "]" + "</td>\n";
            html = html + "<td width=\"10\">" + "[" + idx.getTotal() + "]" + "</td>\n";
            if (pi > 1) {
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + idx.codigoDesde_Pag(1, RPP) + "<br>" + "<b>hasta: </b>" + idx.codigoDesde_Pag(pi, RPP));
                } else {
                    tip = "";
                }
                html = html + "<td width=\"17\">";
                html = html + "<input name='pag" + id + "' type='submit' value='�' class='boxNumerada'  " + tip + " onClick=\"this.value='" + (pi - 1) + "'\" >";
                html = html + "</td>\n";
                htm2 = htm2 + "<td>" + "</td>\n";
            }
            for (i = pi; i <= pt; i++) {
                tip = "";
                html = html + "<td width=\"17\">";
                html = html + "<input name='pag" + id + "' type='submit' value='" + i + "' ";
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + idx.codigoDesde_Pag(i, RPP) + "<br>" + "<b>hasta: </b>" + idx.codigoHasta_Pag(i, RPP));
                } else {
                    tip = "";
                }
                if (idx.pag == i) {
                    html = html + "class='boxNumeradaActiva' disabled " + tip + ">";
                } else {
                    html = html + "class='boxNumerada' " + tip + ">";
                }
                html = html + "</td>\n";
            }
            if (tpag > pt) {
                if (conResumen) {
                    tip = PrmApp.verOverTip("<b>desde: </b>" + idx.codigoHasta_Pag(pt, RPP) + "<br>" + "<b>hasta: </b>" + idx.codigoHasta_Pag(tpag, RPP));
                } else {
                    tip = "";
                }
                html = html + "<td width=\"17\">";
                html = html + "<input name='pag" + id + "' type='submit' value='�' class='boxNumerada'  " + tip + " onClick=\"this.value='" + (pt + 1) + "'\" >";
                html = html + "</td>\n";
                htm2 = htm2 + "<td>" + "</td>\n";
            }
            html = html + "<td width=\"600\" align=\"right\">" + "reg. por p�gina:" + "</td>\n";
            html = html + "<td width=\"50\" align='right'>";
            html = html + RPP;
            html = html + "</td>\n";
            html = html + "</tr>\n";
            html = html + "</table>\n";
        } else {
            html = "<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
            html = html + "<tr class='Paginador'>\n";
            html = html + "<td width=\"10\">" + "[" + 0 + "/" + 0 + "]" + "</td>\n";
            html = html + "<td width=\"10\">" + "[" + 0 + "]" + "</td>\n";
            html = html + "<td width=\"17\">";
            html = html + "<input name='pag1' type='submit' value='1' class='boxNumeradaActiva' disabled>";
            html = html + "</td>\n";
            html = html + "<td width=\"600\" align=\"right\">" + "</td>\n";
            html = html + "</tr>\n";
            html = html + "</table>\n";
        }
        return html;
    }
}
