package mac.sc.sis.mantenimiento.models;

import java.util.ArrayList;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoRemote;
import mac.sc.sis.mantenimiento.controllers.PrmPaginacion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class ModSesion {

    private String idSesion;
    private int idUsuario;
    private String usuario;
    private String nombreUsuario;
    private String tipoUsuario;
    private String tipou;
    private int etapa;
    private String msg;
    private int volver;

    //** CONTROL DE ERRORES VIA MESSAGE BEAN **//	   
    private String tip = "";
    private int tipoTip = 0;
    private boolean conTip = false;

    //MODELOS APLICACION
    private ModMantenimiento mantenimiento;
    private macScSisMantenimientoRemote beanMan;
    private PrmPaginacion pset;
    
    //COLECCIONES APLICACION
    private ArrayList listadoAreas;
    private ArrayList listadoUsuarios;
    

    public ModSesion() {
        idSesion = "";
        idUsuario = 0;
        usuario = "";
        nombreUsuario = "";
        etapa = 0;
        mantenimiento = new ModMantenimiento();
        beanMan = null;
        pset = new PrmPaginacion();
        msg = "";
        tipoUsuario = "normal";
        tipou = "";
        volver = 0;
        listadoAreas = new ArrayList();
        listadoUsuarios = new ArrayList();
    }

    public String getIdSesion() {
        return idSesion;
    }

    public void setIdSesion(String idSesion) {
        this.idSesion = idSesion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public ModMantenimiento getMantenimiento() {
        return mantenimiento;
    }

    public void setMantenimiento(ModMantenimiento mantenimiento) {
        this.mantenimiento = mantenimiento;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public int getEtapa() {
        return etapa;
    }

    public void setEtapa(int etapa) {
        this.etapa = etapa;
    }

    public macScSisMantenimientoRemote getBeanMan() {
        return beanMan;
    }

    public void setBeanMan(macScSisMantenimientoRemote beanMan) {
        this.beanMan = beanMan;
    }

    public PrmPaginacion getPset() {
        return pset;
    }

    public void setPset(PrmPaginacion pset) {
        this.pset = pset;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip, int tipoTip) {
        this.setTipoTip(tipoTip);
        this.setConTip(true);
        this.tip = tip;
    }

    public void cleanTip() {
        this.setTipoTip(0);
        this.setConTip(false);
        this.tip = "";
    }

    public int getTipoTip() {
        return tipoTip;
    }

    public void setTipoTip(int tipoTip) {
        this.tipoTip = tipoTip;
    }

    public boolean isConTip() {
        return conTip;
    }

    public void setConTip(boolean conTip) {
        this.conTip = conTip;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public String getTipou() {
        return tipou;
    }

    public void setTipou(String tipou) {
        this.tipou = tipou;
    }

    public int getVolver() {
        return volver;
    }

    public void setVolver(int volver) {
        this.volver = volver;
    }

    public ArrayList getListadoAreas() {
        return listadoAreas;
    }

    public void setListadoAreas(ArrayList listadoAreas) {
        this.listadoAreas = listadoAreas;
    }

    public ArrayList getListadoUsuarios() {
        return listadoUsuarios;
    }

    public void setListadoUsuarios(ArrayList listadoUsuarios) {
        this.listadoUsuarios = listadoUsuarios;
    }
    
    public ArrayList pedidosPorTipo(ArrayList pedidos, String tipo){
         ArrayList pedidosPorTipo = new ArrayList();
        try{  
        for(int i=0;i<pedidos.size();i++){
            Mantenimiento mant = (Mantenimiento)pedidos.get(i);
            if(mant.getTipo().equals(tipo)){
               pedidosPorTipo.add(mant); 
            }
        }
      }catch(Exception ex){
          EscribeFichero.addError(ex, true);
      }
        return pedidosPorTipo;
    }

    
}
