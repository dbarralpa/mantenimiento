package mac.sc.sis.mantenimiento.models;

import java.util.ArrayList;



public class ModPaginacion implements Cloneable{    
    
    public int          pag         = 1;        
    public ArrayList    idx         = new ArrayList();
    
    public ModAuxiliar getDetalle(int index){
        return(ModAuxiliar)idx.get(index);
    }
    
    public int getTotal(){
        return idx.size();
    }
    
    public int getIndice(int index){
        return getDetalle(index).getNumero();
    }
    
    public String getCodigo(int index){
        return getDetalle(index).getCodigo();
    }
    
    public int getEstadoSeccion(int pg, int rpp){
        int	estado	= 0;
        int rd		= 0;
        int rh		= 0;
        int tmp		= 0;
        rd = (pg-1) * rpp;
        rh = (pg * rpp);
        if(rh>idx.size()){	rh = idx.size(); }
        for(int i=rd; i<rh; i++){
            tmp = getDetalle(i).getEstado();
            if(tmp==1){
                estado = 1;
                break;
            }else if(tmp==2){
                estado = 2;
                break;
            }
        }
        return estado;
    }
    
    public String codigoDesde_Pag(int pg, int rpp){
        int index = 0;
        index = (pg-1) * rpp;
        if(index>=idx.size()){	index = idx.size() - 1; }
        return getDetalle(index).getAuxiliar();
    }
    public String codigoHasta_Pag(int pg, int rpp){
        int index = 0;
        index = (pg * rpp) - 1;
        if(index>=idx.size()){	index = idx.size() - 1; }
        return getDetalle(index).getAuxiliar();
    }
    
    public ModPaginacion copia(){
        Object obj = null;
        try{
            obj = super.clone();
        } catch(CloneNotSupportedException ex) { }
        return (ModPaginacion)obj;
    }
}

