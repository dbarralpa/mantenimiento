package mac.sc.sis.mantenimiento.views;

import java.util.ArrayList;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoBean;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import javax.servlet.http.HttpServletRequest;
import mac.sc.sis.mantenimiento.models.ModEmpExt;
import mac.sc.sis.mantenimiento.models.ModEmplExt;
import mac.sc.sis.mantenimiento.models.ModFalla;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.models.ModIntervalo;
import mac.sc.sis.mantenimiento.models.ModMantenimiento;
import mac.sc.sis.mantenimiento.models.ModMaquina;
import mac.sc.sis.mantenimiento.beans.Trabajo;
import mac.sc.sis.mantenimiento.models.ModArea;
import mac.sc.sis.mantenimiento.models.ModHorario;
import mac.sc.sis.mantenimiento.models.ModPaginacion;
import mac.sc.sis.mantenimiento.models.ModPerMant;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.models.ModTipoFalla;
import mac.sc.sis.mantenimiento.models.ModTurnoMaq;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class VwMantenimiento extends PrmApp {
    
    public VwMantenimiento() {
    }
    
    public static String listarMaquinas(ArrayList vectorMantenimiento, int idMaquina) {
        String html = null;
        html = "<select name=\"maquina\" class=\"Estilo10\" onchange=\"form.submit()\">";
        html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
        for (int i = 0; i < vectorMantenimiento.size(); i++) {
            ModMaquina maquina = (ModMaquina) vectorMantenimiento.get(i);
            html += "<option value='" + String.valueOf(maquina.getIdMaquina()) + "'";
            if (idMaquina == maquina.getIdMaquina()) {
                html += " selected>";
            } else {
                html += ">";
            }
            html += maquina.getUso() + "&nbsp;" + maquina.getNombreCorto() + "</option>";
            maquina = null;
        }
        html += "</select>";
        return html;
    }
    
    public static String listarTodasMaquinas(HttpServletRequest request) {
        String html = null;
        ModSesion Sesion = null;
        try{
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            html = "<select name=\"maquina\" class=\"Estilo10\" onchange=\"submit();\">";
            html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
            for (int i = 0; i < Sesion.getMantenimiento().getListadoMaquinas().size(); i++) {
                ModMaquina maquina = (ModMaquina) Sesion.getMantenimiento().getListadoMaquinas().get(i);
                html += "<option value='" + String.valueOf(maquina.getIdMaquina()) + "'";
                if (Sesion.getMantenimiento().getMaquina().getIdMaquina() == maquina.getIdMaquina()) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += maquina.getNombreCorto() + "</option>";
                maquina = null;
            }
            html += "</select>";
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    public static String listarIntervalos(HttpServletRequest request) {
        String html = null;
        ModSesion Sesion = null;
        try{
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            html = "<select name=\"intervalo\" class=\"Estilo10\">";
            html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
            for (int i = 0; i < Sesion.getMantenimiento().getIntervalos().size(); i++) {
                ModIntervalo intervalo = (ModIntervalo) Sesion.getMantenimiento().getIntervalos().get(i);
                html += "<option value='" + String.valueOf(intervalo.getId()) + "'";
                if (Sesion.getMantenimiento().getMaquina().getModIntervalo().getId() == intervalo.getId()) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += intervalo.getDescripcion() + "</option>";
                intervalo = null;
            }
            html += "</select>";
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String listarTipoFallas(ArrayList vectorTipoFalla, int idTipoFalla) {
        String html = null;
        html = "<select name=\"tipoFalla\" class=\"Estilo10\">";
        html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
        for (int i = 0; i < vectorTipoFalla.size(); i++) {
            ModTipoFalla tipoFalla = (ModTipoFalla) vectorTipoFalla.get(i);
            html += "<option value='" + String.valueOf(tipoFalla.getIdTipoFalla()) + "'";
            if (idTipoFalla == tipoFalla.getIdTipoFalla()) {
                html += " selected>";
            } else {
                html += ">";
            }
            html += tipoFalla.getNombre() + "</option>";
            tipoFalla = null;
        }
        html += "</select>";
        return html;
    }
    
    public static String listarFallas(ArrayList vectorFalla, int idFalla) {
        String html = null;
        html = "<select name=\"falla\" class=\"Estilo10\" onchange=\"form.submit()\">";
        html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
        if (vectorFalla != null) {
            for (int i = 0; i < vectorFalla.size(); i++) {
                ModFalla falla = (ModFalla) vectorFalla.get(i);
                html += "<option value='" + String.valueOf(falla.getIdFalla()) + "' ";
                if (idFalla == falla.getIdFalla()) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += falla.getNombre() + "</option>";
                falla = null;
            }
            if (idFalla == -2) {
                html += "<option value='-2' class='red' selected>" + "OTRAS" + "</option>";
            } else {
                html += "<option value='-2' class='red'>" + "OTRAS" + "</option>";
            }
        }
        html += "</select>";
        return html;
    }
     public static String tipoFallas(HttpServletRequest request) {
        String html = null;
        ModSesion Sesion = null;
        Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
        html = "<select name=\"tipoFalla\" onchange=\"submit()\" class=\"Estilo10\">";
        html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
        for (int i = 0; i < Sesion.getMantenimiento().getListadoTipoFallas().size(); i++) {
            ModTipoFalla tipoFalla = (ModTipoFalla) Sesion.getMantenimiento().getListadoTipoFallas().get(i);
            html += "<option value='" + tipoFalla.getInicial() + "'";
            if (Sesion.getMantenimiento().getTipoFalla().getInicial().equals(tipoFalla.getInicial())) {
                html += " selected>";
            } else {
                html += ">";
            }
            html += tipoFalla.getNombre() + "</option>";
            tipoFalla = null;
        }
        html += "</select>";
        return html;
    }
    
    public static String grillaFallas(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            if (Sesion.getMantenimiento().getListadoFallas().size() > 0) {
                for (int i = 0; i < Sesion.getMantenimiento().getListadoFallas().size(); i++) {
                    ModFalla falla = (ModFalla) Sesion.getMantenimiento().getListadoFallas().get(i);
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantFalla?editFalla=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_editar.gif' border='0' " + verOverTip("<b> Editar Falla: </b>" + falla.getIdFalla()) + " />";
                    html += "</a></td>\n";
                    html += "<td><a href='../../FrmMantFalla?deleteFalla=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_eliminar.gif' border='0' " + verOverTip("<b> Eliminar Falla: </b>" + falla.getIdFalla()) + " />";
                    html += "</a></td>\n";
                    html += "<td>" + falla.getIdFalla() + "</td>\n";
                    html += "<td " + verOverLib("<b> Nombre Falla: </b><br/>" + falla.getNombre() + "<b><br/> Descripci�n Falla: </b><br/>" + verHtmlNull(falla.getDescripcion())) + "align='left'>" + verHtmlNull(falla.getNombre(), 12) + "</td>\n";
                    if (falla.getTipoFalla().getNombre() != null) {
                        html += "<td " + verOverLib("<b> Tipo Falla: </b><br/>" + falla.getTipoFalla().getNombre() + "<b><br/> Descripci�n Tipo Falla: </b><br/>" + falla.getTipoFalla().getDescripcion()) + "align='left'>" + verHtmlNull(falla.getTipoFalla().getNombre(), 15) + "</td>\n";
                    } else {
                        html += "<td>" + "No Clasificado" + "</td>\n";
                    }
                    html += "</tr>\n";
                    falla = null;
                }
            } else {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String grillaTipoFallas(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            if (Sesion.getMantenimiento().getListadoTipoFallas().size() > 0) {
                for (int i = 0; i < Sesion.getMantenimiento().getListadoTipoFallas().size(); i++) {
                    ModTipoFalla tipoFalla = (ModTipoFalla) Sesion.getMantenimiento().getListadoTipoFallas().get(i);
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantTFalla?editTFalla=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_editar.gif' border='0' " + verOverTip("<b> Editar Tipo Falla: </b>" + tipoFalla.getIdTipoFalla()) + " />";
                    html += "</a></td>\n";
                    html += "<td><a href='../../FrmMantTFalla?deleteTFalla=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_eliminar.gif' border='0' " + verOverTip("<b> Eliminar Tipo Falla: </b>" + tipoFalla.getIdTipoFalla()) + " />";
                    html += "</a></td>\n";
                    html += "<td>" + tipoFalla.getIdTipoFalla() + "</td>\n";
                    if (tipoFalla.getDescripcion() != null) {
                        html += "<td " + verOverLib("<b> Descripci�n Tipo Falla: </b><br/>" + tipoFalla.getDescripcion()) + "align='left'>" + tipoFalla.getNombre() + "</td>\n";
                    } else {
                        html += "<td>" + tipoFalla.getNombre() + "</td>\n";
                    }
                    html += "</tr>\n";
                    tipoFalla = null;
                }
            } else {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='5' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='5' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String grillaEmpExternas(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            if (Sesion.getMantenimiento().getListadoEmpExt().size() > 0) {
                for (int i = 0; i < Sesion.getMantenimiento().getListadoEmpExt().size(); i++) {
                    ModEmpExt empExt = (ModEmpExt) Sesion.getMantenimiento().getListadoEmpExt().get(i);
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantEmpExt?editEmpExt=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_editar.gif' border='0' " + verOverTip("<b> Editar Empresa: </b>" + empExt.getVc_rut()) + " />";
                    html += "</a></td>\n";
                    html += "<td><a href='../../FrmMantEmpExt?deleteEmpExt=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_eliminar.gif' border='0' " + verOverTip("<b> Eliminar Empresa: </b>" + empExt.getVc_rut()) + " />";
                    html += "</a></td>\n";
                    html += "<td " + verOverLib("<b> Rut: </b><br/>" + empExt.getVc_rut()) + "align='left'>" + empExt.getVc_rut() + "</td>\n";
                    html += "<td " + verOverLib("<b> Nombre: </b><br/>" + empExt.getVc_nombre()) + "align='left'>" + verHtmlNull(empExt.getVc_nombre(), 27) + "</td>\n";
                    html += "<td " + verOverLib("<b> Direcci�n: </b><br/>" + empExt.getVc_direccion()) + "align='left'>" + verHtmlNull(empExt.getVc_direccion(), 54) + "</td>\n";
                    html += "</tr>\n";
                    empExt = null;
                }
            } else {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String grillaPerMant(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            if (Sesion.getMantenimiento().getListadoPerMant().size() > 0) {
                for (int i = 0; i < Sesion.getMantenimiento().getListadoPerMant().size(); i++) {
                    ModPerMant personal = (ModPerMant) Sesion.getMantenimiento().getListadoPerMant().get(i);
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantPerMant?editPerMant=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_editar.gif' border='0' " + verOverTip("<b> Editar Personal: </b>" + personal.getVc_rut()) + " />";
                    html += "</a></td>\n";
                    html += "<td><a href='../../FrmMantPerMant?deletePerMant=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_eliminar.gif' border='0' " + verOverTip("<b> Eliminar Personal: </b>" + personal.getVc_rut()) + " />";
                    html += "</a></td>\n";
                    html += "<td " + verOverLib("<b> Rut: </b><br/>" + personal.getVc_rut()) + "align='left'>" + personal.getVc_rut() + "</td>\n";
                    html += "<td " + verOverLib("<b> Nombre: </b><br/>" + personal.getVc_nombre()) + "align='left'>" + verHtmlNull(personal.getVc_nombre(), 27) + "</td>\n";
                    html += "<td " + verOverLib("<b> Apellido: </b><br/>" + personal.getVc_apellido()) + "align='left'>" + verHtmlNull(personal.getVc_apellido(), 54) + "</td>\n";
                    html += "</tr>\n";
                    personal = null;
                }
            } else {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String grillaMaquinas(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            if (Sesion.getMantenimiento().getListadoMaquinas().size() > 0) {
                for (int i = 0; i < Sesion.getMantenimiento().getListadoMaquinas().size(); i++) {
                    ModMaquina maquina = (ModMaquina) Sesion.getMantenimiento().getListadoMaquinas().get(i);
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantMaquina?editMaquina=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_editar.gif' border='0' " + verOverTip("<b> Editar M�quina: </b>" + maquina.getIdMaquina()) + " />";
                    html += "</a></td>\n";
                    html += "<td><a href='../../FrmMantMaquina?deleteMaquina=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_eliminar.gif' border='0' " + verOverTip("<b> Eliminar M�quina: </b>" + maquina.getIdMaquina()) + " />";
                    html += "</a></td>\n";
                    html += "<td>" + maquina.getIdMaquina() + "</td>\n";
                    html += "<td " + verOverLib("<b> Uso: </b><br/>" + maquina.getUso()) + "align='left'>" + verHtmlNull(maquina.getUso(), 25) + "</td>\n";
                    html += "<td " + verOverLib("<b> Nombre: </b><br/>" + maquina.getNombreCorto()) + "align='left'>" + verHtmlNull(maquina.getNombreCorto(), 25) + "</td>\n";
                    html += "<td " + verOverLib("<b> Turno: </b><br/>" + maquina.getTurnoMaq().getNombreTurnoMaq()) + "align='left'>" + verHtmlNull(maquina.getTurnoMaq().getNombreTurnoMaq(), 30) + "</td>\n";
                    html += "</tr>\n";
                    maquina = null;
                }
            } else {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='7' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String listarPedidosMantenimiento(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            ModPaginacion ver = null;
            
            ver = Sesion.getPset().getDetalle("MANT");
            
            int tmp = 0;
            int ri = 0;
            int rt = 0;
            
            if (ver.getTotal() == 0) {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='10' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            } else {
                ri = Sesion.getPset().RPP * (ver.pag - 1);
                rt = Sesion.getPset().RPP * ver.pag;
                if (rt > ver.getTotal()) {
                    rt = ver.getTotal();
                }
                for (int i = ri; i < rt; i++) {
                    int indice;
                    if (Sesion.getMantenimiento().isOrdenAscNew()) {
                        indice = i;
                    } else {
                        indice = ver.getTotal() - (i + 1);
                    }
                    Mantenimiento obj = (Mantenimiento) Sesion.getMantenimiento().getListadoPedidosMantenimientos().get(ver.getIndice(indice));
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantenimiento?ver=" + ver.getIndice(indice) + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_ver.gif' border='0' " + verOverTip("<b>Ver O.M: </b>" + obj.getCodigoMantenimiento()) + " />";
                    html += "</a></td>\n";
                    html += "<td>" + obj.getCodigoMantenimiento() + "</td>\n";
                    html += "<td " + verOverLib("<b> Especialidad: </b><br/>" + obj.getNombreArea()) + "align='left'>" + verHtmlNull(obj.getNombreArea(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> M�quina: </b><br/>" + obj.getNombreMaquina()) + "align='left'>" + verHtmlNull(obj.getNombreMaquina(), 15) + "</td>\n";
                    if (obj.getNombreFalla() != null) {
                        html += "<td " + verOverLib("<b> Falla: </b><br/>" + obj.getNombreFalla() + "<br/><b> Descripci�n Falla: </b><br/>" + verHtmlNull(obj.getDescFalla())) + "align='left'>" + verHtmlNull(obj.getNombreFalla(), 15) + "</td>\n";
                    } else {
                        html += "<td>" + "Otras" + "</td>\n";
                    }
                    if (obj.getDescTipoFalla() != null) {
                        html += "<td " + verOverLib("<b> Descripci�n Tipo Falla: </b><br/>" + verHtmlNull(obj.getDescTipoFalla())) + "align='left'>" + verHtmlNull(obj.getDescTipoFalla()) + "</td>\n";
                    } else {
                        html += "<td>" + "No Clasificado" + "</td>\n";
                    }
                    html += "<td " + verOverLib("<b> Observaci�n: </b><br/>" + obj.getObservacionFalla()) + "align='left'>" + verHtmlNull(obj.getObservacionFalla(), 15) + "</td>\n";
                    html += "<td>";
                    if (obj.getFechaHoy() == null) {
                        html += "&nbsp;";
                    } else {
                        html += obj.getFechaHoy().substring(0, 10);
                    }
                    html += "</td>\n";
                    html += "</tr>\n";
                    obj = null;
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='9' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------
    public static String listarPedidosMantePreventivo(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            ModPaginacion ver = null;
            
            ver = Sesion.getPset().getDetalle("MANT1");
            
            int tmp = 0;
            int ri = 0;
            int rt = 0;
            
            if (ver.getTotal() == 0) {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='10' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            } else {
                ri = Sesion.getPset().RPP * (ver.pag - 1);
                rt = Sesion.getPset().RPP * ver.pag;
                if (rt > ver.getTotal()) {
                    rt = ver.getTotal();
                }
                for (int i = ri; i < rt; i++) {
                    int indice;
                    if (Sesion.getMantenimiento().isOrdenAscPreventivo()) {
                        indice = i;
                    } else {
                        indice = ver.getTotal() - (i + 1);
                    }
                    Mantenimiento obj = (Mantenimiento) Sesion.getMantenimiento().getListadoPedidosMantenPreventivos().get(ver.getIndice(indice));
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantenimiento?ver1=" + ver.getIndice(indice) + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_ver.gif' border='0' " + verOverTip("<b>Ver O.T: </b>" + obj.getCodigoMantenimiento()) + " />";
                    html += "</a></td>\n";
                    html += "<td>" + obj.getCodigoMantenimiento() + "</td>\n";
                    html += "<td " + verOverLib("<b> �rea: </b><br/>" + obj.getNombreArea()) + "align='left'>" + verHtmlNull(obj.getNombreArea(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> M�quina: </b><br/>" + obj.getNombreMaquina()) + "align='left'>" + verHtmlNull(obj.getNombreMaquina(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Fecha mantenci�n: </b><br/>" + obj.getFechaMantePreventivo()) + "align='left'>" + verHtmlNull(obj.getFechaMantePreventivo(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Fecha inicio: </b><br/>" + obj.getFechaDetencionEquipo()) + "align='left'>" + verHtmlNull(obj.getFechaDetencionEquipo(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Fecha T�rmino: </b><br/>" + obj.getFechaEntregaEquipo()) + "align='left'>" + verHtmlNull(obj.getFechaEntregaEquipo(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Ejecutor: </b><br/>" + obj.getEjecutor1()) + "align='left'>" + verHtmlNull(obj.getEjecutor1(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Observaci�n: </b><br/>" + obj.getObservacionCerrar()) + "align='left'>" + verHtmlNull(obj.getObservacionCerrar(), 15) + "</td>\n";
                    html += "</tr>\n";
                    obj = null;
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='10' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------------------
    public static String listarSeguimientoPreventivo(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            ModPaginacion ver = null;
            
            ver = Sesion.getPset().getDetalle("MANT1");
            
            int tmp = 0;
            int ri = 0;
            int rt = 0;
            
            if (ver.getTotal() == 0) {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='10' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            } else {
                ri = Sesion.getPset().RPP * (ver.pag - 1);
                rt = Sesion.getPset().RPP * ver.pag;
                if (rt > ver.getTotal()) {
                    rt = ver.getTotal();
                }
                for (int i = ri; i < rt; i++) {
                    int indice;
                    if (Sesion.getMantenimiento().isOrdenAscNew()) {
                        indice = i;
                    } else {
                        indice = ver.getTotal() - (i + 1);
                    }
                    Mantenimiento obj = (Mantenimiento) Sesion.getMantenimiento().getListadoPricipal().get(ver.getIndice(indice));
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantenimientoPreventivo?ver2=" + ver.getIndice(indice) + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_entrar.gif' border='0' " + verOverTip("<b>Ver O.T: </b>" + obj.getCodigoMantenimiento()) + " />";
                    html += "</a></td>\n";
                    html += "<td>" + obj.getCodigoMantenimiento() + "</td>\n";
                    html += "<td " + verOverLib("<b> �rea: </b><br/>" + obj.getNombreArea()) + "align='left'>" + verHtmlNull(obj.getNombreArea(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> M�quina: </b><br/>" + obj.getNombreMaquina()) + "align='left'>" + verHtmlNull(obj.getNombreMaquina(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Fecha mantenci�n: </b><br/>" + obj.getFechaMantePreventivo()) + "align='left'>" + verHtmlNull(obj.getFechaMantePreventivo(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Fecha inicio: </b><br/>" + obj.getFechaDetencionEquipo()) + "align='left'>" + verHtmlNull(obj.getFechaDetencionEquipo(), 13) + "</td>\n";
                    html += "<td " + verOverLib("<b> Fecha T�rmino: </b><br/>" + obj.getFechaEntregaEquipo()) + "align='left'>" + verHtmlNull(obj.getFechaEntregaEquipo(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Ejecutor: </b><br/>" + obj.getEjecutor1()) + "align='left'>" + verHtmlNull(obj.getEjecutor1(), 15) + "</td>\n";
                    html += "<td " + verOverLib("<b> Observaci�n: </b><br/>" + obj.getObservacionCerrar()) + "align='left'>" + verHtmlNull(obj.getObservacionCerrar(), 15) + "</td>\n";
                    html += "</tr>\n";
                    obj = null;
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='10' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------
    
    public static String listarAreas(ArrayList vectorAreas, int idArea) {
        return listarAreas(vectorAreas, idArea, true);
    }
    
    public static String listarAreas(ArrayList vectorAreas, int idArea, boolean conSubmit) {
        String html = null;
        html = "<select name=\"area\" class=\"Estilo10\" ";
        if (conSubmit) {
            html += "onchange=\"form.submit()\">";
        } else {
            html += ">";
        }
        html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
        for (int i = 0; i < vectorAreas.size(); i++) {
            ModArea area = (ModArea) vectorAreas.get(i);
            html += "<option value='" + String.valueOf(area.getIdArea()) + "'";
            if (area.getIdArea() == idArea) {
                html += " selected>";
            } else {
                html += ">";
            }
            html += area.getNombre() + "</option>";
            area = null;
        }
        html += "</select>";
        return html;
    }
    
    public static String listarUsuarios(ArrayList listadoUsuarios, int idUsuario) {
        String html = null;
        html = "<select name=\"usuario\" class=\"Estilo10\">";
        html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
        for (int i = 0; i < listadoUsuarios.size(); i++) {
            Trabajo trabajo = (Trabajo) listadoUsuarios.get(i);
            html += "<option value='" + String.valueOf(trabajo.getIdUsuario()) + "'";
            if (trabajo.getIdUsuario() == idUsuario) {
                html += " selected>";
            } else {
                html += ">";
            }
            html += trabajo.getNombreUsuario() + "&nbsp;" + trabajo.getApellidoUsuario() + "</option>";
            trabajo = null;
        }
        html += "</select>";
        return html;
    }
    
     public static String listarPreventivasPorBusqueda(HttpServletRequest request) {
        String html = null;
        ModSesion Sesion = null;
        Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
        html = "<select name=\"maquinaPreventiva\" class=\"Estilo10\">";
        html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
        for (int i = 0; i < Sesion.getMantenimiento().getListadoMaquinas().size(); i++) {
            ModMaquina maquina = (ModMaquina) Sesion.getMantenimiento().getListadoMaquinas().get(i);
            html += "<option value='" + String.valueOf(maquina.getIdMaquina()) + "'";
            if (maquina.getIdMaquina() == Sesion.getMantenimiento().getIdDetalle1()) {
                html += " selected>";
            } else {
                html += ">";
            }
            html +=  maquina.getNombre() + "</option>";
            maquina = null;
        }
        html += "</select>";
        return html;
    }
    
    public static String listarPedidosXtipo(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            ModPaginacion ver = null;
            ver = Sesion.getPset().getDetalle("SOLI");
            
            int tmp = 0;
            int ri = 0;
            int rt = 0;
            
            if (ver.getTotal() == 0) {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='14' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            } else {
                ri = Sesion.getPset().RPP * (ver.pag - 1);
                rt = Sesion.getPset().RPP * ver.pag;
                if (rt > ver.getTotal()) {
                    rt = ver.getTotal();
                }
                for (int i = ri; i < rt; i++) {
                    int indice;
                    if (Sesion.getMantenimiento().isOrdenAsc()) {
                        indice = i;
                    } else {
                        indice = ver.getTotal() - (i + 1);
                    }
                    
                    Mantenimiento obj = (Mantenimiento) Sesion.getMantenimiento().getListadoPricipal().get(ver.getIndice(indice));
                    if (obj.getEstado().equals("NN")) {
                        html += "<tr class='FilaNormal'>\n";
                    } else if (obj.getEstado().equals("DG")) {
                        html += "<tr class='FilaActivo'>\n";
                    } else if (obj.getEstado().equals("AQ")) {
                        html += "<tr class='FilaTemporal'>\n";
                    } else if (obj.getEstado().equals("SE")) {
                        html += "<tr class='FilaDisable'>\n";
                    } else if (obj.getEstado().equals("SI")) {
                        html += "<tr class='FilaNuevo'>\n";
                    } else if (obj.getEstado().equals("VB")) {
                        html += "<tr class='FilaModificado'>\n";
                    } else if (obj.getEstado().equals("NC")) {
                        html += "<tr class='FilaAnulado'>\n";
                    } else {
                        html += "<tr class='FilaNormal'>\n";
                    }
                    html += "<td><a href='../../FrmNuevoStatus?detGrilla=" + ver.getIndice(indice) + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_entrar.gif' border='0' " + verOverTip("<b>O.M: </b>" + obj.getCodigoMantenimiento()) + " />";
                    html += "</a></td>\n";
                    String overLibUsuarioMant = "";
                    if(obj.getNombreUsuarioMantenimiento() != null && !obj.getNombreUsuarioMantenimiento().trim().equals("")){
                        overLibUsuarioMant = "<br/><b>Cliente: </b><br/>" + obj.getNombreUsuarioMantenimiento();
                    }
                    html += "<td " + verOverLib("<b>O.M: </b><br/>" + obj.getCodigoMantenimiento() + overLibUsuarioMant) + " align='left' >" + obj.getCodigoMantenimiento() + "</td>\n";
                    html += "<td " + verOverLib("<b>�rea: </b><br/>" + verHtmlNull(obj.getNombreArea())) + " align='left' >" + verHtmlNull(obj.getNombreArea(), 6) + "</td>\n";
                    html += "<td " + verOverLib("<b>M�quina: </b><br/>" + verHtmlNull(obj.getNombreMaquina())) + " align='left' >";
                    html += verHtmlNull(obj.getNombreMaquina(), 6) + "</td>\n";
                    if (obj.getNombreFalla() != null) {
                        String nombreTipoFalla = "";
                        if (obj.getNombreTipoFalla() != null) {
                            nombreTipoFalla = obj.getNombreTipoFalla();
                        } else {
                            nombreTipoFalla = "No Clasificado";
                        }
                        html += "<td " + verOverLib("<b>Falla: </b><br/>" + verHtmlNull(obj.getNombreFalla()) + "<br/><b>Observaci�n: </b><br/>" + verHtmlNull(obj.getObservacionFalla()) + "<br/><b>Tipo Falla: </b><br/>" + nombreTipoFalla + "<br/><b>Descripci�n Tipo Falla: </b><br/>" + verHtmlNull(obj.getObservacionFalla())) + "align='left'>" + verHtmlNull(obj.getNombreFalla(), 6) + "</td>\n";
                    } else {
                        html += "<td " + verOverLib("<b>Falla: </b><br/>" + "Otras" + "<br/><b>Observaci�n: </b><br/>" + verHtmlNull(obj.getObservacionFalla())) + "align='left'>" + verHtmlNull(obj.getObservacionFalla(), 6) + "</td>\n";
                    }
                    if (obj.getFechaHoy() == null) {
                        html += "<td>&nbsp;</td>\n";
                    } else {
                        html += "<td " + verOverLib("<b>Fecha: </b><br/>" + obj.getFechaHoy().substring(0, 10)) + "align='left'>" + verHtmlNull(obj.getFechaHoy().substring(0, 10)) + "</td>\n";
                    }
                    if (obj.getFechaDiagnostico() == null) {
                        html += "<td>&nbsp;</td>\n";
                    } else {
                        String overLibEncargado = "";
                        if(obj.getPersonalMant().getVc_nombre() != null && obj.getPersonalMant().getVc_apellido() != null){
                            overLibEncargado = "<b>Encargado: </b><br/>" + obj.getPersonalMant().getVc_nombre() + " " + obj.getPersonalMant().getVc_apellido() + "<br/>";
                        }
                        html += "<td " + verOverLib("<b>Fecha: </b><br/>" + obj.getFechaDiagnostico().substring(0, 10) + "<br/>" + overLibEncargado + "<b>Observaci�n: </b><br/>" + verHtmlNull(obj.getObservacionDiagnostico())) + "align='left'>" + verHtmlNull(obj.getFechaDiagnostico().substring(0, 10)) + "</td>\n";
                    }
                    if (obj.getFechaAdquisicion() == null) {
                        html += "<td>&nbsp;</td>\n";
                    } else {
                        html += "<td " + verOverLib("<b>Fecha: </b><br/>" + obj.getFechaAdquisicion().substring(0, 10) + "<br/><b>Observaci�n: </b><br/>" + verHtmlNull(obj.getObservacionAdquisicion())) + "align='left'>" + verHtmlNull(obj.getFechaAdquisicion().substring(0, 10)) + "</td>\n";
                    }
                    if (obj.getFechaSevicioExterno() == null) {
                        html += "<td>&nbsp;</td>\n";
                    } else {
                        String overLibServExt = "";
                        if(obj.getEmpExt().getVc_nombre() != null && obj.getEmplEmt().getVc_nombre() != null){
                            overLibServExt  = "<b>Empresa Externa: </b><br/>" + obj.getEmpExt().getVc_nombre() + "<br/>";
                            overLibServExt += "<b>Empleado Externo: </b><br/>" + obj.getEmplEmt().getVc_nombre() + " " + obj.getEmplEmt().getVc_apellido() + "<br/>";
                        }
                        html += "<td " + verOverLib("<b>Fecha: </b><br/>" + obj.getFechaSevicioExterno().substring(0, 10) + "<br/>" + overLibServExt + "<b>Observaci�n: </b><br/>" + obj.getObservacionSevicioExterno()) + "align='left'>" + verHtmlNull(obj.getFechaSevicioExterno().substring(0, 10)) + "</td>\n";
                    }
                    if (obj.getFechaMantenimientoCorrectivo() == null) {
                        html += "<td>&nbsp;</td>\n";
                    } else {
                        String overLibEncargado = "";
                        if(obj.getPersonalMant().getVc_nombre() != null && obj.getPersonalMant().getVc_apellido() != null){
                            overLibEncargado = "<b>Encargado: </b><br/>" + obj.getPersonalMant().getVc_nombre() + " " + obj.getPersonalMant().getVc_apellido() + "<br/>";
                        }
                        html += "<td " + verOverLib("<b>Fecha: </b><br/>" + obj.getFechaMantenimientoCorrectivo().substring(0, 10) + "<br/>" + overLibEncargado + "<b>Observaci�n: </b><br/>" + obj.getObservacionMantenimientoCorrectivo()) + "align='left'>" + verHtmlNull(obj.getFechaMantenimientoCorrectivo().substring(0, 10)) + "</td>\n";
                    }
                    if (obj.getFechaVistoBueno() == null) {
                        html += "<td>&nbsp;</td>\n";
                    } else {
                        html += "<td " + verOverLib("<b>Fecha: </b><br/>" + obj.getFechaVistoBueno().substring(0, 10) + "<br/><b>Observaci�n: </b><br/>" + verHtmlNull(obj.getObservacionVistoBueno())) + "align='left'>" + verHtmlNull(obj.getFechaVistoBueno().substring(0, 10)) + "</td>\n";
                    }
                    if (obj.getFechaNoConformidad() == null) {
                        html += "<td>&nbsp;</td>\n";
                    } else {
                        html += "<td " + verOverLib("<b>Fecha: </b><br/>" + obj.getFechaNoConformidad().substring(0, 10) + "<br/><b>Observaci�n: </b><br/>" + verHtmlNull(obj.getObservacionNoConformidad())) + "align='left'>" + verHtmlNull(obj.getFechaNoConformidad().substring(0, 10)) + "</td>\n";
                    }
                    html += "</tr>\n";
                    obj = null;
                }
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='14' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String listarEstados(HttpServletRequest request) {
        String html = "";
        int idEstado = 0;
        boolean editable = true;
        String observacion = "";
        String fecha = "";
        ModSesion Sesion = null;
        ArrayList listaEstados = null;
        Mantenimiento detalle = null;
        
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            listaEstados = Sesion.getMantenimiento().getListaEstados();
            detalle = Sesion.getMantenimiento().getDetalle();
            
            if (detalle.getFechaDiagnostico() != null) {
                idEstado = 1;
                observacion = detalle.getObservacionDiagnostico();
                fecha = detalle.getFechaDiagnostico();
            }
            if (detalle.getFechaAdquisicion() != null) {
                idEstado = 2;
                observacion = detalle.getObservacionAdquisicion();
                fecha = detalle.getFechaAdquisicion();
            }
            if (detalle.getFechaSevicioExterno() != null) {
                idEstado = 3;
                observacion = detalle.getObservacionSevicioExterno();
                fecha = detalle.getFechaSevicioExterno();
            }
            if (detalle.getFechaMantenimientoCorrectivo() != null) {
                idEstado = 4;
                observacion = detalle.getObservacionMantenimientoCorrectivo();
                fecha = detalle.getFechaMantenimientoCorrectivo();
            }
            if (detalle.getFechaVistoBueno() != null) {
                idEstado = 5;
                observacion = detalle.getObservacionVistoBueno();
                fecha = detalle.getFechaVistoBueno();
                editable = false;
            }
            if (detalle.getFechaNoConformidad() != null) {
                idEstado = 6;
                observacion = detalle.getObservacionNoConformidad();
                fecha = detalle.getFechaNoConformidad();
                editable = false;
            }
            
            html = "<tr>";
            for (int i = 0; i < listaEstados.size(); i++) {
                Trabajo trabajo = (Trabajo) listaEstados.get(i);
                if (Sesion.getTipoUsuario().equals("normal")) {
                    if (idEstado == 0) {
                        html += "<td class=\"NoSelectedOn\">";
                        if ((trabajo.getIdEstado() == 5 || trabajo.getIdEstado() == 6) && (editable)) {
                            html += "<a href='../../FrmNuevoStatus?changeState=" + trabajo.getIdEstado() + "'>";
                            html += trabajo.getNombreEstado() + "</a></td>";
                        } else {
                            html += trabajo.getNombreEstado() + "</td>";
                        }
                    } else if (trabajo.getIdEstado() == idEstado) {
                        html += "<td class=\"Selected\" " + verOverLib("<b>Fecha " + trabajo.getNombreEstado() + ": </b><br/>" + fecha.substring(0, 10) + "<br/><b>Observaci�n " + trabajo.getNombreEstado() + ": </b><br/>" + verHtmlNull(observacion)) + ">" + trabajo.getNombreEstado() + "</td>";
                    } else {
                        if (trabajo.getIdEstado() > idEstado) {
                            html += "<td class=\"NoSelectedOn\" >";
                            if ((trabajo.getIdEstado() == 5 || trabajo.getIdEstado() == 6) && (editable)) {
                                html += "<a href='../../FrmNuevoStatus?changeState=" + trabajo.getIdEstado() + "'>";
                                html += trabajo.getNombreEstado() + "</a></td>";
                            } else {
                                html += trabajo.getNombreEstado() + "</td>";
                            }
                        } else {
                            html += "<td class=\"NoSelectedOff\" >";
                            html += trabajo.getNombreEstado() + "</td>";
                        }
                    }
                } else if(Sesion.getUsuario().equals("corrego") && Sesion.getNombreUsuario().equals(detalle.getNombreUsuarioMantenimiento())) {
                    //} else if(Sesion.getUsuario().equals("corrego")) {
                    if (idEstado == 0) {
                        html += "<td class=\"NoSelectedOn\">";
                        html += "<a href='../../FrmNuevoStatus?changeState=" + trabajo.getIdEstado() + "'>";
                        html += trabajo.getNombreEstado() + "</a></td>";
                    } else if (trabajo.getIdEstado() == idEstado) {
                        html += "<td class=\"Selected\" " + verOverLib("<b>Fecha " + trabajo.getNombreEstado() + ": </b><br/>" + fecha.substring(0, 10) + "<br/><b>Observaci�n " + trabajo.getNombreEstado() + ": </b><br/>" + verHtmlNull(observacion)) + ">" + trabajo.getNombreEstado() + "</td>";
                    } else {
                        if (trabajo.getIdEstado() > idEstado) {
                            html += "<td class=\"NoSelectedOn\" >";
                            html += "<a href='../../FrmNuevoStatus?changeState=" + trabajo.getIdEstado() + "'>";
                            html += trabajo.getNombreEstado() + "</a></td>";
                        } else {
                            html += "<td class=\"NoSelectedOff\" >";
                            html += trabajo.getNombreEstado() + "</td>";
                        }
                    }
                }else{
                    if (idEstado == 0) {
                        html += "<td class=\"NoSelectedOn\">";
                        if (trabajo.getIdEstado() != 5 && trabajo.getIdEstado() != 6) {
                            html += "<a href='../../FrmNuevoStatus?changeState=" + trabajo.getIdEstado() + "'>";
                            html += trabajo.getNombreEstado() + "</a></td>";
                        } else {
                            html += trabajo.getNombreEstado() + "</td>";
                        }
                    } else if (trabajo.getIdEstado() == idEstado) {
                        html += "<td class=\"Selected\" " + verOverLib("<b>Fecha " + trabajo.getNombreEstado() + ": </b><br/>" + fecha.substring(0, 10) + "<br/><b>Observaci�n " + trabajo.getNombreEstado() + ": </b><br/>" + verHtmlNull(observacion)) + ">" + trabajo.getNombreEstado() + "</td>";
                    } else {
                        if (trabajo.getIdEstado() > idEstado) {
                            html += "<td class=\"NoSelectedOn\" >";
                            if (trabajo.getIdEstado() != 5 && trabajo.getIdEstado() != 6) {
                                html += "<a href='../../FrmNuevoStatus?changeState=" + trabajo.getIdEstado() + "'>";
                                html += trabajo.getNombreEstado() + "</a></td>";
                            } else {
                                html += trabajo.getNombreEstado() + "</td>";
                            }
                        } else {
                            html += "<td class=\"NoSelectedOff\" >";
                            html += trabajo.getNombreEstado() + "</td>";
                        }
                    }
                }
                trabajo = null;
            }
            html += "</tr>";
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='10' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        
        return html;
    }
    
    public static String comboEmprExt(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            html = "<select name=\"cmbEmpExt\" class=\"Estilo10\" onchange=\"form.submit()\">";
            html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
            for (int i = 0; i < Sesion.getMantenimiento().getListadoEmpExt().size(); i++) {
                ModEmpExt empExt = (ModEmpExt) Sesion.getMantenimiento().getListadoEmpExt().get(i);
                html += "<option value='" + String.valueOf(empExt.getId()) + "'";
                if (empExt.getId() == Sesion.getMantenimiento().getEmpExt().getId()) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += empExt.getVc_nombre() + "</option>";
                empExt = null;
            }
            html += "</select>";
        } catch (Exception ex) {
            html = "<table border='1'>\n";
            html += "<tr class='FilaNormal'>\n" + "<td align='center'>\n";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
            html += "</table>\n";
        }
        return html;
    }
    
    public static String comboTurnoMaq(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            html = "<select name=\"cmbTMaq\" class=\"Estilo10\" >";
            html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
            for (int i = 0; i < Sesion.getMantenimiento().getListadoTurnosMaquina().size(); i++) {
                ModTurnoMaq turnoMaq = (ModTurnoMaq) Sesion.getMantenimiento().getListadoTurnosMaquina().get(i);
                html += "<option value='" + String.valueOf(turnoMaq.getId()) + "'";
                if (turnoMaq.getIdTurnoMaq() == Sesion.getMantenimiento().getMaquina().getTurnoMaq().getIdTurnoMaq()) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += turnoMaq.getNombreTurnoMaq() + "</option>";
                turnoMaq = null;
            }
            html += "</select>";
        } catch (Exception ex) {
            html = "<table border='1'>\n";
            html += "<tr class='FilaNormal'>\n" + "<td align='center'>\n";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
            html += "</table>\n";
        }
        return html;
    }
    
    public static String comboPerMant(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            html = "<select name=\"cmbPerMant\" class=\"Estilo10\">";
            html += "<option value='-1'>" + "<----Seleccione---->" + "</option>";
            for (int i = 0; i < Sesion.getMantenimiento().getListadoPerMant().size(); i++) {
                ModPerMant personal = (ModPerMant) Sesion.getMantenimiento().getListadoPerMant().get(i);
                html += "<option value='" + String.valueOf(personal.getId()) + "'";
                if (personal.getId() == Sesion.getMantenimiento().getPerMant().getId()) {
                    html += " selected>";
                } else {
                    html += ">";
                }
                html += personal.getVc_nombre() + " " + personal.getVc_apellido() + "</option>";
                personal = null;
            }
            html += "</select>";
        } catch (Exception ex) {
            html = "<table border='1'>\n";
            html += "<tr class='FilaNormal'>\n" + "<td align='center'>\n";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
            html += "</table>\n";
        }
        return html;
    }
    
    public static String grillaEmplExt(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            if (Sesion.getMantenimiento().getEmpExt().getEmpleados().size() > 0) {
                for (int i = 0; i < Sesion.getMantenimiento().getEmpExt().getEmpleados().size(); i++) {
                    ModEmplExt empleado = (ModEmplExt) Sesion.getMantenimiento().getEmpExt().getEmpleados().get(i);
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantEmplExt?editEmplExt=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_editar.gif' border='0' " + verOverTip("<b> Editar Empleado: </b>" + empleado.getVc_rut()) + " />";
                    html += "</a></td>\n";
                    html += "<td><a href='../../FrmMantEmplExt?deleteEmplExt=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_eliminar.gif' border='0' " + verOverTip("<b> Eliminar Empleado: </b>" + empleado.getVc_rut()) + " />";
                    html += "</a></td>\n";
                    html += "<td " + verOverLib("<b> Rut: </b><br/>" + empleado.getVc_rut()) + "align='left'>" + empleado.getVc_rut() + "</td>\n";
                    html += "<td " + verOverLib("<b> Nombre: </b><br/>" + empleado.getVc_nombre()) + "align='left'>" + verHtmlNull(empleado.getVc_nombre(), 36) + "</td>\n";
                    html += "<td " + verOverLib("<b> Direcci�n: </b><br/>" + empleado.getVc_apellido()) + "align='left'>" + verHtmlNull(empleado.getVc_apellido(), 36) + "</td>\n";
                    html += "</tr>\n";
                    empleado = null;
                }
            } else {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String grillaTurnosMaquina(HttpServletRequest request) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            if (Sesion.getMantenimiento().getListadoTurnosMaquina().size() > 0) {
                for (int i = 0; i < Sesion.getMantenimiento().getListadoTurnosMaquina().size(); i++) {
                    ModTurnoMaq turnoMaquina = (ModTurnoMaq) Sesion.getMantenimiento().getListadoTurnosMaquina().get(i);
                    html += "<tr class='FilaNormal' onmouseover=\"this.style.backgroundColor='#F7EFD6';\" onmouseout=\"this.style.backgroundColor ='#FFFFFF';\">\n";
                    html += "<td><a href='../../FrmMantTMaq?editTMaq=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_editar.gif' border='0' " + verOverTip("<b> Editar Turno M�quina: </b>" + turnoMaquina.getIdTurnoMaq()) + " />";
                    html += "</a></td>\n";
                    html += "<td><a href='../../FrmMantTMaq?deleteTMaq=" + i + "'>";
                    html += "<img src='../../Imagenes/btn17x17/btn_eliminar.gif' border='0' " + verOverTip("<b> Eliminar Turno M�quina: </b>" + turnoMaquina.getIdTurnoMaq()) + " />";
                    html += "</a></td>\n";
                    html += "<td>" + turnoMaquina.getIdTurnoMaq() + "</td>\n";
                    html += "<td " + verOverLib("<b> Nombre Turno M�quina: </b><br/>" + turnoMaquina.getNombreTurnoMaq()) + "align='left'>" + PrmApp.verHtmlNull(turnoMaquina.getNombreTurnoMaq()) + "</td>\n";
                    html += "<td " + verOverLib("<b> Descripci�n Turno M�quina: </b><br/>" + turnoMaquina.getDescTurnoMaq()) + "align='left'>" + PrmApp.verHtmlNull(turnoMaquina.getDescTurnoMaq()) + "</td>\n";
                    html += "</tr>\n";
                    turnoMaquina = null;
                }
            } else {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='6' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public static String checksBoxHorario(HttpServletRequest request) {
        return checksBoxHorario(request, false);
    }
    
    public static String checksBoxHorario(HttpServletRequest request, boolean noEditable) {
        String html = "";
        String html2 = "";
        String html3 = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            html += "<tr><td colspan=\"3\" height=\"2\">&nbsp;</td></tr>" + "\n";
            if (noEditable) {
                html2 = " readonly=\"yes\" ";
                html3 = " disabled=\"disabled\" ";
            }
            if (Sesion.getMantenimiento().getTurnoMaquina().getHorarios().size() > 0) {
                for (int i = 0; i < Sesion.getMantenimiento().getTurnoMaquina().getHorarios().size(); i++) {
                    ModHorario horario = (ModHorario) Sesion.getMantenimiento().getTurnoMaquina().getHorarios().get(i);
                    html += "<tr>" + "\n";
                    html += "<td width=\"30\">" + "&nbsp;" + "</td>" + "\n";
                    if (horario.isEstado()) {
                        html += "<td valign=\"middle\" align=\"left\" width=\"100\"><input type=\"checkbox\" name=\"dia" + horario.getDiaHorario() + "\" id=\"dia" + horario.getDiaHorario() + "\" value=\"ON\" checked=\"checked\" " + html3 + " />" + horario.getDiaHorario() + "</td>" + "\n";
                    } else {
                        html += "<td valign=\"middle\" align=\"left\" width=\"100\"><input type=\"checkbox\" name=\"dia" + horario.getDiaHorario() + "\" id=\"dia" + horario.getDiaHorario() + "\" value=\"ON\" " + html3 + " />" + horario.getDiaHorario() + "</td>" + "\n";
                    }
                    html += "<td>" + "\n";
                    html += "<table>" + "\n";
                    html += "<tr>" + "\n";
                    html += "<td width=\"60\" align=\"right\">Inicio</td>" + "\n";
                    html += "<td width=\"10\">:</td>" + "\n";
                    html += "<td width=\"40\" align=\"left\"><input type=\"text\" name=\"inicio" + horario.getDiaHorario() + "\" id=\"inicio" + horario.getDiaHorario() + "\" value=\"" + horario.getHoraInicio() + "\" size=\"5\" class=\"Estilo10\" " + html2 + " /></td>" + "\n";
                    html += "<td width=\"10\"></td>" + "\n";
                    html += "<td width=\"150\" align=\"left\">Hh:mm (Ej.: 07:15)</td>" + "\n";
                    html += "</tr>" + "\n";
                    html += "<tr>" + "\n";
                    html += "<td width=\"60\" align=\"right\">Termino</td>" + "\n";
                    html += "<td width=\"10\">:</td>" + "\n";
                    html += "<td width=\"40\" align=\"left\"><input type=\"text\" name=\"termino" + horario.getDiaHorario() + "\" id=\"termino" + horario.getDiaHorario() + "\" value=\"" + horario.getHoraTermino() + "\" size=\"5\" class=\"Estilo10\" " + html2 + " /></td>" + "\n";
                    html += "<td width=\"10\"></td>" + "\n";
                    html += "<td width=\"150\" align=\"left\">Hh:mm (Ej.: 16:45)</td>" + "\n";
                    html += "</tr>" + "\n";
                    html += "</table>" + "\n";
                    html += "</td>" + "\n";
                    html += "</tr>" + "\n";
                    html += "<tr><td colspan=\"3\" height=\"2\">&nbsp;</td></tr>" + "\n";
                    horario = null;
                }
            } else {
                html = "<tr class='FilaNormal'>\n" + "<td colspan='2' align='center'>";
                html += "SIN DATOS";
                html += "</td>\n" + "</tr>\n";
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='2' align='center'>";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
    
    public String make_ScriptEMPL(ArrayList empresas) {
        String html = "";
        String shtml1 = "";
        String shtml2 = "";
        ModEmpExt empresa = null;
        ModEmplExt empleado = null;
        
        html = "<SCRIPT Language=\"JavaScript\">" + "\n";
        html = html + "function getEmpl() {" + "\n";
        html = html + "p=document.frmNuevoMant.EMPR[document.frmNuevoMant.EMPR.selectedIndex].value;" + "\n";
        html = html + "var empl_i0=new Array('0');" + "\n";
        html = html + "var empl_n0=new Array('---Seleccione Empresa---');" + "\n";
        
        for (int i = 0; i < empresas.size(); i++) {
            empresa = (ModEmpExt) empresas.get(i);
            shtml1 = "var empl_i" + (empresa.getId() + 1) + "=new Array('0'";
            shtml2 = "var empl_n" + (empresa.getId() + 1) + "=new Array('---Elija Empleado---'";
            for (int j = 0; j < empresa.getEmpleados().size(); j++) {
                empleado = (ModEmplExt) empresa.getEmpleados().get(j);
                shtml1 = shtml1 + ", '" + (empleado.getId() + 1) + "'";
                shtml2 = shtml2 + ", '" + empleado.getVc_nombre() + " " + empleado.getVc_apellido() + "'";
                empleado = null;
            }
            shtml1 = shtml1 + ");";
            shtml2 = shtml2 + ");";
            html = html + shtml1 + "\n";
            html = html + shtml2 + "\n";
            empresa = null;
        }
        
        html = html + "seleccion=eval(\"empl_i\"+p);" + "\n";
        html = html + "seleccionNames=eval(\"empl_n\"+p);" + "\n";
        html = html + "cuantos_add=seleccion.length;" + "\n";
        html = html + "document.frmNuevoMant.EMPL.length=cuantos_add;" + "\n";
        html = html + "for(i=0;i<cuantos_add;i++){" + "\n";
        html = html + "document.frmNuevoMant.EMPL.options[i].value=seleccion[i];" + "\n";
        html = html + "document.frmNuevoMant.EMPL.options[i].text=seleccionNames[i];" + "\n";
        html = html + "if (seleccion[i]==\"0\") {" + "\n";
        html = html + "document.frmNuevoMant.EMPL.options[i].selected = true;" + "\n";
        html = html + "}" + "\n";
        html = html + "}" + "\n";
        html = html + "}" + "\n";
        html = html + "</SCRIPT>" + "\n";
        
        return html;
    }
    
    public String make_SelectEMPR(ArrayList empresas) {
        String html = "";
        ModEmpExt empresa = null;
        html = "<select name=\"EMPR\" size=1 OnChange=\"javascript:getEmpl();\">" + "\n";
        html = html + "<option value=\"0\" selected>" + "---Todas las Empresas---" + "\n";
        for (int i = 0; i < empresas.size(); i++) {
            empresa = (ModEmpExt) empresas.get(i);
            html += "<option value=\"" + (empresa.getId() + 1) + "\">" + empresa.getVc_nombre() + "\n";
            empresa = null;
        }
        html = html + "</select>" + "\n";
        return html;
    }
    
    public String make_SelectEMPL() {
        String html = "";
        html = "<select name=\"EMPL\">" + "\n";
        html = html + "<option value=\"0\" selected>" + "---Seleccione Empleado---" + "\n";
        html = html + "</select>" + "\n";
        return html;
    }
    public String tipoMantencion(HttpServletRequest request) {
        
        ModSesion Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
        String html = "";
        html = "<select name=\"tipo\" class=\"Estilo10\">" + "\n";
        //html = html + "<option value=\"-1\">" + "---Seleccione Tipo mantenci�n---" + "\n";
        //html = html + "<option value=\"C\">" + "---Correctiva---" + "\n";
        if(new macScSisMantenimientoBean().validarUsuarioALink(Sesion.getUsuario(),"mantencion")){
            html = html + "<option value=\"P\">" + "---Preventiva---" + "\n";
        }
        html = html + "</select>" + "\n";
        return html;
    }
    
    public String grillaTareas(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            for (int i = 0; i < Sesion.getMantenimiento().getListadoTareas().size(); i++) {
                Mantenimiento mante = (Mantenimiento) Sesion.getMantenimiento().getListadoTareas().get(i);
                html += "<tr class='FilaNormal'>\n";
                html += "<td class=\"ConBorder\" " + verOverLib("<b> N�mero: </b><br/>" + (i+1)) + "align='left'>" + (i+1) + "</td>\n";
                html += "<td class=\"ConBorder\" " + verOverLib("<b> Tarea: </b><br/>" + mante.getNombreArea()) + "align='left'>" + mante.getNombreArea() + "</td>\n";
                html += "<td class=\"ConBorder\" " + verOverLib("<b> Tipo de tarea: </b><br/>" + mante.getComentario()) + "align='left'>" + mante.getComentario() + "</td>\n";
                html += "<td " + verOverLib("<b> Tarea : </b><br/>" + mante.getId_tareas()) + "align='left' class=\"ConBorder\"><input name=\"tarea"+i+"\" type=\"checkbox\"/></td>\n";
                html += "</tr>\n";
                mante = null;
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='9' align='center' class=\"ConBorder\">";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
   
    public String grillaPresupuestoHoras(HttpServletRequest req) {
        String html = "";
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) req.getSession().getAttribute("Sesion");
            for (int i = 0; i < Sesion.getMantenimiento().getListadoMaquinas().size(); i++) {
                ModMaquina maquina = (ModMaquina) Sesion.getMantenimiento().getListadoMaquinas().get(i);
                html += "<tr class='FilaNormal'>\n";
                html += "<td class=\"ConBorder\" " + verOverLib("<b> Id: </b><br/>" + maquina.getIdMaquina()) + "align='left'>" + maquina.getIdMaquina() + "</td>\n";
                html += "<td class=\"ConBorder\" " + verOverLib("<b> M�quina: </b><br/>" + maquina.getNombreCorto()) + "align='left'>" + maquina.getNombreCorto() + "</td>\n";
                html += "<td class=\"ConBorder\" " + verOverLib("<b> Uso: </b><br/>" + maquina.getUso()) + "align='left'>" + maquina.getUso() + "</td>\n";
                html += "<td class=\"ConBorder\" " + verOverLib("<b> Horas: </b><br/>" + maquina.getHorasTrabajo()) + "align='left'>" + maquina.getHorasTrabajo() + "</td>\n";
                html += "<td class=\"ConBorder\" " + verOverLib("<b> Horas reales: </b><br/>" + maquina.getHorasReales()) + "align='left'>" + maquina.getHorasReales() + "</td>\n";
                html += "</tr>\n";
                maquina = null;
            }
        } catch (Exception ex) {
            html = "<tr class='FilaNormal'>\n" + "<td colspan='9' align='center' class=\"ConBorder\">";
            EscribeFichero.addError(ex, true);
            html += EscribeFichero.getLogHTML();
            html += "</td>\n" + "</tr>\n";
        }
        return html;
    }
}
