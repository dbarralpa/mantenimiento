package mac.sc.sis.mantenimiento.models;

public class ModHorario implements Cloneable {

    private int id;
    private int idHorario;
    private String horaInicio;
    private String horaTermino;
    private String diaHorario;
    private boolean estado;

    public ModHorario() {
        id = 0;
        idHorario = 0;
        horaInicio = "";
        horaTermino = "";
        diaHorario = "";
        estado = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraTermino() {
        return horaTermino;
    }

    public void setHoraTermino(String horaTermino) {
        this.horaTermino = horaTermino;
    }

    public String getDiaHorario() {
        return diaHorario;
    }

    public void setDiaHorario(String diaHorario) {
        this.diaHorario = diaHorario;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public ModHorario copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
            //
        }
        return (ModHorario) obj;
    }
}
