
package mac.sc.sis.mantenimiento.clases;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for macScSisMantenimiento enterprise bean.
 */
public interface macScSisMantenimientoRemote extends EJBObject, macScSisMantenimientoRemoteBusiness {
    
    
}
