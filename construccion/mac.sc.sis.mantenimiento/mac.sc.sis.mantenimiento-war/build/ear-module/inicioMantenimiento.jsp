<%@page contentType="text/html" import="mac.sc.sis.mantenimiento.clases.macScSisMantenimientoBean"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<%    
    if(request.getParameter("idSesion")!=null){
        Sesion.setIdSesion(request.getParameter("idSesion"));
    }
    if(request.getParameter("usuario")!=null){
        Sesion.setUsuario(request.getParameter("usuario").trim());
        if(Sesion.getUsuario().equals("dbarra") || Sesion.getUsuario().equals("jmaldonado") || Sesion.getUsuario().equals("corrego") || Sesion.getUsuario().equals("vhernandez") || Sesion.getUsuario().equals("gvillaroel")){
            Sesion.setTipoUsuario("admin");            
        }else if(Sesion.getUsuario().equals("marias")){
            Sesion.setTipoUsuario("calidad");            
        }else{
            Sesion.setTipoUsuario("normal");            
        }
    }
    if(request.getParameter("tipou")!=null){
        Sesion.setTipou(request.getParameter("tipou").trim());
    }
%>

<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/estiloPaginas.css" />   
        
        <script type="text/javascript" src="js/overlib.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>              
    </head>
    <body>     
    <%try{%>        
        <form action="FrmIniMantenimiento" method="post" name="frmIniMant">             
            <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="Estilo10">                
                <tr>
                    <td height="103" colspan="2" align="center"><img src="Imagenes/mantenimiento.jpg" width="500" height="147"></td>
                </tr> 
                <tr><td colspan="2" align="right">&nbsp;</td></tr>
                <tr><td colspan="2" align="right">&nbsp;</td></tr>
                <!--tr><td colspan="2" align="center"><a href="FrmIniMantenimiento?tipoUsuario=normal&usuario=ngonzalez">usuario normal</a></td></tr>
                <tr><td colspan="2" align="center"><a href="FrmIniMantenimiento?tipoUsuario=admin&usuario=corrego">usuario administrador</a></td></tr>
                <tr><td colspan="2" align="center"><a href="FrmIniMantenimiento?tipoUsuario=calidad&usuario=marias">usuario calidad</a></td></tr>
                <tr><td colspan="2" align="center">&nbsp;</td></tr>                
                <tr><td colspan="2" align="center">Nombre Usuario: <%=Sesion.getNombreUsuario()%></td></tr>
                <tr><td colspan="2" align="center">Tipo Usuario: <%=Sesion.getTipoUsuario()%></td></tr>
                <tr><td colspan="2" align="right">&nbsp;</td></tr-->
                <tr><td colspan="2" align="right">&nbsp;</td></tr>               
                <tr>
                    <td align="right">
                        <input type="image" src="Imagenes/solicitudIcon.jpg" width="100" height="90" name="ingresar_pedido" id="ingresar_pedido"/>
                    </td>                    
                    <td><font class="Estilo10"><a href="FrmIniMantenimiento?ingresar_pedido.x=SI">Ingrese su pedido</a></font> </td>
                </tr>                
                <tr>
                    <td align="right"><font class="Estilo10"><a href="FrmIniMantenimiento?admin_pedido.x=SI">Seguimiento de pedidos</a></font></td>
                    <td>
                        <input type="image" src="Imagenes/servicioIcon100.jpg" width="100" height="90" name="admin_pedido" id="admin_pedido"/>
                    </td>
                </tr>    
                <%--if(Sesion.getTipoUsuario().equals("admin")){--%>
                <%if(new macScSisMantenimientoBean().validarUsuarioALink(Sesion.getUsuario(),"mantencion")){%>
                <tr>                    
                    <td align="right">
                        <input type="image" src="Imagenes/mantenimientoIcon113.jpg" width="90" height="80" name="mant_sistema" id="mant_sistema"/>
                    </td>
                    <td><font class="Estilo10"><a href="FrmIniMantenimiento?mant_sistema.x=SI">&nbsp;Mantenedores</a></font></td>
                </tr>
                <tr>                    
                    <td align="right"><font class="Estilo10"><a href="FrmMantenimientoPreventivo?mant_prev.x=SI">Programa preventivo&nbsp;</a></font></td>
                    <td>
                        <input type="image" src="Imagenes/solicitudIcon.jpg" width="90" height="80" name="mant_prev" id="mant_prev"/>
                    </td>
                </tr>
                <tr>                    
                    <td align="right">
                        <input type="image" src="Imagenes/servicioIcon100.jpg" width="90" height="80" name="mant_sistema" id="mant_sistema"/>
                    </td>
                    <!--td><font class="Estilo10"><a href="FrmIniMantenimiento?mant_informes.x=SI">&nbsp;Informes</a></font></td-->
                    <td><font class="Estilo10"><a href="jsp/mantenimiento/informes.jsp">&nbsp;Informes</a></font></td>
                </tr>
                <%}%>
                <tr><td colspan="2" align="right">&nbsp;</td></tr>                
                <tr><td colspan="2" align="right">&nbsp;</td></tr>                
                <tr>
                    <td colspan="2">
                        <table width="300" align="center">                            
                            <td align="center">                 
                                <%if(Sesion.getTipou().equals("usuAdm") || Sesion.getTipou().equals("adm")){%>
                                    <a href="/mac/jsp/sistemas/EquiposElectricosAdm.jsp?idSesion=<%=Sesion.getIdSesion()%>&usuario=<%=Sesion.getUsuario()%>&tipou=<%=Sesion.getTipou()%>" target="_self"><img src="/mac.sc.sis.mantenimiento-war/Imagenes/volver2.jpg" border="0" /></a>                                
                                <%}else{%>
                                    <a href="/mac/jsp/sistemas/EquiposElectricos.jsp?idSesion=<%=Sesion.getIdSesion()%>&usuario=<%=Sesion.getUsuario()%>&tipou=<%=Sesion.getTipou()%>" target="_self"><img src="/mac.sc.sis.mantenimiento-war/Imagenes/volver2.jpg" border="0" /></a>                                
                                <%}%>
                            </td>  
                        </table>                           
                    </td>
                </tr> 
            </table>            
        </form>    
    <%
        }catch(Exception ex){                    
            mac.sc.sis.mantenimiento.utils.EscribeFichero.addError(ex, true);            
        }
    %>
    </body>
</html>


