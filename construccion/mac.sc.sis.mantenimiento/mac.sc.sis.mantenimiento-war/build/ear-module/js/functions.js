function getElemento(obj){    
    return document.getElementById(obj);
}
		
function showOverLib(titulo, contenido, fg, bg){
    overlib(contenido, CAPTION, titulo,
        FGCOLOR, fg,
        BGCOLOR, bg,
        BORDER, 1,
        CAPTIONFONT, "Verdana",
        CAPTIONSIZE, 1,
        TEXTFONT,"Verdana",
        TEXTSIZE, 1);
}

function changeValue(obj, texto){
    var vItem = getElemento(obj);
    vItem.value=texto;
}
