<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />


<html>
    <head>
        <title>Seguimiento mantenimiento preventivo</title>
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/jquery-calendar.css" />
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
        <script type="text/javascript" src="../../js/jquery.js"></script>
        <script type="text/javascript" src="../../js/jquery-calendar.js"></script>
        <script ype="text/javascript">
            function validar(){
            if(document.frmMant.finalizarProceso.checked == true){
            if(document.frmMant.dia.value != "" && document.frmMant.dia1.value != "" && document.frmMant.ejecutor.value != "" && document.frmMant.txtObvservacion.value != ""){
            
            }else{
            alert('Debe llenar todos los campos');
            return false;
            }
            }else{
            if(document.frmMant.dia.value != "" && document.frmMant.ejecutor.value != ""){
            
            }else{
            alert('Debe llenar al menos fecha inicio y ejecutor');
            return false;
            }
            }
            }
            
            $(document).ready(function (){ 

		$("#dia, #dia1").calendar();
		
             });
        </script>
    </head>
    <body>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantenimientoPreventivo" method="post" name="frmMant"> 
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>                     
                    <td colspan="3">
                        <table>
                            <tr>
                                <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/servicioIcon100.jpg" width="100" height="90"></font></td>
                                <td width="5%" align="center">&nbsp;</td>
                                <td width="65%" align="left" colspan="2"><br><font class="Estilo10 Estilo12">Seguimiento de pedidos</font><br></td>
                            </tr>
                        </table>
                    </td>                    
                </tr> 
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <%if(Sesion.getMantenimiento().getEtapa()==0){%> 
                    <td>
                        <table width="870" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">              
                            <tr>
                                <td>
                                    <%=vw.listarPreventivasPorBusqueda(request)%>
                                    &nbsp;&nbsp;<input type="submit" name="enviarMaquina" value="Listar" class="Estilo10">
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="ScrollStyle">
                                    <table width="870"  border="0" cellpadding="0" cellspacing="0" >
                                        
                                        <tr>
                                            <td>
                                                <table width="870"  border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                                    <tr>
                                                        <td colspan="8" class="Titulo" align="center">Listado de pedidos preventivos</td>     
                                                        <td colspan="2" class="Titulo" align="right" <%=vw.verOverTip("Haga click para ordenar de forma ascendente o descendente")%>>
                                                            <%if(Sesion.getMantenimiento().isOrdenAscNew()){%>
                                                            <input class="Estilo10" type="submit" value="descendente" name="ordenDesc2" id="ordenDesc2"/>
                                                            <%}else{%>
                                                            <input class="Estilo10" type="submit" value="ascendente" name="ordenAsc2" id="ordenAsc2"/>
                                                            <%}%>                        
                                                        </td>                                   
                                                    </tr>
                                                    <tr>
                                                        <td colspan="10"><%=Sesion.getPset().verPaginador("MANT1", true, Sesion.getMantenimiento().isOrdenAscNew())%></td>
                                                    </tr>
                                                    <tr class="CabeceraListado">
                                                        <td width="20">&nbsp;</td>                                            
                                                        <td width="71">O.T</td>
                                                        <td width="110">&Aacute;rea</td>
                                                        <td width="110">M&aacute;quina</td>
                                                        <td width="110">Programa</td>
                                                        <td width="100">Inicio</td>
                                                        <td width="110">T&eacute;rmino</td>
                                                        <td width="100">Ejecutor</td>
                                                        <td width="120">Observaci&oacute;n</td>
                                                        <td width="2">&nbsp;</td>
                                                    </tr>
                                                </table>
                                                <DIV class="ScrollStyle1">
                                                    <table width="850"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                                        <tr class="FilaNull">
                                                            <td width="20"></td>                                                
                                                            <td width="71"></td>
                                                            <td width="110"></td>
                                                            <td width="110"></td>
                                                            <td width="110"></td>
                                                            <td width="100"></td>
                                                            <td width="110"></td>
                                                            <td width="100"></td>
                                                            <td width="120"></td>
                                                        </tr>
                                                        <%=vw.listarSeguimientoPreventivo(request)%>
                                                    </table>
                                                </DIV>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr><td colspan="3">&nbsp;</td></tr>  
                            <tr><td colspan="3">
                                    <table width="661" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">                
                                        <tr>    
                                            <td align="center" colspan="2">&nbsp;</td>                    
                                            <td align="right">
                                                <a href="../../XlsSolIngRequest?mantencion=PP">
                                                    <img alt="" border="0" src="../../Imagenes/btn_excel.gif" border="0" <%=vw.verOverTip("Haga click para exportar a excel")%>/>
                                                </a>&nbsp;&nbsp;
                                                <input type="image" src="../../Imagenes/volver2.jpg" name="volverAtras1" id="volverAtras1" value="SI" <%=vw.verOverTip("Haga click volver")%> />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                          
                        </table> 
                    </td>
                    <%}else{%>
                    
                    <td align="center">
                        <table width="60%" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">                                                        
                            <tr class="Paginador">
                                <td>N&uacute;mero</td>
                                <td>:</td>
                                <td class="ConBorder">
                                    <input type="text" name="numero" id="numero" value="<%=Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento()%>"/>
                                </td>                                
                            </tr>   
                            <tr class="Paginador">
                                <td>Fecha inicio</td>
                                <td>:</td>
                                <td class="ConBorder">
                                    <%if(Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo() != null){%>
                                    <input type="text" name="dia" id="dia" class="calendarFocus" value="<%=Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo()%>"/>
                                    <%}else{%>
                                    <input type="text" name="dia" id="dia" class="calendarFocus"/>
                                    <%}%>
                                </td>                                
                            </tr>         
                            <tr class="Paginador">                                
                                <td>Fecha t&eacute;rmino</td>
                                <td>:</td>
                                <td class="ConBorder">
                                    <input type="text" name="dia1" id="dia1" class="calendarFocus"/>
                                </td>                                
                            </tr>
                            <tr class="Paginador">                                
                                <td valign="top">Ejecutor</td>
                                <td valign="top">:</td>
                                <td class="ConBorder" valign="top">
                                    <%if(Sesion.getMantenimiento().getDetalle().getEjecutor1() != null){%>
                                    <input type="text" name="ejecutor" id="ejecutor" value="<%=Sesion.getMantenimiento().getDetalle().getEjecutor1()%>">
                                    <%}else{%>
                                    <input type="text" name="ejecutor" id="ejecutor">&nbsp;EJ:&nbsp;Jos&eacute; Pineda;Luis Perez
                                    <%}%>
                                </td>                                
                            </tr>       
                            <tr class="Paginador">                                
                                <td valign="top">Observaci�n</td>
                                <td valign="top">:</td>
                                <td class="ConBorder" valign="top">
                                    <%if(Sesion.getMantenimiento().getDetalle().getObservacionCerrar() != null){%>
                                    <textarea cols="30" rows="2" class="txtTexto" name="txtObvservacion" id="txtObvservacion"><%=Sesion.getMantenimiento().getDetalle().getObservacionCerrar()%></textarea>
                                    <%}else{%>
                                    <textarea cols="30" rows="2" class="txtTexto" name="txtObvservacion" id="txtObvservacion"></textarea>
                                    <%}%>
                                </td>                                
                            </tr>                                                                    
                        </table> 
                        <br>
                        <table width="661" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">              
                            <tr>
                                <td class="ScrollStyle">    
                                    <table width="661" border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                        <tr>                              
                                            <td colspan="5" class="Titulo" align="center">Listado de tareas sin hacer</td>
                                        </tr>
                                        
                                        <tr class="CabeceraListado">
                                            <td width="20">N�</td>
                                            <td width="500">TAREA</td>
                                            <td width="100">TIPO</td>
                                            <td width="20">ELI</td>
                                            <td width="21">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle2">
                                        <table width="640"  border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="500"></td>
                                                <td width="100"></td>
                                                <td width="20"></td>
                                            </tr>
                                            <%=vw.grillaTareas(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                            <tr><td colspan="3">&nbsp;</td></tr>  
                            <tr><td colspan="3">
                                    <table width="661" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">                
                                        <tr>    
                                            <td align="center" colspan="2">&nbsp;</td>                    
                                            <td align="right">
                                                Finalizar proceso<input type="checkbox" name="finalizarProceso">&nbsp;&nbsp;
                                                <%if(Sesion.getTipoUsuario().equals("calidad")){%>
                                                <input type="submit" disabled onclick="return validar()" name="actualizarTareas" id="actualizarTareas" value="Actualizar" class="Estilo10" <%=vw.verOverTip("Haga click para actualizar")%> />&nbsp;
                                                <%}else{%>
                                                <input type="submit" onclick="return validar()" name="actualizarTareas" id="actualizarTareas" value="Actualizar" class="Estilo10" <%=vw.verOverTip("Haga click para actualizar")%> />&nbsp;
                                                <%}%>
                                                <input type="image" src="../../Imagenes/volver2.jpg" name="volverAtras" id="volverAtras" value="SI" <%=vw.verOverTip("Haga click volver")%> />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr> 
                        </table>
                    </td>
                    <%}%>
                </tr>
            </table>
        </form>
    </body>
</html>
