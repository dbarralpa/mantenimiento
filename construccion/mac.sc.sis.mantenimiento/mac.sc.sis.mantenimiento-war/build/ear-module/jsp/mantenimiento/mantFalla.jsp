<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>        
        <title>Solicitud de Servicio de Mantenimiento</title>               
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
    </head>
    
    <body>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantFalla" method="post" name="frmMFail">           
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">                                
                <tr>                  
                    <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/solicitudIcon.jpg" width="100" height="90"></font></td>
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="65%" align="left"><br><font class="Estilo10 Estilo12">Mantenedor Fallas</font><br></td>
                </tr>                   
                <%if(Sesion.getMantenimiento().getEtapaFalla()==0){%>
                <tr>
                    <td align="right"><font class="Estilo10">&Aacute;rea</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%=vw.listarAreas(Sesion.getListadoAreas(), Sesion.getMantenimiento().getCodigoEspecialidad())%>
                        </font>                        
                    </td>
                </tr>                
                <tr>
                    <td align="right"><font class="Estilo10">M&aacute;quina o equipo</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%=vw.listarMaquinas(Sesion.getBeanMan().listarMaquinas(Sesion.getMantenimiento().getCodigoEspecialidad()), Sesion.getMantenimiento().getMaquina().getIdMaquina())%>                            
                        </font>
                    </td>
                </tr>                   
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3">
                        <table width="300" align="center" cellpadding="0" cellspacing="0">              
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="300"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                                        <tr>
                                            <td colspan="6" class="Titulo" align="center">Listado de Fallas</td>                                    
                                        </tr>                                        
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>
                                            <td width="20">&nbsp;</td>
                                            <td width="40">ID</td>
                                            <td width="90">FALLA</td>                                            
                                            <td width="110">TIPO FALLA</td>                                            
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyleSmall">
                                        <table width="280"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="20"></td>
                                                <td width="40"></td>
                                                <td width="90"></td>                                            
                                                <td width="110"></td>                                            
                                            </tr>
                                            <%=vw.grillaFallas(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                        </table>  
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="ingresarNewFail" id="ingresarNewFail" value="Nueva Falla" class="Estilo10" <%=vw.verOverTip("Haga click para ingresar nueva Falla")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaFalla()==1){%>
                <tr>
                    <td align="right"><font class="Estilo10">Nombre Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreFalla" id="nombreFalla" size="40" value="<%=Sesion.getMantenimiento().getFalla().getNombre()%>" class="Estilo10"/>
                    </td>
                </tr>                  
                <tr>
                    <td align="right" valign="top"><font class="Estilo10">Descripción Falla</font></td>
                    <td align="center" valign="top">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <textarea rows="3" cols="50" name="descFalla" id="descFalla" class="Estilo10"><%=Sesion.getMantenimiento().getFalla().getDescripcion()%></textarea>
                        </font>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Tipo Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%=vw.listarTipoFallas(Sesion.getBeanMan().listarTipoFallas(), Sesion.getMantenimiento().getFalla().getTipoFalla().getIdTipoFalla())%>                         
                        </font>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="grabarFalla" id="grabarFalla" value="Grabar" class="Estilo10" <%=vw.verOverTip("Haga click para grabar nueva falla")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaFalla()==2){%>
                <tr>
                    <td align="right"><font class="Estilo10">Id Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="idFalla" id="idFalla" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getFalla().getIdFalla()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreFalla" id="nombreFalla" size="40" value="<%=Sesion.getMantenimiento().getFalla().getNombre()%>" class="Estilo10"/>
                    </td>
                </tr>  
                <tr>
                    <td align="right" valign="top"><font class="Estilo10">Descripción Falla</font></td>
                    <td align="center" valign="top">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <textarea rows="3" cols="50" name="descFalla" id="descFalla" class="Estilo10"><%=vw.verHtmlNull(Sesion.getMantenimiento().getFalla().getDescripcion())%></textarea>
                        </font>
                    </td>
                </tr>  
                <tr>
                    <td align="right"><font class="Estilo10">Tipo Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%=vw.listarTipoFallas(Sesion.getBeanMan().listarTipoFallas(), Sesion.getMantenimiento().getFalla().getTipoFalla().getIdTipoFalla())%>                         
                        </font>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="actualizarFalla" id="actualizarFalla" value="Actualizar" class="Estilo10" <%=vw.verOverTip("Haga click para actualizar falla")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaFalla()==3){%>
                <tr>
                    <td align="right"><font class="Estilo10">Id Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="idFalla" id="idFalla" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getFalla().getIdFalla()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreFalla" id="nombreFalla" size="40" readonly="yes" value="<%=Sesion.getMantenimiento().getFalla().getNombre()%>" class="Estilo10"/>
                    </td>
                </tr>  
                <tr>
                    <td align="right" valign="top"><font class="Estilo10">Descripción Falla</font></td>
                    <td align="center" valign="top">:</td>
                    <td align="left">
                        <font class="Estilo10">
                        <textarea rows="3" cols="50" name="descFalla" id="descFalla" class="Estilo10" readonly="yes"><%=vw.verHtmlNull(Sesion.getMantenimiento().getFalla().getDescripcion())%></textarea>
                        </font>
                    </td>
                </tr>   
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="eliminarFalla" id="eliminarFalla" value="Eliminar" class="Estilo10" <%=vw.verOverTip("Haga click para eliminar falla")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}%>
            </table>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
    <script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
    </script>
    <%Sesion.cleanTip();%>
<%}%>
