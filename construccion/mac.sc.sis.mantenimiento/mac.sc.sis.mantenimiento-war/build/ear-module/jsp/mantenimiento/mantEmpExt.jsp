<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>        
        <title>Solicitud de Servicio de Mantenimiento</title>               
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
    </head>
    
    <body>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantEmpExt" method="post" name="frmMEExt">           
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">                                
                <tr>                  
                    <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/solicitudIcon.jpg" width="100" height="90"></font></td>
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="65%" align="left"><br><font class="Estilo10 Estilo12">Mantenedor Empresas Externas</font><br></td>
                </tr>                   
                <%if(Sesion.getMantenimiento().getEtapaEmpExt()==0){%>                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3">
                        <table width="751" align="center" cellpadding="0" cellspacing="0">              
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="751"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                                        <tr>
                                            <td colspan="6" class="Titulo" align="center">Listado Empresas Externas</td>                                    
                                        </tr>                                        
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>
                                            <td width="20">&nbsp;</td>                                          
                                            <td width="100">RUT</td>
                                            <td width="200">NOMBRE</td>                                            
                                            <td width="391">DIRECCION</td>                                            
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle">
                                        <table width="730"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="20"></td>                                          
                                                <td width="100"></td>
                                                <td width="200"></td>                                            
                                                <td width="391"></td>                                          
                                            </tr>
                                            <%=vw.grillaEmpExternas(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                        </table>  
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="ingresarNewEmpExt" id="ingresarNewEmpExt" value="Nueva Empresa Externa" class="Estilo10" <%=vw.verOverTip("Haga click para ingresar nueva Empresa Externa")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaEmpExt()==1){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutEmpExt" id="rutEmpExt" size="20" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr>                  
                <tr>
                    <td align="right"><font class="Estilo10">Nombre</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreEmpExt" id="nombreEmpExt" size="50" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Direcci�n</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="direcEmpExt" id="direcEmpExt" size="50" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_direccion()%>" class="Estilo10"/>
                    </td>
                </tr>  
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="grabarEmpExt" id="grabarEmpExt" value="Grabar" class="Estilo10" <%=vw.verOverTip("Haga click para grabar nueva Empresa Externa")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaEmpExt()==2){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutEmpExt" id="rutEmpExt" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreEmpExt" id="nombreEmpExt" size="50" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Direcci�n</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="direcEmpExt" id="direcEmpExt" size="50" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_direccion()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="actualizarEmpExt" id="actualizarEmpExt" value="Actualizar" class="Estilo10" <%=vw.verOverTip("Haga click para actualizar Empresa Externa")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaEmpExt()==3){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutEmpExt" id="rutEmpExt" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreEmpExt" id="nombreEmpExt" size="50" readonly="yes" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Direcci�n</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="direcEmpExt" id="direcEmpExt" size="50" readonly="yes" value="<%=Sesion.getMantenimiento().getEmpExt().getVc_direccion()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="eliminarEmpExt" id="eliminarEmpExt" value="Eliminar" class="Estilo10" <%=vw.verOverTip("Haga click para eliminar Empresa Externa")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}%>
            </table>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
    <script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
    </script>
    <%Sesion.cleanTip();%>
<%}%>
