<%@page contentType="text/html" import="mac.sc.sis.mantenimiento.clases.macScSisMantenimientoBean"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>        
        <title>Solicitud de Servicio de Mantenimiento</title>               
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
        <link rel="stylesheet" href="../../themes/base/jquery.ui.all.css"/>
        <script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/jquery.ui.core.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/jquery.ui.widget.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/jquery.ui.datepicker.js"></script>
        
    </head>
    
    <body>
        <script>
     $(document).ready(function(){
            $( "#dia2" ).datepicker();
            $( "#dia2" ).datepicker();
        });
       </script>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantenimiento" method="post" name="frmMant">           
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">                                
                <tr>                  
                    <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/solicitudIcon.jpg" width="100" height="90"></font></td>
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="65%" align="left"><br><font class="Estilo10 Estilo12">Tareas por hacer</font><br></td>
                </tr>   
                <% if(Sesion.getMantenimiento().getEtapa()==1) {%>
                <!--tr>
                    <td align="right"><font class="Estilo10">Tipo mantenci�n</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%--=vw.tipoMantencion(request)--%>
                        </font>                        
                    </td>
                </tr-->
                <tr>
                    <td align="right"><font class="Estilo10">&Aacute;rea</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%=vw.listarAreas(Sesion.getListadoAreas(), Sesion.getMantenimiento().getCodigoEspecialidad())%>
                        </font>                        
                    </td>
                </tr>
                <tr>
                    <td align="right"><font class="Estilo10">Solicitado por</font></td>
                    <td align="center">:</td>
                    <td align="left"><font class="Estilo10"><%=Sesion.getNombreUsuario()%></font></td>
                </tr>
                <tr>
                    <td align="right"><font class="Estilo10">M&aacute;quina o equipo</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%=vw.listarMaquinas(Sesion.getBeanMan().listarMaquinas(Sesion.getMantenimiento().getCodigoEspecialidad()), 
                            Sesion.getMantenimiento().getMaquina().getIdMaquina())%>
                            <%if(Sesion.getTipoUsuario().equals("admin")){%>
                            <a href="../../FrmMantMaquina?newMaquina=SI">ingresar nueva M�quina</a>
                            <%}%>
                        </font>
                    </td>
                </tr>     
                <tr>
                    <td align="right"><font class="Estilo10">Listado Fallas</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%if(Sesion.getMantenimiento().getMaquina().getIdMaquina()==-1 || Sesion.getMantenimiento().getCodigoEspecialidad()==-1){%>                            
                            <%=vw.listarFallas(null, Sesion.getMantenimiento().getCodigoFalla())%>                        
                            <%}else{%>
                            <%=vw.listarFallas(Sesion.getMantenimiento().getListadoFallas(), Sesion.getMantenimiento().getCodigoFalla())%>                        
                            <%}%> 
                            <%if(Sesion.getTipoUsuario().equals("admin")){%>
                            <a href="../../FrmMantFalla?newFalla=SI">ingresar nueva Falla</a>
                            <%}%>
                        </font>
                    </td>
                </tr> 
                <%if(Sesion.getMantenimiento().getDescFalla()!=null && !Sesion.getMantenimiento().getDescFalla().trim().equals("")){%>
                <tr>
                    <td align="right" valign="top"><font class="Estilo10">Descripci�n Falla</font></td>
                    <td align="center" valign="top">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <textarea rows="3" cols="50" name="descFalla" id="descFalla" class="Estilo10" readonly="yes"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDescFalla())%></textarea>
                        </font>
                    </td>
                </tr> 
                <%}%>
                <tr>
                    <td align="right"><font class="Estilo10">Tipo falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <%=vw.tipoFallas(request)%>
                        </font>                        
                    </td>
                </tr>
                <tr>
                    <DIV id="popCal" class="Estilo10" style="BORDER-RIGHT: 0px ridge; BORDER-TOP: 0px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 0px ridge; WIDTH: 10px; BORDER-BOTTOM: 0px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                        <IFRAME name="popFrame" src="../popcjs.html" frameBorder=0 width=183 scrolling=no height=188></IFRAME>
                    </DIV>
                    <td align="right"><font class="Estilo10">Fecha Detenci�n</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input class="Estilo10" name="dia" id="dia" size="12" value="<%=vw.verHtmlNull(Sesion.getMantenimiento().getDiaDetencionEquipo())%>" readonly="yes"/>
                        <a onclick="popFrame.fPopCalendar(dia,dia,popCal);return false">
                            <img src="../../Imagenes/calendar.gif" width="16" height="16" alt="calendar"/>
                        </a>&nbsp;a�o-mes-dia
                    </td>  
                </tr>  
                <tr>
                    <td align="right"><font class="Estilo10">Hora Detenci�n</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <input class="Estilo10" name="hora" id="hora" size="8" value="<%=vw.verHtmlNull(Sesion.getMantenimiento().getHoraDetencionEquipo())%>"/>
                        </font>&nbsp;Hh:mm (Ej.: 15:30)
                    </td>
                </tr> 
                <tr>
                    <td align="right" valign="top"><font class="Estilo10">Observaci&oacute;n</font></td>
                    <td align="center" valign="top">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <textarea rows="5" cols="50" name="txtObservacion" id="txtObservacion" class="Estilo10"><%=Sesion.getMantenimiento().getMensaje()%></textarea>
                        </font>
                    </td>
                </tr>                                                          
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="ingresarNew" id="ingresarNew" value="Grabar" class="Estilo10" <%=vw.verOverTip("Haga click para ingresar solicitud")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver al listado")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>                
                <% }else if(Sesion.getMantenimiento().getEtapa()==0){%>                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3">
                        <table width="751" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">              
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="751"  border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                        <tr>
                                            <td colspan="7" class="Titulo" align="center">listado de pedidos correctivos</td>     
                                            <td colspan="2" class="Titulo" align="right" <%=vw.verOverTip("Haga click para ordenar de forma ascendente o descendente")%>>
                                                <%if(Sesion.getMantenimiento().isOrdenAscNew()){%>
                                                <input class="Estilo10" type="submit" value="descendente" name="ordenDesc" id="ordenDesc"/>
                                                <%}else{%>
                                                <input class="Estilo10" type="submit" value="ascendente" name="ordenAsc" id="ordenAsc"/>
                                                <%}%>                        
                                            </td>                                   
                                        </tr>
                                        <tr>
                                            <td colspan="9"><%=Sesion.getPset().verPaginador("MANT", true, Sesion.getMantenimiento().isOrdenAscNew())%></td>
                                        </tr>
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>                                            
                                            <td width="71">O.M</td>
                                            <td width="110">Especialidad</td>
                                            <td width="110">M&aacute;quina</td>
                                            <td width="110">Falla</td>
                                            <td width="100">T.Falla</td>
                                            <td width="110">Observaci&oacute;n</td>
                                            <td width="100">Fecha registro</td>
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle">
                                        <table width="730"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>                                                
                                                <td width="71"></td>
                                                <td width="110"></td>
                                                <td width="110"></td>
                                                <td width="110"></td>
                                                <td width="100"></td>
                                                <td width="110"></td>
                                                <td width="100"></td>                           
                                            </tr>
                                            <%=vw.listarPedidosMantenimiento(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>                 
                            <tr><td align="right">
                                    <a href="../../XlsSolIngRequest?mantencion=C">
                                        <img alt="" border="0" src="../../Imagenes/btn_excel.gif" border="0" <%=vw.verOverTip("Haga click para exportar a excel")%>/>
                                    </a>
                            </td></tr>                             
                        </table>  
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr>
                
                
                
                <%--if(Sesion.getTipoUsuario().equals("admin") || Sesion.getTipoUsuario().equals("calidad")){--%>
                <%if(new macScSisMantenimientoBean().validarUsuarioALink(Sesion.getUsuario(),"mantencion")){%>
                <tr>
                    <td colspan="3">
                        <table width="870" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">              
                            <tr>
                                <!--DIV id="popCal1" class="Estilo10" style="BORDER-RIGHT: 0px ridge; BORDER-TOP: 0px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 0px ridge; WIDTH: 10px; BORDER-BOTTOM: 0px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                    <IFRAME name="popFrame1" src="../popcjs.html" frameBorder=0 width=183 scrolling=no height=188></IFRAME>
                                    <td align="left">
                                        <input class="Estilo10" name="dia2" id="dia2" size="12" value="" readonly="yes"/>
                                        <a onclick="popFrame1.fPopCalendar(dia2,dia2,popCal1);return false">
                                            <img src="../../Imagenes/calendar.gif" width="16" height="16" alt="calendar"/>
                                        </a>&nbsp;&nbsp;
                                        <input type="submit" value="Listar" class="Estilo10" name="enviarFecha">
                                    </td>  
                                </DIV-->
                                fecha b&uacute;squeda:&nbsp;&nbsp; <input id="dia2" name="dia2" type="text" size="15" readonly="yes" <%if(request.getParameter("dia2") != null && !request.getParameter("dia2").equals("")){%>value="<%=request.getParameter("dia2")%>"<%}%> />
                                &nbsp;&nbsp;&nbsp;<input type="submit" value="Listar" class="Estilo10" name="enviarFecha">
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="870"  border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                        <tr>
                                            <td colspan="8" class="Titulo" align="center">Listado de pedidos preventivos</td>     
                                            <td colspan="2" class="Titulo" align="right" <%=vw.verOverTip("Haga click para ordenar de forma ascendente o descendente")%>>
                                                <%if(Sesion.getMantenimiento().isOrdenAscPreventivo()){%>
                                                <input class="Estilo10" type="submit" value="descendente" name="ordenDesc1" id="ordenDesc1"/>
                                                <%}else{%>
                                                <input class="Estilo10" type="submit" value="ascendente" name="ordenAsc1" id="ordenAsc1"/>
                                                <%}%>
                                            </td>                                   
                                        </tr>
                                        <tr>
                                            <td colspan="10"><%=Sesion.getPset().verPaginador("MANT1", true, Sesion.getMantenimiento().isOrdenAscPreventivo())%></td>
                                        </tr>
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>                                            
                                            <td width="70">O.T</td>
                                            <td width="110">&Aacute;rea</td>
                                            <td width="110">M&aacute;quina</td>
                                            <td width="110">Programa</td>
                                            <td width="100">Inicio</td>
                                            <td width="110">T&eacute;rmino</td>
                                            <td width="100">Ejecutor</td>
                                            <td width="120">Observaci&oacute;n</td>
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle1">
                                        <table width="850"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>                                                
                                                <td width="70"></td>
                                                <td width="110"></td>
                                                <td width="110"></td>
                                                <td width="110"></td>
                                                <td width="100"></td>
                                                <td width="110"></td>
                                                <td width="100"></td>
                                                <td width="120"></td>
                                            </tr>
                                            <%=vw.listarPedidosMantePreventivo(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>                 
                            <tr><td align="right">
                                    <a href="../../XlsSolIngRequest?mantencion=P">
                                        <img alt="" border="0" src="../../Imagenes/btn_excel.gif" border="0" <%=vw.verOverTip("Haga click para exportar a excel")%>/>
                                    </a>
                            </td></tr>                             
                        </table>
                    </td>
                </tr>
                
                
                <%}%>
                
                
                
                <tr><td colspan="3">&nbsp;</td></tr>                 
                <tr>
                    <td colspan="3">
                        <table width="300" align="center">
                            <td align="right">
                                <input type="submit" name="nuevaSol" id="nuevaSol" value="Nueva solicitud" class="Estilo10" <%=vw.verOverTip("Haga click para agregar una nueva solicitud")%> />
                            </td>                                        
                            <td width="50">&nbsp;</td>
                            <td align="left">
                                <a href="../../"><img alt="" src="../../Imagenes/volver2.jpg" border="0" <%=vw.verOverTip("Haga click para volver")%> /></a>
                            </td>  
                        </table>                           
                    </td>
                </tr>         
                <%}else if(Sesion.getMantenimiento().getEtapa()==2) {%>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">
                        <table width="700" border="1" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Detalle Solicitud</td>                    
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Orden</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento()%></td>
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Solicitado por</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=Sesion.getNombreUsuario()%></td>
                                        </tr>  
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">&Aacute;rea</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getNombreArea())%></td>
                                        </tr> 
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">M&aacute;quina o equipo</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=Sesion.getMantenimiento().getDetalle().getNombreMaquina()%></td>                                
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>                                        
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Fecha-Hora Detenci�n Equipo</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top">             
                                                <%if(Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo()!=null){%>
                                                <%=Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo().substring(0, 16)%>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                                
                                            </td>
                                        </tr>                                         
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Falla</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top">                                                
                                                <%if(Sesion.getMantenimiento().getDetalle().getNombreFalla()!=null){%>
                                                <%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getNombreFalla())%>
                                                <%}else{%>
                                                Otras
                                                <%}%>
                                            </td>
                                        </tr> 
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <%if(Sesion.getMantenimiento().getDetalle().getDescFalla()!=null && !Sesion.getMantenimiento().getDetalle().getDescFalla().trim().equals("")){%>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Descripci�n Falla</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><textarea rows="3" cols="50" name="descFalla" id="descFalla" class="Estilo10" readonly="yes"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getDescFalla())%></textarea></td>
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <%}%>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Observaci&oacute;n Falla</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionFalla())%></td>
                                        </tr> 
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>                    
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <a href="../../XlsDetSolRequest">
                                        <img alt="" border="0" src="../../Imagenes/btn_excel.gif" border="0" <%=vw.verOverTip("Haga click para exportar a excel")%>/>
                                    </a>
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver al listado")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapa()== 3) {%>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">
                        <table width="700" border="1" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Detalle Solicitud</td>                    
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Orden</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento()%></td>
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">&Aacute;rea</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getNombreArea())%></td>
                                        </tr> 
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">M&aacute;quina o equipo</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=Sesion.getMantenimiento().getDetalle().getNombreMaquina()%></td>                                
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>                                        
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Programa</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getFechaMantePreventivo())%>
                                            </td>
                                        </tr>                                         
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Inicio</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo())%>                                        
                                            </td>
                                        </tr> 
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Fin</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getFechaEntregaEquipo())%>                                        
                                            </td>
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Ejecutor</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getEjecutor1())%>                                        
                                            </td>
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                        <tr class="Paginador">
                                            <td width="150" valign="top">Observaci&oacute;n</td>
                                            <td width="5" valign="top">:</td>
                                            <td valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionCerrar())%>                                        
                                            </td>
                                        </tr>                                       
                                        <tr class="FilaDisable"><td height="5" colspan="3"></td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>                    
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <a href="../../XlsCheckList">
                                        <img alt="" border="0" src="../../Imagenes/btn_excel.gif" border="0" <%=vw.verOverTip("Haga click para exportar a excel")%>/>
                                    </a>
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver al listado")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}%>   
            </table>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
<script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
</script>
<%Sesion.cleanTip();%>
<%}%>
