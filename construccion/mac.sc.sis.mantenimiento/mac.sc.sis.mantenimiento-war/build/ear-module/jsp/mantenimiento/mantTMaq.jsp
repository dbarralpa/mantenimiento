<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>        
        <title>Solicitud de Servicio de Mantenimiento</title>               
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
    </head>
    
    <body>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantTMaq" method="post" name="frmTMaq">           
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">                                
                <tr>                  
                    <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/solicitudIcon.jpg" width="100" height="90"></font></td>
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="65%" align="left"><br><font class="Estilo10 Estilo12">Mantenedor Turno-M�quina</font><br></td>
                </tr>                   
                <%if(Sesion.getMantenimiento().getEtapaTMaq()==0){%>                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3">
                        <table width="751" align="center" cellpadding="0" cellspacing="0">              
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="751"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                                        <tr>
                                            <td colspan="6" class="Titulo" align="center">Listado Turno-M�quina</td>                                    
                                        </tr>                                        
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>
                                            <td width="20">&nbsp;</td>
                                            <td width="40">ID</td>
                                            <td width="300">NOMBRE</td>
                                            <td width="350">DESCRIPCION</td>
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle">
                                        <table width="730"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="20"></td>
                                                <td width="40"></td>
                                                <td width="300"></td>
                                                <td width="350"></td>
                                            </tr>
                                            <%=vw.grillaTurnosMaquina(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                        </table>  
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="ingresarNewTMaq" id="ingresarNewTMaq" value="Nuevo Turno-Maquina" class="Estilo10" <%=vw.verOverTip("Haga click para ingresar nuevo Turno M�quina")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaTMaq()==1){%>               
                <tr>        
                    <td width="100">&nbsp;</td>
                    <td valign="top" align="right">
                        <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido" bgcolor="#E3EEFF">                        
                            <tr>                    
                                <td colspan="3" align="center" class="Titulo">Datos Turno</td>                    
                            </tr>
                            <tr><td colspan="3" height="2">&nbsp;</td></tr>
                            <tr>
                                <td width="150" align="right">Nombre Turno</td>
                                <td width="10">:</td>
                                <td align="left"><input type="text" name="nombreTMaq" id="nombreTMaq" size="20" value="<%=Sesion.getMantenimiento().getTurnoMaquina().getNombreTurnoMaq()%>" class="Estilo10"/></td>
                            </tr>
                            <tr>
                                <td width="150" align="right">Descripci�n Turno</td>
                                <td width="10">:</td>
                                <td align="left">
                                    <font class="Estilo10">
                                        <textarea rows="3" cols="35" name="descTMaq" id="descTMaq" class="Estilo10"><%=Sesion.getMantenimiento().getTurnoMaquina().getDescTurnoMaq()%></textarea>
                                    </font>
                                </td>
                            </tr>
                            <tr><td colspan="3" height="2">&nbsp;</td></tr>
                        </table>
                    </td>  
                    <td valign="top" align="left">        
                        <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Horario</td>                    
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#E3EEFF">                                                                    
                                        <%=vw.checksBoxHorario(request)%>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr><td colspan="4">&nbsp;</td></tr> 
                <tr>
                    <td colspan="4">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="grabarTMaq" id="grabarTMaq" value="Grabar" class="Estilo10" <%=vw.verOverTip("Haga click para grabar nuevo Turno M�quina")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaTMaq()==2){%>
                <tr>        
                    <td width="100">&nbsp;</td>
                    <td valign="top" align="right">
                        <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido" bgcolor="#E3EEFF">                        
                            <tr>                    
                                <td colspan="3" align="center" class="Titulo">Datos Turno</td>                    
                            </tr>
                            <tr><td colspan="3" height="2">&nbsp;</td></tr>
                            <tr>
                                <td width="150" align="right">ID Turno</td>
                                <td width="10">:</td>
                                <td align="left"><input type="text" name="idTMaq" id="idTMaq" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getTurnoMaquina().getIdTurnoMaq()%>" class="Estilo10"/></td>
                            </tr>
                            <tr>
                                <td width="150" align="right">Nombre Turno</td>
                                <td width="10">:</td>
                                <td align="left"><input type="text" name="nombreTMaq" id="nombreTMaq" size="20" value="<%=Sesion.getMantenimiento().getTurnoMaquina().getNombreTurnoMaq()%>" class="Estilo10"/></td>
                            </tr>
                            <tr>
                                <td width="150" align="right">Descripci�n Turno</td>
                                <td width="10">:</td>
                                <td align="left">
                                    <font class="Estilo10">
                                        <textarea rows="3" cols="35" name="descTMaq" id="descTMaq" class="Estilo10"><%=Sesion.getMantenimiento().getTurnoMaquina().getDescTurnoMaq()%></textarea>
                                    </font>
                                </td>
                            </tr>
                            <tr><td colspan="3" height="2">&nbsp;</td></tr>
                        </table>
                    </td>  
                    <td valign="top" align="left">        
                        <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Horario</td>                    
                            </tr>
                            <tr>
                                <td align="center">                                    
                                    <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#E3EEFF">                                                                    
                                        <%=vw.checksBoxHorario(request)%>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr><td colspan="4">&nbsp;</td></tr> 
                <tr>
                    <td colspan="4">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="actualizarTMaq" id="actualizarTMaq" value="Actualizar" class="Estilo10" <%=vw.verOverTip("Haga click para actualizar Turno M�quina")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaTMaq()==3){%>
                <tr>        
                    <td width="100">&nbsp;</td>
                    <td valign="top" align="right">
                        <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido" bgcolor="#E3EEFF">                        
                            <tr>                    
                                <td colspan="3" align="center" class="Titulo">Datos Turno</td>                    
                            </tr>
                            <tr><td colspan="3" height="2">&nbsp;</td></tr>
                            <tr>
                                <td width="150" align="right">ID Turno</td>
                                <td width="10">:</td>
                                <td align="left"><input type="text" name="idTMaq" id="idTMaq" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getTurnoMaquina().getIdTurnoMaq()%>" class="Estilo10"/></td>
                            </tr>
                            <tr>
                                <td width="150" align="right">Nombre Turno</td>
                                <td width="10">:</td>
                                <td align="left"><input type="text" name="nombreTMaq" id="nombreTMaq" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getTurnoMaquina().getNombreTurnoMaq()%>" class="Estilo10"/></td>
                            </tr>
                            <tr>
                                <td width="150" align="right">Descripci�n Turno</td>
                                <td width="10">:</td>
                                <td align="left">
                                    <font class="Estilo10">
                                        <textarea rows="3" cols="35" name="descTMaq" id="descTMaq" readonly="yes" class="Estilo10"><%=Sesion.getMantenimiento().getTurnoMaquina().getDescTurnoMaq()%></textarea>
                                    </font>
                                </td>
                            </tr>
                            <tr><td colspan="3" height="2">&nbsp;</td></tr>
                        </table>
                    </td>  
                    <td valign="top" align="left">        
                        <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Horario</td>                    
                            </tr>
                            <tr>
                                <td align="center">                                    
                                    <table width="400" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#E3EEFF">                                                                    
                                        <%=vw.checksBoxHorario(request, true)%>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                <tr><td colspan="4">&nbsp;</td></tr> 
                <tr>
                    <td colspan="4">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="eliminarTMaq" id="eliminarTMaq" value="Eliminar" class="Estilo10" <%=vw.verOverTip("Haga click para eliminar Turno M�quina")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}%>
            </table>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
    <script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
    </script>
    <%Sesion.cleanTip();%>
<%}%>
