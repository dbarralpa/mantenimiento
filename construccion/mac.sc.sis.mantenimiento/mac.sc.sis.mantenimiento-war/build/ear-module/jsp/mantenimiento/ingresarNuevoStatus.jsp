<%@page contentType="text/html" import="mac.sc.sis.mantenimiento.clases.macScSisMantenimientoBean"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>                
        <title>Solicitud de Servicio de Mantenimiento</title>
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />      
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>        
    </head>
    
    <body>
        <%try{%>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmNuevoStatus" method="post" name="frmNuevoMant">            
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr>                     
                    <td colspan="3">
                        <table>
                            <tr>
                                <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/servicioIcon100.jpg" width="100" height="90"></font></td>
                                <td width="5%" align="center">&nbsp;</td>
                                <td width="65%" align="left" colspan="2"><br><font class="Estilo10 Estilo12">Seguimiento de pedidos</font><br></td>
                            </tr>
                        </table>
                    </td>                    
                </tr> 
                <%if(Sesion.getMantenimiento().getEtapa()==0){%>                         
                <tr>
                    <td width="48%" align="center">
                        <%if(new macScSisMantenimientoBean().validarUsuarioALink(Sesion.getUsuario(),"mantencion")){%>   
                        <table width="100%" border="0">
                            <tr>
                                <td width="50%" align="right">Por Areas:</td>
                                <td width="50%" align="left">
                                    <font class="Estilo10">
                                        <%=vw.listarAreas(Sesion.getListadoAreas(), Sesion.getMantenimiento().getCodigoEspecialidad(), false)%>                    
                                    </font>
                                </td>      
                                <td align="left"><input class="Estilo10" type="submit" value="Listar" name="listarXarea" id="listarXarea"/></td>  
                            </tr>
                            <tr class="Estilo10">
                                <td width="50%" align="right">Por Usuario:</td>
                                <td width="50%" align="left">
                                    <font class="Estilo10">
                                        <%=vw.listarUsuarios(Sesion.getListadoUsuarios(), Sesion.getMantenimiento().getIdUsuarioArea())%>                     
                                    </font>
                                </td>                                                                                   
                                <td align="left"><input class="Estilo10" type="submit" value="Listar" name="listarXusuario" id="listarXusuario"/></td>
                            </tr>
                            <tr class="Estilo10">
                                <td width="50%" align="right">Por Tipo:</td>
                                <td width="50%" align="left">
                                    <font class="Estilo10">
                                        <%=vw.tipoMantencion(request)%>              
                                    </font>
                                </td>                                                                                   
                                <td align="left"><input class="Estilo10" type="submit" value="Listar" name="listarXtipo" id="listarXtipo"/></td>
                            </tr>
                            <tr class="Estilo10">                                 
                                <td align="right" colspan="3"><input class="Estilo10" type="submit" value="Listar todos" name="listarTodos" id="listarTodos"/></td>                                
                            </tr>
                        </table>
                        <%}else{%>
                        &nbsp;
                        <%}%>
                    </td>
                    <td width="4%" align="center">&nbsp;</td>
                    <td width="48%" align="center">
                        <table width="250" border="1" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Cuadro estad�stico</td>                    
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">                            
                                        <tr class="FilaNormal">
                                            <td width="160" <%=vw.verOverTip("<b>Cantidad de Solicitudes no revisadas</b>")%>>no revisadas</td>
                                            <td width="5">:</td>
                                            <td width="35"><%=Sesion.getMantenimiento().getENONE().size()%></td>                                                            
                                            <td align="center"><input type="submit" id="listarNone" name="listarNone" value="Listar" class="Estilo10"></td>                                            
                                        </tr>                                        
                                        <tr class="FilaActivo">
                                            <td width="160" <%=vw.verOverTip("<b>Cantidad de Solicitudes en diagnostico</b>")%>>en diagnostico</td>
                                            <td width="5">:</td>
                                            <td width="35"><%=Sesion.getMantenimiento().getEDIAG().size()%></td>                                                              
                                            <td align="center"><input type="submit" id="listarDiag" name="listarDiag" value="Listar" class="Estilo10"></td>                                            
                                        </tr> 
                                        <tr class="FilaTemporal">
                                            <td width="160" <%=vw.verOverTip("<b>Cantidad de Solicitudes en adquisici�n</b>")%>>en adquisici�n</td>
                                            <td width="5">:</td>
                                            <td width="35"><%=Sesion.getMantenimiento().getEADQN().size()%></td>                                                              
                                            <td align="center"><input type="submit" id="listarAdq" name="listarAdq" value="Listar" class="Estilo10"></td>                                            
                                        </tr> 
                                        <tr class="FilaDisable">
                                            <td width="160" <%=vw.verOverTip("<b>Cantidad de Solicitudes en servicio externo</b>")%>>en servicio externo</td>
                                            <td width="5">:</td>
                                            <td width="35"><%=Sesion.getMantenimiento().getESERE().size()%></td>                                           
                                            <td align="center"><input type="submit" id="listarServExt" name="listarServExt" value="Listar" class="Estilo10"></td>                                            
                                        </tr> 
                                        <tr class="FilaNuevo">
                                            <td width="160" <%=vw.verOverTip("<b>Cantidad de Solicitudes en servicio interno</b>")%>>en servicio interno</td>
                                            <td width="5">:</td>
                                            <td width="35"><%=Sesion.getMantenimiento().getESERI().size()%></td>                                            
                                            <td align="center"><input type="submit" id="listarServInt" name="listarServInt" value="Listar" class="Estilo10"></td>                                            
                                        </tr>
                                        <tr class="FilaModificado">
                                            <td width="160" <%=vw.verOverTip("<b>Cantidad de Solicitudes con visto bueno</b>")%>>con visto bueno</td>
                                            <td width="5">:</td>
                                            <td width="35"><%=Sesion.getMantenimiento().getEVISB().size()%></td>                                                            
                                            <td align="center"><input type="submit" id="listarVistoBueno" name="listarVistoBueno" value="Listar" class="Estilo10"></td>                                            
                                        </tr>
                                        <tr class="FilaAnulado">
                                            <td width="160" <%=vw.verOverTip("<b>Cantidad de Solicitudes con no conformidad</b>")%>>con no conformidad</td>
                                            <td width="5">:</td>
                                            <td width="35"><%=Sesion.getMantenimiento().getENOCO().size()%></td>                                                            
                                            <td align="center"><input type="submit" id="listarNoConformidad" name="listarNoConformidad" value="Listar" class="Estilo10"></td>                                            
                                        </tr>
                                        <tr>                                                                    
                                            <td align="center" colspan="4" class="Titulo"><input type="submit" id="listarAll" name="listarAll" value="Listar Todas" class="Estilo10"></td>                                            
                                        </tr>
                                    </table>                            
                                </td>
                            </tr>                 
                        </table>  
                    </td>
                </tr>                                
                <tr><td colspan="3">&nbsp;</td></tr>     
                <tr>
                    <td colspan="3">
                        <table width="751" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">              
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="751"  border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                        <tr>
                                            <td colspan="11" class="Titulo" align="center">Solicitudes <%=Sesion.getMantenimiento().getTipo()%> </td>
                                            <td colspan="2" class="Titulo" align="right" <%=vw.verOverTip("Haga click para ordenar de forma ascendente o descendente")%>>
                                                <%if(Sesion.getMantenimiento().isOrdenAsc()){%>
                                                <input class="Estilo10" type="submit" value="descendente" name="ordenDesc" id="ordenDesc"/>
                                                <%}else{%>
                                                <input class="Estilo10" type="submit" value="ascendente" name="ordenAsc" id="ordenAsc"/>
                                                <%}%>                        
                                            </td>
                                        </tr>    
                                        <tr>
                                            <td colspan="13"><%=Sesion.getPset().verPaginador("SOLI", true, Sesion.getMantenimiento().isOrdenAsc())%></td>
                                        </tr>
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>
                                            <td width="30">O.M</td>
                                            <td width="52">AREA</td>
                                            <td width="52">MAQ.</td>
                                            <td width="52">FALLA</td>
                                            <td width="75">F.ING.</td>
                                            <td width="75">DIAG.</td>
                                            <td width="75">ADQUI.</td>
                                            <td width="75">SEV.EXT.</td>
                                            <td width="75">SEV.INT.</td>
                                            <td width="75">V�B�C.</td>
                                            <td width="75">RECHA.</td>
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle">
                                        <table width="730"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="30"></td>
                                                <td width="52"></td>
                                                <td width="52"></td>
                                                <td width="52"></td>
                                                <td width="75"></td>
                                                <td width="75"></td>
                                                <td width="75"></td>
                                                <td width="75"></td>
                                                <td width="75"></td>
                                                <td width="75"></td>
                                                <td width="75"></td>                                                                                                                  
                                            </tr>
                                            <%=vw.listarPedidosXtipo(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>                 
                            <tr><td align="right">
                                    <a href="../../XlsSolSegRequest">
                                        <img alt="" border="0" src="../../Imagenes/btn_excel.gif" border="0" <%=vw.verOverTip("Haga click para exportar a excel")%>/>
                                    </a>
                            </td></tr>   
                        </table>  
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3">                        
                        <table align="center">
                            <tr>                            
                                <td align="right">&nbsp;</td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <a href="../../"><img alt="" src="../../Imagenes/volver2.jpg" border="0" <%=vw.verOverTip("Haga click para volver")%> /></a>
                                </td> 
                            </tr>                 
                        </table>
                    </td> 
                </tr>  
                <%}else if(Sesion.getMantenimiento().getEtapa()==1){%> 
                <tr><td colspan="3">        
                        <table width="700" border="1" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Detalle Solicitud</td>                    
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">                            
                                        <tr class="Paginador">
                                            <td width="110" valign="top">M�quina</td>
                                            <td width="5" valign="top">:</td>
                                            <td width="200" class="ConBorder" valign="top"><%=Sesion.getMantenimiento().getDetalle().getNombreMaquina()%></td>
                                            <td>&nbsp;</td>                    
                                            <td width="110" valign="top">Orden</td>
                                            <td width="5" valign="top">:</td>
                                            <td width="200" class="ConBorder" valign="top"><%=Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento()%></td>
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <tr class="Paginador">
                                            <td valign="top">Area</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getNombreArea())%></td>
                                            <td>&nbsp;</td>
                                            <td valign="top">Cliente</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getNombreUsuarioMantenimiento())%></td>                                
                                        </tr>    
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <%if(Sesion.getMantenimiento().getDetalle().getNumeroSap()!=null && !Sesion.getMantenimiento().getDetalle().getNumeroSap().trim().equals("")){%>
                                        <tr class="Paginador">
                                            <td valign="top">N�mero Sap</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getNumeroSap())%></td>
                                            <td>&nbsp;</td>    
                                            <td valign="top">&nbsp;</td>
                                            <td valign="top">&nbsp;</td>                                            
                                            <td valign="top">&nbsp;</td>                                             
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <%}%>   
                                        <%if(Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_rut()!=null && !Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_rut().trim().equals("")){%>
                                        <tr class="Paginador">
                                            <td valign="top">Personal Encargado</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_nombre()).concat("&nbsp;").concat(vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_apellido()))%></td>
                                            <td>&nbsp;</td>    
                                            <td valign="top">&nbsp;</td>
                                            <td valign="top">&nbsp;</td>                                            
                                            <td valign="top">&nbsp;</td>                                             
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <%}%>   
                                        <%if(Sesion.getMantenimiento().getDetalle().getTurnoMaq().getNombreTurnoMaq()!=null && !Sesion.getMantenimiento().getDetalle().getTurnoMaq().getNombreTurnoMaq().trim().equals("")){%>
                                        <tr class="Paginador">
                                            <td valign="top">Turno M�quina</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getTurnoMaq().getNombreTurnoMaq())%></td>
                                            <td>&nbsp;</td>                                                            
                                            <td valign="top">
                                                <%if(Sesion.getTipoUsuario().equals("admin")){%>
                                                <a class="NewItem" href="../../FrmMantMaquina?cambiarTurno=SI" <%=vw.verOverTip("Haga click para cambiar el turno actual de la M�quina")%>>Cambiar Turno</a>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td> 
                                            <td valign="top">&nbsp;</td>
                                            <td valign="top">&nbsp;</td> 
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <%}%> 
                                        <%if(Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo()!=null && !Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo().trim().equals("")){%>
                                        <tr class="Paginador">
                                            <td valign="top">Fecha Hora Detenci�n Equipo</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo().substring(0, 16))%></td>
                                            <td>&nbsp;</td>    
                                            <td valign="top">&nbsp;</td>
                                            <td valign="top">&nbsp;</td>                                            
                                            <td valign="top">&nbsp;</td> 
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <%}%>       
                                        <%if(Sesion.getMantenimiento().getDetalle().getFechaEntregaEquipo()!=null && !Sesion.getMantenimiento().getDetalle().getFechaEntregaEquipo().trim().equals("")){%>
                                        <tr class="Paginador">
                                            <td valign="top">Fecha Hora T�rmino Equipo</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getFechaEntregaEquipo().substring(0, 16))%></td>
                                            <td>&nbsp;</td>    
                                            <td valign="top">&nbsp;</td>
                                            <td valign="top">&nbsp;</td>                                            
                                            <td valign="top">&nbsp;</td>                                             
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <%}%>  
                                        <%if((Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo()!=null && !Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo().trim().equals(""))&&
                                             (Sesion.getMantenimiento().getDetalle().getFechaEntregaEquipo()!=null && !Sesion.getMantenimiento().getDetalle().getFechaEntregaEquipo().trim().equals(""))){%>
                                        <tr class="Paginador">
                                            <td valign="top">Horas Detenci�n Equipo</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">Horas: <%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getHorasDetencion())%> Minutos: <%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getMinutosDetencion())%></td>
                                            <td>&nbsp;</td>    
                                            <td valign="top">
                                                <%if(Sesion.getTipoUsuario().equals("admin")){%>
                                                <a class="NewItem" href="../../FrmNuevoStatus?calcularHDM=SI" <%=vw.verOverTip("Haga click para calcular horas de detenci�n equipo")%>>Calcular Horas</a>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td> 
                                            <td valign="top">&nbsp;</td>                                            
                                            <td valign="top">&nbsp;</td>                                             
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>  
                                        <%}%> 
                                        <tr class="Paginador">
                                            <td valign="top">Fecha Falla</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">
                                                <%if(Sesion.getMantenimiento().getDetalle().getFechaHoy()!=null){%>
                                                <%=Sesion.getMantenimiento().getDetalle().getFechaHoy().substring(0,10)%>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td valign="top">Falla</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">
                                                <%if(Sesion.getMantenimiento().getDetalle().getNombreFalla()!=null){%>
                                                <%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getNombreFalla())%>
                                                <%}else{%>
                                                Otras
                                                <%}%>
                                            </td>                                                                                
                                        </tr>
                                        <%if((Sesion.getMantenimiento().getDetalle().getObservacionFalla()!=null) && (!Sesion.getMantenimiento().getDetalle().getObservacionFalla().trim().equals(""))){%>
                                        <tr class="Paginador">
                                            <td valign="top">Observaci�n Falla</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionFalla())%></td>
                                            <td>&nbsp;</td>    
                                            <td valign="top">&nbsp;</td>
                                            <td valign="top">&nbsp;</td>                                            
                                            <td valign="top">
                                                <%if(Sesion.getTipoUsuario().equals("admin")){%>
                                                <a class="NewItem" href="../../FrmMantFalla?addFail=SI" <%=vw.verOverTip("Haga click para ingresar nueva falla")%>>ingresar nueva Falla</a>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td>                                             
                                        </tr>
                                        <%}%>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <tr class="Paginador">
                                            <td valign="top">Fecha Diagn�stico</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">
                                                <%if(Sesion.getMantenimiento().getDetalle().getFechaDiagnostico()!=null){%>
                                                <%=Sesion.getMantenimiento().getDetalle().getFechaDiagnostico().substring(0,10)%>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td valign="top">Observaci�n</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionDiagnostico())%></td>                                
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <tr class="Paginador">
                                            <td valign="top">Fecha Adquisici�n</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">                                    
                                                <%if(Sesion.getMantenimiento().getDetalle().getFechaAdquisicion()!=null){%>
                                                <%=Sesion.getMantenimiento().getDetalle().getFechaAdquisicion().substring(0,10)%>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td valign="top">Observaci�n</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionAdquisicion())%></td>                                
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <tr class="Paginador">
                                            <td valign="top">Fecha Servicio Externo</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">                                    
                                                <%if(Sesion.getMantenimiento().getDetalle().getFechaSevicioExterno()!=null){%>
                                                <%=Sesion.getMantenimiento().getDetalle().getFechaSevicioExterno().substring(0,10)%>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td valign="top">Observaci�n</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionSevicioExterno())%></td>                                
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>  
                                        <%if(Sesion.getMantenimiento().getDetalle().getEmpExt().getVc_rut()!=null && !Sesion.getMantenimiento().getDetalle().getEmpExt().getVc_rut().trim().equals("")){%>
                                        <tr class="Paginador">
                                            <td valign="top">Empresa Externa</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getEmpExt().getVc_nombre())%></td>
                                            <td>&nbsp;</td>    
                                            <td valign="top">Empleado Externo</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getEmplEmt().getVc_nombre())%>&nbsp;<%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getEmplEmt().getVc_apellido())%></td>                                          
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <%}%>  
                                        <tr class="Paginador">
                                            <td valign="top">Fecha Servicio Interno</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">                                    
                                                <%if(Sesion.getMantenimiento().getDetalle().getFechaMantenimientoCorrectivo()!=null){%>
                                                <%=Sesion.getMantenimiento().getDetalle().getFechaMantenimientoCorrectivo().substring(0,10)%>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td valign="top">Observaci�n</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionMantenimientoCorrectivo())%></td>                                
                                        </tr>
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <tr class="Paginador">
                                            <td valign="top">Fecha V�B� Cliente</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">                                    
                                                <%if(Sesion.getMantenimiento().getDetalle().getFechaVistoBueno()!=null){%>
                                                <%=Sesion.getMantenimiento().getDetalle().getFechaVistoBueno().substring(0,10)%>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td valign="top">Observaci�n</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionVistoBueno())%></td>                                
                                        </tr>       
                                        <tr class="FilaDisable"><td height="5" colspan="7"></td></tr>
                                        <tr class="Paginador">
                                            <td valign="top">Fecha Rechazo</td>
                                            <td valign="top">:</td>
                                            <td valign="top" class="ConBorder">                                    
                                                <%if(Sesion.getMantenimiento().getDetalle().getFechaNoConformidad()!=null){%>
                                                <%=Sesion.getMantenimiento().getDetalle().getFechaNoConformidad().substring(0,10)%>
                                                <%}else{%>
                                                &nbsp;
                                                <%}%>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td valign="top">Observaci�n</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top"><%=vw.verHtmlNull(Sesion.getMantenimiento().getDetalle().getObservacionNoConformidad())%></td>                                
                                        </tr>
                                    </table>                            
                                </td>
                            </tr>                 
                        </table>
                </td></tr>     
                <%if(!Sesion.getTipoUsuario().equals("calidad")){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">
                        <table width="700" border="1" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Bit�cora Solicitud</td>                    
                            </tr>
                            <tr><td>
                                    <table class="Contenido" width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <%=vw.listarEstados(request)%>                            
                                    </table>                                
                            </td></tr>               
                        </table>
                </td></tr>
                <%}%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">
                        <table width="700" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">
                            <tr>  
                                <td align="center">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver al listado")%> />
                                </td>
                            </tr>                 
                        </table>
                </td></tr>
                <% }else if(Sesion.getMantenimiento().getEtapa()==2){%> 
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr><td colspan="3">
                        <table width="700" border="1" align="center" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr>                    
                                <td align="center" class="Titulo">Detalle&nbsp;<%=Sesion.getMantenimiento().getTituloBitacora()%></td>                    
                            </tr>
                            <tr>
                                <td align="center">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">                                                        
                                        <tr class="Paginador">
                                            <%if(Sesion.getMantenimiento().getDetalle().getIdEstado()<5){%>
                                            <DIV id="popCal" class="Estilo10" style="BORDER-RIGHT: 0px ridge; BORDER-TOP: 0px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 0px ridge; WIDTH: 10px; BORDER-BOTTOM: 0px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                                <IFRAME name="popFrame" src="../popcjs.html" frameBorder=0 width=183 scrolling=no height=188></IFRAME>
                                            </DIV>
                                            <td>Fecha</td>
                                            <td>:</td>
                                            <td class="ConBorder">
                                                <input class="Estilo10" name="dia" id="dia" size="12" value="" readonly="yes"/>
                                                <a onclick="popFrame.fPopCalendar(dia,dia,popCal);return false">
                                                    <img src="../../Imagenes/calendar.gif" width="16" height="16" alt="calendar"/>
                                                </a>&nbsp;a�o-mes-dia
                                            </td>                                
                                            <%}else{%>
                                            <td>Fecha</td>
                                            <td>:</td>
                                            <td class="ConBorder">
                                                <input class="Estilo10" name="dia" id="dia" size="12" value="<%=vw.getFechaActual(vw.FORMATO_YMD)%>" readonly="yes"/>&nbsp;a�o-mes-dia
                                            </td>                                
                                            <%}%>
                                        </tr>                             
                                        <tr class="Paginador">                                
                                            <td valign="top">Observaci�n</td>
                                            <td valign="top">:</td>
                                            <td class="ConBorder" valign="top">
                                                <textarea cols="30" rows="2" class="txtTexto" name="txtObvservacion" id="txtObvservacion"></textarea>
                                            </td>                                
                                        </tr>                                         
                                        <tr class="Paginador"> 
                                            <td>Encargado Trabajo</td>
                                            <td>:</td>
                                            <td class="ConBorder">
                                                <%if(Sesion.getMantenimiento().getDetalle().getIdEstado()==1){%>
                                                <%=vw.comboPerMant(request)%>
                                                <%}else{                                                    
                                                String nombrePerMant = "";
                                                if(Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_nombre()!=null && !Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_nombre().trim().equals("")){
                                                nombrePerMant += Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_nombre().trim();
                                                }
                                                if(Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_apellido()!=null && !Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_apellido().trim().equals("")){
                                                nombrePerMant += " " + Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_apellido().trim();
                                                }
                                                %>
                                                <input class="Estilo10" name="txtEncargado" id="txtEncargado" size="40" value="<%=nombrePerMant%>" readonly="yes"/>
                                                <%}%>
                                            </td>  
                                        </tr>                                         
                                        <tr class="Paginador"> 
                                            <td>N�mero Sap</td>
                                            <td>:</td>
                                            <td class="ConBorder">
                                                <%
                                                String numSap = "";
                                                if(Sesion.getMantenimiento().getDetalle().getNumeroSap()!=null && !Sesion.getMantenimiento().getDetalle().getNumeroSap().trim().equals("")){
                                                    numSap = Sesion.getMantenimiento().getDetalle().getNumeroSap().trim();
                                                }
                                                %>
                                                <%if(Sesion.getMantenimiento().getDetalle().getIdEstado()==1){%>                                                
                                                <input class="Estilo10" name="txtSap" id="txtSap" size="12" value="<%=numSap%>" />
                                                <%}else{%>    
                                                <input class="Estilo10" name="txtSap" id="txtSap" size="12" value="<%=numSap%>" readonly="yes"/>
                                                <%}%>
                                            </td>  
                                        </tr> 
                                        <%if(Sesion.getMantenimiento().getDetalle().getIdEstado()==3 || Sesion.getMantenimiento().getDetalle().getIdEstado()==4){%>                                           
                                        <tr class="Paginador">
                                            <DIV id="popCal1" class="Estilo10" style="BORDER-RIGHT: 0px ridge; BORDER-TOP: 0px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 0px ridge; WIDTH: 10px; BORDER-BOTTOM: 0px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                                <IFRAME name="popFrame1" src="../popcjs.html" frameBorder=0 width=183 scrolling=no height=188></IFRAME>
                                            </DIV>
                                            <td><font class="Estilo10">Fecha T�rmino</font></td>
                                            <td>:</td>
                                            <td class="ConBorder">
                                                <input class="Estilo10" name="diaTermino" id="diaTermino" size="12" value="" readonly="yes"/>
                                                <a onclick="popFrame1.fPopCalendar(diaTermino,diaTermino,popCal1);return false">
                                                    <img src="../../Imagenes/calendar.gif" width="16" height="16" alt="calendar"/>
                                                </a>&nbsp;a�o-mes-dia
                                            </td>  
                                        </tr>  
                                        <tr class="Paginador">
                                            <td><font class="Estilo10">Hora T�rmino</font></td>
                                            <td>:</td>
                                            <td class="ConBorder">
                                                <font class="Estilo10">
                                                    <input class="Estilo10" name="horaTermino" id="horaTermino" size="8" value=""/>
                                                </font>&nbsp;Hh:mm (Ej.: 15:30)
                                            </td>
                                        </tr>  
                                        <%if(Sesion.getMantenimiento().getDetalle().getIdEstado()==3){%>
                                        <%=vw.make_ScriptEMPL(Sesion.getMantenimiento().getListadoEmpExt())%>
                                        <tr class="Paginador">
                                            <td><font class="Estilo10">Empresa Externa</font></td>
                                            <td>:</td>
                                            <td class="ConBorder">                                                
                                                <%=vw.make_SelectEMPR(Sesion.getMantenimiento().getListadoEmpExt())%>                                         
                                            </td>
                                        </tr> 
                                        <tr class="Paginador">
                                            <td><font class="Estilo10">Empleado Externo</font></td>
                                            <td>:</td>
                                            <td class="ConBorder">                                                
                                                <%=vw.make_SelectEMPL()%>                                                
                                            </td>
                                        </tr> 
                                        <%}%>
                                        <%}%>
                                    </table>                     
                                </td>
                            </tr>                 
                        </table>
                </td></tr>
                <tr><td colspan="3">&nbsp;</td></tr>  
                <tr><td colspan="3">
                        <table width="700" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">                
                            <tr>    
                                <td align="center" colspan="2">&nbsp;</td>                    
                                <td align="right">
                                    <input type="submit" name="guardarEstado" id="guardarEstado" value="Grabar" class="Estilo10" <%=vw.verOverTip("Haga click para guardar")%> />&nbsp;
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para cancelar acci�n y volver")%> />
                                </td>
                            </tr>
                        </table>
                </td></tr>
                <%}%>
            </table>
            <%
                }catch(Exception ex){                    
                    mac.sc.sis.mantenimiento.utils.EscribeFichero.addError(ex, true);
                }
            %>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
<script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
</script>
<%Sesion.cleanTip();%>
<%}%>
