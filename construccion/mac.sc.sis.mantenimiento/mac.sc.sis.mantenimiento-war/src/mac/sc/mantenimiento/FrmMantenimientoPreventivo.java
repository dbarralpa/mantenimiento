/*
 * FrmMantenimientoPreventivo.java
 *
 * Created on 15 de febrero de 2012, 15:34
 */

package mac.sc.mantenimiento;

import java.io.*;
import java.util.ArrayList;
import javax.rmi.PortableRemoteObject;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.contador.Contador;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoRemoteHome;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModIntervalo;
import mac.sc.sis.mantenimiento.models.ModMantenimiento;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

/**
 *
 * @author dbarra
 * @version
 */
public class FrmMantenimientoPreventivo extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ModSesion Sesion = null;
        int url = 0;
        try {
            response.setContentType("text/html; charset=iso-8859-1");
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            if (request.getParameter("mant_prev.x") != null) {
                /*if (!Sesion.getUsuario().trim().equals("")) {
                    Contador.insertarContador(61, Sesion.getUsuario());
                }*/
                if (Sesion.getBeanMan() == null) {
                    conexionEJB.ConexionEJB cal = new conexionEJB.ConexionEJB();
                    Object ref1 = cal.conexionBean("macScSisMantenimientoBean");
                    macScSisMantenimientoRemoteHome manRemoteHome = (macScSisMantenimientoRemoteHome) PortableRemoteObject.narrow(ref1, macScSisMantenimientoRemoteHome.class);
                    Sesion.setBeanMan(manRemoteHome.create());
                }
                Sesion.getMantenimiento().setListadoTareas(new ArrayList());
                Sesion.setMantenimiento(new ModMantenimiento());
                Sesion.getMantenimiento().setListadoMaquinas(Sesion.getBeanMan().listarMaquinas());
                //Sesion.getMantenimiento().setIntervalos(Sesion.getBeanMan().listarIntervalos());
                Sesion.getMantenimiento().getMaquina().setIdMaquina(-1);
                Sesion.getMantenimiento().getMaquina().setModIntervalo(new ModIntervalo());
                Sesion.getMantenimiento().getMaquina().getModIntervalo().setId(-1);
                Sesion.setEtapa(0);
            }
            if(request.getParameter("enviar") != null){
                if(request.getParameter("maquina") != null && !request.getParameter("maquina").equals("-1") && request.getParameter("intervalo") != null && !request.getParameter("intervalo").equals("-1")){
                    Sesion.getMantenimiento().setListadoTareas(Sesion.getBeanMan().listareasIntervalos(Integer.parseInt(request.getParameter("intervalo")),Integer.parseInt(request.getParameter("maquina"))));
                    Sesion.getMantenimiento().getMaquina().setIdMaquina(Integer.parseInt(request.getParameter("maquina")));
                    Sesion.getMantenimiento().getMaquina().getModIntervalo().setId(Integer.parseInt(request.getParameter("intervalo")));
                    Sesion.setEtapa(1);
                }
            }
             if(request.getParameter("maquina") != null && !request.getParameter("maquina").equals("-1")){
                Sesion.getMantenimiento().setIntervalos(Sesion.getBeanMan().listarIntervalos(Integer.parseInt(request.getParameter("maquina"))));
                Sesion.getMantenimiento().getMaquina().setIdMaquina(Integer.parseInt(request.getParameter("maquina")));
             }
            
            if(request.getParameter("enviarAgregarTarea") != null){
                if(request.getParameter("agregarTarea") != null && !request.getParameter("agregarTarea").trim().equals("")){
                    int idTarea = Sesion.getBeanMan().maxTareas();
                    int idMquinaTarea = Sesion.getBeanMan().idMquinaTarea(Sesion.getMantenimiento().getMaquina().getIdMaquina(),Sesion.getMantenimiento().getMaquina().getModIntervalo().getId());
                    int tipo = 0;
                    String tipo1 = "";
                    if(request.getParameter("mecanica") != null){
                        tipo = 1;
                        tipo1 = "MEC�NICA";
                    }
                    if(request.getParameter("electrica") != null){
                        tipo = 2;
                        tipo1 = "EL�CTRICA";
                    }
                    boolean est = Sesion.getBeanMan().insertarTarea(idTarea,request.getParameter("agregarTarea"),tipo,idMquinaTarea,Sesion.getMantenimiento().getMaquina().getIdMaquina());
                    if(est){
                        Mantenimiento man = new Mantenimiento();
                        man.setNombreArea(request.getParameter("agregarTarea"));
                        man.setComentario(tipo1);
                        man.setId_tareas(idTarea);
                        Sesion.getMantenimiento().getListadoTareas().add(man);
                        man = null;
                    }
                }
            }
            if(request.getParameter("eliminarTareas") != null){
                int idMquinaTarea = Sesion.getBeanMan().idMquinaTarea(Sesion.getMantenimiento().getMaquina().getIdMaquina(),Sesion.getMantenimiento().getMaquina().getModIntervalo().getId());
                for (int i = 0; i < Sesion.getMantenimiento().getListadoTareas().size(); i++) {
                    if(request.getParameter("tarea"+i) != null){
                        Mantenimiento mante = (Mantenimiento) Sesion.getMantenimiento().getListadoTareas().get(i);
                        if(Sesion.getBeanMan().borrarTarea(idMquinaTarea, Sesion.getMantenimiento().getMaquina().getIdMaquina(), mante.getId_tareas())){
                            mante.setEstado("S");
                            Sesion.getMantenimiento().getListadoTareas().set(i,mante);
                        }
                        
                    }
                }
                for (int i = 0; i < Sesion.getMantenimiento().getListadoTareas().size(); i++) {
                    Mantenimiento mante = (Mantenimiento) Sesion.getMantenimiento().getListadoTareas().get(i);
                    if(mante.getEstado() != null && mante.getEstado().equals("S")){
                        Sesion.getMantenimiento().getListadoTareas().remove(i);
                        i -= 1;
                    }
                    mante = null;
                }
            }
            
            if(request.getParameter("enviarTiempoEjecucion") != null){
                if(Sesion.getMantenimiento().getMaquina().getIdMaquina() != -1 && !request.getParameter("ano").equals("-1") && !request.getParameter("mes").equals("-1") && !request.getParameter("tiempoEjecucion").trim().equals("") && !request.getParameter("presupuesto").trim().equals("")){
                    boolean est = Sesion.getBeanMan().countForecast(Sesion.getMantenimiento().getMaquina().getIdMaquina(),Integer.parseInt(request.getParameter("ano")),Integer.parseInt(request.getParameter("mes")));
                    Sesion.getBeanMan().insertarForecast(Sesion.getMantenimiento().getMaquina().getIdMaquina(),Double.parseDouble(request.getParameter("tiempoEjecucion")),Double.parseDouble(request.getParameter("presupuesto")),Integer.parseInt(request.getParameter("ano")),Integer.parseInt(request.getParameter("mes")),est);
                }
            }
            if(request.getParameter("volverInicio.x") != null){
                url = 1;
            }
            if(request.getParameter("pagMANT1") != null){
                Sesion.getPset().getDetalle("MANT1").pag = Integer.parseInt(request.getParameter("pagMANT1"));
                url = 2;
            }
            if(request.getParameter("ordenDesc2")!=null){
                Sesion.getMantenimiento().setOrdenAscNew(false);
                url = 2;
            }
            
            if(request.getParameter("ordenAsc2")!=null){
                Sesion.getMantenimiento().setOrdenAscNew(true);
                url = 2;
            }
            if(request.getParameter("ver2")!=null){
                Sesion.getMantenimiento().setIdDetalle(Integer.parseInt(request.getParameter("ver2")));
                Sesion.getMantenimiento().setDetalle((Mantenimiento)Sesion.getMantenimiento().getListadoPricipal().get(Sesion.getMantenimiento().getIdDetalle()));
                Sesion.getMantenimiento().setListadoTareas(Sesion.getBeanMan().listarTareasNoHechas(Sesion.getMantenimiento().getDetalle().getId_tareas()));
                Sesion.getMantenimiento().setEtapa(4);
                url = 2;
            }
            if(request.getParameter("volverAtras1.x")!=null){
                Sesion.setMantenimiento(new ModMantenimiento());
                Sesion.getMantenimiento().setListaEstados(Sesion.getBeanMan().listarTodosLosEstados());
                Sesion.getMantenimiento().setTipo("USUARIO");
                Sesion.getMantenimiento().setListadoPricipal(Sesion.getBeanMan().solicitudesPorUsuario(Sesion.getIdUsuario()));
                Sesion.getMantenimiento().setListadoTMP(new ArrayList());
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
                PrmApp.setEstadosListado(Sesion.getMantenimiento());
                url = 4;
            }
             if(request.getParameter("volverAtras.x")!=null){
                Sesion.getMantenimiento().setEtapa(0);
                url = 2;
            }
            
            if(request.getParameter("actualizarTareas") != null){
                int a = 0;
                boolean esta = false;
                if(request.getParameter("finalizarProceso") != null){
                    if(!request.getParameter("dia").trim().equals("") && !request.getParameter("dia1").trim().equals("") && !request.getParameter("txtObvservacion").trim().equals("") && !request.getParameter("ejecutor").trim().equals("")){
                        for (int i = 0; i < Sesion.getMantenimiento().getListadoTareas().size(); i++) {
                            Mantenimiento mante = (Mantenimiento) Sesion.getMantenimiento().getListadoTareas().get(i);
                            Sesion.getBeanMan().insertarTareaHecha(Sesion.getMantenimiento().getDetalle().getId_tareas(),mante.getId_tareas());
                            a++;
                            mante = null;
                        }
                        if(Sesion.getMantenimiento().getListadoTareas().size() == a){
                            esta = Sesion.getBeanMan().actualizarMantencionPreventiva(Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento(),request.getParameter("dia"),request.getParameter("dia1"),request.getParameter("ejecutor"),request.getParameter("txtObvservacion"),true);
                        }
                        if(esta){
                            esta = Sesion.getBeanMan().actualizarPrograma(Sesion.getMantenimiento().getDetalle().getId_tareas());
                        }
                        if(esta){
                            Sesion.getMantenimiento().setListadoPedidosMantenPreventivos(Sesion.getBeanMan().listarPedidosMantePreventivos(Sesion.getIdUsuario(),true));
                            //Sesion.getPset().indiceMant1(Sesion.getMantenimiento().getListadoPedidosMantenPreventivos());
                            Sesion.getMantenimiento().setListadoPricipal(Sesion.getMantenimiento().getListadoPedidosMantenPreventivos());
                            Sesion.getPset().indiceMant1(Sesion.getMantenimiento().getListadoPricipal());
                            Sesion.getMantenimiento().setEtapa(0);
                        }else{
                            Sesion.getMantenimiento().setEtapa(3);
                        }
                    }
                }else{
                   if(!request.getParameter("dia").trim().equals("") && !request.getParameter("ejecutor").trim().equals("")){ 
                    for (int i = 0; i < Sesion.getMantenimiento().getListadoTareas().size(); i++) {
                        if(request.getParameter("tarea"+i) != null){
                            Mantenimiento mante = (Mantenimiento) Sesion.getMantenimiento().getListadoTareas().get(i);
                            Sesion.getBeanMan().insertarTareaHecha(Sesion.getMantenimiento().getDetalle().getId_tareas(),mante.getId_tareas());
                        }
                    }
                     esta = Sesion.getBeanMan().actualizarMantencionPreventiva(Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento(),request.getParameter("dia"),request.getParameter("dia1"),request.getParameter("ejecutor"),request.getParameter("txtObvservacion"),false);
                     if(esta){
                      Sesion.getMantenimiento().setListadoPedidosMantenPreventivos(Sesion.getBeanMan().listarPedidosMantePreventivos(Sesion.getIdUsuario(),true));  
                      Sesion.getPset().indiceMant1(Sesion.getMantenimiento().getListadoPedidosMantenPreventivos());
                      Sesion.getMantenimiento().setListadoPricipal(Sesion.getMantenimiento().getListadoPedidosMantenPreventivos());
                      Sesion.getMantenimiento().setEtapa(0);
                     }else{
                       Sesion.setTip("No se actualizaron los datos", 3);
                     }
                   }else{
                       Sesion.setTip("Se debe llenar al menos los campos fecha inicio y ejecutor", 3);   
                   }
                }
                url = 2;
            }
            if (request.getParameter("enviarMaquina") != null) {
                Sesion.getMantenimiento().setIdDetalle1(Integer.parseInt(request.getParameter("maquinaPreventiva")));
                Sesion.getMantenimiento().setTipo("POR TIPO");
                Sesion.getMantenimiento().setTipoAnterior("POR TIPO");
                Sesion.getMantenimiento().setListadoPricipal(Sesion.getBeanMan().listarPedidosMantePreventivos(Sesion.getIdUsuario(), Sesion.getMantenimiento().getIdDetalle1()));
                Sesion.getMantenimiento().setListadoTMP(new ArrayList());
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getPset().indiceMant1(Sesion.getMantenimiento().getListadoPricipal());
                PrmApp.setEstadosListado(Sesion.getMantenimiento());
                url = 2;
            }
                        
            
            if (url == 0) {
                response.sendRedirect("jsp/mantenimiento/modificarProgramaPreventivo.jsp");
            }else if (url == 2) {
                response.sendRedirect("jsp/mantenimiento/mantenimientoPreventivo.jsp");
            }else if(url == 4){
                response.sendRedirect("jsp/mantenimiento/ingresarNuevoStatus.jsp");
            }  else {
                response.sendRedirect("inicioMantenimiento.jsp");
            }
            
            
            
        } catch (Exception ex) {
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
