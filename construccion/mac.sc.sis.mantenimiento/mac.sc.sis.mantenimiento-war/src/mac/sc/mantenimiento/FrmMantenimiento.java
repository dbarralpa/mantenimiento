package mac.sc.mantenimiento;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.models.ModFalla;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModMantenimiento;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantenimiento extends HttpServlet {
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        byte est = 0;
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
            
            if(request.getParameter("area")!=null){
                Sesion.getMantenimiento().setCodigoEspecialidad(Integer.parseInt(request.getParameter("area")));
                Sesion.getMantenimiento().getMaquina().setIdMaquina(-1);
            }
            
            if(request.getParameter("maquina")!=null){
                Sesion.getMantenimiento().getMaquina().setIdMaquina(Integer.parseInt(request.getParameter("maquina")));
                Sesion.getMantenimiento().setCodigoFalla(-1);
                if(Sesion.getMantenimiento().getMaquina().getIdMaquina()==-1 || Sesion.getMantenimiento().getCodigoEspecialidad()==-1){
                    Sesion.getMantenimiento().setListadoFallas(null);
                }else{
                    Sesion.getMantenimiento().setListadoFallas(Sesion.getBeanMan().listarFallas(Sesion.getMantenimiento().getMaquina().getIdMaquina()));
                }
            }
            
            if(request.getParameter("falla")!=null){
                Sesion.getMantenimiento().setCodigoFalla(Integer.parseInt(request.getParameter("falla")));
                if(Sesion.getMantenimiento().getListadoFallas()!=null && Sesion.getMantenimiento().getCodigoFalla()>0){
                    for(int i=0; i<Sesion.getMantenimiento().getListadoFallas().size(); i++){
                        ModFalla falla = (ModFalla)Sesion.getMantenimiento().getListadoFallas().get(i);
                        if(Sesion.getMantenimiento().getCodigoFalla()==falla.getIdFalla()){
                            Sesion.getMantenimiento().setDescFalla(falla.getDescripcion());
                            break;
                        }else{
                            Sesion.getMantenimiento().setDescFalla("");
                        }
                    }
                }else{
                    Sesion.getMantenimiento().setDescFalla("");
                }
            }
            
            if(request.getParameter("txtObservacion")!=null){
                Sesion.getMantenimiento().setMensaje(request.getParameter("txtObservacion").trim());
            }
            
            if(request.getParameter("tipoFalla")!=null){
                Sesion.getMantenimiento().getTipoFalla().setInicial(request.getParameter("tipoFalla"));
            }
            
            if(request.getParameter("nuevaSol")!=null){
                Sesion.getMantenimiento().setEtapa(1);
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getMantenimiento().getMaquina().setIdMaquina(-1);
                Sesion.getMantenimiento().setCodigoFalla(-1);
                Sesion.getMantenimiento().setMensaje("");
                Sesion.getMantenimiento().setListadoTipoFallas(Sesion.getBeanMan().listarTipoFallas());
            }
            
            if(request.getParameter("ver")!=null){
                Sesion.getMantenimiento().setIdDetalle(Integer.parseInt(request.getParameter("ver")));
                Sesion.getMantenimiento().setDetalle((Mantenimiento)Sesion.getMantenimiento().getListadoPedidosMantenimientos().get(Sesion.getMantenimiento().getIdDetalle()));
                Sesion.getMantenimiento().setEtapa(2);
            }
            
            if(request.getParameter("ver1")!=null){
                Sesion.getMantenimiento().setIdDetalle(Integer.parseInt(request.getParameter("ver1")));
                Sesion.getMantenimiento().setDetalle((Mantenimiento)Sesion.getMantenimiento().getListadoPedidosMantenPreventivos().get(Sesion.getMantenimiento().getIdDetalle()));
                Sesion.getMantenimiento().setEtapa(3);
            }
            if(request.getParameter("ver2")!=null){
                Sesion.getMantenimiento().setIdDetalle(Integer.parseInt(request.getParameter("ver2")));
                Sesion.getMantenimiento().setDetalle((Mantenimiento)Sesion.getMantenimiento().getListadoPricipal().get(Sesion.getMantenimiento().getIdDetalle()));
                Sesion.getMantenimiento().setListadoTareas(Sesion.getBeanMan().listarTareasNoHechas(Sesion.getMantenimiento().getDetalle().getId_tareas()));
                Sesion.getMantenimiento().setEtapa(4);
                est = 1;
            }
            
            
            
            if(request.getParameter("volverAtras.x")!=null){
                Sesion.getMantenimiento().setEtapa(0);
                est = 2;
            }
            
            if(request.getParameter("volverMant_0.x")!=null){
                Sesion.getMantenimiento().setEtapa(0);
            }
            
            if(request.getParameter("pagMANT") != null){
                Sesion.getPset().getDetalle("MANT").pag = Integer.parseInt(request.getParameter("pagMANT"));
            }
            if(request.getParameter("pagMANT1") != null){
                Sesion.getPset().getDetalle("MANT1").pag = Integer.parseInt(request.getParameter("pagMANT1"));
            }
            if(request.getParameter("ingresarNew")!=null){
                String dia = "";
                String hora = "";
                if(request.getParameter("dia")!=null && !request.getParameter("dia").trim().equals("")){
                    dia = request.getParameter("dia");
                }
                if(request.getParameter("hora")!=null && !request.getParameter("hora").trim().equals("")){
                    hora = request.getParameter("hora");
                }
                Sesion.getMantenimiento().setDiaDetencionEquipo(dia);
                Sesion.getMantenimiento().setHoraDetencionEquipo(hora);
                Sesion.getMantenimiento().setFechaDetencionEquipo(PrmApp.verFecha(dia).trim().concat(" ").concat(hora.trim()).concat(":00"));
                //Sesion.getMantenimiento().setTipo(request.getParameter("tipo"));
                
                if(Sesion.getMantenimiento().getCodigoEspecialidad()!=-1 &&
                        Sesion.getMantenimiento().getMaquina().getIdMaquina()!=-1 &&
                        Sesion.getMantenimiento().getCodigoFalla()!=-1 &&
                        !Sesion.getMantenimiento().getFechaDetencionEquipo().equals("") &&
                        !Sesion.getMantenimiento().getTipoFalla().getInicial().equals("-1")) {
                    if(Sesion.getMantenimiento().getCodigoFalla()>0){
                        Sesion = insertarDatosSolicitud(Sesion);
                    }else if(Sesion.getMantenimiento().getCodigoFalla()==-2 && !Sesion.getMantenimiento().getMensaje().equals("")){
                        Sesion = insertarDatosSolicitud(Sesion);
                    }else{
                        Sesion.setTip("Si no especifica una falla, debera colocar una observación", 3);
                    }
                }else{
                    Sesion.setTip("Debe ingresar todos los datos antes de insertar", 3);
                }
            }
            
            if(request.getParameter("ordenDesc")!=null){
                Sesion.getMantenimiento().setOrdenAscNew(false);
            }
            
            if(request.getParameter("ordenAsc")!=null){
                Sesion.getMantenimiento().setOrdenAscNew(true);
            }
            if(request.getParameter("ordenDesc2")!=null){
                Sesion.getMantenimiento().setOrdenAscNew(false);
                est = 2;
            }
            
            if(request.getParameter("ordenAsc2")!=null){
                Sesion.getMantenimiento().setOrdenAscNew(true);
                est = 2;
            }
            if(request.getParameter("ordenDesc1")!=null){
                Sesion.getMantenimiento().setOrdenAscPreventivo(false);
                est = 0;
            }
            
            if(request.getParameter("ordenAsc1")!=null){
                Sesion.getMantenimiento().setOrdenAscPreventivo(true);
                est = 0;
            }
            
            if(request.getParameter("enviarFecha") != null){
                if(!request.getParameter("dia2").trim().equals("")){
                    String fecha = request.getParameter("dia2").substring(0,4)+request.getParameter("dia2").substring(5,7)+request.getParameter("dia2").substring(8,10);
                    Sesion.getMantenimiento().setListadoPedidosMantenPreventivos(Sesion.getBeanMan().listarPedidosMantePreventivos(Sesion.getIdUsuario(),fecha));
                    Sesion.getPset().indiceMant1(Sesion.getMantenimiento().getListadoPedidosMantenPreventivos());
                }else{
                    Sesion.setTip("Debe ingresar una fecha de busqueda", 3);
                }
            }
            
        }catch(Exception ex){
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }
        request.getSession().setAttribute("Sesion", Sesion);
        if(est == 0){
            response.sendRedirect("jsp/mantenimiento/solicitudMantenimiento.jsp");
        }else if(est == 4){
            response.sendRedirect("jsp/mantenimiento/ingresarNuevoStatus.jsp");
        }else{
            response.sendRedirect("jsp/mantenimiento/mantenimientoPreventivo.jsp");
        }
        
    }
    
    private ModSesion insertarDatosSolicitud(ModSesion Sesion){
        try {
            if(Sesion.getBeanMan().insertarMantenimiento(
                    Sesion.getMantenimiento().getCodigoEspecialidad(),
                    Sesion.getIdUsuario(),
                    String.valueOf(Sesion.getMantenimiento().getMaquina().getIdMaquina()),
                    Sesion.getMantenimiento().getCodigoFalla(),
                    Sesion.getMantenimiento().getMensaje(),
                    Sesion.getMantenimiento().getFechaDetencionEquipo(),
                    PrmApp.getFechaActual().concat(" ").concat(PrmApp.getHoraActual()),
                    Sesion.getMantenimiento().getTipoFalla().getInicial())) {
                Sesion.setMantenimiento(new ModMantenimiento());
                Sesion.getMantenimiento().setListadoPedidosMantenimientos(Sesion.getBeanMan().listarPedidosMantenimiento(Sesion.getIdUsuario()));
                Sesion.getMantenimiento().setListadoPedidosMantenPreventivos(Sesion.getBeanMan().listarPedidosMantePreventivos(Sesion.getIdUsuario(),false));
                Sesion.getPset().indiceMant(Sesion.getMantenimiento().getListadoPedidosMantenimientos());
                Sesion.getPset().indiceMant1(Sesion.getMantenimiento().getListadoPedidosMantenPreventivos());
                Sesion.setTip("Datos insertados satisfactoriamente", 0);
            }else{
                Sesion.setTip("Error al intentar insertar", 1);
            }
        } catch (Exception ex) {
            EscribeFichero.addError(ex, true);
        }
        return Sesion;
    }
    
    public String getServletInfo() {
        return "Servlet Mantenimiento";
    }
    
}
