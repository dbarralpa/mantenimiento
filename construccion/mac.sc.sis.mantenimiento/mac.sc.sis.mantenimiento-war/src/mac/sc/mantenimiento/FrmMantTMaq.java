package mac.sc.mantenimiento;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModHorario;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.models.ModTurnoMaq;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantTMaq extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        int redirect = 0; 
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
  
            if(request.getParameter("ingresarNewTMaq")!=null){ 
                Sesion.getMantenimiento().setTurnoMaquina(new ModTurnoMaq());                
                Sesion.getMantenimiento().getTurnoMaquina().setHorarios(getHorarioSemanalDefault());
                Sesion.getMantenimiento().setEtapaTMaq(1);            
            }
            
            if(request.getParameter("volverMant_0.x")!=null){
                if(Sesion.getVolver()==0){                    
                    redirect = 0;
                }else{                    
                    redirect = 1;
                }
            }
            
            if(request.getParameter("volverMant_1.x")!=null){                
                Sesion.getMantenimiento().setEtapaTMaq(0);
            }
            
            if(request.getParameter("grabarTMaq")!=null){                 
                if(request.getParameter("nombreTMaq")!=null){
                    Sesion.getMantenimiento().getTurnoMaquina().setNombreTurnoMaq(request.getParameter("nombreTMaq").trim());                
                }  
                if(request.getParameter("descTMaq")!=null){
                    Sesion.getMantenimiento().getTurnoMaquina().setDescTurnoMaq(request.getParameter("descTMaq").trim());                         
                }
                
                boolean valido = false;
                int mensaje = 0;
                for(int i=0; i<Sesion.getMantenimiento().getTurnoMaquina().getHorarios().size(); i++){
                    ModHorario horario = (ModHorario)Sesion.getMantenimiento().getTurnoMaquina().getHorarios().get(i);
                    if(request.getParameter("dia" + horario.getDiaHorario())!=null && 
                      (request.getParameter("inicio" + horario.getDiaHorario())!=null && !request.getParameter("inicio" + horario.getDiaHorario()).trim().equals("")) && 
                      (request.getParameter("termino" + horario.getDiaHorario())!=null && !request.getParameter("termino" + horario.getDiaHorario()).trim().equals(""))){
                        valido = true;
                        horario.setEstado(true);                                                
                        horario.setHoraInicio(request.getParameter("inicio" + horario.getDiaHorario()).trim());
                        horario.setHoraTermino(request.getParameter("termino" + horario.getDiaHorario()).trim());
                        if(!PrmApp.vFormatoHora(horario.getHoraInicio(), PrmApp.FORMATO_HM) || !PrmApp.vFormatoHora(horario.getHoraTermino(), PrmApp.FORMATO_HM)){
                            mensaje = 1;
                        }else{
                            horario.setHoraInicio(PrmApp.verHora(horario.getHoraInicio(), PrmApp.FORMATO_HM));
                            horario.setHoraTermino(PrmApp.verHora(horario.getHoraTermino(), PrmApp.FORMATO_HM));
                        }
                    }else{
                        horario.setEstado(false);
                    }
                    Sesion.getMantenimiento().getTurnoMaquina().getHorarios().set(i, horario);
                    horario = null;
                }
                
                if(!Sesion.getMantenimiento().getTurnoMaquina().getNombreTurnoMaq().equals("") && 
                   !Sesion.getMantenimiento().getTurnoMaquina().getDescTurnoMaq().equals("") && valido){                                   
                }else{
                    mensaje = 2;
                }
                
                if(mensaje==0){                
                    Sesion.getMantenimiento().getTurnoMaquina().setId(Sesion.getMantenimiento().getListadoTurnosMaquina().size());
                    if(Sesion.getBeanMan().insertarNuevoTurnoMaquina(Sesion.getMantenimiento().getTurnoMaquina())){                        
                        Sesion.getMantenimiento().setListadoTurnosMaquina(Sesion.getBeanMan().listarTurnosMaquinas());                            
                        Sesion.getMantenimiento().setTurnoMaquina(new ModTurnoMaq());
                        Sesion.getMantenimiento().setEtapaTMaq(0);                        
                        Sesion.setTip("Datos insertados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar insertar", 1);
                    }  
                }else if(mensaje==1){
                    Sesion.setTip("Formato hora inv�lido", 1);
                }else{
                    Sesion.setTip("Debe rellenar los campos Nombre y Descripci�n y al menos asignar un d�a", 3);
                }
            }
            
            if(request.getParameter("editTMaq")!=null){ 
                int indice = Integer.parseInt(request.getParameter("editTMaq"));
                Sesion.getMantenimiento().setTurnoMaquina((ModTurnoMaq)Sesion.getMantenimiento().getListadoTurnosMaquina().get(indice));                
                Sesion.getMantenimiento().getTurnoMaquina().setId(indice);
                Sesion.getMantenimiento().setEtapaTMaq(2);
            }
            
            if(request.getParameter("deleteTMaq")!=null){      
                int indice = Integer.parseInt(request.getParameter("deleteTMaq"));
                Sesion.getMantenimiento().setTurnoMaquina((ModTurnoMaq)Sesion.getMantenimiento().getListadoTurnosMaquina().get(indice));  
                Sesion.getMantenimiento().getTurnoMaquina().setId(indice);
//                if(!Sesion.getBeanMan().tipoDeFallaAsociado(Sesion.getMantenimiento().getTipoFalla().getIdTipoFalla())){
//                    Sesion.getMantenimiento().setEtapaTipoFalla(3);                                    
//                }else{
//                    Sesion.setTip("Tipo Falla imposible de eliminar, esta asociada a una Falla", 3);
//                }                
                Sesion.getMantenimiento().setEtapaTMaq(3);
            }
            
            if(request.getParameter("actualizarTMaq")!=null){  
                if(request.getParameter("nombreTMaq")!=null){
                    Sesion.getMantenimiento().getTurnoMaquina().setNombreTurnoMaq(request.getParameter("nombreTMaq").trim());                
                }  
                if(request.getParameter("descTMaq")!=null){
                    Sesion.getMantenimiento().getTurnoMaquina().setDescTurnoMaq(request.getParameter("descTMaq").trim());                         
                }
                
                boolean valido = false;
                int mensaje = 0;
                for(int i=0; i<Sesion.getMantenimiento().getTurnoMaquina().getHorarios().size(); i++){
                    ModHorario horario = (ModHorario)Sesion.getMantenimiento().getTurnoMaquina().getHorarios().get(i);
                    if(request.getParameter("dia" + horario.getDiaHorario())!=null && 
                      (request.getParameter("inicio" + horario.getDiaHorario())!=null && !request.getParameter("inicio" + horario.getDiaHorario()).trim().equals("")) && 
                      (request.getParameter("termino" + horario.getDiaHorario())!=null && !request.getParameter("termino" + horario.getDiaHorario()).trim().equals(""))){
                        valido = true;
                        horario.setEstado(true);                                                
                        horario.setHoraInicio(request.getParameter("inicio" + horario.getDiaHorario()).trim());
                        horario.setHoraTermino(request.getParameter("termino" + horario.getDiaHorario()).trim());
                        if(!PrmApp.vFormatoHora(horario.getHoraInicio(), PrmApp.FORMATO_HM) || !PrmApp.vFormatoHora(horario.getHoraTermino(), PrmApp.FORMATO_HM)){
                            mensaje = 1;
                        }else{
                            horario.setHoraInicio(PrmApp.verHora(horario.getHoraInicio(), PrmApp.FORMATO_HM));
                            horario.setHoraTermino(PrmApp.verHora(horario.getHoraTermino(), PrmApp.FORMATO_HM));
                        }
                    }else{
                        horario.setEstado(false);
                    }
                    Sesion.getMantenimiento().getTurnoMaquina().getHorarios().set(i, horario);
                    horario = null;
                }
                
                if(!Sesion.getMantenimiento().getTurnoMaquina().getNombreTurnoMaq().equals("") && 
                   !Sesion.getMantenimiento().getTurnoMaquina().getDescTurnoMaq().equals("") && valido){                                   
                }else{
                    mensaje = 2;
                }
                
                if(mensaje==0){                                    
                    if(Sesion.getBeanMan().actualizarTurnoMaquina(Sesion.getMantenimiento().getTurnoMaquina())){   
                        Sesion.getMantenimiento().getTurnoMaquina().setHorarios(Sesion.getBeanMan().listarHorariosXTurno(Sesion.getMantenimiento().getTurnoMaquina().getIdTurnoMaq()));
                        Sesion.getMantenimiento().getListadoTurnosMaquina().set(Sesion.getMantenimiento().getTurnoMaquina().getId(), Sesion.getMantenimiento().getTurnoMaquina());
                        Sesion.getMantenimiento().setEtapaTMaq(0);                        
                        Sesion.setTip("Datos actualizados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar actualizar", 1);
                    }  
                }else if(mensaje==1){
                    Sesion.setTip("Formato hora inv�lido", 1);
                }else{
                    Sesion.setTip("Debe rellenar los campos Nombre y Descripci�n y al menos asignar un d�a", 3);
                }
            }
            
            if(request.getParameter("eliminarTMaq")!=null){                  
                if(Sesion.getBeanMan().eliminarTurnoMaq(Sesion.getMantenimiento().getTurnoMaquina().getIdTurnoMaq())){
                    Sesion.getMantenimiento().getListadoTurnosMaquina().remove(Sesion.getMantenimiento().getTurnoMaquina().getId());
                    Sesion.getMantenimiento().setTurnoMaquina(new ModTurnoMaq());
                    Sesion.getMantenimiento().setEtapaTMaq(0);
                    Sesion.setTip("Registro eliminado satisfactoriamente", 0);
                }else{
                    Sesion.setTip("Error al intentar eliminar", 1);
                }  
            }
            
        }catch(Exception ex){   
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }        
        if(redirect==0){
            response.sendRedirect("jsp/mantenimiento/mantTMaq.jsp");
        }else if(redirect==1){
            response.sendRedirect("jsp/mantenimiento/mantMain.jsp");
            Sesion.setVolver(0);
        }else{
            response.sendRedirect("#");
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }
    
    private ArrayList getHorarioSemanalDefault(){
        ArrayList horarios = new ArrayList();
        ModHorario horario = null;
        
        horario = new ModHorario();
        horario.setDiaHorario("Lunes");
        horario.setHoraInicio("07:15");
        horario.setHoraTermino("16:45");
        horario.setEstado(true);
        horarios.add(horario);
        
        horario = new ModHorario();
        horario.setDiaHorario("Martes");
        horario.setHoraInicio("07:15");
        horario.setHoraTermino("16:45");
        horario.setEstado(true);
        horarios.add(horario);
        
        horario = new ModHorario();
        horario.setDiaHorario("Miercoles");
        horario.setHoraInicio("07:15");
        horario.setHoraTermino("16:45");
        horario.setEstado(true);
        horarios.add(horario);
        
        horario = new ModHorario();
        horario.setDiaHorario("Jueves");
        horario.setHoraInicio("07:15");
        horario.setHoraTermino("16:45");
        horario.setEstado(true);
        horarios.add(horario);
        
        horario = new ModHorario();
        horario.setDiaHorario("Viernes");
        horario.setHoraInicio("07:15");
        horario.setHoraTermino("16:45");
        horario.setEstado(true);
        horarios.add(horario);
        
        horario = new ModHorario();
        horario.setDiaHorario("Sabado");
        horario.setHoraInicio("08:15");
        horario.setHoraTermino("14:45");
        horario.setEstado(true);
        horarios.add(horario);
        
        horario = new ModHorario();
        horario.setDiaHorario("Domingo");
        horario.setHoraInicio("00:00");
        horario.setHoraTermino("00:00");
        horario.setEstado(false);
        horarios.add(horario); 
        
        horario = null;
        return horarios;
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Servlet Mantenedor Turnos Maquinas";
    }
    
}
