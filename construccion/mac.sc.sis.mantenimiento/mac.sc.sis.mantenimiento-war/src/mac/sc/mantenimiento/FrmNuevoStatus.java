package mac.sc.mantenimiento;

import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.beans.Trabajo;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModAuxiliar;
import mac.sc.sis.mantenimiento.models.ModEmpExt;
import mac.sc.sis.mantenimiento.models.ModEmplExt;
import mac.sc.sis.mantenimiento.models.ModHorario;
import mac.sc.sis.mantenimiento.models.ModMantenimiento;
import mac.sc.sis.mantenimiento.models.ModPerMant;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

/**
 *
 * @author ngonzalez
 * @version
 */
public class FrmNuevoStatus extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        byte est = 0;
        try {            
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");

            if (request.getParameter("calcularHDM") != null) {
                if (Sesion.getMantenimiento().getDetalle().getTurnoMaq().getHorarios().size() > 0) {
                    PrmApp.calcularHorasMinutosDetencion(Sesion.getMantenimiento().getDetalle());
                } else {
                    Sesion.setTip("C�lculo imposible, deben existir las fechas de Detenci�n y Entrega previamente", 3);
                }
            }

            if (request.getParameter("listarXarea") != null) {
                Sesion.getMantenimiento().setCodigoEspecialidad(Integer.parseInt(request.getParameter("area")));
                Sesion.getMantenimiento().setTipo("POR AREA");
                Sesion.getMantenimiento().setTipoAnterior("POR AREA");
                Sesion.getMantenimiento().setListadoPricipal(Sesion.getBeanMan().solicitudesPorArea(Sesion.getMantenimiento().getCodigoEspecialidad()));
                Sesion.getMantenimiento().setListadoTMP(new ArrayList());
                Sesion.getMantenimiento().setIdUsuarioArea(-1);
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
                PrmApp.setEstadosListado(Sesion.getMantenimiento());
            }

            if (request.getParameter("listarXusuario") != null) {
                Sesion.getMantenimiento().setIdUsuarioArea(Integer.parseInt(request.getParameter("usuario")));
                Sesion.getMantenimiento().setTipo("POR USUARIO");
                Sesion.getMantenimiento().setTipoAnterior("POR USUARIO");
                Sesion.getMantenimiento().setListadoPricipal(Sesion.getBeanMan().solicitudesPorUsuario(Sesion.getMantenimiento().getIdUsuarioArea()));
                Sesion.getMantenimiento().setListadoTMP(new ArrayList());
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
                PrmApp.setEstadosListado(Sesion.getMantenimiento());
            }
            
            if (request.getParameter("listarXtipo") != null) {
                Sesion.getMantenimiento().setListadoMaquinas(Sesion.getBeanMan().listarMaquinasPreventivas());
                Sesion.getMantenimiento().setTipo("POR TIPO");
                Sesion.getMantenimiento().setTipoAnterior("POR TIPO");
                Sesion.getMantenimiento().setListadoPricipal(Sesion.getBeanMan().listarPedidosMantePreventivos(Sesion.getIdUsuario(), true));
                Sesion.getMantenimiento().setListadoTMP(new ArrayList());
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getPset().indiceMant1(Sesion.getMantenimiento().getListadoPricipal());
                PrmApp.setEstadosListado(Sesion.getMantenimiento());
                est = 1;
            }

            if (request.getParameter("listarTodos") != null) {
                Sesion.getMantenimiento().setTipo("TODOS");
                Sesion.getMantenimiento().setTipoAnterior("TODOS");
                Sesion.getMantenimiento().setListadoPricipal(Sesion.getBeanMan().listarTodosLosPedidosDeMantenimiento());
                Sesion.getMantenimiento().setListadoTMP(new ArrayList());
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getMantenimiento().setIdUsuarioArea(-1);
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
                PrmApp.setEstadosListado(Sesion.getMantenimiento());
            }

            if (request.getParameter("listarNone") != null) {
                Sesion.getMantenimiento().setTipo("NO REVISADAS");
                Sesion.getMantenimiento().setListadoPricipal(setListadoPrincipal(Sesion.getMantenimiento(), Sesion.getMantenimiento().getENONE()));
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
            }

            if (request.getParameter("listarDiag") != null) {
                Sesion.getMantenimiento().setTipo("EN DIAGNOSTICO");
                Sesion.getMantenimiento().setListadoPricipal(setListadoPrincipal(Sesion.getMantenimiento(), Sesion.getMantenimiento().getEDIAG()));
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
            }

            if (request.getParameter("listarAdq") != null) {
                Sesion.getMantenimiento().setTipo("EN ADQUISICI�N");
                Sesion.getMantenimiento().setListadoPricipal(setListadoPrincipal(Sesion.getMantenimiento(), Sesion.getMantenimiento().getEADQN()));
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
            }

            if (request.getParameter("listarServExt") != null) {
                Sesion.getMantenimiento().setTipo("EN SERVICIO EXTERNO");
                Sesion.getMantenimiento().setListadoPricipal(setListadoPrincipal(Sesion.getMantenimiento(), Sesion.getMantenimiento().getESERE()));
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
            }

            if (request.getParameter("listarServInt") != null) {
                Sesion.getMantenimiento().setTipo("EN SERVICIO INTERNO");
                Sesion.getMantenimiento().setListadoPricipal(setListadoPrincipal(Sesion.getMantenimiento(), Sesion.getMantenimiento().getESERI()));
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
            }

            if (request.getParameter("listarVistoBueno") != null) {
                Sesion.getMantenimiento().setTipo("CON VISTO BUENO");
                Sesion.getMantenimiento().setListadoPricipal(setListadoPrincipal(Sesion.getMantenimiento(), Sesion.getMantenimiento().getEVISB()));
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
            }

            if (request.getParameter("listarNoConformidad") != null) {
                Sesion.getMantenimiento().setTipo("CON NO CONFORMIDAD");
                Sesion.getMantenimiento().setListadoPricipal(setListadoPrincipal(Sesion.getMantenimiento(), Sesion.getMantenimiento().getENOCO()));
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
            }

            if (request.getParameter("listarAll") != null) {
                if (Sesion.getMantenimiento().getListadoTMP().size() > 0) {
                    Sesion.getMantenimiento().setTipo(Sesion.getMantenimiento().getTipoAnterior());
                    Sesion.getMantenimiento().setListadoPricipal(Sesion.getMantenimiento().getListadoTMP());
                    Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
                }
            }

            if (request.getParameter("pagSOLI") != null) {
                Sesion.getPset().getDetalle("SOLI").pag = Integer.parseInt(request.getParameter("pagSOLI"));
            }

            if (request.getParameter("detGrilla") != null) {
                Sesion.getMantenimiento().setIdDetalle(Integer.parseInt(request.getParameter("detGrilla")));
                Sesion.getMantenimiento().setDetalle((Mantenimiento) Sesion.getMantenimiento().getListadoPricipal().get(Sesion.getMantenimiento().getIdDetalle()));
                if ((Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo() != null && !Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo().trim().equals(""))
                        && (Sesion.getMantenimiento().getDetalle().getFechaEntregaEquipo() != null && !Sesion.getMantenimiento().getDetalle().getFechaEntregaEquipo().trim().equals(""))) {
                    Sesion.getMantenimiento().getDetalle().getTurnoMaq().setHorarios(Sesion.getBeanMan().listarHorariosXTurno(Sesion.getMantenimiento().getDetalle().getTurnoMaq().getIdTurnoMaq()));
                    PrmApp.calcularHorasMinutosDetencion(Sesion.getMantenimiento().getDetalle());
                }
                Sesion.getMantenimiento().setEtapa(1);
            }

            if (request.getParameter("volverMant_0.x") != null) {
                Sesion.getMantenimiento().setEtapa(0);
            }

            if (request.getParameter("ordenDesc") != null) {
                Sesion.getMantenimiento().setOrdenAsc(false);
            }

            if (request.getParameter("ordenAsc") != null) {
                Sesion.getMantenimiento().setOrdenAsc(true);
            }

            if (request.getParameter("changeState") != null) {
                int idEstado = Integer.parseInt(request.getParameter("changeState"));
                for (int i = 0; i < Sesion.getMantenimiento().getListaEstados().size(); i++) {
                    Trabajo obj = (Trabajo) Sesion.getMantenimiento().getListaEstados().get(i);
                    if (obj.getIdEstado() == idEstado) {
                        if (obj.getIdEstado() == 1) {
                            if (Sesion.getMantenimiento().getListadoPerMant().isEmpty()) {
                                Sesion.getMantenimiento().setListadoPerMant(Sesion.getBeanMan().listarPerMant());
                            }
                            Sesion.getMantenimiento().setPerMant(new ModPerMant());
                            Sesion.getMantenimiento().getPerMant().setId(-1);
                        } else if (obj.getIdEstado() == 3) {
                            Sesion.getMantenimiento().setListadoEmpExt(Sesion.getBeanMan().listarEmpExt());
                            Sesion.getMantenimiento().setEmpExt(new ModEmpExt());
                            Sesion.getMantenimiento().getEmpExt().setId(-1);
                        }
                        Sesion.getMantenimiento().setTituloBitacora(obj.getNombreEstado());
                        break;
                    }
                    obj = null;
                }
                Sesion.getMantenimiento().getDetalle().setIdEstado(idEstado);
                Sesion.getMantenimiento().setEtapa(2);
            }

            if (request.getParameter("volverMant_1.x") != null) {
                Sesion.getMantenimiento().setEtapa(1);
            }

            if (request.getParameter("guardarEstado") != null) {
                String fecha;
                String observacion;
                String sap;
                String rut_encargado;
                String diaTermino = "";
                String horaTermino = "";
                String fechaTerminoEquipo = "";
                String rutEmpr = "";
                String rutEmpl = "";
                ModEmpExt empresa = null;
                ModEmplExt empleado = null;

                if (request.getParameter("dia") != null) {
                    fecha = request.getParameter("dia");
                } else {
                    fecha = "";
                }
                if (request.getParameter("txtObvservacion") != null) {
                    observacion = request.getParameter("txtObvservacion");
                } else {
                    observacion = "";
                }
                if (request.getParameter("txtSap") != null) {
                    sap = request.getParameter("txtSap");
                } else {
                    sap = "";
                }

                ModPerMant personal = new ModPerMant();
                if (request.getParameter("cmbPerMant") != null && !request.getParameter("cmbPerMant").equals("-1")) {
                    int indice = Integer.parseInt(request.getParameter("cmbPerMant"));
                    personal = (ModPerMant) Sesion.getMantenimiento().getListadoPerMant().get(indice);
                    rut_encargado = personal.getVc_rut();
                } else {
                    rut_encargado = Sesion.getMantenimiento().getDetalle().getPersonalMant().getVc_rut();
                }

                boolean validador = false;
                if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 1) {
                    if (request.getParameter("cmbPerMant").equals("-1")) {
                        validador = false;
                    } else {
                        validador = true;
                    }
                } else {
                    validador = true;
                }

                if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 3 || Sesion.getMantenimiento().getDetalle().getIdEstado() == 4) {
                    boolean dTermino = false;
                    if (request.getParameter("diaTermino") != null && !request.getParameter("diaTermino").trim().equals("")) {
                        diaTermino = request.getParameter("diaTermino").trim();
                        dTermino = true;
                    }
                    boolean hTermino = false;
                    if (request.getParameter("horaTermino") != null && !request.getParameter("horaTermino").trim().equals("")) {
                        horaTermino = request.getParameter("horaTermino").trim();
                        hTermino = true;
                    }
                    if (!dTermino || !hTermino) {
                        validador = false;
                    }
                }

                if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 3) {
                    int idEmpr = -1;
                    int idEmpl = -1;
                    if (request.getParameter("EMPR") != null && Integer.parseInt(request.getParameter("EMPR")) > 0) {
                        idEmpr = Integer.parseInt(request.getParameter("EMPR")) - 1;
                        empresa = (ModEmpExt) Sesion.getMantenimiento().getListadoEmpExt().get(idEmpr);
                        rutEmpr = empresa.getVc_rut();
                    }
                    if (request.getParameter("EMPL") != null && Integer.parseInt(request.getParameter("EMPL")) > 0) {
                        idEmpl = Integer.parseInt(request.getParameter("EMPL")) - 1;
                        empleado = (ModEmplExt) empresa.getEmpleados().get(idEmpl);
                        rutEmpl = empleado.getVc_rut();
                    }
                    if (idEmpr != -1 && idEmpl != -1) {
                    } else {
                        validador = false;
                    }
                }


                if (!fecha.equals("") && !observacion.equals("") && validador) {

                    boolean estadoUpdate;
                    if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 3) {
                        fechaTerminoEquipo = PrmApp.verFecha(diaTermino).concat(" ").concat(horaTermino).concat(":00");
                        estadoUpdate = Sesion.getBeanMan().updateSolicitud(Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento(), sap, rut_encargado, fechaTerminoEquipo, rutEmpr, rutEmpl);
                    } else if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 4) {
                        fechaTerminoEquipo = PrmApp.verFecha(diaTermino).concat(" ").concat(horaTermino).concat(":00");
                        estadoUpdate = Sesion.getBeanMan().updateSolicitud(Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento(), sap, rut_encargado, fechaTerminoEquipo);
                    } else {
                        estadoUpdate = Sesion.getBeanMan().updateSolicitud(Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento(), sap, rut_encargado);
                    }

                    if (estadoUpdate && Sesion.getBeanMan().insertarNuevoEstado(Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento(),
                            Sesion.getMantenimiento().getDetalle().getIdEstado(),
                            PrmApp.verFecha(fecha).concat(" 00:00:00"), observacion)) {
                        Mantenimiento obj = Sesion.getMantenimiento().getDetalle();
                        if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 1) {
                            obj.setFechaDiagnostico(fecha);
                            obj.setObservacionDiagnostico(observacion);
                            obj.setNumeroSap(sap);
                            obj.setPersonalMant(personal);
                            personal = null;
                        } else if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 2) {
                            obj.setFechaAdquisicion(fecha);
                            obj.setObservacionAdquisicion(observacion);
                        } else if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 3) {
                            obj.setFechaSevicioExterno(fecha);
                            obj.setObservacionSevicioExterno(observacion);
                            obj.setFechaEntregaEquipo(diaTermino.concat(" ").concat(horaTermino).concat(":00"));
                            obj.setEmpExt(empresa);
                            obj.setEmplEmt(empleado);
                            obj.getTurnoMaq().setHorarios(Sesion.getBeanMan().listarHorariosXTurno(obj.getTurnoMaq().getIdTurnoMaq()));
                            if(obj.getFechaDetencionEquipo()!=null && obj.getFechaEntregaEquipo()!=null){
                                PrmApp.calcularHorasMinutosDetencion(obj);
                            } 
                        } else if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 4) {
                            obj.setFechaMantenimientoCorrectivo(fecha);
                            obj.setObservacionMantenimientoCorrectivo(observacion);
                            obj.setFechaEntregaEquipo(diaTermino.concat(" ").concat(horaTermino).concat(":00"));
                            obj.getTurnoMaq().setHorarios(Sesion.getBeanMan().listarHorariosXTurno(obj.getTurnoMaq().getIdTurnoMaq()));
                            if(obj.getFechaDetencionEquipo()!=null && obj.getFechaEntregaEquipo()!=null){
                                PrmApp.calcularHorasMinutosDetencion(obj);
                            }                            
                        } else if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 5) {
                            obj.setFechaVistoBueno(fecha);
                            obj.setObservacionVistoBueno(observacion);
                        } else if (Sesion.getMantenimiento().getDetalle().getIdEstado() == 6) {
                            obj.setFechaNoConformidad(fecha);
                            obj.setObservacionNoConformidad(observacion);
                        }

                        Sesion.getMantenimiento().setDetalle(obj);
                        Sesion.getMantenimiento().setTipo("USUARIO");
                        Sesion.getMantenimiento().setListadoPricipal(Sesion.getBeanMan().solicitudesPorUsuario(Sesion.getIdUsuario()));
                        Sesion.getMantenimiento().setListadoTMP(new ArrayList());
                        Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
                        PrmApp.setEstadosListado(Sesion.getMantenimiento());
                        obj = null;

                        Sesion.getMantenimiento().setEtapa(1);
                        Sesion.setTip("Datos insertados satisfactoriamente", 0);
                    } else {
                        Sesion.setTip("Error al intentar insertar", 1);
                    }
                } else {
                    Sesion.setTip("Debe rellenar todos los campos", 3);
                }
            }

        } catch (Exception ex) {
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }
        request.getSession().setAttribute("Sesion", Sesion);
        if(est == 1){
        response.sendRedirect("jsp/mantenimiento/mantenimientoPreventivo.jsp");    
        }else{
        response.sendRedirect("jsp/mantenimiento/ingresarNuevoStatus.jsp");        
        }
        
    }

    private ArrayList setListadoPrincipal(ModMantenimiento mantenimiento, ArrayList subListado) {
        if (mantenimiento.getListadoTMP().isEmpty()) {
            mantenimiento.setListadoTMP(mantenimiento.getListadoPricipal());
        }

        mantenimiento.setListadoPricipal(new ArrayList());
        for (int i = 0; i < subListado.size(); i++) {
            ModAuxiliar obj = (ModAuxiliar) subListado.get(i);
            mantenimiento.getListadoPricipal().add(mantenimiento.getListadoTMP().get(obj.getId()));
            obj = null;
        }
        return mantenimiento.getListadoPricipal();
    }   

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet Nuevo Status";
    }
}
