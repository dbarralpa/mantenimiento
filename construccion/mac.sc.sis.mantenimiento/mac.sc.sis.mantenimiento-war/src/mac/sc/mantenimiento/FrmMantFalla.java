package mac.sc.mantenimiento;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.models.ModFalla;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantFalla extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        int redirect = 0; 
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
            
            if(request.getParameter("newFalla")!=null){   
                Sesion.getMantenimiento().setFalla(new ModFalla());  
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getMantenimiento().getMaquina().setIdMaquina(-1);                
                Sesion.getMantenimiento().setMensaje("");                                
                Sesion.getMantenimiento().setListadoFallas(new ArrayList());
                Sesion.getMantenimiento().setListadoMaquinas(new ArrayList());
            }
            
            if(request.getParameter("addFail")!=null){                           
                Sesion.getMantenimiento().setCodigoEspecialidad(Sesion.getMantenimiento().getDetalle().getCodigoEspecialidad());
                Sesion.getMantenimiento().getMaquina().setIdMaquina(Sesion.getMantenimiento().getDetalle().getCodigoMaquina());
                Sesion.getMantenimiento().setListadoFallas(Sesion.getBeanMan().listarFallas(Sesion.getMantenimiento().getMaquina().getIdMaquina()));
                Sesion.setVolver(2);
            }
            
            if(request.getParameter("area")!=null){
                Sesion.getMantenimiento().setCodigoEspecialidad(Integer.parseInt(request.getParameter("area")));
                Sesion.getMantenimiento().getMaquina().setIdMaquina(-1);                
            }
            
            if(request.getParameter("maquina")!=null){
                Sesion.getMantenimiento().getMaquina().setIdMaquina(Integer.parseInt(request.getParameter("maquina"))); 
                if(Sesion.getMantenimiento().getMaquina().getIdMaquina()>0){
                    Sesion.getMantenimiento().setListadoFallas(Sesion.getBeanMan().listarFallas(Sesion.getMantenimiento().getMaquina().getIdMaquina()));
                }
            }
            
            if(request.getParameter("tipoFalla")!=null){                                              
                Sesion.getMantenimiento().getFalla().getTipoFalla().setIdTipoFalla(Integer.parseInt(request.getParameter("tipoFalla")));             
            }
            
            if(request.getParameter("ingresarNewFail")!=null){
                if((Sesion.getMantenimiento().getCodigoEspecialidad()==-1) || (Sesion.getMantenimiento().getMaquina().getIdMaquina()==-1)){
                    Sesion.setTip("Debe seleccionar un �rea y m�quina antes", 3);
                }else{                                   
                    Sesion.getMantenimiento().setFalla(new ModFalla());
                    Sesion.getMantenimiento().setEtapaFalla(1);
                }                
            }
            
            if(request.getParameter("volverMant_0.x")!=null){
                Sesion.getMantenimiento().setFalla(new ModFalla());  
                Sesion.getMantenimiento().setMensaje("");                
                Sesion.getMantenimiento().setListadoFallas(new ArrayList());
                Sesion.getMantenimiento().setListadoMaquinas(new ArrayList());
                if(Sesion.getVolver()==0){                    
                    redirect = 1;
                }else if(Sesion.getVolver()==1){                    
                    redirect = 2;
                }else if(Sesion.getVolver()==2){                    
                    redirect = 3;
                }else{
                    redirect = 0;
                } 
            }
            
            if(request.getParameter("volverMant_1.x")!=null){                
                Sesion.getMantenimiento().setEtapaFalla(0);
            }
            
            if(request.getParameter("grabarFalla")!=null){                
                if(request.getParameter("nombreFalla")!=null){
                    Sesion.getMantenimiento().getFalla().setNombre(request.getParameter("nombreFalla").trim());                
                }  
                if(request.getParameter("descFalla")!=null){
                    Sesion.getMantenimiento().getFalla().setDescripcion(request.getParameter("descFalla").trim());                
                }                                
                
                if(!Sesion.getMantenimiento().getFalla().getNombre().equals("") && !Sesion.getMantenimiento().getFalla().getDescripcion().equals("") 
                    && Sesion.getMantenimiento().getFalla().getTipoFalla().getIdTipoFalla()>0)
                {            
                    Sesion.getMantenimiento().getFalla().setIdMaquina(Sesion.getMantenimiento().getMaquina().getIdMaquina());
                    if(Sesion.getBeanMan().insertarNuevoFallo(Sesion.getMantenimiento().getFalla())){
                        Sesion.getMantenimiento().setListadoFallas(Sesion.getBeanMan().listarFallas(Sesion.getMantenimiento().getFalla().getIdMaquina()));   
                        Sesion.getMantenimiento().setFalla(new ModFalla());
                        Sesion.getMantenimiento().setEtapaFalla(0);                        
                        Sesion.setTip("Datos insertados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar insertar", 1);
                    }                 
                }else{
                    Sesion.setTip("Debe digitar una nueva falla y una breve descripci�n", 3);
                }
            }
            
            if(request.getParameter("editFalla")!=null){                                  
                Sesion.getMantenimiento().setFalla((ModFalla)Sesion.getMantenimiento().getListadoFallas().get(Integer.parseInt(request.getParameter("editFalla"))));
                Sesion.getMantenimiento().setEtapaFalla(2);
            }
            
            if(request.getParameter("deleteFalla")!=null){                                  
                Sesion.getMantenimiento().setFalla((ModFalla)Sesion.getMantenimiento().getListadoFallas().get(Integer.parseInt(request.getParameter("deleteFalla"))));
                
                boolean existe = false;
                for(int i=0; i<Sesion.getMantenimiento().getListadoPedidosMantenimientos().size(); i++){
                    Mantenimiento mantenimiento = (Mantenimiento)Sesion.getMantenimiento().getListadoPedidosMantenimientos().get(i);
                    if(mantenimiento.getCodigoFalla()==Sesion.getMantenimiento().getFalla().getIdFalla()){
                        existe = true;
                        break;
                    }
                }
                if(!existe){                    
                    Sesion.getMantenimiento().setEtapaFalla(3);
                }else{
                    Sesion.setTip("Falla imposible de eliminar, esta asociada a una solicitud", 3);
                }                
            }
            
            if(request.getParameter("actualizarFalla")!=null){                  
                if(request.getParameter("nombreFalla")!=null){
                    Sesion.getMantenimiento().getFalla().setNombre(request.getParameter("nombreFalla").trim());                
                }
                if(request.getParameter("descFalla")!=null){
                    Sesion.getMantenimiento().getFalla().setDescripcion(request.getParameter("descFalla").trim());                
                }
                if(!Sesion.getMantenimiento().getFalla().getNombre().equals("") && !Sesion.getMantenimiento().getFalla().getDescripcion().equals("") && Sesion.getMantenimiento().getFalla().getTipoFalla().getIdTipoFalla()>0){
                    if(Sesion.getBeanMan().actualizarFalla(Sesion.getMantenimiento().getFalla())){
                        Sesion.getMantenimiento().setListadoFallas(Sesion.getBeanMan().listarFallas(Sesion.getMantenimiento().getFalla().getIdMaquina()));
                        Sesion.getMantenimiento().setFalla(new ModFalla());                        
                        Sesion.getMantenimiento().setEtapaFalla(0);
                        Sesion.setTip("Datos actualizados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar actualizar", 1);
                    }                 
                }else{
                    Sesion.setTip("Debe digitar una falla y descripci�n antes", 3);
                }
            }
            
            if(request.getParameter("eliminarFalla")!=null){                  
                if(Sesion.getBeanMan().eliminarFalla(Sesion.getMantenimiento().getFalla().getIdFalla())){
                    Sesion.getMantenimiento().setListadoFallas(Sesion.getBeanMan().listarFallas(Sesion.getMantenimiento().getFalla().getIdMaquina()));
                    Sesion.getMantenimiento().setFalla(new ModFalla());                      
                    Sesion.getMantenimiento().setEtapaFalla(0);
                    Sesion.setTip("Registro eliminado satisfactoriamente", 0);
                }else{
                    Sesion.setTip("Error al intentar eliminar", 1);
                }  
            }
            
        }catch(Exception ex){            
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }        
        if(redirect==0){
            response.sendRedirect("jsp/mantenimiento/mantFalla.jsp");
        }else if(redirect==1){
            response.sendRedirect("jsp/mantenimiento/solicitudMantenimiento.jsp");
        }else if(redirect==2){
            response.sendRedirect("jsp/mantenimiento/mantMain.jsp");
            Sesion.setVolver(0);
        }else if(redirect==3){
            response.sendRedirect("jsp/mantenimiento/ingresarNuevoStatus.jsp");
            Sesion.setVolver(0);
        }else{
            response.sendRedirect("#");
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Servlet Mantenedor Falla";
    }
    
}
