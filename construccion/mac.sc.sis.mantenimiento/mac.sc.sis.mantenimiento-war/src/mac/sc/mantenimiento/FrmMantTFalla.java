package mac.sc.mantenimiento;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.models.ModTipoFalla;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantTFalla extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        int redirect = 0; 
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
  
            if(request.getParameter("ingresarNewTFail")!=null){ 
                Sesion.getMantenimiento().setTipoFalla(new ModTipoFalla());
                Sesion.getMantenimiento().setEtapaTipoFalla(1);            
            }
            
            if(request.getParameter("volverMant_0.x")!=null){
                if(Sesion.getVolver()==0){                    
                    redirect = 0;
                }else{                    
                    redirect = 1;
                }
            }
            
            if(request.getParameter("volverMant_1.x")!=null){                
                Sesion.getMantenimiento().setEtapaTipoFalla(0);
            }
            
            if(request.getParameter("grabarTFalla")!=null){ 
                Sesion.getMantenimiento().setTipoFalla(new ModTipoFalla());
                if(request.getParameter("nombreTFalla")!=null){
                    Sesion.getMantenimiento().getTipoFalla().setNombre(request.getParameter("nombreTFalla").trim());                
                }  
                if(request.getParameter("descTFalla")!=null){
                    Sesion.getMantenimiento().getTipoFalla().setDescripcion(request.getParameter("descTFalla").trim());                
                }
                
                if(!Sesion.getMantenimiento().getTipoFalla().getNombre().equals("") && !Sesion.getMantenimiento().getTipoFalla().getDescripcion().equals("")){ 
                    Sesion.getMantenimiento().getTipoFalla().setId(Sesion.getMantenimiento().getListadoTipoFallas().size());
                    if(Sesion.getBeanMan().insertarNuevoTipoFallo(Sesion.getMantenimiento().getTipoFalla())){                        
                        Sesion.getMantenimiento().setListadoTipoFallas(Sesion.getBeanMan().listarTipoFallas());
                        Sesion.getMantenimiento().setTipoFalla(new ModTipoFalla());
                        Sesion.getMantenimiento().setEtapaTipoFalla(0);                        
                        Sesion.setTip("Datos insertados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar insertar", 1);
                    }                 
                }else{
                    Sesion.setTip("Debe digitar un nuevo Tipo de Falla y una breve descripción", 3);
                }
            }
            
            if(request.getParameter("editTFalla")!=null){                
                Sesion.getMantenimiento().setTipoFalla((ModTipoFalla)Sesion.getMantenimiento().getListadoTipoFallas().get(Integer.parseInt(request.getParameter("editTFalla"))));                
                Sesion.getMantenimiento().setEtapaTipoFalla(2);
            }
            
            if(request.getParameter("deleteTFalla")!=null){                  
                Sesion.getMantenimiento().setTipoFalla((ModTipoFalla)Sesion.getMantenimiento().getListadoTipoFallas().get(Integer.parseInt(request.getParameter("deleteTFalla"))));                                 
                if(!Sesion.getBeanMan().tipoDeFallaAsociado(Sesion.getMantenimiento().getTipoFalla().getIdTipoFalla())){
                    Sesion.getMantenimiento().setEtapaTipoFalla(3);                                    
                }else{
                    Sesion.setTip("Tipo Falla imposible de eliminar, esta asociada a una Falla", 3);
                }                
            }
            
            if(request.getParameter("actualizarTFalla")!=null){                  
                if(request.getParameter("nombreTFalla")!=null){
                    Sesion.getMantenimiento().getTipoFalla().setNombre(request.getParameter("nombreTFalla").trim());                
                }
                if(request.getParameter("descTFalla")!=null){
                    Sesion.getMantenimiento().getTipoFalla().setDescripcion(request.getParameter("descTFalla").trim());                
                }
                if(!Sesion.getMantenimiento().getTipoFalla().getNombre().equals("") && !Sesion.getMantenimiento().getTipoFalla().getDescripcion().equals("")){                    
                    if(Sesion.getBeanMan().actualizarTipoFalla(Sesion.getMantenimiento().getTipoFalla())){
                        Sesion.getMantenimiento().getListadoTipoFallas().set(Sesion.getMantenimiento().getTipoFalla().getId(), Sesion.getMantenimiento().getTipoFalla());
                        Sesion.getMantenimiento().setEtapaTipoFalla(0);
                        Sesion.setTip("Datos actualizados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar actualizar", 1);
                    }                 
                }else{
                    Sesion.setTip("Debe digitar un Tipo Falla y descripción antes", 3);
                }
            }
            
            if(request.getParameter("eliminarTFalla")!=null){                  
                if(Sesion.getBeanMan().eliminarTipoFalla(Sesion.getMantenimiento().getTipoFalla().getIdTipoFalla())){
                    Sesion.getMantenimiento().getListadoTipoFallas().remove(Sesion.getMantenimiento().getTipoFalla().getId());
                    Sesion.getMantenimiento().setTipoFalla(new ModTipoFalla());
                    Sesion.getMantenimiento().setEtapaTipoFalla(0);
                    Sesion.setTip("Registro eliminado satisfactoriamente", 0);
                }else{
                    Sesion.setTip("Error al intentar eliminar", 1);
                }  
            }
            
        }catch(Exception ex){  
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }        
        if(redirect==0){
            response.sendRedirect("jsp/mantenimiento/mantTFalla.jsp");
        }else if(redirect==1){
            response.sendRedirect("jsp/mantenimiento/mantMain.jsp");
            Sesion.setVolver(0);
        }else{
            response.sendRedirect("#");
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Servlet Mantenedor Tipo Falla";
    }
    
}
