package mac.sc.mantenimiento;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.models.ModEmpExt;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantEmpExt extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        int redirect = 0; 
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
  
            if(request.getParameter("ingresarNewEmpExt")!=null){ 
                Sesion.getMantenimiento().setEmpExt(new ModEmpExt());
                Sesion.getMantenimiento().setEtapaEmpExt(1);            
            }
            
            if(request.getParameter("volverMant_0.x")!=null){
                if(Sesion.getVolver()==0){                    
                    redirect = 0;
                }else{                    
                    redirect = 1;
                }
            }
            
            if(request.getParameter("volverMant_1.x")!=null){                
                Sesion.getMantenimiento().setEtapaEmpExt(0);
            }
            
            if(request.getParameter("grabarEmpExt")!=null){ 
                Sesion.getMantenimiento().setEmpExt(new ModEmpExt());
                if(request.getParameter("rutEmpExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().setVc_rut(request.getParameter("rutEmpExt").trim());                
                }  
                if(request.getParameter("nombreEmpExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().setVc_nombre(request.getParameter("nombreEmpExt").trim());                         
                }
                if(request.getParameter("direcEmpExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().setVc_direccion(request.getParameter("direcEmpExt").trim());                         
                }
                
                if(!Sesion.getMantenimiento().getEmpExt().getVc_rut().equals("") && 
                   !Sesion.getMantenimiento().getEmpExt().getVc_nombre().equals("") && 
                   !Sesion.getMantenimiento().getEmpExt().getVc_direccion().equals("")){
                        Sesion.getMantenimiento().getEmpExt().setId(Sesion.getMantenimiento().getListadoEmpExt().size());
                        if(Sesion.getBeanMan().insertarNuevoEmpExt(Sesion.getMantenimiento().getEmpExt())){                        
                            Sesion.getMantenimiento().setListadoEmpExt(Sesion.getBeanMan().listarEmpExt());                            
                            Sesion.getMantenimiento().setEmpExt(new ModEmpExt());
                            Sesion.getMantenimiento().setEtapaEmpExt(0);                        
                            Sesion.setTip("Datos insertados satisfactoriamente", 0);
                        }else{
                            Sesion.setTip("Error al intentar insertar", 1);
                        }                 
                }else{
                    Sesion.setTip("Debe rellenar todos los campos", 3);
                }
            }
            
            if(request.getParameter("editEmpExt")!=null){ 
                int indice = Integer.parseInt(request.getParameter("editEmpExt"));
                Sesion.getMantenimiento().setEmpExt((ModEmpExt)Sesion.getMantenimiento().getListadoEmpExt().get(indice));                
                Sesion.getMantenimiento().getEmpExt().setId(indice);
                Sesion.getMantenimiento().setEtapaEmpExt(2);
            }
            
            if(request.getParameter("deleteEmpExt")!=null){      
                int indice = Integer.parseInt(request.getParameter("deleteEmpExt"));
                Sesion.getMantenimiento().setEmpExt((ModEmpExt)Sesion.getMantenimiento().getListadoEmpExt().get(indice));  
                Sesion.getMantenimiento().getEmpExt().setId(indice);
//                if(!Sesion.getBeanMan().tipoDeFallaAsociado(Sesion.getMantenimiento().getTipoFalla().getIdTipoFalla())){
//                    Sesion.getMantenimiento().setEtapaTipoFalla(3);                                    
//                }else{
//                    Sesion.setTip("Tipo Falla imposible de eliminar, esta asociada a una Falla", 3);
//                }                
                Sesion.getMantenimiento().setEtapaEmpExt(3);
            }
            
            if(request.getParameter("actualizarEmpExt")!=null){  
                if(request.getParameter("nombreEmpExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().setVc_nombre(request.getParameter("nombreEmpExt").trim());                
                }
                if(request.getParameter("direcEmpExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().setVc_direccion(request.getParameter("direcEmpExt").trim());                
                }
                
                if(!Sesion.getMantenimiento().getEmpExt().getVc_nombre().equals("") &&
                   !Sesion.getMantenimiento().getEmpExt().getVc_direccion().equals("")){                    
                    if(Sesion.getBeanMan().actualizarEmpExt(Sesion.getMantenimiento().getEmpExt())){
                        Sesion.getMantenimiento().getListadoEmpExt().set(Sesion.getMantenimiento().getEmpExt().getId(), Sesion.getMantenimiento().getEmpExt());
                        Sesion.getMantenimiento().setEtapaEmpExt(0);
                        Sesion.setTip("Datos actualizados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar actualizar", 1);
                    }                 
                }else{
                    Sesion.setTip("Debe digitar un Nombre y Direcci�n antes", 3);
                }
            }
            
            if(request.getParameter("eliminarEmpExt")!=null){                  
                if(Sesion.getBeanMan().eliminarEmpExt(Sesion.getMantenimiento().getEmpExt().getVc_rut())){
                    Sesion.getMantenimiento().getListadoEmpExt().remove(Sesion.getMantenimiento().getEmpExt().getId());
                    Sesion.getMantenimiento().setEmpExt(new ModEmpExt());
                    Sesion.getMantenimiento().setEtapaEmpExt(0);
                    Sesion.setTip("Registro eliminado satisfactoriamente", 0);
                }else{
                    Sesion.setTip("Error al intentar eliminar", 1);
                }  
            }
            
        }catch(Exception ex){   
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }        
        if(redirect==0){
            response.sendRedirect("jsp/mantenimiento/mantEmpExt.jsp");
        }else if(redirect==1){
            response.sendRedirect("jsp/mantenimiento/mantMain.jsp");
            Sesion.setVolver(0);
        }else{
            response.sendRedirect("#");
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Servlet Mantenedor Empresa Externa";
    }
    
}
