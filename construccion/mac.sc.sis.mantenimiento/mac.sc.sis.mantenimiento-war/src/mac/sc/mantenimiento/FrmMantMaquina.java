package mac.sc.mantenimiento;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.models.ModMaquina;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.models.ModTurnoMaq;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantMaquina extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        int redirect = 0;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");

            if (request.getParameter("newMaquina") != null) {
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getMantenimiento().getMaquina().setIdMaquina(-1);
                Sesion.getMantenimiento().setCodigoFalla(-1);
                Sesion.getMantenimiento().setMensaje("");
                Sesion.getMantenimiento().setListadoFallas(new ArrayList());
                Sesion.getMantenimiento().setListadoMaquinas(new ArrayList());
                Sesion.getMantenimiento().setListadoTurnosMaquina(Sesion.getBeanMan().listarTurnosMaquinas());
            }

            if (request.getParameter("cambiarTurno") != null) {
                Sesion.getMantenimiento().setCodigoEspecialidad(Sesion.getMantenimiento().getDetalle().getCodigoEspecialidad());
                Sesion.getMantenimiento().setListadoMaquinas(Sesion.getBeanMan().listarMaquinas(Sesion.getMantenimiento().getCodigoEspecialidad()));
                Sesion.getMantenimiento().setListadoTurnosMaquina(Sesion.getBeanMan().listarTurnosMaquinas());

                ModMaquina maquina = new ModMaquina();
                for (int i = 0; i < Sesion.getMantenimiento().getListadoMaquinas().size(); i++) {
                    maquina = (ModMaquina) Sesion.getMantenimiento().getListadoMaquinas().get(i);
                    if (maquina.getIdMaquina() == Sesion.getMantenimiento().getDetalle().getCodigoMaquina()) {
                        break;
                    }
                    maquina = null;
                }
                Sesion.getMantenimiento().setMaquina(maquina);
                Sesion.getMantenimiento().setEtapaMaquina(2);
                Sesion.setVolver(2);
            }

            if (request.getParameter("area") != null) {
                Sesion.getMantenimiento().setCodigoEspecialidad(Integer.parseInt(request.getParameter("area")));
                Sesion.getMantenimiento().setListadoMaquinas(Sesion.getBeanMan().listarMaquinas(Sesion.getMantenimiento().getCodigoEspecialidad()));
            }

            if (request.getParameter("ingresarNewMaquina") != null) {
                if (Sesion.getMantenimiento().getCodigoEspecialidad() == -1) {
                    Sesion.setTip("Debe seleccionar un �rea antes", 3);
                } else {
                    Sesion.getMantenimiento().setEtapaMaquina(1);
                    Sesion.getMantenimiento().getMaquina().setUso("");
                    Sesion.getMantenimiento().getMaquina().setNombreCorto("");
                    Sesion.getMantenimiento().getMaquina().setTurnoMaq(new ModTurnoMaq());
                    Sesion.getMantenimiento().getMaquina().getTurnoMaq().setId(-1);
                    Sesion.getMantenimiento().setListadoTurnosMaquina(Sesion.getBeanMan().listarTurnosMaquinas());
                }
            }

            if (request.getParameter("volverMant_0.x") != null) {
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getMantenimiento().getMaquina().setIdMaquina(-1);
                Sesion.getMantenimiento().setCodigoFalla(-1);
                Sesion.getMantenimiento().setMensaje("");
                Sesion.getMantenimiento().setListadoFallas(new ArrayList());
                Sesion.getMantenimiento().setListadoMaquinas(new ArrayList());
                if (Sesion.getVolver() == 0) {
                    redirect = 1;
                } else if (Sesion.getVolver() == 1) {
                    redirect = 2;
                } else {
                    redirect = 0;
                }
            }

            if (request.getParameter("volverMant_1.x") != null) {
                Sesion.getMantenimiento().setEtapaMaquina(0);
                if (Sesion.getVolver() == 2) {
                    redirect = 3;
                }
            }

            if (request.getParameter("grabarMaquina") != null) {
                if (request.getParameter("usoMaquina") != null) {
                    Sesion.getMantenimiento().getMaquina().setUso(request.getParameter("usoMaquina").trim());
                }
                if (request.getParameter("nombreMaquina") != null) {
                    Sesion.getMantenimiento().getMaquina().setNombreCorto(request.getParameter("nombreMaquina").trim());
                }
                int idTMaq = -1;
                if (request.getParameter("cmbTMaq") != null) {
                    idTMaq = Integer.parseInt(request.getParameter("cmbTMaq").trim());
                    if (idTMaq > -1) {
                        Sesion.getMantenimiento().getMaquina().setTurnoMaq((ModTurnoMaq) Sesion.getMantenimiento().getListadoTurnosMaquina().get(idTMaq));
                    }
                }
                if (!Sesion.getMantenimiento().getMaquina().getUso().equals("") && !Sesion.getMantenimiento().getMaquina().getNombreCorto().equals("") && idTMaq > -1) {
                    if (Sesion.getBeanMan().insertarNuevaMaquina(Sesion.getMantenimiento().getCodigoEspecialidad(),
                            Sesion.getMantenimiento().getMaquina().getUso(),
                            Sesion.getMantenimiento().getMaquina().getNombreCorto(),
                            Sesion.getMantenimiento().getMaquina().getTurnoMaq().getIdTurnoMaq())) {
                        Sesion.getMantenimiento().setListadoMaquinas(Sesion.getBeanMan().listarMaquinas(Sesion.getMantenimiento().getCodigoEspecialidad()));
                        Sesion.getMantenimiento().setEtapaMaquina(0);
                        Sesion.setTip("Datos insertados satisfactoriamente", 0);
                    } else {
                        Sesion.setTip("Error al intentar insertar", 1);
                    }
                } else {
                    Sesion.setTip("Debe digitar un nuevo uso, m�quina y asignar un turno", 3);
                }
            }

            if (request.getParameter("editMaquina") != null) {
                int indice = Integer.parseInt(request.getParameter("editMaquina"));
                Sesion.getMantenimiento().setMaquina((ModMaquina) Sesion.getMantenimiento().getListadoMaquinas().get(indice));
                Sesion.getMantenimiento().setListadoTurnosMaquina(Sesion.getBeanMan().listarTurnosMaquinas());
                Sesion.getMantenimiento().setEtapaMaquina(2);
            }

            if (request.getParameter("deleteMaquina") != null) {
                int indice = Integer.parseInt(request.getParameter("deleteMaquina"));
                ModMaquina maquina = (ModMaquina) Sesion.getMantenimiento().getListadoMaquinas().get(indice);

                boolean existe = false;
                for (int i = 0; i < Sesion.getMantenimiento().getListadoPedidosMantenimientos().size(); i++) {
                    Mantenimiento mantenimiento = (Mantenimiento) Sesion.getMantenimiento().getListadoPedidosMantenimientos().get(i);
                    if (mantenimiento.getCodigoMaquina() == maquina.getIdMaquina()) {
                        existe = true;
                        break;
                    }
                }
                if (!existe) {
                    Sesion.getMantenimiento().setMaquina(maquina);
                    Sesion.getMantenimiento().setListadoTurnosMaquina(Sesion.getBeanMan().listarTurnosMaquinas());
                    Sesion.getMantenimiento().setEtapaMaquina(3);
                } else {
                    Sesion.setTip("M�quina imposible de eliminar, esta asociada a una solicitud", 3);
                }
            }

            if (request.getParameter("actualizarMaquina") != null) {
                if (request.getParameter("usoMaquina") != null) {
                    Sesion.getMantenimiento().getMaquina().setUso(request.getParameter("usoMaquina").trim());
                }
                if (request.getParameter("nombreMaquina") != null) {
                    Sesion.getMantenimiento().getMaquina().setNombreCorto(request.getParameter("nombreMaquina").trim());
                }
                int idTMaq = -1;
                if (request.getParameter("cmbTMaq") != null) {
                    idTMaq = Integer.parseInt(request.getParameter("cmbTMaq").trim());
                    if (idTMaq > -1) {
                        Sesion.getMantenimiento().getMaquina().setTurnoMaq((ModTurnoMaq) Sesion.getMantenimiento().getListadoTurnosMaquina().get(idTMaq));
                    }
                }
                if (!Sesion.getMantenimiento().getMaquina().getUso().equals("") && !Sesion.getMantenimiento().getMaquina().getNombreCorto().equals("") && idTMaq > -1) {
                    if (Sesion.getBeanMan().actualizarMaquina(Sesion.getMantenimiento().getCodigoEspecialidad(),
                            Sesion.getMantenimiento().getMaquina().getIdMaquina(),
                            Sesion.getMantenimiento().getMaquina().getUso(),
                            Sesion.getMantenimiento().getMaquina().getNombreCorto(),
                            Sesion.getMantenimiento().getMaquina().getTurnoMaq().getIdTurnoMaq())) {
                        if (Sesion.getVolver() == 2) {
                            Sesion.getMantenimiento().getDetalle().setTurnoMaq(Sesion.getMantenimiento().getMaquina().getTurnoMaq());
                            redirect = 3;
                        } else {
                            Sesion.getMantenimiento().setListadoMaquinas(Sesion.getBeanMan().listarMaquinas(Sesion.getMantenimiento().getCodigoEspecialidad()));
                            Sesion.getMantenimiento().setEtapaMaquina(0);
                        }
                        Sesion.setTip("Datos actualizados satisfactoriamente", 0);
                    } else {
                        Sesion.setTip("Error al intentar actualizar", 1);
                    }
                } else {
                    Sesion.setTip("Debe digitar un nuevo uso, y asignar un turno", 3);
                }
            }

            if (request.getParameter("eliminarMaquina") != null) {
                if (Sesion.getBeanMan().eliminarMaquina(Sesion.getMantenimiento().getCodigoEspecialidad(), Sesion.getMantenimiento().getMaquina().getIdMaquina())) {
                    Sesion.getMantenimiento().setListadoMaquinas(Sesion.getBeanMan().listarMaquinas(Sesion.getMantenimiento().getCodigoEspecialidad()));
                    Sesion.getMantenimiento().setEtapaMaquina(0);
                    Sesion.setTip("Registro eliminado satisfactoriamente", 0);
                } else {
                    Sesion.setTip("Error al intentar eliminar", 1);
                }
            }

        } catch (Exception ex) {
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }
        if (redirect == 0) {
            response.sendRedirect("jsp/mantenimiento/mantMaquina.jsp");
        } else if (redirect == 1) {
            response.sendRedirect("jsp/mantenimiento/solicitudMantenimiento.jsp");
        } else if (redirect == 2) {
            response.sendRedirect("jsp/mantenimiento/mantMain.jsp");
            Sesion.setVolver(0);
        } else if (redirect == 3) {
            response.sendRedirect("jsp/mantenimiento/ingresarNuevoStatus.jsp");
            Sesion.setVolver(0);
        } else {
            response.sendRedirect("#");
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet Mantenedor Maquina";
    }
}
