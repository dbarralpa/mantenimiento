package mac.sc.mantenimiento;

import java.io.*;
import java.util.ArrayList;
import javax.rmi.PortableRemoteObject;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoRemoteHome;
import mac.sc.sis.mantenimiento.models.ModEmpExt;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantMain extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        int redirect = 0;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            if (Sesion.getBeanMan() == null) {
                conexionEJB.ConexionEJB cal = new conexionEJB.ConexionEJB();
                Object ref1 = cal.conexionBean("macScSisMantenimientoBean");
                macScSisMantenimientoRemoteHome manRemoteHome = (macScSisMantenimientoRemoteHome) PortableRemoteObject.narrow(ref1, macScSisMantenimientoRemoteHome.class);
                Sesion.setBeanMan(manRemoteHome.create());
            }  

            if (request.getParameter("newMaquina") != null) {
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getMantenimiento().setListadoMaquinas(new ArrayList());
                Sesion.setVolver(1);
                redirect = 1;
            }

            if (request.getParameter("newFalla") != null) {
                Sesion.getMantenimiento().setCodigoEspecialidad(-1);
                Sesion.getMantenimiento().getMaquina().setIdMaquina(-1);
                Sesion.setVolver(1);
                redirect = 2;
            }

            if (request.getParameter("newTFalla") != null) {
                Sesion.getMantenimiento().setListadoTipoFallas(Sesion.getBeanMan().listarTipoFallas());
                Sesion.setVolver(1);
                redirect = 3;
            }

            if (request.getParameter("newEmpExt") != null) {
                Sesion.getMantenimiento().setListadoEmpExt(Sesion.getBeanMan().listarEmpExt());
                Sesion.setVolver(1);
                redirect = 4;
            }

            if (request.getParameter("newEmplExt") != null) {
                Sesion.getMantenimiento().setListadoEmpExt(Sesion.getBeanMan().listarEmpExt());
                Sesion.getMantenimiento().setEmpExt(new ModEmpExt());
                Sesion.getMantenimiento().getEmpExt().setId(-1);
                Sesion.setVolver(1);
                redirect = 5;
            }

            if (request.getParameter("newPerMant") != null) {
                Sesion.getMantenimiento().setListadoPerMant(Sesion.getBeanMan().listarPerMant());
                Sesion.setVolver(1);
                redirect = 6;
            }

            if (request.getParameter("newTMaq") != null) {
                Sesion.getMantenimiento().setListadoTurnosMaquina(Sesion.getBeanMan().listarTurnosMaquinas());
                Sesion.setVolver(1);
                redirect = 7;
            }

            if (request.getParameter("volverMant_0.x") != null) {
                redirect = 0;
            }

        } catch (Exception ex) {
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }
        if (redirect == 0) {
            response.sendRedirect("");
        } else if (redirect == 1) {
            response.sendRedirect("jsp/mantenimiento/mantMaquina.jsp");
        } else if (redirect == 2) {
            response.sendRedirect("jsp/mantenimiento/mantFalla.jsp");
        } else if (redirect == 3) {
            response.sendRedirect("jsp/mantenimiento/mantTFalla.jsp");
        } else if (redirect == 4) {
            response.sendRedirect("jsp/mantenimiento/mantEmpExt.jsp");
        } else if (redirect == 5) {
            response.sendRedirect("jsp/mantenimiento/mantEmplExt.jsp");
        } else if (redirect == 6) {
            response.sendRedirect("jsp/mantenimiento/mantPerMant.jsp");
        } else if (redirect == 7) {
            response.sendRedirect("jsp/mantenimiento/mantTMaq.jsp");
        } else {
            response.sendRedirect("#");
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet Mantenedores Sistema";
    }
}
