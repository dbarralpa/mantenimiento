package mac.sc.mantenimiento;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.models.ModPerMant;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantPerMant extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        int redirect = 0; 
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
  
            if(request.getParameter("ingresarNewPerMant")!=null){ 
                Sesion.getMantenimiento().setPerMant(new ModPerMant());
                Sesion.getMantenimiento().setEtapaPerMant(1);            
            }
            
            if(request.getParameter("volverMant_0.x")!=null){
                if(Sesion.getVolver()==0){                    
                    redirect = 0;
                }else{                    
                    redirect = 1;
                }
            }
            
            if(request.getParameter("volverMant_1.x")!=null){                
                Sesion.getMantenimiento().setEtapaPerMant(0);
            }
            
            if(request.getParameter("grabarPerMant")!=null){ 
                Sesion.getMantenimiento().setPerMant(new ModPerMant());
                if(request.getParameter("rutPerMant")!=null){
                    Sesion.getMantenimiento().getPerMant().setVc_rut(request.getParameter("rutPerMant").trim());                
                }  
                if(request.getParameter("nombrePerMant")!=null){
                    Sesion.getMantenimiento().getPerMant().setVc_nombre(request.getParameter("nombrePerMant").trim());                         
                }
                if(request.getParameter("apelliPerMant")!=null){
                    Sesion.getMantenimiento().getPerMant().setVc_apellido(request.getParameter("apelliPerMant").trim());                         
                }
                
                if(!Sesion.getMantenimiento().getPerMant().getVc_rut().equals("") && 
                   !Sesion.getMantenimiento().getPerMant().getVc_nombre().equals("") && 
                   !Sesion.getMantenimiento().getPerMant().getVc_apellido().equals("")){
                        Sesion.getMantenimiento().getPerMant().setId(Sesion.getMantenimiento().getListadoPerMant().size());
                        if(Sesion.getBeanMan().insertarNuevoPerMant(Sesion.getMantenimiento().getPerMant())){                        
                            Sesion.getMantenimiento().setListadoPerMant(Sesion.getBeanMan().listarPerMant());                            
                            Sesion.getMantenimiento().setPerMant(new ModPerMant());
                            Sesion.getMantenimiento().setEtapaPerMant(0);                        
                            Sesion.setTip("Datos insertados satisfactoriamente", 0);
                        }else{
                            Sesion.setTip("Error al intentar insertar", 1);
                        }                 
                }else{
                    Sesion.setTip("Debe rellenar todos los campos", 3);
                }
            }
            
            if(request.getParameter("editPerMant")!=null){ 
                int indice = Integer.parseInt(request.getParameter("editPerMant"));
                Sesion.getMantenimiento().setPerMant((ModPerMant)Sesion.getMantenimiento().getListadoPerMant().get(indice));                
                Sesion.getMantenimiento().getPerMant().setId(indice);
                Sesion.getMantenimiento().setEtapaPerMant(2);
            }
            
            if(request.getParameter("deletePerMant")!=null){      
                int indice = Integer.parseInt(request.getParameter("deletePerMant"));
                Sesion.getMantenimiento().setPerMant((ModPerMant)Sesion.getMantenimiento().getListadoPerMant().get(indice));  
                Sesion.getMantenimiento().getPerMant().setId(indice);
//                if(!Sesion.getBeanMan().tipoDeFallaAsociado(Sesion.getMantenimiento().getTipoFalla().getIdTipoFalla())){
//                    Sesion.getMantenimiento().setEtapaTipoFalla(3);                                    
//                }else{
//                    Sesion.setTip("Tipo Falla imposible de eliminar, esta asociada a una Falla", 3);
//                }                
                Sesion.getMantenimiento().setEtapaPerMant(3);
            }
            
            if(request.getParameter("actualizarPerMant")!=null){  
                if(request.getParameter("nombrePerMant")!=null){
                    Sesion.getMantenimiento().getPerMant().setVc_nombre(request.getParameter("nombrePerMant").trim());                
                }
                if(request.getParameter("apelliPerMant")!=null){
                    Sesion.getMantenimiento().getPerMant().setVc_apellido(request.getParameter("apelliPerMant").trim());                
                }
                
                if(!Sesion.getMantenimiento().getPerMant().getVc_nombre().equals("") &&
                   !Sesion.getMantenimiento().getPerMant().getVc_apellido().equals("")){                    
                    if(Sesion.getBeanMan().actualizarPerMant(Sesion.getMantenimiento().getPerMant())){
                        Sesion.getMantenimiento().getListadoPerMant().set(Sesion.getMantenimiento().getPerMant().getId(), Sesion.getMantenimiento().getPerMant());
                        Sesion.getMantenimiento().setEtapaPerMant(0);
                        Sesion.setTip("Datos actualizados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar actualizar", 1);
                    }                 
                }else{
                    Sesion.setTip("Debe rellenar todos los campos antes", 3);
                }
            }
            
            if(request.getParameter("eliminarPerMant")!=null){                  
                if(Sesion.getBeanMan().eliminarPerMant(Sesion.getMantenimiento().getPerMant().getVc_rut())){
                    Sesion.getMantenimiento().getListadoPerMant().remove(Sesion.getMantenimiento().getPerMant().getId());
                    Sesion.getMantenimiento().setPerMant(new ModPerMant());
                    Sesion.getMantenimiento().setEtapaPerMant(0);
                    Sesion.setTip("Registro eliminado satisfactoriamente", 0);
                }else{
                    Sesion.setTip("Error al intentar eliminar", 1);
                }  
            }
            
        }catch(Exception ex){   
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }        
        if(redirect==0){
            response.sendRedirect("jsp/mantenimiento/mantPerMant.jsp");
        }else if(redirect==1){
            response.sendRedirect("jsp/mantenimiento/mantMain.jsp");
            Sesion.setVolver(0);
        }else{
            response.sendRedirect("#");
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Servlet Mantenedor Personal Mantenimiento";
    }
    
}
