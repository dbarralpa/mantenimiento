package mac.sc.mantenimiento;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.models.ModEmpExt;
import mac.sc.sis.mantenimiento.models.ModEmplExt;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

public class FrmMantEmplExt extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html; charset=iso-8859-1");
        ModSesion Sesion = null;
        int redirect = 0; 
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
            
            if(request.getParameter("cmbEmpExt")!=null){ 
                int idEmp = Integer.parseInt(request.getParameter("cmbEmpExt"));
                if(idEmp!=-1){
                    Sesion.getMantenimiento().setEmpExt((ModEmpExt)Sesion.getMantenimiento().getListadoEmpExt().get(idEmp));                    
                }else{
                    Sesion.getMantenimiento().setEmpExt(new ModEmpExt());
                    Sesion.getMantenimiento().getEmpExt().setId(-1);
                }                
                Sesion.getMantenimiento().setEtapaEmplExt(0);            
            }
  
            if(request.getParameter("ingresarNewEmplExt")!=null){ 
                if(Sesion.getMantenimiento().getEmpExt().getId()==-1){
                    Sesion.setTip("Debe seleccionar una Empresa antes", 3);
                }else{
                    Sesion.getMantenimiento().getEmpExt().setEmpleado(new ModEmplExt());
                    Sesion.getMantenimiento().setEtapaEmplExt(1); 
                }          
            }
            
            if(request.getParameter("volverMant_0.x")!=null){
                if(Sesion.getVolver()==0){                    
                    redirect = 0;
                }else{                    
                    redirect = 1;
                }
            }
            
            if(request.getParameter("volverMant_1.x")!=null){                
                Sesion.getMantenimiento().setEtapaEmplExt(0);
            }
            
            if(request.getParameter("grabarEmplExt")!=null){ 
                Sesion.getMantenimiento().getEmpExt().setEmpleado(new ModEmplExt());
                if(request.getParameter("rutEmplExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().getEmpleado().setVc_rut(request.getParameter("rutEmplExt").trim());                
                }  
                if(request.getParameter("nombreEmplExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().getEmpleado().setVc_nombre(request.getParameter("nombreEmplExt").trim());                         
                }
                if(request.getParameter("apelliEmplExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().getEmpleado().setVc_apellido(request.getParameter("apelliEmplExt").trim());                         
                }
                
                if(!Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_rut().equals("") && 
                   !Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_nombre().equals("") && 
                   !Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_apellido().equals("")){
                        Sesion.getMantenimiento().getEmpExt().getEmpleado().setId(Sesion.getMantenimiento().getEmpExt().getEmpleados().size());
                        Sesion.getMantenimiento().getEmpExt().getEmpleado().setVc_rut_emp(Sesion.getMantenimiento().getEmpExt().getVc_rut());
                        if(Sesion.getBeanMan().insertarNuevoEmplExt(Sesion.getMantenimiento().getEmpExt().getEmpleado())){                        
                            Sesion.getMantenimiento().getEmpExt().setEmpleados(Sesion.getBeanMan().listarEmplExt(Sesion.getMantenimiento().getEmpExt().getVc_rut()));                            
                            Sesion.getMantenimiento().getEmpExt().setEmpleado(new ModEmplExt());
                            Sesion.getMantenimiento().setEtapaEmplExt(0);                        
                            Sesion.setTip("Datos insertados satisfactoriamente", 0);
                        }else{
                            Sesion.setTip("Error al intentar insertar", 1);
                        }                 
                }else{
                    Sesion.setTip("Debe rellenar todos los campos", 3);
                }
            }
            
            if(request.getParameter("editEmplExt")!=null){ 
                int indice = Integer.parseInt(request.getParameter("editEmplExt"));
                Sesion.getMantenimiento().getEmpExt().setEmpleado((ModEmplExt)Sesion.getMantenimiento().getEmpExt().getEmpleados().get(indice));                
                Sesion.getMantenimiento().getEmpExt().getEmpleado().setId(indice);
                Sesion.getMantenimiento().setEtapaEmplExt(2);
            }
            
            if(request.getParameter("deleteEmplExt")!=null){      
                int indice = Integer.parseInt(request.getParameter("deleteEmplExt"));
                Sesion.getMantenimiento().getEmpExt().setEmpleado((ModEmplExt)Sesion.getMantenimiento().getEmpExt().getEmpleados().get(indice));  
                Sesion.getMantenimiento().getEmpExt().getEmpleado().setId(indice);
//                if(!Sesion.getBeanMan().tipoDeFallaAsociado(Sesion.getMantenimiento().getTipoFalla().getIdTipoFalla())){
//                    Sesion.getMantenimiento().setEtapaTipoFalla(3);                                    
//                }else{
//                    Sesion.setTip("Tipo Falla imposible de eliminar, esta asociada a una Falla", 3);
//                }                
                Sesion.getMantenimiento().setEtapaEmplExt(3);
            }
            
            if(request.getParameter("actualizarEmplExt")!=null){  
                if(request.getParameter("nombreEmplExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().getEmpleado().setVc_nombre(request.getParameter("nombreEmplExt").trim());                
                }
                if(request.getParameter("apelliEmplExt")!=null){
                    Sesion.getMantenimiento().getEmpExt().getEmpleado().setVc_apellido(request.getParameter("apelliEmplExt").trim());                
                }
                
                if(!Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_nombre().equals("") &&
                   !Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_apellido().equals("")){                    
                    if(Sesion.getBeanMan().actualizarEmplExt(Sesion.getMantenimiento().getEmpExt().getEmpleado())){
                        Sesion.getMantenimiento().getEmpExt().getEmpleados().set(Sesion.getMantenimiento().getEmpExt().getEmpleado().getId(), 
                                                                                 Sesion.getMantenimiento().getEmpExt().getEmpleado());
                        Sesion.getMantenimiento().setEtapaEmplExt(0);
                        Sesion.setTip("Datos actualizados satisfactoriamente", 0);
                    }else{
                        Sesion.setTip("Error al intentar actualizar", 1);
                    }                 
                }else{
                    Sesion.setTip("Debe digitar un Nombre y Apellido", 3);
                }
            }
            
            if(request.getParameter("eliminarEmplExt")!=null){                  
                if(Sesion.getBeanMan().eliminarEmplExt(Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_rut(), 
                                                       Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_rut_emp())){
                    Sesion.getMantenimiento().getEmpExt().getEmpleados().remove(Sesion.getMantenimiento().getEmpExt().getEmpleado().getId());
                    Sesion.getMantenimiento().getEmpExt().setEmpleado(new ModEmplExt());
                    Sesion.getMantenimiento().setEtapaEmplExt(0);
                    Sesion.setTip("Registro eliminado satisfactoriamente", 0);
                }else{
                    Sesion.setTip("Error al intentar eliminar", 1);
                }  
            }
            
        }catch(Exception ex){   
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }        
        if(redirect==0){
            response.sendRedirect("jsp/mantenimiento/mantEmplExt.jsp");
        }else if(redirect==1){
            response.sendRedirect("jsp/mantenimiento/mantMain.jsp");
            Sesion.setVolver(0);
        }else{
            response.sendRedirect("#");
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Servlet Mantenedor Empleado Empresa Externa";
    }
    
}
