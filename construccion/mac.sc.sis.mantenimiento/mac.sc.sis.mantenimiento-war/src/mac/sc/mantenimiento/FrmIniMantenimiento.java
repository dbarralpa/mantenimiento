package mac.sc.mantenimiento;

import java.io.*;
import java.util.ArrayList;
import javax.rmi.PortableRemoteObject;
import javax.servlet.*;
import javax.servlet.http.*;
import mac.contador.Contador;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoRemoteHome;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModMantenimiento;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

/**
 *
 * @author ngonzalez
 * @version
 */
public class FrmIniMantenimiento extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModSesion Sesion = null;
        try {
            response.setContentType("text/html; charset=iso-8859-1");
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            int url = 0;

            if (Sesion.getBeanMan() == null) {
                conexionEJB.ConexionEJB cal = new conexionEJB.ConexionEJB();
                Object ref1 = cal.conexionBean("macScSisMantenimientoBean");
                macScSisMantenimientoRemoteHome manRemoteHome = (macScSisMantenimientoRemoteHome) PortableRemoteObject.narrow(ref1, macScSisMantenimientoRemoteHome.class);
                Sesion.setBeanMan(manRemoteHome.create());
            }

            if (Sesion.getListadoAreas().isEmpty()) {
                Sesion.setListadoAreas(Sesion.getBeanMan().listarTodasLasAreas());
            }

            Sesion.setListadoUsuarios(Sesion.getBeanMan().listarTodosLosUsuarios());

            if (request.getParameter("tipoUsuario") != null) {
                Sesion.setTipoUsuario(request.getParameter("tipoUsuario"));
            }

            if (request.getParameter("usuario") != null) {
                Sesion.setUsuario(request.getParameter("usuario"));
            }

            if (!Sesion.getUsuario().trim().equals("")) {
                Sesion.setIdUsuario(Sesion.getBeanMan().obtenerIdUsuario(Sesion.getUsuario()));
                Sesion.setNombreUsuario(Sesion.getBeanMan().obtenernombreUsuario(Sesion.getIdUsuario()));
            }

            if (request.getParameter("ingresar_pedido.x") != null) {
                if (!Sesion.getUsuario().trim().equals("")) {
                    Contador.insertarContador(20, Sesion.getUsuario());
                }
                Sesion.setMantenimiento(new ModMantenimiento());
                Sesion.getMantenimiento().setListadoPedidosMantenimientos(Sesion.getBeanMan().listarPedidosMantenimiento(Sesion.getIdUsuario()));
                Sesion.getMantenimiento().setListadoPedidosMantenPreventivos(Sesion.getBeanMan().listarPedidosMantePreventivos(Sesion.getIdUsuario(),false));
                //Sesion.getPset().indiceMant(Sesion.pedidosPorTipo(Sesion.getMantenimiento().getListadoPedidosMantenimientos(),"C"));
                //Sesion.getPset().indiceMant1(Sesion.pedidosPorTipo(Sesion.getMantenimiento().getListadoPedidosMantenimientos(),"P"));
                Sesion.getPset().indiceMant(Sesion.getMantenimiento().getListadoPedidosMantenimientos());
                Sesion.getPset().indiceMant1(Sesion.getMantenimiento().getListadoPedidosMantenPreventivos());
                url = 1;
            }

            if (request.getParameter("admin_pedido.x") != null) {
                if (!Sesion.getUsuario().trim().equals("")) {
                    Contador.insertarContador(59, Sesion.getUsuario());
                }
                Sesion.setMantenimiento(new ModMantenimiento());
                Sesion.getMantenimiento().setListaEstados(Sesion.getBeanMan().listarTodosLosEstados());
                Sesion.getMantenimiento().setTipo("USUARIO");
                Sesion.getMantenimiento().setListadoPricipal(Sesion.getBeanMan().solicitudesPorUsuario(Sesion.getIdUsuario()));
                Sesion.getMantenimiento().setListadoTMP(new ArrayList());
                Sesion.getPset().indiceSoli(Sesion.getMantenimiento().getListadoPricipal());
                PrmApp.setEstadosListado(Sesion.getMantenimiento());
                url = 2;
            }
            if (request.getParameter("mant_prev.x") != null) {
                if (!Sesion.getUsuario().trim().equals("")) {
                    Contador.insertarContador(61, Sesion.getUsuario());
                }
                url = 4;
            }
            
            if (request.getParameter("mant_sistema.x") != null) {
                if (!Sesion.getUsuario().trim().equals("")) {
                    Contador.insertarContador(61, Sesion.getUsuario());
                }
                url = 3;
            }
            
            if (request.getParameter("mant_informes.x") != null) {
                /*if (!Sesion.getUsuario().trim().equals("")) {
                    Contador.insertarContador(61, Sesion.getUsuario());
                }*/
                url = 5;
            }

            if (url == 0) {
                response.sendRedirect("inicioMantenimiento.jsp");
            } else if (url == 1) {
                response.sendRedirect("jsp/mantenimiento/solicitudMantenimiento.jsp");
            } else if (url == 2) {
                response.sendRedirect("jsp/mantenimiento/ingresarNuevoStatus.jsp");
            } else if (url == 3) {
                response.sendRedirect("jsp/mantenimiento/mantMain.jsp");
            } else if (url == 4) {
                response.sendRedirect("jsp/mantenimiento/modificarProgramaPreventivo.jsp");

            }else if (url == 5) {
                response.sendRedirect("FrmInformes?status=yes");

            }else {
                response.sendRedirect("inicioMantenimiento.jsp");
            }

            response.setContentType("text/html");
        } catch (Exception ex) {
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet Inicio Mantenimiento";
    }
}
