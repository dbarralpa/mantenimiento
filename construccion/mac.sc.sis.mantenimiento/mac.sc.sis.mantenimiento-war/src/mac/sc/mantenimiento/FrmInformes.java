/*
 * FrmInformes.java
 *
 * Created on 17 de febrero de 2012, 13:56
 */

package mac.sc.mantenimiento;

import java.io.*;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModMaquina;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

/**
 *
 * @author dbarra
 * @version
 */
public class FrmInformes extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModSesion Sesion = null;
        try {
            response.setContentType("text/html; charset=iso-8859-1");
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            /*if(request.getParameter("status") != null){
              
              ArrayList maquinas = Sesion.getBeanMan().listarMaquinas();  
              ArrayList programaPreventivoHoras = Sesion.getBeanMan().programaPreventivoHoras();
              
              for(int i=0;i<maquinas.size();i++){
                  ModMaquina maquina = (ModMaquina)maquinas.get(i);
                  double horas = 0d;
                  double presupuesto = 0d;
                  double horasReales = 0d;
                  for(int a=0;a<programaPreventivoHoras.size();a++){
                      ModMaquina maquina1 = (ModMaquina)programaPreventivoHoras.get(a);
                      if(maquina.getIdMaquina() == maquina1.getIdMaquina()){
                          horas += maquina1.getHorasTrabajo();
                          presupuesto += maquina1.getPresupuesto();
                          horasReales += PrmApp.horasDeMantencion(Timestamp.valueOf(maquina1.getModMante().getFechaDetencionEquipo()),Timestamp.valueOf(maquina1.getModMante().getFechaEntregaEquipo()));
                      }
                      maquina1 = null;
                  }
                  maquina.setHorasTrabajo(horas);
                  maquina.setPresupuesto(presupuesto);
                  maquina.setHorasReales(horasReales);
                  maquinas.set(i,maquina);
                  maquina = null;
              }
              Sesion.getMantenimiento().setListadoMaquinas(maquinas);
              
              response.sendRedirect("jsp/mantenimiento/informes.jsp");  
            }*/
            if(request.getParameter("mes") != null && !request.getParameter("mes").equals("-1")){
            Sesion.getMantenimiento().getDetalle().setAno(Integer.parseInt(request.getParameter("ano")));
            Sesion.getMantenimiento().getDetalle().setMes(Integer.parseInt(request.getParameter("mes")));
            }
            if(request.getParameter("volver_1.x") != null){
                response.sendRedirect("inicioMantenimiento.jsp");  
            }else{
                response.sendRedirect("jsp/mantenimiento/informes.jsp");  
            }

           
        } catch (Exception ex) {
            Sesion.setTip("Error general, vuelva a cargar los datos", 1);
            EscribeFichero.addError(ex, true);
        }
        request.getSession().setAttribute("Sesion", Sesion);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet Inicio Mantenimiento";
    }
}
