package excel;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.AsExcel;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;


/**
 *
 * @author 13701758k
 */
public class XlsDetSolRequest extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModSesion Sesion = null;
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/Source_DetalleSolicitud.xls");
            
            excel.setValor(0, 0, 34, Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento(), "DET_TNUMERO", true);
            excel.setValor(0, 8, 29, PrmApp.getFechaActual(), "DET_TTEXTO", true);
            if(Sesion.getMantenimiento().getDetalle().getNombreTipoFalla()!=null){
                if(Sesion.getMantenimiento().getDetalle().getNombreTipoFalla().equals("MEC�NICAS")){
                    excel.setValor(0, 8, 5, "X", "DET_TTEXTO", true);
                }else if(Sesion.getMantenimiento().getDetalle().getNombreTipoFalla().equals("MATRICERIA")){
                    excel.setValor(0, 8, 11, "X", "DET_TTEXTO", true);
                }else if(Sesion.getMantenimiento().getDetalle().getNombreTipoFalla().equals("ELECTRICAS")){
                    excel.setValor(0, 8, 18, "X", "DET_TTEXTO", true);
                }else{
                    excel.setValor(0, 8, 23, "X", "DET_TTEXTO", true);
                }
            }else{
                excel.setValor(0, 8, 23, "X", "DET_TTEXTO", true);
            }
            //Fecha-Hora Detencion
            if(Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo()!=null){
                excel.setValor(0, 12, 3, Sesion.getMantenimiento().getDetalle().getFechaDetencionEquipo().substring(0, 16), "DET_STFECHA", true);
            }else{
                excel.setValor(0, 12, 3, "", "DET_STFECHA", true);
            }
            //DIV./AREA
            excel.setValor(0, 15, 15, Sesion.getMantenimiento().getDetalle().getNombreArea(), "DET_STTEXTO", true);
            //MAQUINA O EQUIPO
            excel.setValor(0, 17, 15, Sesion.getMantenimiento().getDetalle().getNombreMaquina(), "DET_STTEXTO", true);
            //SOLICITADA POR
            excel.setValor(0, 19, 15, Sesion.getNombreUsuario(), "DET_STTEXTO", true);
            
            if(Sesion.getMantenimiento().getDetalle().getDescFalla()!=null && !Sesion.getMantenimiento().getDetalle().getDescFalla().trim().equals("")){
                String detalleProblema = Sesion.getMantenimiento().getDetalle().getDescFalla();
                if(Sesion.getMantenimiento().getDetalle().getObservacionFalla()!=null && !Sesion.getMantenimiento().getDetalle().getObservacionFalla().trim().equals("")){
                    detalleProblema += ", " + Sesion.getMantenimiento().getDetalle().getObservacionFalla();
                }
                excel.setValor(0, 27, 15, detalleProblema, "DET_STESTADO", true);
            }else{                
                excel.setValor(0, 27, 15, Sesion.getMantenimiento().getDetalle().getObservacionFalla(), "DET_STESTADO", true);
            }
            
            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition","attachment; filename=\"SolicitudesIngresadas.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        }catch(Exception ex){
            EscribeFichero.addError(ex, true);
        }
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Servlet Exportacion Excel Solicitudes Usuario";
    }
}