/*
 * XlsProgramaPreventivo.java
 *
 * Created on 15 de febrero de 2012, 16:56
 */

package excel;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoBean;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModMaquina;
import mac.sc.sis.mantenimiento.models.ModPrograma;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.AsExcel;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFPicture;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 *
 * @author dbarra
 * @version
 */
public class XlsProgramaPreventivo extends HttpServlet {
    
    SimpleDateFormat forma1 = new SimpleDateFormat("yyyy");
    int year = Integer.parseInt(forma1.format(new Date()));
    //int year = 2013;
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ModSesion Sesion = null;
        
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            
            byte[] meses = {1, 2, 3,4,5,6,7,8,9,10,11,12};
            int hoj0 = 0;
            int hoj1 = 1;
            int hoj2 = 2;
            int hoj3 = 3;
            int hoj4 = 4;
            String[] cabecera = {"M�QUINA","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
            HSSFWorkbook objWB = new HSSFWorkbook();
            HSSFSheet hoja1 = objWB.createSheet("M�quinas cr�ticas");
            HSSFSheet hoja2 = objWB.createSheet("Met. Mec.");
            HSSFSheet hoja3 = objWB.createSheet("Bobinas");
            HSSFSheet hoja4 = objWB.createSheet("EquipLevan");
            HSSFSheet hoja5 = objWB.createSheet("SoldadorasMig.");
            HSSFFont fuente = objWB.createFont();
            HSSFCellStyle estiloCelda = objWB.createCellStyle();
            agregarDatos(Sesion, meses, hoja1, objWB, request, cabecera,hoj0,"CR",fuente);
            agregarDatos(Sesion, meses, hoja2, objWB, request, cabecera,hoj1,"NC",fuente);
            agregarDatos(Sesion, meses, hoja3, objWB, request, cabecera,hoj2,"BO",fuente);
            agregarDatos(Sesion, meses, hoja4, objWB, request, cabecera,hoj3,"EX",fuente);
            agregarDatos(Sesion, meses, hoja5, objWB, request, cabecera,hoj4,"SO",fuente);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"Programa preventivo.xls\"");
            objWB.write(response.getOutputStream());
            
        } catch (Exception ex) {
        }
        
        
    }
    
    private void agregarDatos(ModSesion Sesion, byte[] meses, HSSFSheet hoja1, HSSFWorkbook objWB,HttpServletRequest request, String[] cabecera, int hoj, String param, HSSFFont fuente){
        
        try {
            ArrayList programa = Sesion.getBeanMan().cartaGantt(year,param);
            ArrayList maquinas = Sesion.getBeanMan().listarMaquinasConPrograma(year,param);
            ArrayList forecast = Sesion.getBeanMan().forecast_tiempo(year,param);
            ArrayList program = new ArrayList();
            for (int e = 0; e < maquinas.size(); e++) {
                ModMaquina maquina = (ModMaquina)maquinas.get(e);
                ArrayList pictures = new ArrayList();
                for (int a = 0; a < meses.length; a++) {
                    String nombre = "";
                    double tiempo = 0d;
                    String fecha = "";
                    for (int i = 0; i < programa.size(); i++) {
                        ModPrograma prog = (ModPrograma)programa.get(i);
                        if(maquina.getIdMaquina() == prog.getIdMaquina()){
                            if(meses[a] == prog.getMes()){
                                nombre += prog.getNombre();
                                fecha = prog.getFecha();
                                double val = PrmApp.forecast(maquina.getIdMaquina(),prog.getMes(),forecast);
                                if(val > 0){
                                    tiempo += val;
                                }else{
                                    tiempo += prog.getTiempo();
                                }
                            }
                        }
                        prog = null;
                    }
                    ModPrograma prog = new ModPrograma();
                    prog.setNombre(nombre);
                    prog.setTiempo(tiempo);
                    prog.setFecha(fecha);
                    pictures.add(prog);
                    prog = null;
                }
                ModPrograma modp = new ModPrograma();
                modp.setIdMaquina(maquina.getIdMaquina());
                modp.setNombre(maquina.getNombreCorto());
                modp.setProgramas(pictures);
                program.add(modp);
                maquina = null;
                modp = null;
            }
            
            createExcel(hoja1, objWB, (short) 0, (short) 0, "CARTA GANTT SCHAFFNER "+year, request,"",false,(short)11,2,500,HSSFFont.SS_NONE,hoj,fuente);
            objWB.getSheetAt(hoj).addMergedRegion(new CellRangeAddress(0, 0, 0, 12));
            for(int i=0;i<cabecera.length;i++){
                createExcel(hoja1, objWB, (short) 1, (short) i, cabecera[i], request,"",false,(short)6,2,500,HSSFFont.SS_NONE,hoj,fuente);
            }
            int val = 2;
            int val1 = 3;
            for(int i=0;i<program.size();i++){
                ModPrograma prog = (ModPrograma)program.get(i);
                createExcel(hoja1, objWB, (short) val1, (short) 0, prog.getNombre(), request,"",false,(short)8,CellStyle.ALIGN_JUSTIFY,250,HSSFFont.SS_NONE,hoj,fuente);
                for(int c=0;c<prog.getProgramas().size();c++){
                    ModPrograma picture = (ModPrograma) prog.getProgramas().get(c);
                    createExcel(hoja1, objWB, (short) val, (short) (c+1), picture.getFecha(), request,"",false,(short)8,2,250,HSSFFont.BOLDWEIGHT_BOLD,hoj,fuente);
                    if(!picture.getNombre().equals("")){
                        createExcel(hoja1, objWB, (short) val1, (short) (c+1), String.valueOf(picture.getTiempo()), request,picture.getNombre(),true,(short)8,3,500,HSSFFont.SS_NONE,hoj,fuente);
                    }else{
                        createExcel(hoja1, objWB, (short) val1, (short) (c+1), String.valueOf(""), request,picture.getNombre(),false,(short)8,3,500,HSSFFont.SS_NONE,hoj,fuente);
                    }
                    picture = null;
                }
                prog = null;
                val += 2;
                val1 +=2;
            }
        } catch (Exception ex) {
        }
    }
    private int loadPicture( File path, HSSFWorkbook wb ) throws IOException {
        int pictureIndex;
        FileInputStream fis = null;
        ByteArrayOutputStream bos = null;
        try {
            // read in the image file
            fis = new FileInputStream(path);
            bos = new ByteArrayOutputStream( );
            int c;
            // copy the image bytes into the ByteArrayOutputStream
            while ( (c = fis.read()) != -1)
                bos.write( c );
            // add the image bytes to the workbook
            pictureIndex = wb.addPicture(bos.toByteArray(), HSSFWorkbook.PICTURE_TYPE_PNG
                    );
        }
        
        finally {
            if (fis != null)
                fis.close();
            if (bos != null)
                bos.close();
        }
        return pictureIndex;
    }
    private void createExcel(HSSFSheet hoja1, HSSFWorkbook objWB, short numFila, short colum, String valor, HttpServletRequest request, String nomImage,
            boolean est, short altLetra, int alignment,int altoFila, short boldweight,int hoj, HSSFFont fuente) {
        HSSFRow fila = null;
        try {
            if (objWB.getSheetAt(hoj).getRow(numFila) != null) {
                fila = objWB.getSheetAt(hoj).getRow(numFila);
            }else{
                fila = hoja1.createRow((short) numFila);
            }
            fila.setHeight((short) altoFila);
            
            //HSSFFont fuente = objWB.createFont();
            
            fuente.setFontHeightInPoints(altLetra);
            fuente.setFontName(fuente.FONT_ARIAL);
            //fuente.setItalic(true);
            fuente.setBoldweight(boldweight);
            HSSFCellStyle estiloCelda = objWB.createCellStyle();
            estiloCelda.setWrapText(true);
            estiloCelda.setAlignment((short)alignment);
            estiloCelda.setFont(fuente);
            estiloCelda.setTopBorderColor((short) 8);
            estiloCelda.setFillBackgroundColor((short) 22);
            HSSFCell celda = fila.createCell((short) colum);
            celda.setCellStyle(estiloCelda);
            celda.setCellType(HSSFCell.CELL_TYPE_STRING);
            HSSFRichTextString texto = new HSSFRichTextString(valor);
            celda.setCellValue(texto);
            if(est){
                HSSFPatriarch patriarch = hoja1.createDrawingPatriarch();
                HSSFClientAnchor anchor = new HSSFClientAnchor(10, 10, 600, 255, (short) colum, numFila, (short) colum, numFila);
                patriarch.createPicture(anchor, loadPicture(new File(request.getSession().getServletContext().getRealPath("/") + "Imagenes/cartagantt/"+nomImage+".PNG"), objWB));
            }
            //ajustaColumnas(objWB);
            
        } catch (Exception e) {
        }
    }
    private void ajustaColumnas(HSSFWorkbook objWB) {
        final short numeroColumnas = objWB.getSheetAt(0).getRow(0).getLastCellNum();
        for (int i = 0; i < numeroColumnas; i++) {
            objWB.getSheetAt(0).autoSizeColumn(i);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
