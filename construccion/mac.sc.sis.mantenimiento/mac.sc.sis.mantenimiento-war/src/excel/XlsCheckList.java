/*
 * XlsCheckList.java
 *
 * Created on 10 de febrero de 2012, 17:42
 */

package excel;

import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoBean;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.AsExcel;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;
import mac.sc.sis.mantenimiento.controllers.PrmApp;

/**
 *
 * @author dbarra
 * @version
 */
public class XlsCheckList extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            macScSisMantenimientoBean mante = new macScSisMantenimientoBean();
            ArrayList tareas = mante.listarTareasPreventivos(Sesion.getMantenimiento().getDetalle().getId_tareas());
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/Source_checkList.xls");
            
            /*excel.setValor(0, 3, 3, PrmApp.getFechaActual(), "DET_TTEXTO");
            excel.setValor(0, 4, 4, PrmApp.getHoraActual(), "DET_TTEXTO");*/
            excel.setValor(0, 2, 2, mante.obtenernombreUsuario(Sesion.getMantenimiento().getDetalle().getCodigoSolicitante()), "DET_TTEXTO", true);
            excel.setValor(0, 5, 2, Sesion.getMantenimiento().getDetalle().getNombreMaquina(), "DET_TTEXTO", true);
            excel.setValor(0, 6, 2, "0", "DET_TTEXTO", true);
            excel.setValor(0, 7, 2, "0", "DET_TTEXTO", true);
            excel.setValor(0, 8, 2, 0, "DET_TTEXTO", true);
            excel.setValor(0, 9, 2, Sesion.getMantenimiento().getDetalle().getNombreArea(), "DET_TTEXTO", true);
            
            excel.setValor(0, 2, 7, PrmApp.getFechaActual()+ " " + PrmApp.getHoraActual(),"DET_TTEXTO", true);
            excel.setValor(0, 3, 7, Sesion.getMantenimiento().getDetalle().getFechaPrograma().toString(),"DET_TFECHA", true);
            excel.setValor(0, 5, 7, Sesion.getMantenimiento().getDetalle().getCodigoMantenimiento(), "DET_TTEXTO", true);
            int a = 14;
            for (int i = 0; i < tareas.size(); i++) {
                Mantenimiento man = (Mantenimiento)tareas.get(i);
                //excel.setValor(0,corre , 3,8, avanceNC.getDescripcionAvance()+" Evento registrado: "+avanceNC.getFechaAvance(), "DET_STTEXTO",corre+6);
                if(man.getNombreArea().length()<100){
                    excel.setValor(0, a+i, 0, i+1,"DET_TGENERAL", true);
                    
                    excel.setValor(0, a+i, 1,9, man.getNombreArea(),"DET_TGENERAL",a+i, true);
                    
                    if(man.getId_tareas() == 1){
                        excel.setValor(0, a+i, 10, "MEC�NICA","DET_TGENERAL", true);
                    }else if(man.getId_tareas() == 2){
                        excel.setValor(0, a+i, 10, "EL�CTRICA","DET_TGENERAL", true);
                    }
                    excel.setValor(0, a+i, 11, "","DET_TGENERAL", true);
                    man = null;
                }else{
                    excel.setValor(0, a+i, 0, i+1,"DET_TGENERAL", false);
                    
                    excel.setValor(0, a+i, 1,9, man.getNombreArea(),"DET_TGENERAL",a+i, false);
                    
                    if(man.getId_tareas() == 1){
                        excel.setValor(0, a+i, 10, "MEC�NICA","DET_TGENERAL", false);
                    }else if(man.getId_tareas() == 2){
                        excel.setValor(0, a+i, 10, "EL�CTRICA","DET_TGENERAL", false);
                    }
                    excel.setValor(0, a+i, 11, "","DET_TGENERAL", false);
                    man = null;
                }
            }
            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"Check list.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        } catch (Exception ex) {
            EscribeFichero.addError(ex, true);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
