/*
 * XlsMantecionCorrectiva.java
 *
 * Created on 24 de febrero de 2012, 10:42
 */

package excel;

import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.clases.macScSisMantenimientoBean;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModMaquina;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.AsExcel;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;

/**
 *
 * @author dbarra
 * @version
 */
public class XlsMantecionCorrectiva extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/Source_Mantenciones_correctivas.xls");
            macScSisMantenimientoBean mante = new macScSisMantenimientoBean();
            ArrayList correctivas = mante.horasDetenidasCorrectivas(Sesion.getMantenimiento().getDetalle().getAno(),Sesion.getMantenimiento().getDetalle().getMes());
            double horasMecanicas = 0d;
            int countMecanicas = 0;
            double horasElectricas = 0d;
            int countElectricas = 0;
            double horasMatriceria = 0d;
            int countMatriceria = 0;
            for(int a=0;a<correctivas.size();a++){
                Mantenimiento mant = (Mantenimiento)correctivas.get(a);
                ArrayList horarios = mante.listarHorariosXTurno(mant.getTurnoMaq().getIdTurnoMaq());
                if(mant.getTipo().equals("ME")){
                    int minutosDetencion = PrmApp.mDetencion(mant.getFechaDetencionEquipo(),mant.getFechaEntregaEquipo(),horarios);
                    mant.setHorasDetencion(String.valueOf((double)minutosDetencion/60));
                    horasMecanicas += (double)minutosDetencion/60;
                    countMecanicas++;
                    correctivas.set(a,mant);
                }else if(mant.getTipo().equals("EL")){
                    int minutosDetencion = PrmApp.mDetencion(mant.getFechaDetencionEquipo(),mant.getFechaEntregaEquipo(),horarios);
                    mant.setHorasDetencion(String.valueOf((double)minutosDetencion/60));
                    horasElectricas+= (double)minutosDetencion/60;
                    countElectricas++;
                    correctivas.set(a,mant);
                }else if(mant.getTipo().equals("MA")){
                    int minutosDetencion = PrmApp.mDetencion(mant.getFechaDetencionEquipo(),mant.getFechaEntregaEquipo(),horarios);
                    mant.setHorasDetencion(String.valueOf((double)minutosDetencion/60));
                    horasMatriceria+= (double)minutosDetencion/60;
                    countMatriceria++;
                    correctivas.set(a,mant);
                }
            }
            //double horasDisponibles = 180 * correctivas.size();
            for(int a=0;a<correctivas.size();a++){
                Mantenimiento mant = (Mantenimiento)correctivas.get(a);
                //int hoja, int fila, int fromCelda, int toCelda, String valorStr, String estilo, int fila1
                excel.setValor(0, a+5, 0,1, mant.getNombreMaquina(), "DET_STNUMERO",a+5, true);
                //int hoja, int fila, int celda, String valorStr, String estilo
                excel.setValor(0, a+5, 2,3, Double.parseDouble(mant.getHorasDetencion()), "DET_TNUMERO",a+5, true);
                excel.setValor(0, a+5, 4,7, mant.getObservacionFalla(), "DET_TTEXTO",a+5, true);
            }
            excel.setValor(0, 3, 10, horasMecanicas, "DET_STNUMERO", true);
            excel.setValor(0, 3, 11, (180*countMecanicas), "DET_STNUMERO", true);
            excel.setValor(0, 3, 12, countMecanicas, "DET_STNUMERO", true);
            
            excel.setValor(0, 4, 10, horasMatriceria, "DET_STNUMERO", true);
            excel.setValor(0, 4, 11, (180*countMatriceria), "DET_STNUMERO", true);
            excel.setValor(0, 4, 12, countMatriceria, "DET_STNUMERO", true);
            
            excel.setValor(0, 5, 10, horasElectricas, "DET_STNUMERO", true);
            excel.setValor(0, 5, 11, (180*countElectricas), "DET_STNUMERO", true);
            excel.setValor(0, 5, 12, countElectricas, "DET_STNUMERO", true);
            
            excel.setValor(0, 6, 10, horasElectricas+horasMecanicas+horasMatriceria, "DET_STNUMERO", true);
            excel.setValor(0, 6, 11, (180*countElectricas)+(180*countMecanicas)+(180*countMatriceria), "DET_STNUMERO", true);
            excel.setValor(0, 6, 12, countElectricas+countMecanicas+countMatriceria, "DET_STNUMERO", true);
            
            int inc = 0;
            ArrayList maquinasCriticas = mante.listarMaquinasCriticas();
            double horasMecanicas1 = 0;
            double horasElectricas1 = 0;
            double horasMatriceria1 = 0;
            for(int i=0;i<maquinasCriticas.size();i++){
                horasMecanicas = 0;
                horasElectricas = 0;
                horasMatriceria = 0;
                ModMaquina maquina = (ModMaquina)maquinasCriticas.get(i);
                //if(maquina.getTipo().equals("CR")){
                for(int a=0;a<correctivas.size();a++){
                    Mantenimiento mant = (Mantenimiento)correctivas.get(a);
                    if(maquina.getIdMaquina() == mant.getCodigoMaquina()){
                        //ArrayList horarios = mante.listarHorariosXTurno(mant.getTurnoMaq().getIdTurnoMaq());
                        if(mant.getTipo().equals("ME")){
                            horasMecanicas += Double.parseDouble(mant.getHorasDetencion());
                            
                        }else if(mant.getTipo().equals("EL")){
                            horasElectricas+= Double.parseDouble(mant.getHorasDetencion());
                            
                        }else if(mant.getTipo().equals("MA")){
                            horasMatriceria+= Double.parseDouble(mant.getHorasDetencion());
                        }
                    }
                }
                horasMatriceria1 += horasMatriceria;
                horasMecanicas1 += horasMecanicas;
                horasElectricas1 += horasElectricas;
                excel.setValor(1, inc+6, 1, maquina.getNombreCorto(), "DET_STNUMERO", true);
                excel.setValor(1, inc+6, 2, horasMecanicas, "DET_STNUMERO", true);
                excel.setValor(1, inc+6, 3, horasElectricas, "DET_STNUMERO", true);
                excel.setValor(1, inc+6, 4, horasMatriceria, "DET_STNUMERO", true);
                excel.setValor(1, inc+6, 5, 180, "DET_STNUMERO", true);
                excel.setValor(1, inc+6, 6, (horasMecanicas+horasElectricas+horasMatriceria), "DET_STNUMERO", true);
                excel.setValor(1, inc+6, 7, ((horasMecanicas+horasElectricas+horasMatriceria)*100)/180, "DET_STNUMERO", true);
                inc++;
                maquina.setHorasReales(horasMecanicas+horasElectricas+horasMatriceria);
                maquinasCriticas.set(i,maquina);
                //}
                maquina = null;
            }
            excel.setValor(1, inc+7, 1, "Total", "DET_STNUMERO", true);
            excel.setValor(1, inc+7, 2, horasMecanicas1, "DET_STNUMERO", true);
            excel.setValor(1, inc+7, 3, horasElectricas1, "DET_STNUMERO", true);
            excel.setValor(1, inc+7, 4, horasMatriceria1, "DET_STNUMERO", true);
            
            ArrayList horas = mante.horasPresupuestadasPreventivas(Sesion.getMantenimiento().getDetalle().getAno(),Sesion.getMantenimiento().getDetalle().getMes());
            inc = 0;
            horasMecanicas1 = 0;
            horasElectricas1 = 0;
            horasMatriceria1 = 0;
            double totalParada = 0;
            double horasDispo = 0;
            for(int a=0;a<maquinasCriticas.size();a++){
                ModMaquina maquina = (ModMaquina)maquinasCriticas.get(a);
                boolean est5 = false;
                for(int i=0;i<horas.size();i++){
                    Mantenimiento man = (Mantenimiento)horas.get(i);
                    if(maquina.getIdMaquina() == man.getCodigoMaquina()){
                        excel.setValor(2, a+8, 1, man.getNombreMaquina(), "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 2, man.getPresupuesto(), "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 3, (man.getPresupuesto()*100)/180, "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 4, man.getHorasDetenidas(), "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 5, (man.getHorasDetenidas()*100)/180, "DET_STNUMERO", true);
                        //double val = PrmApp.horasTotalesCorrectivas(man.getCodigoMaquina(),maquinasCriticas);
                        double val = maquina.getHorasReales();
                        excel.setValor(2, a+8, 6, val, "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 7, (val*100)/180, "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 8, 180, "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 9, man.getHorasDetenidas()+val, "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 10, 100, "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 11, 180-(man.getHorasDetenidas()+val), "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 12, ((180-(man.getHorasDetenidas()+val))*100)/180, "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 13, 100-(((180-(man.getHorasDetenidas()+val))*100)/180), "DET_STNUMERO", true);
                        excel.setValor(2, a+8, 14, 180, "DET_STNUMERO", true);
                        est5 = true;
                        horasMecanicas1 += man.getPresupuesto();
                        horasElectricas1 += man.getHorasDetenidas();
                        horasMatriceria1 += val;
                        horasDispo += 180;
                        totalParada +=  (man.getHorasDetenidas()+val);
                    }
                }
                if(!est5){
                    excel.setValor(2, a+8, 1, maquina.getNombreCorto(), "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 2, 0, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 3, 0, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 4, 0, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 5, 0, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 6, 0, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 7, 0, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 8, 180, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 9, 0, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 10, 100, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 11, 180, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 12, 100, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 13, 0, "DET_STNUMERO", true);
                    excel.setValor(2, a+8, 14, 180, "DET_STNUMERO", true);
                }
                inc = a+9;
            }
            excel.setValor(2, inc, 1, "Total", "DET_STNUMERO", true);
            excel.setValor(2, inc, 2, horasMecanicas1, "DET_STNUMERO", true);
            excel.setValor(2, inc, 3, "", "DET_STNUMERO", true);
            excel.setValor(2, inc, 4, horasElectricas1, "DET_STNUMERO", true);
            excel.setValor(2, inc, 5, "", "DET_STNUMERO", true);
            excel.setValor(2, inc, 6, horasMatriceria1, "DET_STNUMERO", true);
            excel.setValor(2, inc, 7, "", "DET_STNUMERO", true);
            excel.setValor(2, inc, 8, horasDispo, "DET_STNUMERO", true);
            excel.setValor(2, inc, 9, totalParada, "DET_STNUMERO", true);
            
            ArrayList gastosMantencion = mante.listarGastosMantencion(Sesion.getMantenimiento().getDetalle().getAno(),Sesion.getMantenimiento().getDetalle().getMes());
            for(int i=0;i<gastosMantencion.size();i++){
                Mantenimiento man = (Mantenimiento)gastosMantencion.get(i);
                /*excel.setValor(3, i+6, 1,2, man.getNombreMaquina(), "DET_STNUMERO",i+6, true);
                excel.setValor(3, i+6, 3,4, man.getCodProducto(), "DET_TTEXTO",i+6, true);
                excel.setValor(3, i+6, 5,6, man.getPresupuesto(), "DET_STNUMERO",i+6, true);
                excel.setValor(3, i+6, 7,8, man.getCosto(), "DET_STNUMERO",i+6, true);*/
                excel.setValor(3, i+6, 1,2, man.getNombreMaquina(), "DET_STNUMERO",i+6, true);
                excel.setValor(3, i+6, 3,4, man.getObservacionMantenimientoCorrectivo(), "DET_TTEXTO",i+6, true);
                excel.setValor(3, i+6, 5,6, 0, "DET_STNUMERO",i+6, true);
                excel.setValor(3, i+6, 7,8, 0, "DET_STNUMERO",i+6, true);
                man = null;
            }
            
            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"Mantenciones correctivas.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        } catch (Exception ex) {
            EscribeFichero.addError(ex, true);
        }
        
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
