package excel;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.AsExcel;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;


/**
 *
 * @author 13701758k
 */
public class XlsSolSegRequest extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModSesion Sesion = null;
        try {
            Sesion = (ModSesion) request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();

            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/Source_SeguimientoSolicitudes.xls");

            excel.setValor(0, 5, 2, PrmApp.getFechaActual(), "DET_TTEXTO", true);
            excel.setValor(0, 6, 2, PrmApp.getHoraActual(), "DET_TTEXTO", true);

            for (int i = 0; i < Sesion.getMantenimiento().getListadoPricipal().size(); i++) {
                int indice;
                if (Sesion.getMantenimiento().isOrdenAsc()) {
                    indice = i;
                } else {
                    indice = Sesion.getMantenimiento().getListadoPricipal().size() - (i + 1);
                }
                Mantenimiento obj = (Mantenimiento) Sesion.getMantenimiento().getListadoPricipal().get(indice);
                excel.setValor(0, 11 + i, 1, obj.getCodigoMantenimiento(), "DET_TNUMERO", true);
                if (obj.getNombreUsuarioMantenimiento() != null && !obj.getNombreUsuarioMantenimiento().trim().equals("")) {
                    excel.setValor(0, 11 + i, 2, obj.getNombreUsuarioMantenimiento(), "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 2, "No Asignado", "DET_TTEXTO", true);
                }
                excel.setValor(0, 11 + i, 3, obj.getNombreArea(), "DET_TTEXTO", true);
                excel.setValor(0, 11 + i, 4, obj.getNombreMaquina(), "DET_TTEXTO", true);
                if (obj.getNombreFalla() != null) {
                    excel.setValor(0, 11 + i, 5, obj.getNombreFalla(), "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 5, "Otras", "DET_TTEXTO", true);
                }
                if (obj.getNombreTipoFalla() != null) {
                    excel.setValor(0, 11 + i, 6, obj.getNombreTipoFalla(), "DET_TTEXTO", true);
                    excel.setValor(0, 11 + i, 7, obj.getDescTipoFalla(), "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 6, "No Clasificado", "DET_TTEXTO", true);
                    excel.setValor(0, 11 + i, 7, "", "DET_TTEXTO", true);
                }
                excel.setValor(0, 11 + i, 8, obj.getObservacionFalla(), "DET_TTEXTO", true);
                if (obj.getFechaHoy() == null) {
                    excel.setValor(0, 11 + i, 9, "", "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 9, obj.getFechaHoy().substring(0, 10), "DET_TTEXTO", true);
                }
                if (obj.getFechaDiagnostico() == null) {
                    excel.setValor(0, 11 + i, 10, "", "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 10, obj.getFechaDiagnostico().substring(0, 10), "DET_TTEXTO", true);
                }
                if (obj.getPersonalMant().getVc_nombre() != null && obj.getPersonalMant().getVc_apellido() != null) {
                    excel.setValor(0, 11 + i, 11, obj.getPersonalMant().getVc_nombre() + " " + obj.getPersonalMant().getVc_apellido(), "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 11, "No Asignado", "DET_TTEXTO", true);
                }
                excel.setValor(0, 11 + i, 12, obj.getObservacionDiagnostico(), "DET_TTEXTO", true);
                if (obj.getFechaAdquisicion() == null) {
                    excel.setValor(0, 11 + i, 13, "", "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 13, obj.getFechaAdquisicion().substring(0, 10), "DET_TTEXTO", true);
                }
                excel.setValor(0, 11 + i, 14, obj.getObservacionAdquisicion(), "DET_TTEXTO", true);
                if (obj.getFechaSevicioExterno() == null) {
                    excel.setValor(0, 11 + i, 15, "", "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 15, obj.getFechaSevicioExterno().substring(0, 10), "DET_TTEXTO", true);
                }
                if (obj.getEmpExt().getVc_nombre() != null) {
                    excel.setValor(0, 11 + i, 16, obj.getEmpExt().getVc_nombre(), "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 16, "No Asignado", "DET_TTEXTO", true);
                }
                if (obj.getEmplEmt().getVc_nombre() != null) {
                    excel.setValor(0, 11 + i, 17, obj.getEmplEmt().getVc_nombre() + " " + obj.getEmplEmt().getVc_apellido(), "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 17, "No Asignado", "DET_TTEXTO", true);
                }
                excel.setValor(0, 11 + i, 18, obj.getObservacionSevicioExterno(), "DET_TTEXTO", true);
                if (obj.getFechaMantenimientoCorrectivo() == null) {
                    excel.setValor(0, 11 + i, 19, "", "DET_TFECHA", true);
                } else {
                    excel.setValor(0, 11 + i, 19, obj.getFechaMantenimientoCorrectivo().substring(0, 10), "DET_TFECHA", true);
                }
                if (obj.getPersonalMant().getVc_nombre() != null && obj.getPersonalMant().getVc_apellido() != null) {
                    excel.setValor(0, 11 + i, 20, obj.getPersonalMant().getVc_nombre() + " " + obj.getPersonalMant().getVc_apellido(), "DET_TTEXTO", true);
                } else {
                    excel.setValor(0, 11 + i, 20, "No Asignado", "DET_TTEXTO", true);
                }
                excel.setValor(0, 11 + i, 21, obj.getObservacionMantenimientoCorrectivo(), "DET_TTEXTO", true);
                if (obj.getFechaVistoBueno() == null) {
                    excel.setValor(0, 11 + i, 22, "", "DET_TFECHA", true);
                } else {
                    excel.setValor(0, 11 + i, 22, obj.getFechaVistoBueno().substring(0, 10), "DET_TFECHA", true);
                }
                excel.setValor(0, 11 + i, 23, obj.getObservacionVistoBueno(), "DET_TTEXTO", true);
                if (obj.getFechaNoConformidad() == null) {
                    excel.setValor(0, 11 + i, 24, "", "DET_TFECHA", true);
                } else {
                    excel.setValor(0, 11 + i, 24, obj.getFechaNoConformidad().substring(0, 10), "DET_TFECHA", true);
                }
                excel.setValor(0, 11 + i, 25, obj.getObservacionNoConformidad(), "DET_TTEXTO", true);
                if (obj.getFechaDetencionEquipo() == null) {
                    excel.setValor(0, 11 + i, 26, "", "DET_TFECHA", true);
                } else {
                    excel.setValor(0, 11 + i, 26, obj.getFechaDetencionEquipo().substring(0, 19), "DET_TFECHA", true);
                }
                if (obj.getFechaEntregaEquipo() == null) {
                    excel.setValor(0, 11 + i, 27, "", "DET_TFECHA", true);
                } else {
                    excel.setValor(0, 11 + i, 27, obj.getFechaEntregaEquipo().substring(0, 19), "DET_TFECHA", true);
                }
                if ((obj.getFechaDetencionEquipo() != null && !obj.getFechaDetencionEquipo().trim().equals(""))
                        && (obj.getFechaEntregaEquipo() != null && !obj.getFechaEntregaEquipo().trim().equals(""))) {
                    obj.getTurnoMaq().setHorarios(Sesion.getBeanMan().listarHorariosXTurno(obj.getTurnoMaq().getIdTurnoMaq()));
                    PrmApp.calcularHorasMinutosDetencion(obj);
                    excel.setValor(0, 11 + i, 28, Double.parseDouble(obj.getHorasDetencion()), "DET_TNUMERICO", true);
                    excel.setValor(0, 11 + i, 29, Double.parseDouble(obj.getMinutosDetencion()), "DET_TNUMERICO", true);
                } else {
                    excel.setValor(0, 11 + i, 28, 0, "DET_TNUMERICO", true);
                    excel.setValor(0, 11 + i, 29, 0, "DET_TNUMERICO", true);
                }
                obj = null;
            }

            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment;filename=\"SeguimientoSolicitudes.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        } catch (Exception ex) {
            EscribeFichero.addError(ex, true);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet Exportacion Excel Solicitudes Usuario";
    }
}
