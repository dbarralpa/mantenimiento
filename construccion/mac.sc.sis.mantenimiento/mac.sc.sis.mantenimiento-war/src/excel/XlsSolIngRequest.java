package excel;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mac.sc.sis.mantenimiento.beans.Mantenimiento;
import mac.sc.sis.mantenimiento.controllers.PrmApp;
import mac.sc.sis.mantenimiento.models.ModSesion;
import mac.sc.sis.mantenimiento.utils.AsExcel;
import mac.sc.sis.mantenimiento.utils.EscribeFichero;


/**
 *
 * @author 13701758k
 */
public class XlsSolIngRequest extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModSesion Sesion = null;
        try{
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");
            AsExcel excel = new AsExcel();
            if(request.getParameter("mantencion").equals("C")){
                excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/Source_SolicitudesIngresadas.xls");
            } else if(request.getParameter("mantencion").equals("P") || request.getParameter("mantencion").equals("PP")){
                excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "template/excel/Source_SolicitudesPreventivasIngresadas.xls");
            }
            
            excel.setValor(0, 5, 2, PrmApp.getFechaActual(), "DET_TTEXTO", true);
            excel.setValor(0, 6, 2, PrmApp.getHoraActual(), "DET_TTEXTO", true);
            
            //for(int i = 0; i < Sesion.pedidosPorTipo(Sesion.getMantenimiento().getListadoPedidosMantenimientos(),request.getParameter("tipo")).size(); i++){
            if(request.getParameter("mantencion").equals("C")){
                for(int i = 0; i < Sesion.getMantenimiento().getListadoPedidosMantenimientos().size(); i++){
                    int indice;
                    if(Sesion.getMantenimiento().isOrdenAscNew()){
                        indice = i;
                    }else{
                        indice = Sesion.getMantenimiento().getListadoPedidosMantenimientos().size() - (i+1);
                        //indice = Sesion.pedidosPorTipo(Sesion.getMantenimiento().getListadoPedidosMantenimientos(),request.getParameter("tipo")).size() - (i+1);
                    }
                    Mantenimiento obj = (Mantenimiento)Sesion.getMantenimiento().getListadoPedidosMantenimientos().get(indice);
                    //Mantenimiento obj = (Mantenimiento)Sesion.pedidosPorTipo(Sesion.getMantenimiento().getListadoPedidosMantenimientos(),request.getParameter("tipo")).get(indice);
                    excel.setValor(0, 11+i, 1, obj.getCodigoMantenimiento(), "DET_TNUMERO", true);
                    excel.setValor(0, 11+i, 2, obj.getNombreArea(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 3, obj.getNombreMaquina(), "DET_TTEXTO", true);
                    if(obj.getNombreFalla()!=null){
                        excel.setValor(0, 11+i, 4, obj.getNombreFalla(), "DET_TTEXTO", true);
                    }else{
                        excel.setValor(0, 11+i, 4, "Otras", "DET_TTEXTO", true);
                    }
                    if(obj.getDescTipoFalla()!=null){
                        excel.setValor(0, 11+i, 5, obj.getDescTipoFalla(), "DET_TTEXTO", true);
                    }else{
                        excel.setValor(0, 11+i, 5, "No Clasificado", "DET_TTEXTO", true);
                    }
                    
                    excel.setValor(0, 11+i, 6, obj.getObservacionFalla(), "DET_TTEXTO", true);
                    if(obj.getFechaHoy()==null){
                        excel.setValor(0, 11+i, 7, "", "DET_TTEXTO", true);
                    }else{
                        excel.setValor(0, 11+i, 7, obj.getFechaHoy().substring(0,19), "DET_TTEXTO", true);
                    }
                    if(obj.getFechaDetencionEquipo()==null){
                        excel.setValor(0, 11+i, 8, "", "DET_TTEXTO", true);
                    }else{
                        excel.setValor(0, 11+i, 8, obj.getFechaDetencionEquipo().substring(0,19), "DET_TTEXTO", true);
                    }
                    //excel.setValor(0, 11+i, 9, obj.getTipo(), "DET_TTEXTO");
                    obj = null;
                }
            }
            if(request.getParameter("mantencion").equals("P")){
                for(int i = 0; i < Sesion.getMantenimiento().getListadoPedidosMantenPreventivos().size(); i++){
                    int indice;
                    if(Sesion.getMantenimiento().isOrdenAscNew()){
                        indice = i;
                    }else{
                        indice = Sesion.getMantenimiento().getListadoPedidosMantenPreventivos().size() - (i+1);
                    }
                    Mantenimiento obj = (Mantenimiento)Sesion.getMantenimiento().getListadoPedidosMantenPreventivos().get(indice);
                    excel.setValor(0, 11+i, 1, obj.getCodigoMantenimiento(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 2, obj.getNombreArea(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 3, obj.getNombreMaquina(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 4, obj.getFechaMantePreventivo(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 5, obj.getFechaDetencionEquipo(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 6, obj.getFechaEntregaEquipo(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 7, obj.getEjecutor1(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 8, obj.getObservacionCerrar(), "DET_TTEXTO", true);
                    obj = null;
                }
            }
            if(request.getParameter("mantencion").equals("PP")){
                for(int i = 0; i < Sesion.getMantenimiento().getListadoPricipal().size(); i++){
                    int indice;
                    if(Sesion.getMantenimiento().isOrdenAscNew()){
                        indice = i;
                    }else{
                        indice = Sesion.getMantenimiento().getListadoPricipal().size() - (i+1);
                    }
                    Mantenimiento obj = (Mantenimiento)Sesion.getMantenimiento().getListadoPricipal().get(indice);
                    excel.setValor(0, 11+i, 1, obj.getCodigoMantenimiento(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 2, obj.getNombreArea(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 3, obj.getNombreMaquina(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 4, obj.getFechaMantePreventivo(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 5, obj.getFechaDetencionEquipo(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 6, obj.getFechaEntregaEquipo(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 7, obj.getEjecutor1(), "DET_TTEXTO", true);
                    excel.setValor(0, 11+i, 8, obj.getObservacionCerrar(), "DET_TTEXTO", true);
                    obj = null;
                }
            }
            
            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            if(request.getParameter("mantencion").equals("C")){
            response.setHeader("Content-Disposition","attachment;filename=\"Mantenciones correctivas.xls\"");    
            }
            if(request.getParameter("mantencion").equals("P") || request.getParameter("mantencion").equals("PP")) {
            response.setHeader("Content-Disposition","attachment;filename=\"Mantenciones Preventivas.xls\"");    
            }
            
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        }catch(Exception ex){
            EscribeFichero.addError(ex, true);
        }
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    public String getServletInfo() {
        return "Servlet Exportacion Excel Solicitudes Usuario";
    }
}