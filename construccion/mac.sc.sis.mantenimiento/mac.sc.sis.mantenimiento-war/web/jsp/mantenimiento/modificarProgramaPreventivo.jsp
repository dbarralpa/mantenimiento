<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1" import="mac.sc.sis.mantenimiento.beans.Mantenimiento"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Programa preventivo</title>
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" />
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
        <script type="text/javascript">
            function validar(){
            if(document.frmMantPre.maquina.options[document.frmMantPre.maquina.selectedIndex].value != "-1" && document.frmMantPre.intervalo.options[document.frmMantPre.intervalo.selectedIndex].value != "-1"){
            }else{
            alert('Debe elegir maquina e intervalo');
            return false;
            }
            }
            function valCheck(valor){
            if(valor == 'mecanica'){
               document.frmMantPre.electrica.checked = false;
            }
            if(valor == 'electrica'){
               document.frmMantPre.mecanica.checked = false;
            }
            }
            
            function valTareas(){
            if(document.frmMantPre.maquina.options[document.frmMantPre.maquina.selectedIndex].value != "-1" && 
               document.frmMantPre.intervalo.options[document.frmMantPre.intervalo.selectedIndex].value != "-1" &&
               document.frmMantPre.agregarTarea.value.replace(/^\s*|\s*$/g,"") != "" && 
               (document.frmMantPre.mecanica.checked == true || document.frmMantPre.electrica.checked == true)){
            }else{
                alert('Debe agregar todos los datos, maquina, intervalo, tarea y tipo');
                return false;
            }
            }
            function validarPresupuesto(){
             if(document.frmMantPre.tiempoEjecucion.value.replace(/^\s*|\s*$/g,"") != "" && 
                document.frmMantPre.presupuesto.value.replace(/^\s*|\s*$/g,"") != "" &&
                document.frmMantPre.ano.options[document.frmMantPre.ano.selectedIndex].value != "-1" && 
                document.frmMantPre.mes.options[document.frmMantPre.mes.selectedIndex].value != "-1" ){
             }else{
             alert('Debe agregar tiempo, presupuesto, a�o y mes');
             return false;
             }
            }
        </script>
    </head>
    <body>
        <form action="../../FrmMantenimientoPreventivo" method="post" name="frmMantPre">  
            <%java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy");%>
            <center><h4>MODIFICAR PROGRAMA PREVENTIVO</h4></center>
            <%if(Sesion.getEtapa() == 0 || Sesion.getEtapa() == 1){%>
            <table cellpadding="0" cellspacing="0" width="700px" align="center">
                <tr>
                    <td>M&aacute;quinas:&nbsp;</td>
                    <td><%=vw.listarTodasMaquinas(request)%></td>   
                    <td width="60px">&nbsp;</td>
                    <td>Intervalos:&nbsp;</td>
                    <td><%=vw.listarIntervalos(request)%></td>
                    <td width="30px">&nbsp;</td>
                    <td><input type="submit" class="Estilo10" name="enviar" value="buscar" onclick="return validar()"> </td>
                </tr>
                <tr>
                    <td height="10px">&nbsp;</td>
                </tr>               
                <tr>
                    <td colspan="7">
                        <table width="661" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">              
                            <tr>
                                <td class="ScrollStyle">    
                                    <table width="661" border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                        <tr>                              
                                            <td colspan="5" class="Titulo" align="center">Listado de tareas</td>
                                        </tr>
                                        
                                        <tr class="CabeceraListado">
                                            <td width="20">N�</td>
                                            <td width="500">TAREA</td>
                                            <td width="100">TIPO</td>
                                            <td width="20">ELI</td>
                                            <td width="21">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle2">
                                        <table width="640"  border="0" cellpadding="0" cellspacing="0" class="Contenido">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="500"></td>
                                                <td width="100"></td>
                                                <td width="20"></td>
                                            </tr>
                                            <%=vw.grillaTareas(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                            <tr>
                                <%if(Sesion.getTipoUsuario().equals("calidad")){%>
                                <td align="right"><input type="submit" disabled name="eliminarTareas" value="Eliminar tareas" class="Estilo10" onclick="return validar();">&nbsp;&nbsp;&nbsp;</td>
                                <%}else{%>
                                <td align="right"><input type="submit" name="eliminarTareas" value="Eliminar tareas" class="Estilo10" onclick="return validar();">&nbsp;&nbsp;&nbsp;</td>
                                <%}%>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="10px">&nbsp;</td>
                </tr>
                <tr>
                    <%if(Sesion.getEtapa() == 1){%>
                    <td height="60px" colspan="7">
                        <fieldset>
                            <legend>Modificar presupuesto:</legend>
                            Tiempo de ejecuci&oacute;n:&nbsp;&nbsp;<%= ((Mantenimiento)Sesion.getMantenimiento().getListadoTareas().get(0)).getHorasDetencion()%>&nbsp;horas
                            &nbsp;&nbsp;<input type="text" size="5" name="tiempoEjecucion">
                            &nbsp;&nbsp;Presupuesto:&nbsp;&nbsp;<%= ((Mantenimiento)Sesion.getMantenimiento().getListadoTareas().get(0)).getPresupuesto()%>
                            <input type="text" size="5" name="presupuesto">
                            &nbsp;&nbsp;
                            <select name="ano" class="Estilo10">
                                <option value="-1">Elija a�o</option>
                                <option value="<%=format.format(new java.util.Date())%>"><%=format.format(new java.util.Date())%></option>
                                <option value="<%=Integer.parseInt(format.format(new java.util.Date()))+1%>"><%=Integer.parseInt(format.format(new java.util.Date()))+1%></option>
                            </select>
                            &nbsp;&nbsp;
                            <select name="mes" class="Estilo10">
                                <option value="-1">Elija mes</option>
                                <option value="1">Enero</option>
                                <option value="2">Febrero</option>
                                <option value="3">Marzo</option>
                                <option value="4">Abril</option>
                                <option value="5">Mayo</option>
                                <option value="6">Junio</option>
                                <option value="7">Julio</option>
                                <option value="8">Agosto</option>
                                <option value="9">Septiembre</option>
                                <option value="10">Octubre</option>
                                <option value="11">Noviembre</option>
                                <option value="12">Diciembre</option>
                            </select>
                            &nbsp;
                            <%if(Sesion.getTipoUsuario().equals("calidad")){%>
                            <input type="submit" disabled class="Estilo10" name="enviarTiempoEjecucion" value="Actualizar" onclick="return validarPresupuesto();">
                            <%}else{%>
                            <input type="submit" class="Estilo10" name="enviarTiempoEjecucion" value="Actualizar" onclick="return validarPresupuesto();">
                            <%}%>
                        </fieldset>
                    </td>
                    <%}else{%>
                    <td height="60px" colspan="7">
                        <fieldset>
                            <legend>Modificar presupuesto:</legend>
                            Tiempo de ejecuci&oacute;n:&nbsp;&nbsp;0&nbsp;&nbsp;Presupuesto:&nbsp;&nbsp;0
                        </fieldset>
                    </td>
                    <%}%>
                </tr>
                <tr>
                    <td height="10px">&nbsp;</td>
                </tr>
                <tr>
                    <td  colspan="7">
                        <fieldset>
                            <legend>Agregar tarea:</legend>
                            <br/>
                            Tarea:&nbsp;&nbsp;<input type="text" size="50" name="agregarTarea">
                            &nbsp;Mec&aacute;nica&nbsp;<input type="checkbox" name="mecanica" onclick="valCheck('mecanica')">
                            &nbsp;El&eacute;ctrica&nbsp;<input type="checkbox" name="electrica" onclick="valCheck('electrica')">
                            &nbsp;
                            <%if(Sesion.getTipoUsuario().equals("calidad")){%>
                            <input type="submit" disabled class="Estilo10" name="enviarAgregarTarea" value="Agregar tarea" onclick="return valTareas();">
                            <%}else{%>
                            <input type="submit" class="Estilo10" name="enviarAgregarTarea" value="Agregar tarea" onclick="return valTareas();">
                            <%}%>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td height="10px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="7">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td><input type="image" src="../../Imagenes/volver2.jpg" name="volverInicio" id="volverInicio" value="SI" <%=vw.verOverTip("Haga click para cancelar acci�n y volver")%> /></td>
                                <td width="300px">&nbsp;</td>
                                <td>
                                    <a href="../../XlsProgramaPreventivo">
                                        Exportar programa&nbsp;<img alt="" border="0" src="../../Imagenes/btn_excel.gif" width="25px" height="25px" border="0" <%--=vw.verOverTip("Haga click para exportar a excel")--%>/>
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>
        </form>
    </body>
</html>
