<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Informes</title>
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/jquery-calendar.css" />
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
        <script type="text/javascript" src="../../js/jquery.js"></script>
        <script type="text/javascript" src="../../js/jquery-calendar.js"></script>
    </head>
    <body>
        
        <!--table width="661" border="0" align="center" cellpadding="0" cellspacing="0" class="Contenido">              
            <tr>
                <td class="ScrollStyle">    
                    <table width="661" border="0" cellpadding="0" cellspacing="0" class="Contenido">
                        <tr>                              
                            <td colspan="6" class="Titulo" align="center">M&aacute;quinas</td>
                        </tr>
                        
                        <tr class="CabeceraListado">
                            <td width="20">Nº</td>
                            <td width="200">TAREA</td>
                            <td width="200">TIPO</td>
                            <td width="100">PRESU</td>
                            <td width="100">REAL</td>
                            <td width="21">&nbsp;</td>
                        </tr>
                    </table>
                    <DIV class="ScrollStyle2">
                        <table width="640"  border="0" cellpadding="0" cellspacing="0" class="Contenido">
                            <tr class="FilaNull">
                                <td width="20"></td>
                            <td width="200"></td>
                            <td width="200"></td>
                            <td width="100"></td>
                            <td width="100"></td>
                            </tr>
        <%//=vw.grillaPresupuestoHoras(request)%>
                        </table>
                    </DIV>
                </td>
            </tr>
            <tr><td colspan="3">&nbsp;</td></tr>  
            <tr><td colspan="3">
                    <table width="661" border="0" cellpadding="0" cellspacing="0" class="Contenido" align="center">                
                        <tr>    
                            <td align="center" colspan="2">&nbsp;</td>                    
                            <td align="right">
                                Finalizar proceso<input type="checkbox" name="finalizarProceso">&nbsp;&nbsp;
        <input type="submit" onclick="return validar()" name="actualizarTareas" id="actualizarTareas" value="Actualizar" class="Estilo10" <%//=vw.verOverTip("Haga click para actualizar")%> />&nbsp;
        <input type="image" src="../../Imagenes/volver2.jpg" name="volverAtras" id="volverAtras" value="SI" <%//=vw.verOverTip("Haga click volver")%> />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr> 
        </table-->
        <form  action="../../FrmInformes" method="post" name="FrmInformes">
            <table align="center" class="Estilo10">
                <tr>                              
                    <td class="Estilo10" align="center" colspan="3">INFORMES</td>
                </tr>
                <tr>
                    <td height="30px">&nbsp;</td>
                </tr>
                <tr>
                    <td class="Estilo10">Año:&nbsp;<input size="6" type="text" name="ano" value="<%if(Sesion.getMantenimiento().getDetalle().getAno() > 0){%><%=Sesion.getMantenimiento().getDetalle().getAno()%><%}%>"></td>
                    <td width="60px">&nbsp;</td>
                    <td>
                        <select name="mes" onchange="submit()" class="Estilo10">
                            <option value="-1">Elija un mes</option>
                            <option value="1" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 1){%>selected<%}%>>Enero</option>
                            <option value="2" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 2){%>selected<%}%>>Febrero</option>
                            <option value="3" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 3){%>selected<%}%>>Marzo</option>
                            <option value="4" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 4){%>selected<%}%>>Abril</option>
                            <option value="5" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 5){%>selected<%}%>>Mayo</option>
                            <option value="6" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 6){%>selected<%}%>>Junio</option>
                            <option value="7" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 7){%>selected<%}%>>Julio</option>
                            <option value="8" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 8){%>selected<%}%>>Agosto</option>
                            <option value="9" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 9){%>selected<%}%>>Septiembre</option>
                            <option value="10" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 10){%>selected<%}%>>Octubre</option>
                            <option value="11" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 11){%>selected<%}%>>Noviembre</option>
                            <option value="12" <%if(Sesion.getMantenimiento().getDetalle().getMes() == 12){%>selected<%}%>>Diciembre</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td height="30px">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center" colspan="3">
                        <%if(Sesion.getMantenimiento().getDetalle().getMes() > 0 && Sesion.getMantenimiento().getDetalle().getAno() > 0){%>
                        <a href="../../XlsMantecionCorrectiva?mes=<%=Sesion.getMantenimiento().getDetalle().getMes()%>&ano=<%=Sesion.getMantenimiento().getDetalle().getAno()%>">
                            Informes de mantención correctiva y preventiva&nbsp;<img alt="" border="0" src="../../Imagenes/btn_excel.gif" width="25px" height="25px" border="0" <%=vw.verOverTip("Haga click para exportar a excel")%>/>
                        </a>
                        <%}else{%>
                        Informes de mantención correctiva y preventiva&nbsp;<img alt="" border="0" src="../../Imagenes/btn_excel.gif" width="25px" height="25px" border="0" <%=vw.verOverTip("Debe seleccionar mes y año para habilitar la exportación a excel")%>/>
                        <%}%>
                    </td>
                </tr>
                <tr>
                    <td height="30px">&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" colspan="2">
                        <input type="image" src="../../Imagenes/volver2.jpg" name="volver_1" id="volver_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
