<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>        
        <title>Solicitud de Servicio de Mantenimiento</title>               
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
    </head>
    
    <body>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantEmplExt" method="post" name="frmMEmExt">           
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">                                
                <tr>                  
                    <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/solicitudIcon.jpg" width="100" height="90"></font></td>
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="65%" align="left"><br><font class="Estilo10 Estilo12">Mantenedor Empleados Empresas Externas</font><br></td>
                </tr>                   
                <%if(Sesion.getMantenimiento().getEtapaEmplExt()==0){%>   
                <tr>
                    <td align="right"><font class="Estilo10">Empresa Externa</font></td>
                    <td align="center">:</td>
                    <td align="left">                        
                        <%=vw.comboEmprExt(request)%>                        
                    </td>
                </tr>  
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3">
                        <table width="751" align="center" cellpadding="0" cellspacing="0">              
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="751"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                                        <tr>                                            
                                            <td colspan="6" class="Titulo" align="center">Listado Empleados <%=vw.verHtmlNull(Sesion.getMantenimiento().getEmpExt().getVc_nombre())%></td> 
                                        </tr>                                        
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>
                                            <td width="20">&nbsp;</td>
                                            <td width="151">RUT</td>
                                            <td width="270">NOMBRES</td>
                                            <td width="270">APELLIDOS</td>
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle">
                                        <table width="730"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="20"></td>
                                                <td width="151"></td>
                                                <td width="270"></td>
                                                <td width="270"></td>                                         
                                            </tr>
                                            <%=vw.grillaEmplExt(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                        </table>  
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="ingresarNewEmplExt" id="ingresarNewEmplExt" value="Nuevo Empleado" class="Estilo10" <%=vw.verOverTip("Haga click para ingresar nuevo Trabajador Empresa Externa")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaEmplExt()==1){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutEmplExt" id="rutEmplExt" size="20" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr>                  
                <tr>
                    <td align="right"><font class="Estilo10">Nombres</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreEmplExt" id="nombreEmplExt" size="50" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Apellidos</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="apelliEmplExt" id="apelliEmplExt" size="50" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_apellido()%>" class="Estilo10"/>
                    </td>
                </tr>  
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="grabarEmplExt" id="grabarEmplExt" value="Grabar" class="Estilo10" <%=vw.verOverTip("Haga click para grabar nuevo Empleado")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaEmplExt()==2){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutEmplExt" id="rutEmplExt" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreEmplExt" id="nombreEmplExt" size="50" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Apellido</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="apelliEmplExt" id="apelliEmplExt" size="50" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_apellido()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="actualizarEmplExt" id="actualizarEmplExt" value="Actualizar" class="Estilo10" <%=vw.verOverTip("Haga click para actualizar Empleado")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaEmplExt()==3){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutEmplExt" id="rutEmplExt" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreEmplExt" id="nombreEmplExt" size="50" readonly="yes" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Apellido</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="apelliEmplExt" id="apelliEmplExt" size="50" readonly="yes" value="<%=Sesion.getMantenimiento().getEmpExt().getEmpleado().getVc_apellido()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="eliminarEmplExt" id="eliminarEmplExt" value="Eliminar" class="Estilo10" <%=vw.verOverTip("Haga click para eliminar Empleado")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}%>
            </table>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
    <script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
    </script>
    <%Sesion.cleanTip();%>
<%}%>
