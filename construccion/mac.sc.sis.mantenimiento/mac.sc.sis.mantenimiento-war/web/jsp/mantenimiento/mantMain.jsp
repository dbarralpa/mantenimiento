<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>        
        <title>Solicitud de Servicio de Mantenimiento</title>               
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
    </head>
    
    <body>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantMain" method="post" name="frmMMain">           
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">                                
                <tr>                  
                    <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/mantenimientoIcon113.jpg" width="100" height="90"></font></td>
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="65%" align="left"><br><font class="Estilo10 Estilo12">Mantenedores Sistema</font><br></td>
                </tr>    
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>  
                    <td colspan="2" width="35%">&nbsp;</td>
                    <td align="left">
                        <font class="Estilo10">
                            <a href="../../FrmMantMain?newMaquina=SI">Mantenedor M�quinas</a>
                        </font>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>                    
                    <td colspan="2" width="35%">&nbsp;</td>
                    <td align="left">
                        <font class="Estilo10">
                            <a href="../../FrmMantMain?newTFalla=SI">Mantenedor Tipo Falla</a>
                        </font>
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>                    
                    <td colspan="2" width="35%">&nbsp;</td>
                    <td align="left">
                        <font class="Estilo10">
                            <a href="../../FrmMantMain?newFalla=SI">Mantenedor Fallas</a>
                        </font>
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>                    
                    <td colspan="2" width="35%">&nbsp;</td>
                    <td align="left">
                        <font class="Estilo10">
                            <a href="../../FrmMantMain?newEmpExt=SI">Mantenedor Empresas Externas</a>
                        </font>
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>                    
                    <td colspan="2" width="35%">&nbsp;</td>
                    <td align="left">
                        <font class="Estilo10">
                            <a href="../../FrmMantMain?newEmplExt=SI">Mantenedor Empleados Empresas Externas</a>
                        </font>
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>                    
                    <td colspan="2" width="35%">&nbsp;</td>
                    <td align="left">
                        <font class="Estilo10">
                            <a href="../../FrmMantMain?newPerMant=SI">Mantenedor Personal de Mantenci�n</a>
                        </font>
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>                    
                    <td colspan="2" width="35%">&nbsp;</td>
                    <td align="left">
                        <font class="Estilo10">
                            <a href="../../FrmMantMain?newTMaq=SI">Mantenedor Turno-M�quina</a>
                        </font>
                    </td>
                </tr>                
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3" align="center">
                        <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver")%> />                        
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
    <script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
    </script>
    <%Sesion.cleanTip();%>
<%}%>