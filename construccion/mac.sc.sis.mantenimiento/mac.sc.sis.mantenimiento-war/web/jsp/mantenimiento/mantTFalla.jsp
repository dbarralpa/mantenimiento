<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>        
        <title>Solicitud de Servicio de Mantenimiento</title>               
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
    </head>
    
    <body>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantTFalla" method="post" name="frmMTFail">           
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">                                
                <tr>                  
                    <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/solicitudIcon.jpg" width="100" height="90"></font></td>
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="65%" align="left"><br><font class="Estilo10 Estilo12">Mantenedor Tipo Fallas</font><br></td>
                </tr>                   
                <%if(Sesion.getMantenimiento().getEtapaTipoFalla()==0){%>                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3">
                        <table width="300" align="center" cellpadding="0" cellspacing="0">              
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="300"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                                        <tr>
                                            <td colspan="5" class="Titulo" align="center">Listado de Tipos de Falla</td>                                    
                                        </tr>                                        
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>
                                            <td width="20">&nbsp;</td>
                                            <td width="40">ID</td>
                                            <td width="200">TIPO FALLA</td>                                            
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyleSmall">
                                        <table width="280"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="20"></td>
                                                <td width="40"></td>
                                                <td width="200"></td>                                            
                                            </tr>
                                            <%=vw.grillaTipoFallas(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                        </table>  
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="ingresarNewTFail" id="ingresarNewTFail" value="Nuevo Tipo Falla" class="Estilo10" <%=vw.verOverTip("Haga click para ingresar nuevo Tipo de Falla")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaTipoFalla()==1){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Nombre Tipo Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreTFalla" id="nombreTFalla" size="40" value="<%=Sesion.getMantenimiento().getTipoFalla().getNombre()%>" class="Estilo10"/>
                    </td>
                </tr>                  
                <tr>
                    <td align="right" valign="top"><font class="Estilo10">Descripción Tipo Falla</font></td>
                    <td align="center" valign="top">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <textarea rows="3" cols="50" name="descTFalla" id="descTFalla" class="Estilo10"><%=Sesion.getMantenimiento().getTipoFalla().getDescripcion()%></textarea>
                        </font>
                    </td>
                </tr>   
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="grabarTFalla" id="grabarTFalla" value="Grabar" class="Estilo10" <%=vw.verOverTip("Haga click para grabar nuevo Tipo Falla")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaTipoFalla()==2){%>
                <tr>
                    <td align="right"><font class="Estilo10">Id Tipo Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="idTFalla" id="idTFalla" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getTipoFalla().getIdTipoFalla()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre Tipo Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreTFalla" id="nombreTFalla" size="40" value="<%=Sesion.getMantenimiento().getTipoFalla().getNombre()%>" class="Estilo10"/>
                    </td>
                </tr>  
                <tr>
                    <td align="right" valign="top"><font class="Estilo10">Descripción Tipo Falla</font></td>
                    <td align="center" valign="top">:</td>
                    <td align="left">
                        <font class="Estilo10">
                            <textarea rows="3" cols="50" name="descTFalla" id="descTFalla" class="Estilo10"><%=vw.verHtmlNull(Sesion.getMantenimiento().getTipoFalla().getDescripcion())%></textarea>
                        </font>
                    </td>
                </tr>   
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="actualizarTFalla" id="actualizarTFalla" value="Actualizar" class="Estilo10" <%=vw.verOverTip("Haga click para actualizar Tipo de Falla")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaTipoFalla()==3){%>
                <tr>
                    <td align="right"><font class="Estilo10">Id Tipo Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="idTFalla" id="idTFalla" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getTipoFalla().getIdTipoFalla()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre Tipo Falla</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombreTFalla" id="nombreTFalla" size="40" readonly="yes" value="<%=Sesion.getMantenimiento().getTipoFalla().getNombre()%>" class="Estilo10"/>
                    </td>
                </tr>  
                <tr>
                    <td align="right" valign="top"><font class="Estilo10">Descripción Tipo Falla</font></td>
                    <td align="center" valign="top">:</td>
                    <td align="left">
                        <font class="Estilo10">
                        <textarea rows="3" cols="50" name="descTFalla" id="descTFalla" class="Estilo10" readonly="yes"><%=vw.verHtmlNull(Sesion.getMantenimiento().getTipoFalla().getDescripcion())%></textarea>
                        </font>
                    </td>
                </tr>   
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="eliminarTFalla" id="eliminarTFalla" value="Eliminar" class="Estilo10" <%=vw.verOverTip("Haga click para eliminar Tipo de Falla")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}%>
            </table>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
    <script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
    </script>
    <%Sesion.cleanTip();%>
<%}%>
