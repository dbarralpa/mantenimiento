<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<jsp:useBean id="Sesion" scope="session" class="mac.sc.sis.mantenimiento.models.ModSesion"/>
<jsp:useBean id="vw"     scope="page"    class="mac.sc.sis.mantenimiento.views.VwMantenimiento" />

<html>
    <head>        
        <title>Solicitud de Servicio de Mantenimiento</title>               
        <link rel="stylesheet" type="text/css" href="../../css/estiloPaginas.css" /> 
        <link rel="stylesheet" type="text/css" href="../../css/table.css" />       
        <link rel="stylesheet" type="text/css" href="../../css/notification.css" /> 
        
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script type="text/javascript" src="../../js/functions.js"></script>
        <script type="text/javascript" src="../../js/mensajes.js"></script>
    </head>
    
    <body>
        <div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
        <form action="../../FrmMantPerMant" method="post" name="frmMPMant">           
            <table width="80%" align="center" border="0" cellpadding="0" cellspacing="0">                                
                <tr>                  
                    <td width="30%" align="center"><font class="Estilo10"><img alt="" src="../../Imagenes/solicitudIcon.jpg" width="100" height="90"></font></td>
                    <td width="5%" align="center">&nbsp;</td>
                    <td width="65%" align="left"><br><font class="Estilo10 Estilo12">Mantenedor Personal Mantenimiento</font><br></td>
                </tr>                   
                <%if(Sesion.getMantenimiento().getEtapaPerMant()==0){%>                
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td colspan="3">
                        <table width="751" align="center" cellpadding="0" cellspacing="0">              
                            <tr>
                                <td class="ScrollStyle">                         
                                    <table width="751"  border="0" cellpadding="0" cellspacing="1" bgcolor="#000000">
                                        <tr>                                            
                                            <td colspan="6" class="Titulo" align="center">Listado Personal Mantenimiento</td> 
                                        </tr>                                        
                                        <tr class="CabeceraListado">
                                            <td width="20">&nbsp;</td>
                                            <td width="20">&nbsp;</td>
                                            <td width="151">RUT</td>
                                            <td width="270">NOMBRES</td>
                                            <td width="270">APELLIDOS</td>
                                            <td width="20">&nbsp;</td>
                                        </tr>
                                    </table>
                                    <DIV class="ScrollStyle">
                                        <table width="730"  border="0" cellpadding="0" cellspacing="1"  class="Contenido" bgcolor="#000000">
                                            <tr class="FilaNull">
                                                <td width="20"></td>
                                                <td width="20"></td>
                                                <td width="151"></td>
                                                <td width="270"></td>
                                                <td width="270"></td>                                         
                                            </tr>
                                            <%=vw.grillaPerMant(request)%>
                                        </table>
                                    </DIV>
                                </td>
                            </tr>
                        </table>  
                    </td>
                </tr>
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="ingresarNewPerMant" id="ingresarNewPerMant" value="Nuevo Personal Mantenimiento" class="Estilo10" <%=vw.verOverTip("Haga click para ingresar nuevo Personal Mantenimiento")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_0" id="volverMant_0" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaPerMant()==1){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutPerMant" id="rutPerMant" size="20" value="<%=Sesion.getMantenimiento().getPerMant().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr>                  
                <tr>
                    <td align="right"><font class="Estilo10">Nombre</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombrePerMant" id="nombrePerMant" size="50" value="<%=Sesion.getMantenimiento().getPerMant().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Apellido</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="apelliPerMant" id="apelliPerMant" size="50" value="<%=Sesion.getMantenimiento().getPerMant().getVc_apellido()%>" class="Estilo10"/>
                    </td>
                </tr>  
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="grabarPerMant" id="grabarPerMant" value="Grabar" class="Estilo10" <%=vw.verOverTip("Haga click para grabar nuevo Personal Mantenimiento")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaPerMant()==2){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutPerMant" id="rutPerMant" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getPerMant().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombrePerMant" id="nombrePerMant" size="50" value="<%=Sesion.getMantenimiento().getPerMant().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Apellido</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="apelliPerMant" id="apelliPerMant" size="50" value="<%=Sesion.getMantenimiento().getPerMant().getVc_apellido()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="actualizarPerMant" id="actualizarPerMant" value="Actualizar" class="Estilo10" <%=vw.verOverTip("Haga click para actualizar Personal Mantenimiento")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}else if(Sesion.getMantenimiento().getEtapaPerMant()==3){%>
                <tr><td colspan="3">&nbsp;</td></tr>
                <tr>
                    <td align="right"><font class="Estilo10">Rut</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="rutPerMant" id="rutPerMant" size="20" readonly="yes" value="<%=Sesion.getMantenimiento().getPerMant().getVc_rut()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr>
                    <td align="right"><font class="Estilo10">Nombre</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="nombrePerMant" id="nombrePerMant" size="50" readonly="yes" value="<%=Sesion.getMantenimiento().getPerMant().getVc_nombre()%>" class="Estilo10"/>
                    </td>
                </tr>   
                <tr>
                    <td align="right"><font class="Estilo10">Apellido</font></td>
                    <td align="center">:</td>
                    <td align="left">
                        <input type="text" name="apelliPerMant" id="apelliPerMant" size="50" readonly="yes" value="<%=Sesion.getMantenimiento().getPerMant().getVc_apellido()%>" class="Estilo10"/>
                    </td>
                </tr> 
                <tr><td colspan="3">&nbsp;</td></tr> 
                <tr>
                    <td colspan="3">   
                        <table align="center">
                            <tr>                            
                                <td align="right">
                                    <input type="submit" name="eliminarPerMant" id="eliminarPerMant" value="Eliminar" class="Estilo10" <%=vw.verOverTip("Haga click para eliminar Personal Mantenimiento")%> />
                                </td>
                                <td align="center">&nbsp;</td>
                                <td align="left">
                                    <input type="image" src="../../Imagenes/volver2.jpg" name="volverMant_1" id="volverMant_1" value="SI" <%=vw.verOverTip("Haga click para volver")%> />
                                </td>
                            </tr>                 
                        </table>
                    </td>
                </tr>
                <%}%>
            </table>
        </form>
    </body>
</html>
<%if(Sesion.isConTip()){%>
<script type="text/javascript">
        OffMsg("<%=Sesion.getTip()%>", "<%=Sesion.getTipoTip()%>");
</script>
<%Sesion.cleanTip();%>
<%}%>
